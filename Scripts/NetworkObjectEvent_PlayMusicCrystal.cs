﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Valve.VR.InteractionSystem;

[System.Obsolete]
public class NetworkObjectEvent_PlayMusicCrystal : NetworkObjectEvent
{
    [SerializeField] private NetworkObjectEvent_MusicCrystal m_musicCrystal;
    private List<MusicCrystal> m_musicCrystals;

    private float m_delay;
    private bool m_isPlaying;

    private List<AudioClip> a_audioclip;
    private AudioSource _audioSourceAmbiance;

    private int NbrPlaying = 0;
    private bool m_wasButtonPressed;

    // Start is called before the first frame update
    override protected void Start()
    {
        m_delay = 2;
        m_isPlaying = false;
        _audioSourceAmbiance = this.GetComponent<AudioSource>();
        a_audioclip = new List<AudioClip>();
        m_musicCrystals = m_musicCrystal.GetMusicCrystals();
        for (int i = 0; i < m_musicCrystals.Count; i++)
        {
            a_audioclip.Add(m_musicCrystals[i].GetAudioClip());
        }
        base.Start();
        if (isServer)
        {
            m_wasButtonPressed = false;
        }
    }

    public void OnButtonPressed()
    {
        Debug.Log("[NetworkObjectEvent_Button:OnButtonPressed:!isServer=" + !isServer + "]");
        if (!hasAuthority)
        {
            NetworkPlayer player = GameObject.Find("VrGameObject").GetComponentInParent<NetworkPlayer>();
            Debug.Log("[NetworkObjectEvent_Button:Start:m_player.name=" + player.name + "]");
            player.CmdSetAuth(m_NetId, player.GetComponent<NetworkIdentity>());
        }
        if (!isServer)
        {
            CmdButtonPressed(true);
        }
        else
        {
            ButtonPressed(true);
        }
    }

    [Command]
    private void CmdButtonPressed(bool isPressed)
    {
        Debug.Log("[NetworkObjectEvent_Button:CmdButtonPressed:isPressed=" + isPressed + "]");
        ButtonPressed(isPressed);
    }

    private void ButtonPressed(bool isPressed)
    {     
        if (!m_wasButtonPressed && isPressed)
        {
            Debug.Log("[NetworkObjectEvent_Button:ButtonPressed:buttonPressed]");
            m_wasButtonPressed = true;
            EnigmaManager_ApollonRoom NbrPlayCristal = (EnigmaManager_ApollonRoom)m_EnigmaManager;
            NbrPlayCristal.IncrementNbrPlay();
            NetworkPlayer player = GameObject.Find("VrGameObject").GetComponentInParent<NetworkPlayer>();
            TargetPlayCrystalMusic(player.connectionToClient);
        }
    }

    [TargetRpc]
    public void TargetPlayCrystalMusic(NetworkConnection target)
    {
        if (m_isPlaying == false)
        {
            Debug.Log("[PlayMusicCrystal:TargetPlayCrystalMusic]");
            m_isPlaying = true;
            StartCoroutine(OnPlayMusicCrystal());
        }
    }

    private IEnumerator OnPlayMusicCrystal()
    {
        Debug.Log("[OnPlayMusicCrystal:PlayCrystalMusic]");
        List<int> m_listCrystals = m_musicCrystal.GetCurrentListSoluce();
        for (int i = 0; i < m_listCrystals.Count; i++)
        {
            PlayAudioClip(a_audioclip[m_listCrystals[i] - 1]);
            yield return new WaitForSecondsRealtime(m_delay);
        }
        m_isPlaying = false;
    }

    private void PlayAudioClip(AudioClip audioClip)
    {
        Debug.Log("[PlayMusicCrystal:PlayAudioClip:audioclip.name = " + audioClip.name + "]");
        _audioSourceAmbiance.clip = audioClip;
        _audioSourceAmbiance.PlayOneShot(audioClip);

    }

    public override void EnigmaUpdateEvent()
    {
        throw new System.NotImplementedException();
    }

    public override void EnigmaReceiveEvent(int eventCase, List<object> args)
    {
        throw new System.NotImplementedException();
    }
}
