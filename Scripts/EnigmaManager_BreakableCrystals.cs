﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

[System.Obsolete]
public class EnigmaManager_BreakableCrystals : EnigmaManager
{
    [SerializeField] private EnigmaManager m_nextEnigmaPart;
    [SerializeField] private List<NetworkObjectEvent_Pillar> m_pillars;
    [SerializeField] private GameObject m_door;
    [SerializeField] private float m_timeBetweenTwoPillar;
    [SerializeField] private List<ParticleSystem> m_braseroDeco;




    [SerializeField] private SoundShield ShieldOff;


    private List<int> m_shuffleIndex;

    private Network_AudioController m_netAudioController;
    private string EventDialogueEndHephaistos = "event:/Dialogues/Salle3Fin";
    private FMOD.Studio.PLAYBACK_STATE m_stateDialogueEndHephaistos;
    private FMOD.Studio.EventInstance m_instanceDialogueEndHephaistos;


    override public void StartEnigmaManager(int enigmaId)
    {
        Debug.Log("[EnigmaManager_BreakableCrystals:StartEnigmaManager]");
        base.StartEnigmaManager(enigmaId);
        StartCoroutine(SetIsAvailableBreakableCrystals(true)); // available own crystals on start enigma manager
        InitPillars();
        StartCoroutine(TogglePillarsDestruction(true)); // start pillars destruction
        if (m_nextEnigmaPart != null)
        {
            if (m_nextEnigmaPart.name == "EnigmaManager_Braseros 2")
            {
                RpcDialogueFirstPillarFall();
            }
        }
    }

    protected override void OnCompleteEnigma()
    {
        RpcStartBraseroDeco();
        if (m_nextEnigmaPart != null)
        {
            StopAllCoroutines();
            StartCoroutine(TogglePillarsDestruction(false));
            if(m_nextEnigmaPart.name == "EnigmaManager_Braseros 2")
            {
                RpcDialogueReconstructionPillier();
            }
            m_nextEnigmaPart.StartEnigmaManager(m_EnigmaId);
        }
        else
        {
            if(m_door != null)
            {
                RpcTurnOffShields();
            }
            m_netAudioController = GameObject.Find("Network_AudioController").GetComponent<Network_AudioController>();
            RpcDialogueFinEnigmeHephaistos();
            m_GameManager.OnCompleteEnigma(m_EnigmaId);
        }
    }

    [ClientRpc]
    private void RpcDialogueFinEnigmeHephaistos()
    {
        m_instanceDialogueEndHephaistos = FMODUnity.RuntimeManager.CreateInstance(EventDialogueEndHephaistos);
        m_instanceDialogueEndHephaistos.start();
        if(isServer)
        {
            StartCoroutine(WaitDialogueFinish());
        }
    }
    private IEnumerator WaitDialogueFinish()
    {
        while (m_stateDialogueEndHephaistos != FMOD.Studio.PLAYBACK_STATE.STOPPED)
        {
            m_instanceDialogueEndHephaistos.getPlaybackState(out m_stateDialogueEndHephaistos);
            yield return new WaitForSeconds(1);
        }
        m_netAudioController.SetIsFinishHephaistosRoom(true);
    }

    [ClientRpc]
    private void RpcDialogueReconstructionPillier()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/Dialogues/Salle3PilierReconstruit");
    }

    [ClientRpc]
    public void RpcTurnOffShields()
    {
        ShieldOff.StopSonShield();
        m_door.SetActive(false);
    }

    [ClientRpc]
    private void RpcDialogueFirstPillarFall()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/Dialogues/Salle3PilierTombe");
    }

    protected override void OnFailEnigma()
    {
        Debug.Log("[EnigmaManager_BreakableCrystals:OnFailEnigma]");
        m_GameManager.OnFailEnigma(m_EnigmaId, "All the pillars have collapsed and the roof is falling on your head ...");
        EndEnigmaManager();
    }

    protected override void SendHelp()
    {
        throw new System.NotImplementedException();
    }

    private IEnumerator SetIsAvailableBreakableCrystals(bool available)
    {
        Debug.Log("[EnigmaManager_BreakableCrystals:SetIsAvailableBreakableCrystals:available=" + available + "]");
        yield return new WaitForEndOfFrame();
        for(int i=0; i<m_EnigmaObjectsNI.Count; i++)
        {
            if (m_EnigmaObjectsNI[i].TryGetComponent(out NetworkObjectEvent_BreakableCrystal bc))
            {
                OnThrowEvent(bc.GetComponent<NetworkIdentity>(), 1, new List<object>() { available });
            }
        }
        yield return null;
    }

    private void InitPillars()
    {
        Debug.Log("[EnigmaManager_BreakableCrystals:InitPillars]");
        m_shuffleIndex = new List<int>();
        m_pillars.ForEach(delegate (NetworkObjectEvent_Pillar p)
        {
            p.SetEnigmaManager(this);
            m_shuffleIndex.Add(m_shuffleIndex.Count);
        });

        m_shuffleIndex = m_shuffleIndex.OrderBy(item => Guid.NewGuid()).ToList(); // shuffle the list

    }

    private IEnumerator TogglePillarsDestruction(bool destruct)
    {
        Debug.Log("[EnigmaManager_BreakableCrystals:TogglePillarsDestruction:destruct=" + destruct + "]");
        if (destruct)
        {
            for (int i = 0; i < m_shuffleIndex.Count; i++)
            {
                //@todo ajouter le son du monstre qui cogne le mur
                OnThrowEvent(m_pillars[m_shuffleIndex[i]].GetComponent<NetworkIdentity>(), 1, new List<object>() { destruct });
                if (i == m_shuffleIndex.Count - 1)
                {
                    OnFailEnigma();
                }
                else
                {
                    yield return new WaitForSeconds(m_timeBetweenTwoPillar);
                }
            }
        }
        else
        {
            m_pillars.ForEach(delegate (NetworkObjectEvent_Pillar p)
            {
                OnThrowEvent(p.GetComponent<NetworkIdentity>(), 1, new List<object>() { destruct });
            });
        }
        yield return null;
    }

    [ClientRpc]
    private void RpcStartBraseroDeco()
    {
        foreach(ParticleSystem brasero in m_braseroDeco)
        {
            brasero.Play(true);
        }
    }
}
