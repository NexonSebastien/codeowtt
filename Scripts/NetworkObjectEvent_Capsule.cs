﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Valve.VR.InteractionSystem;

[System.Obsolete]
public class NetworkObjectEvent_Capsule : NetworkObjectEvent
{
    private TeleportArea m_teleportArea;
    private NetworkPlayer m_player;
    [SyncVar] bool m_move;
    private float m_lerpStartTime;
    [SerializeField] private float m_lerpTravelTime;
    [SerializeField] private Transform m_positionEnd;
    private Vector3 m_positionSource;
    private Vector3 m_positionTarget;
    [SerializeField] private bool m_capsuleOpenOnStart;
    private GameObject m_parentDefault;
    [SerializeField] private GameObject m_stonePosition;

    override protected void Start()
    {
        base.Start();
        m_parentDefault = transform.parent.gameObject;
        m_positionSource = transform.position;
        m_positionTarget = m_positionEnd.position;
        m_teleportArea = GetComponentInChildren<TeleportArea>();
        if (isServer)
        {
            m_move = false;
        }
        Debug.Log("[NetworkObjectEvent_Capsule:Start:m_capsuleOpenOnStart=" + m_capsuleOpenOnStart + "]");

        if (m_capsuleOpenOnStart)
        {
            OpenCapsule();
        }
        else
        {
            ToggleTeleportPointLocked(true);
            ToggleStonePositionTrigger(false);
        }
    }

    [ServerCallback]
    private void Update()
    {
        if (m_move)
        {
            transform.position = Lerp(m_positionSource, m_positionTarget, m_lerpStartTime, m_lerpTravelTime);
            if (Mathf.Approximately(transform.position.x, m_positionTarget.x)
                && Mathf.Approximately(transform.position.y, m_positionTarget.y)
                && Mathf.Approximately(transform.position.z, m_positionTarget.z)) // if current position is the average target position
            {
                transform.position = m_positionTarget; // force position
                m_move = false;
                transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
                Debug.Log("[NetworkObjectEvent_Capsule:Update:position=" + transform.position.ToString() + "]");
                RpcSetPlayer(null);
            }
        }
    }
    private Vector3 Lerp(Vector3 start, Vector3 end, float lerpStartTime, float lerpTime)
    {
        float timeSincedStarted = Time.time - lerpStartTime;
        float percentage = timeSincedStarted / lerpTime;
        return Vector3.Lerp(start, end, percentage);
    }

    public NetworkPlayer GetPlayer()
    {
        return m_player;
    }

    [ClientRpc]
    private void RpcSetPlayer(NetworkIdentity player)
    {
        if(player)
        {
            Debug.Log("Player : " + player.name);
            m_player = player.GetComponentInParent<NetworkPlayer>();
            m_player.transform.SetParent(this.transform);
            this.tag = player.tag;
            transform.parent = null;
        }
        else
        {
            Debug.Log("Player is null");
            m_player.transform.SetParent(null);
            m_player = null;
            this.tag = "Untagged";
            transform.parent = m_parentDefault.transform;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "BodyTrigger" && m_player == null && isServer)
        {
            Debug.Log("Player detected");
            RpcToggleTeleportPointLocked(true);
            RpcSetPlayer(other.GetComponentInParent<NetworkIdentity>());
            EnigmaManager_CapsuleRoom EnigmaManagerCapsuleRoom = (EnigmaManager_CapsuleRoom)m_EnigmaManager;
            EnigmaManagerCapsuleRoom.OnPlayerEnterCapsule(this);
            UpdateEnigma(m_NetId, true);
        }
        else if (other.name == "evil stone__0")
        {
            //other.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            other.transform.rotation = m_stonePosition.transform.rotation;
            other.transform.position = new Vector3(m_stonePosition.transform.position.x - 0.01f, m_stonePosition.transform.position.y - 0.0423f, m_stonePosition.transform.position.z + 0.08f);
            //Debug.Log("[NetworkObjectEvent_Capsule:OnTriggerEnter:object=" + other.name + ":position=" + other.transform.position.ToString() + "]");
        }
    }

    [ServerCallback]
    private void OnTriggerExit(Collider other)
    {
        if (other.name == "BodyTrigger" && m_player == other.GetComponentInParent<NetworkPlayer>() && !m_move)
        {
            RpcToggleTeleportPointLocked(false);
            RpcSetPlayer(null);
            EnigmaManager_CapsuleRoom EnigmaManagerCapsuleRoom = (EnigmaManager_CapsuleRoom)m_EnigmaManager;
            EnigmaManagerCapsuleRoom.OnPlayerExitCapsule(this);
            UpdateEnigma(m_NetId, false);
        }
    }

    public override void EnigmaReceiveEvent(int eventCase, List<object> args)
    {
        throw new System.NotImplementedException();
    }

    public override void EnigmaUpdateEvent()
    {
        //RpcToggleTeleportPointLocked(true);
    }

    private void ToggleTeleportPointActive(bool active)
    {
        GetComponentInChildren<TeleportPoint>().markerActive = active;
        Debug.Log("[NetworkObjectEvent_Capsule:ToggleTeleportPointActive:active="+active+":markerActive=" + GetComponentInChildren<TeleportPoint>().markerActive + "]");
    }

    private void ToggleTeleportPointLocked(bool locked)
    {
        GetComponentInChildren<TeleportPoint>().SetLocked(locked);
        Debug.Log("[NetworkObjectEvent_Capsule:ToggleTeleportPointLocked:locked=" + locked + ":GetComponentInChildren<TeleportPoint>().locked="+ GetComponentInChildren<TeleportPoint>().locked+"]");
    }

    [ClientRpc]
    public void RpcToggleTeleportPointLocked(bool locked)
    {
        ToggleTeleportPointLocked(locked);
    }

    [ClientRpc]
    public void RpcOpenCapsule()
    {
        OpenCapsule();
    }

    private void OpenCapsule()
    {
        Debug.Log("[NetworkObjectEvent_Capsule:OpenCapsule]");
        if (!transform.FindChild("FBX_CapsuleEnd").FindChild("Sphere.024").GetComponent<Animator>().GetBool("Open") && !m_move)
        {
            Debug.Log("[NetworkObjectEvent_Capsule:OpenCapsule:Animator]");
            transform.FindChild("FBX_CapsuleEnd").FindChild("Sphere.024").GetComponent<Animator>().SetBool("Open", true);
            //ToggleTeleportPointActive(true);
            ToggleTeleportPointLocked(false);
            ToggleStonePositionTrigger(true);
        }
    }

    [ClientRpc]
    public void RpcCloseCapsule()
    {
        CloseCapsule();
    }

    private void CloseCapsule()
    {
        Debug.Log("[NetworkObjectEvent_Capsule:CloseCapsule]");
        if (transform.FindChild("FBX_CapsuleEnd").FindChild("Sphere.024").GetComponent<Animator>().GetBool("Open"))
        {
            Debug.Log("[NetworkObjectEvent_Capsule:CloseCapsule:Animator]");
            transform.FindChild("FBX_CapsuleEnd").FindChild("Sphere.024").GetComponent<Animator>().SetBool("Open", false);
            //ToggleTeleportPointActive(false);
            ToggleTeleportPointLocked(true);
            ToggleStonePositionTrigger(false);
        }
    }

    private void ToggleStonePositionTrigger(bool activated)
    {
        m_stonePosition.GetComponent<CapsuleCollider>().enabled = activated;
    }

    [ClientRpc]
    public void RpcMoveCapsule()
    {
        m_lerpStartTime = Time.time;
        m_move = true;
    }
}
