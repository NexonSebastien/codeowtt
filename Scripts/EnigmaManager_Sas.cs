﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Rendering.PostProcessing;

[System.Obsolete]
public class EnigmaManager_Sas : EnigmaManager
{
    [SerializeField] GameObject m_DoorSasToCorridorP1;
    [SerializeField] GameObject m_DoorSasToCorridorP2;
    [SerializeField] GameObject m_WaterP1;
    [SerializeField] GameObject m_WaterP2;
    /*
    [SerializeField] PostProcessVolume m_PostProcessVolume;
    [SerializeField] PostProcessVolume m_PostProcessVolumeWater;
    */

    private NetworkPlayer m_P1Sas;
    private NetworkPlayer m_P2Sas;
    [SyncVar] bool m_P1InSas = false;
    [SyncVar] bool m_P2InSas = false;
    bool m_verifVidange = false;
    bool m_dialogueSasIsPlayed = false;
    private bool m_SoundMonster = false;

    float m_endPosY = -1;

    //Son
    [SerializeField] GameObject m_AudioManager;
    NetworkObjectEvent_SoundManagerMonster m_ManagerMonster;

    [ServerCallback]
    private void Update()
    {
        if(m_IsEnigmaStarted && !m_IsEnigmaEnded)
        {
            if (m_P1InSas && m_P2InSas)
            {
                if(!m_dialogueSasIsPlayed)
                {
                    RpcDialogueSas();
                    m_dialogueSasIsPlayed = true;
                }

                if (m_WaterP2.transform.position.y > m_endPosY)
                {
                    RpcWaterDown();
                }
                else
                {
                    Debug.Log("EndSas");
                    //RpcChangePostProcessing();
                    FinishPurge();
                    OnCompleteEnigma();
                }
            }
            if(m_P1Sas != null)
            {
                bool isPlayer1HeadUnderWater = m_WaterP1.transform.position.y > m_P1Sas.transform.GetChild(0).position.y;
                RpcChangePlayerVision(m_P1Sas.GetComponent<NetworkIdentity>(), isPlayer1HeadUnderWater);
            }
            if(m_P2Sas != null)
            {
                bool isPlayer2HeadUnderWater = m_WaterP2.transform.position.y > m_P2Sas.transform.GetChild(0).position.y;
                RpcChangePlayerVision(m_P2Sas.GetComponent<NetworkIdentity>(), isPlayer2HeadUnderWater);
            }
        }
    }

    protected override void OnCompleteEnigma()
    {
        EndEnigmaManager();
        m_GameManager.OnCompleteEnigma(m_EnigmaId);
    }

    protected override void OnFailEnigma()
    {
        
    }

    protected override void SendHelp()
    {
        
    }

    public void SetP1Sas(NetworkPlayer player)
    {
        m_P1Sas = player;
    }

    public void SetP2Sas(NetworkPlayer player)
    {
        m_P2Sas = player;
    }

    [ClientRpc]
    private void RpcOpenSas(GameObject door)
    {
        Debug.Log("[Sas:RpcOpenSas]");
        OpenSas(door);
    }

    [ClientRpc]
    private void RpcWaterDown()
    {
        WaterDown();
        m_AudioManager.SetActive(true);
    }

    /*
    [ClientRpc]
    private void RpcChangePostProcessing()
    {
        m_PostProcessVolumeWater.enabled = false;
        m_PostProcessVolume.enabled = true;
    }
    */

    [ClientRpc]
    private void RpcDialogueSas()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/Dialogues/DialogueSas2");
    }

    private void WaterDown()
    {
        //@todo son de vidage de l'eau
        if(m_verifVidange == false)
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/Sas/VidangeSas2");
            m_verifVidange = true;
        }
        
        m_WaterP1.transform.Translate(Vector3.down * 0.2f * Time.deltaTime);
        m_WaterP2.transform.Translate(Vector3.down * 0.2f * Time.deltaTime);
    }

    [ClientRpc]
    private void RpcChangePlayerVision(NetworkIdentity playerNetId, bool isPlayerHeadUnderWater)
    {
        ChangePlayerVision(playerNetId, isPlayerHeadUnderWater);
    }

    private void ChangePlayerVision(NetworkIdentity playerNetId, bool isPlayerHeadUnderWater)
    {
        if (playerNetId.isLocalPlayer)
        {
            Debug.Log("[EnigmaManager_Sas:ChangePlayerVision(" + playerNetId.name + "," + isPlayerHeadUnderWater + "]");
            PostProcessVolumeController ppvc = playerNetId.GetComponent<PostProcessVolumeController>();
            ppvc.SetGlobal(isPlayerHeadUnderWater);
            ppvc.SetWeight(isPlayerHeadUnderWater ? 0.95f : 0);
            ppvc.SetVignetteIntensity(0.2f);
            ppvc.ActiveColorGradding(isPlayerHeadUnderWater);
        }
    }

    private void OpenSas(GameObject door)
    {
        //Son ouverture Sas
        FMODUnity.RuntimeManager.PlayOneShotAttached("event:/Sas/OuvreSas",door);
        door.GetComponent<Animator>().SetBool("IsOpen", true);
    }

    public void SetP1InSas(bool IsInSas)
    {
        Debug.Log("P1InSas");
        m_P1InSas = IsInSas;
    }

    public void SetP2InSas(bool IsInSas)
    {
        Debug.Log("P2InSas");
        m_P2InSas = IsInSas;
    }


    private void FinishPurge()
    {
        RpcOpenSas(m_DoorSasToCorridorP1);
        RpcOpenSas(m_DoorSasToCorridorP2);
        OnCompleteEnigma();
    }

    IEnumerator WaitCriMonstre()
    {
        yield return new WaitForSeconds(60);
        FMODUnity.RuntimeManager.PlayOneShot("event:/Monstre/Cri");
    }
}
