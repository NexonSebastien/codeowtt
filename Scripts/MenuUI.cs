﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[System.Obsolete]
public class MenuUI : MonoBehaviour
{
    private GameObject m_menu;
    private GameObject m_mainMenu;
    private GameObject m_inputs;
    private string m_defaultIpAddress;
    private string m_ipAddress;
    private GameObject m_games;
    private GameObject m_gamesListContent;
    private Dictionary<string, bool> m_gamesListDiscovered;
    [SerializeField] private GameObject m_btnJoinGameItemPrefab;
    private GameObject m_btnInputField;
    private NetworkDiscovery m_networkDiscovery;
    private bool m_isDiscoveringGames;
    private GameObject m_userValidation;
    private GameObject m_userValidationContent;
    private Text m_confirmationTitle;
    private Button m_btnConfirmationYes;
    private GameObject m_connecting;

    private GameObject m_currentPanel;
    private GameObject m_panelOnConfirmation;

    [SerializeField] private List<Texture> m_controllersImage;
    private int m_controllersImageIndex;
    private RawImage m_controllerImage;
    private Text m_controllerName;

    private FMOD.Studio.EventInstance instance;
    private FMOD.Studio.EventInstance instanceVague;
    private string Event = "event:/Musiques/MusicMenu";
    private string EventVague = "event:/Ambiance/Vague";

    private void Awake()
    {
        FMOD.Studio.Bus bus = FMODUnity.RuntimeManager.GetBus("bus:/");
        bus.stopAllEvents(FMOD.Studio.STOP_MODE.IMMEDIATE);

        Debug.Log("[MenuUI:Awake]");
        m_gamesListDiscovered = new Dictionary<string, bool>();
        m_menu = transform.GetChild(0).FindChild("Menu").gameObject;
        m_mainMenu = m_menu.transform.FindChild("MainMenu").gameObject;
        m_inputs = m_menu.transform.FindChild("Inputs").gameObject;
        m_defaultIpAddress = "127.0.0.1";
        m_ipAddress = m_defaultIpAddress;
        m_games = m_menu.transform.FindChild("Games").gameObject;
        m_gamesListContent = m_games.transform.FindChild("GamesView").FindChild("Viewport").FindChild("Content").gameObject;
        m_btnInputField = m_games.transform.FindChild("BtnInputField").gameObject;
        m_networkDiscovery = m_games.GetComponent<NetworkDiscovery>();
        m_isDiscoveringGames = false;
        m_userValidation = m_menu.transform.FindChild("UserValidation").gameObject;
        m_userValidationContent = m_userValidation.transform.FindChild("View").FindChild("Viewport").FindChild("Content").gameObject;
        m_confirmationTitle = m_userValidationContent.transform.FindChild("Title").GetComponent<Text>();
        m_btnConfirmationYes = m_userValidationContent.transform.FindChild("Buttons").Find("BtnYes").GetComponent<Button>();
        m_connecting = m_menu.transform.FindChild("Connecting").gameObject;
        m_controllersImageIndex = 0;
        m_controllerImage = m_inputs.transform.FindChild("ControllerImage").GetComponent<RawImage>();
        m_controllerName = m_inputs.transform.FindChild("Caroussel").FindChild("ControllerName").GetComponent<Text>();

        for (int i=0; i<m_menu.transform.childCount; i++)
        {
            m_menu.transform.GetChild(i).gameObject.SetActive(false);
        }
        ShowPanel(m_mainMenu);
    }
    private void Start()
    {
        var inputDevices = new List<UnityEngine.XR.InputDevice>();
        UnityEngine.XR.InputDevices.GetDevices(inputDevices);
        foreach (var device in inputDevices)
        {
            Debug.Log(string.Format("Device found with name '{0}' and role '{1}'", device.name, device.role.ToString()));
            var featureUsages = new List<UnityEngine.XR.InputFeatureUsage>();
            if (device.TryGetFeatureUsages(featureUsages))
            {
                foreach (var feature in featureUsages)
                {
                    Debug.Log("Feature name=" + feature.name + ":type=" + feature.type.ToString() + ":toString=" + feature.ToString());
                }
            }
        }
        SceneManager.MoveGameObjectToScene(GameObject.Find("VrGameObject"), SceneManager.GetActiveScene());
        instance = FMODUnity.RuntimeManager.CreateInstance(Event);
        instanceVague = FMODUnity.RuntimeManager.CreateInstance(EventVague);
        instanceVague.start();
        instance.start();

        StartCoroutine(DestroyPersistentObjects());
    }

    private IEnumerator DestroyPersistentObjects()
    {
        yield return new WaitForEndOfFrame();
        List<GameObject> dontDestroyOnLoadObjects = GetDontDestroyOnLoadObjects();
        foreach(GameObject go in dontDestroyOnLoadObjects)
        {
            if(go.TryGetComponent(out GameManager gm))
            {
                Destroy(go);
            }
        }
    }

    public static List<GameObject> GetDontDestroyOnLoadObjects()
    {
        List<GameObject> result = new List<GameObject>();

        List<GameObject> rootGameObjectsExceptDontDestroyOnLoad = new List<GameObject>();
        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            rootGameObjectsExceptDontDestroyOnLoad.AddRange(SceneManager.GetSceneAt(i).GetRootGameObjects());
        }

        List<GameObject> rootGameObjects = new List<GameObject>();
        Transform[] allTransforms = Resources.FindObjectsOfTypeAll<Transform>();
        for (int i = 0; i < allTransforms.Length; i++)
        {
            Transform root = allTransforms[i].root;
            if (root.hideFlags == HideFlags.None && !rootGameObjects.Contains(root.gameObject))
            {
                rootGameObjects.Add(root.gameObject);
            }
        }

        for (int i = 0; i < rootGameObjects.Count; i++)
        {
            if (!rootGameObjectsExceptDontDestroyOnLoad.Contains(rootGameObjects[i]))
                result.Add(rootGameObjects[i]);
        }

        /*
        foreach( GameObject obj in result )
            Debug.Log("GetDontDestroyOnLoadObjects obj= " + obj.name);
        */

        return result;
    }


    public void SetIpAddress(string ipAddress)
    {
        m_ipAddress = ipAddress;
        Debug.Log("[MenuUI:SetIpAddress:ipAddress=" + ipAddress + ", m_ipAddress=" +m_ipAddress + "]");
    }

    public void HostGamePrivate()
    {
        if (Application.platform != RuntimePlatform.WebGLPlayer)
        {
            Debug.Log("[MenuUI:HostGamePrivate]");
            ShowPanel(m_connecting);
            PlayerPrefs.SetInt("gameVisibility", 0);
            /*instance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            instance.release();
            instanceVague.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            instanceVague.release();*/
            FMOD.Studio.Bus bus = FMODUnity.RuntimeManager.GetBus("bus:/");
            bus.stopAllEvents(FMOD.Studio.STOP_MODE.IMMEDIATE);
            NetworkManager.singleton.StartHost();
        }
    }

    public void HostGamePublic()
    {
        if (Application.platform != RuntimePlatform.WebGLPlayer)
        {
            Debug.Log("[MenuUI:HostGamePublic]");
            ShowPanel(m_connecting);
            PlayerPrefs.SetInt("gameVisibility", 1);
            /*instance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            instance.release();
            instanceVague.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            instanceVague.release();*/
            FMOD.Studio.Bus bus = FMODUnity.RuntimeManager.GetBus("bus:/");
            bus.stopAllEvents(FMOD.Studio.STOP_MODE.IMMEDIATE);
            NetworkManager.singleton.StartHost();
        }
    }

    public void JoinGame()
    {
        string ipAddress = m_btnInputField.GetComponent<InputField>().text;
        if (string.IsNullOrEmpty(ipAddress)) ipAddress = m_defaultIpAddress; // set to local ip address if ip address text is empty
        Debug.Log("[MenuUI:JoinGame:ipAddress=" + ipAddress + "]");
        JoinGameByIpAddress(ipAddress);
    }

    public void JoinGameByIpAddress(string ipAddress)
    {
        ShowPanel(m_connecting);
        StopDiscoveringGames();
        Debug.Log("String '" + ipAddress + "' is null ? " + (ipAddress == null).ToString());
        Debug.Log("String '" + ipAddress + "' is empty ? " + (ipAddress == String.Empty).ToString());
        Debug.Log("[MenuUI:JoinGameByIpAddress:ipAddress=" + ipAddress + "]");
        NetworkManager.singleton.networkAddress = ipAddress;
        /*instance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        instance.release();
        instanceVague.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        instanceVague.release();*/
        FMOD.Studio.Bus bus = FMODUnity.RuntimeManager.GetBus("bus:/");
        bus.stopAllEvents(FMOD.Studio.STOP_MODE.IMMEDIATE);
        NetworkManager.singleton.StartClient();
    }

    public void ExitGame()
    {
        Debug.Log("[MenuUI:ExitGame]");
        AskConfirmation(ConfirmExitGame, "Exit the game ?");
    }

    public void ConfirmExitGame()
    {
        Debug.Log("[MenuUI:ConfirmExitGame]");
        Application.Quit(0);
    }

    public void Inputs()
    {
        Debug.Log("[MenuUI:Inputs]");
        ShowPanel(m_inputs);
        DisplayControllerImage();
    }

    public void PreviousControllerImage()
    {
        Debug.Log("[MenuUI:PreviousControllerImage]");
        m_controllersImageIndex--;
        DisplayControllerImage();
    }

    public void NextControllerImage()
    {
        Debug.Log("[MenuUI:NextControllerImage]");
        m_controllersImageIndex++;
        DisplayControllerImage();
    }

    private void DisplayControllerImage()
    {
        if (m_controllersImageIndex < 0) m_controllersImageIndex = m_controllersImage.Count - 1;
        else if (m_controllersImageIndex > m_controllersImage.Count - 1) m_controllersImageIndex = 0;
        Debug.Log("[MenuUI:DisplayControllerImage:m_controllersImageIndex=" + m_controllersImageIndex + "]");
        m_controllerImage.texture = m_controllersImage[m_controllersImageIndex];
        if(m_controllersImageIndex == 0)
        {
            // Valve Index
            m_controllerName.text = "Valve Index";
            //m_controllerImage.uvRect = new Rect(0, -0.05f, 1, 1);
            m_controllerImage.uvRect = new Rect(0, 0, 1, 1);
        }
        else if(m_controllersImageIndex == 1)
        {
            // Vive Cosmos
            m_controllerName.text = "Vive Cosmos";
            m_controllerImage.uvRect = new Rect(0, 0, 1, 1);
        }
        else if(m_controllersImageIndex == 2)
        {
            // Oculus Rift S
            m_controllerName.text = "Oculus Rift S";
            m_controllerImage.uvRect = new Rect(0, 0, 1, 1);
        }
    }

    public void MainMenu()
    {
        Debug.Log("[MenuUI:MainMenu]");
        ShowPanel(m_mainMenu);
    }

    public void Games()
    {
        Debug.Log("[MenuUI:Games]");
        m_gamesListDiscovered.Clear();
        foreach(Transform child in m_gamesListContent.transform)
        {
            Destroy(child.gameObject);
        }
        ShowPanel(m_games);
        m_isDiscoveringGames = true;
        StartCoroutine(DiscoverGames());
    }

    private IEnumerator DiscoverGames()
    {
        Debug.Log("[MenuUI:DiscoverGames]");
        m_networkDiscovery.Initialize();
        m_networkDiscovery.StartAsClient();
        yield return new WaitForSecondsRealtime(1.0f);
        foreach(string key in m_gamesListDiscovered.Keys.ToList()) // unckeck every games founded
        {
            m_gamesListDiscovered[key] = false;
        }

        if (m_networkDiscovery.broadcastsReceived != null)
        {
            foreach (string addr in m_networkDiscovery.broadcastsReceived.Keys)
            {
                string ipAddress = addr.Split(':').Last();
                if (!ipAddress.Equals(GetLocalIPAddress()))
                {
                    if (!m_gamesListDiscovered.ContainsKey(ipAddress))
                    {
                        GameObject btnJoinGameItem = Instantiate(m_btnJoinGameItemPrefab);
                        btnJoinGameItem.transform.SetParent(m_gamesListContent.transform);
                        Vector3 rectPos = btnJoinGameItem.transform.GetComponent<RectTransform>().position;
                        btnJoinGameItem.transform.GetComponent<RectTransform>().position = new Vector3(rectPos.x, rectPos.y, +transform.root.position.z);
                        btnJoinGameItem.transform.localScale = new Vector3(1, 0.8f, 1);
                        btnJoinGameItem.GetComponent<Button>().onClick.AddListener(delegate { JoinGameByIpAddress(ipAddress); });
                        btnJoinGameItem.transform.GetChild(0).GetComponent<Text>().text = "Join game [" + ipAddress + "]";
                        btnJoinGameItem.name = ipAddress;
                    }
                    m_gamesListDiscovered[ipAddress] = true; // check games founded
                    Debug.Log("IP FOUNDED = " + ipAddress);
                }
            }
        }

        foreach (string key in m_gamesListDiscovered.Keys.ToList()) // remove unckeck games
        {
            if(m_gamesListDiscovered[key] == false)
            {
                Destroy(m_gamesListContent.transform.FindChild(key).gameObject);
                m_gamesListDiscovered.Remove(key);
                Debug.Log("IP REMOVED = " + key);
            }
        }

        StartCoroutine(DiscoverGames());
        yield return new WaitForSecondsRealtime(1.0f);
        yield return null;
    }

    private void StopDiscoveringGames()
    {
        Debug.Log("[MenuUI:StopDiscoverGames]");
        StopAllCoroutines();
        m_networkDiscovery.StopBroadcast();
        //m_gamesListDiscovered.Clear();
    }

    private void ShowPanel(GameObject panel)
    {
        Debug.Log("[MenuUI:ShowPanel:panel=" + panel.name + "]");
        if (m_isDiscoveringGames)
        {
            StopDiscoveringGames();
        }
        if(m_currentPanel != null)
        {
            m_currentPanel.SetActive(false);
        }
        panel.SetActive(true);
        m_currentPanel = panel;
    }

    public static string GetLocalIPAddress()
    {
        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (var ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                return ip.ToString();
            }
        }
        throw new Exception("No network adapters with an IPv4 address in the system!");
    }

    public void AskConfirmation(UnityAction callback, string title)
    {
        m_panelOnConfirmation = m_currentPanel;
        m_btnConfirmationYes.onClick.RemoveAllListeners();
        m_btnConfirmationYes.onClick.AddListener(callback);
        m_confirmationTitle.text = title;
        ShowPanel(m_userValidation);
    }

    public void CancelConfirmation()
    {
        Debug.Log("[MenuUI:CancelConfirmation]");
        ShowPanel(m_panelOnConfirmation);
    }
}
