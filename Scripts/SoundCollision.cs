﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundCollision : MonoBehaviour
{
    [SerializeField] string Event2;
    [FMODUnity.EventRef]
    private string Event = "event:/Objets/";
    public FMOD.Studio.EventInstance m_instance;


    private void Awake()
    {
        Event += Event2;
        Debug.Log("[EVENT] " + Event);
    }
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Avant son");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision infoCollision) // le type de la variable est Collision
    {
        Debug.Log("[EVENT Collision] " + Event);
        if (infoCollision.gameObject.CompareTag("Floor") == true)
            {
            Debug.Log("[EVENT Collision2] " + Event);
            if (Event != "")
            {
                m_instance = FMODUnity.RuntimeManager.CreateInstance(Event);
                m_instance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(this.gameObject));
                m_instance.start();
                StartCoroutine(WaitCollision());
            }
            }
    }
    IEnumerator WaitCollision()
    {
        yield return new WaitForSeconds(5);
        m_instance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        m_instance.release();
    }
}
