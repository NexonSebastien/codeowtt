﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerMenuUI : MonoBehaviour
{
    public const int GAME_STATUS_DEFEAT = 0;
    public const int GAME_STATUS_VICTORY = 1;
    public const int GAME_STATUS_RUNNING = 2;

    private int m_gameStatusValue;

    private GameObject m_canvas;
    private GameObject m_menu;
    private GameObject m_mainMenu;
    private GameObject m_gameStatus;
    private GameObject m_resume;
    private GameObject m_inputs;
    private GameObject m_userValidation;
    private GameObject m_userValidationContent;
    private Text m_confirmationTitle;
    private Button m_btnConfirmationYes;

    private GameObject m_currentPanel;
    private GameObject m_panelOnConfirmation;
    
    [SerializeField] private List<Texture> m_controllersImage;
    private int m_controllersImageIndex;
    private RawImage m_controllerImage;
    private Text m_controllerName;

    private Text m_endGameMessageText;
    private float m_fadeToAlpha;
    private float m_fadeDuration;
    private Color m_victoryTextColor;
    private Color m_defeatTextColor;
    private string m_victoryText;
    private string m_defeatText;

    private void Awake()
    {
        m_gameStatusValue = GAME_STATUS_RUNNING;
        m_canvas = transform.GetChild(0).gameObject;
        m_menu = m_canvas.transform.FindChild("Menu").gameObject;
        m_mainMenu = m_menu.transform.FindChild("MainMenu").gameObject;
        m_inputs = m_menu.transform.FindChild("Inputs").gameObject;
        m_gameStatus = m_mainMenu.transform.Find("GameStatus").gameObject;
        m_resume = m_gameStatus.transform.Find("Resume").gameObject;
        m_endGameMessageText = m_gameStatus.transform.Find("EndGameMessage").GetComponent<Text>();
        m_fadeToAlpha = 0.0f;
        m_fadeDuration = 1.0f;
        m_victoryTextColor = Color.green;
        m_defeatTextColor = Color.red;
        m_victoryText = "You escaped !";
        m_defeatText = "You died !";
        m_userValidation = m_menu.transform.FindChild("UserValidation").gameObject;
        m_userValidationContent = m_userValidation.transform.FindChild("View").FindChild("Viewport").FindChild("Content").gameObject;
        m_confirmationTitle = m_userValidationContent.transform.FindChild("Title").GetComponent<Text>();
        m_btnConfirmationYes = m_userValidationContent.transform.FindChild("Buttons").Find("BtnYes").GetComponent<Button>();
        m_controllersImageIndex = 0;
        m_controllerImage = m_inputs.transform.FindChild("ControllerImage").GetComponent<RawImage>();
        m_controllerName = m_inputs.transform.FindChild("Caroussel").FindChild("ControllerName").GetComponent<Text>();

        for (int i=0; i<m_menu.transform.childCount; i++)
        {
            m_menu.transform.GetChild(i).gameObject.SetActive(false);
        }
        ShowPanel(m_mainMenu);

        Hide();
    }

    public bool IsActive()
    {
        return m_canvas.activeSelf;
    }

    private void Show()
    {
        ShowPanel(m_mainMenu);
        if (m_gameStatusValue == GAME_STATUS_VICTORY || m_gameStatusValue == GAME_STATUS_DEFEAT)
        {
            m_resume.SetActive(false);
            m_endGameMessageText.gameObject.SetActive(true);
        }
        else if(m_gameStatusValue == GAME_STATUS_RUNNING)
        {
            m_resume.SetActive(true);
            m_endGameMessageText.gameObject.SetActive(false);
        }
        m_canvas.SetActive(true);
    }

    private void Hide()
    {
        m_canvas.SetActive(false);
    }

    public void Toggle()
    {
        if (IsActive())
        {
            Hide();
        }
        else
        {
            Show();
        }
    }

    public void Resume()
    {
        Debug.Log("[PlayerMenuUI:Resume]");
        Hide();
    }

    public void Inputs()
    {
        Debug.Log("[MenuUI:Inputs]");
        ShowPanel(m_inputs);
        DisplayControllerImage();
    }

    public void PreviousControllerImage()
    {
        Debug.Log("[MenuUI:PreviousControllerImage]");
        m_controllersImageIndex--;
        DisplayControllerImage();
    }

    public void NextControllerImage()
    {
        Debug.Log("[MenuUI:NextControllerImage]");
        m_controllersImageIndex++;
        DisplayControllerImage();
    }

    private void DisplayControllerImage()
    {
        if (m_controllersImageIndex < 0) m_controllersImageIndex = m_controllersImage.Count - 1;
        else if (m_controllersImageIndex > m_controllersImage.Count - 1) m_controllersImageIndex = 0;
        Debug.Log("[MenuUI:DisplayControllerImage:m_controllersImageIndex=" + m_controllersImageIndex + "]");
        m_controllerImage.texture = m_controllersImage[m_controllersImageIndex];
        if (m_controllersImageIndex == 0)
        {
            // Valve Index
            m_controllerName.text = "Valve Index";
            //m_controllerImage.uvRect = new Rect(0, -0.05f, 1, 1);
            m_controllerImage.uvRect = new Rect(0, 0, 1, 1);
        }
        else if (m_controllersImageIndex == 1)
        {
            // Vive Cosmos
            m_controllerName.text = "Vive Cosmos";
            m_controllerImage.uvRect = new Rect(0, 0, 1, 1);
        }
        else if (m_controllersImageIndex == 2)
        {
            // Oculus Rift S
            m_controllerName.text = "Oculus Rift S";
            m_controllerImage.uvRect = new Rect(0, 0, 1, 1);
        }
    }

    public void MainMenu()
    {
        Debug.Log("[MenuUI:MainMenu]");
        ShowPanel(m_mainMenu);
    }

    public void BackToMenu()
    {
        Debug.Log("[PlayerMenuUI:BackToMenu]");
        AskConfirmation(ConfirmBackToMenu, "Exit to main menu ?");
    }

    private void ConfirmBackToMenu()
    {
        Debug.Log("[PlayerMenuUI:ConfirmBackToMenu]");
        if (NetworkServer.active)
        {
            Debug.Log("[PlayerMenuUI:BackToMenu:NetworkServer.active=true]");
            if (NetworkManager.singleton.IsClientConnected())
            {
                Debug.Log("[PlayerMenuUI:BackToMenu:StopHost]");
                NetworkManager.singleton.StopHost();
            }
            else
            {
                Debug.Log("[PlayerMenuUI:BackToMenu:StopServer]");
                NetworkManager.singleton.StopServer();
            }
        }
        else
        {
            Debug.Log("[PlayerMenuUI:BackToMenu:NetworkServer.active=false]");
            if (NetworkManager.singleton.IsClientConnected())
            {
                Debug.Log("[PlayerMenuUI:BackToMenu:StopClient]");
                NetworkManager.singleton.StopClient();
            }
        }
    }

    private void SetEndGameMessage(string text, string message)
    {
        m_endGameMessageText.text = text + "\n" + message;
    }

    private void SetTextColor(Color color)
    {
        m_endGameMessageText.color = color;
    }

    public void ShowEndGameMessage(int gameStatus, string message)
    {
        Debug.Log("[PlayerMenuUI:ShowEndGameMessage:gameStatus=" + gameStatus + "]");
        m_gameStatusValue = gameStatus;
        if (gameStatus == GAME_STATUS_VICTORY)
        {
            SetEndGameMessage(m_victoryText, message);
            SetTextColor(m_victoryTextColor);
            Show();
        }
        else if (gameStatus == GAME_STATUS_DEFEAT)
        {
            SetEndGameMessage(m_defeatText, message);
            SetTextColor(m_defeatTextColor);
            Show();
        }
    }

    private void ShowPanel(GameObject panel)
    {
        Debug.Log("[PlayerMenuUI:ShowPanel:panel=" + panel.name + "]");
        if (m_currentPanel != null)
        {
            m_currentPanel.SetActive(false);
        }
        panel.SetActive(true);
        m_currentPanel = panel;
    }

    public void AskConfirmation(UnityAction callback, string title)
    {
        m_panelOnConfirmation = m_currentPanel;
        m_btnConfirmationYes.onClick.RemoveAllListeners();
        m_btnConfirmationYes.onClick.AddListener(callback);
        m_confirmationTitle.text = title;
        ShowPanel(m_userValidation);
    }

    public void CancelConfirmation()
    {
        Debug.Log("[PlayerMenuUI:CancelConfirmation]");
        ShowPanel(m_panelOnConfirmation);
    }
}
