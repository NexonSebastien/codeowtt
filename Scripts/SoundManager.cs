﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private FMOD.Studio.EventInstance m_instanceCave;
    private string EventCave = "event:/Ambiance/Cave";

    // Start is called before the first frame update
    void Start()
    {
        m_instanceCave = FMODUnity.RuntimeManager.CreateInstance(EventCave);
        m_instanceCave.start();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
