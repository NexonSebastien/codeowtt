﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(NetworkIdentity))]
[RequireComponent(typeof(NetworkTransform))]
[System.Obsolete]
public abstract class NetworkObjectEvent : NetworkBehaviour
{
    [SerializeField] protected EnigmaManager m_EnigmaManager;
    protected NetworkIdentity m_NetId;
    private Vector3 m_defaultPosition;
    private Quaternion m_defaultRotation;
    private Rigidbody m_defaultRigidbody;
    private RigidbodyConstraints m_defaultRigidbodyConstraints;
    private bool m_defaultUseGravity;
    private bool m_defaultIsKinematic;
    [SerializeField] private float m_distanceMax;
    private bool m_isOutOfMap;

    virtual protected void Start()
    {
        if(m_distanceMax == 0)
        {
            m_distanceMax = 100.0f;
        }
        m_NetId = GetComponent<NetworkIdentity>();
        m_defaultPosition = transform.position;
        m_defaultRotation = transform.rotation;
        if(TryGetComponent(out Rigidbody r))
        {
            m_defaultRigidbody = r;
            m_defaultRigidbodyConstraints = r.constraints;
            m_defaultUseGravity = r.useGravity;
            m_defaultIsKinematic = r.isKinematic;
        }
        else
        {
            m_defaultRigidbody = null;
        }
        StartCoroutine(CheckOutOfMap());
    }

    private IEnumerator CheckOutOfMap()
    {
        yield return new WaitUntil(() => IsTooFar() || m_isOutOfMap);
        if (hasAuthority && transform.root.tag != "Player")
        {
            m_isOutOfMap = false;
            Debug.Log("[NetworkObjectEvent:CheckOutOfMap:object=" + name + ":distance>" + m_distanceMax + "]");
            if (!isServer)
            {
                CmdResetOutOfMap(GameObject.Find("VrGameObject").GetComponentInParent<NetworkIdentity>());
            }
            else
            {
                ResetOutOfMap();
            }
        }
        StartCoroutine(CheckOutOfMap());
    }

    [Command]
    private void CmdResetOutOfMap(NetworkIdentity player)
    {
        GetComponent<NetworkIdentity>().RemoveClientAuthority(player.connectionToClient);
        ResetOutOfMap();
    }

    [ServerCallback]
    private void ResetOutOfMap()
    {
        Debug.Log("[NetworkObjectEvent:ResetOutOfMap]");
        if (m_defaultRigidbody != null)
        {
            m_defaultRigidbody.constraints = RigidbodyConstraints.FreezeAll;
            m_defaultRigidbody.velocity = new Vector3(0, 0, 0);
            m_defaultRigidbody.angularVelocity = new Vector3(0, 0, 0);
        }
        transform.rotation = m_defaultRotation;
        transform.position = m_defaultPosition;
        if (m_defaultRigidbody != null)
        {
            m_defaultRigidbody.isKinematic = m_defaultIsKinematic;
            m_defaultRigidbody.useGravity = m_defaultUseGravity;
            m_defaultRigidbody.constraints = m_defaultRigidbodyConstraints;
        }
    }

    private bool IsTooFar()
    {
        return Vector3.Distance(transform.position, m_defaultPosition) > m_distanceMax;
    }

    public void SetIsOutOfMap()
    {
        m_isOutOfMap = true;
    }

    // update object states, for example to definitive position/rotation or remove component like Interractable etc
    abstract public void EnigmaUpdateEvent();

    /*
    // ask the server to throw an event, for example play a sound, change the gameobject color etc
    protected void EnigmaThrowEvent(int eventCase, List<object> args)
    {
        CmdThrowEvent(GetComponent<NetworkIdentity>(), eventCase, args);
    }
    */

    // server orders to start an event, for example play a sound, change the gameobject color etc
    abstract public void EnigmaReceiveEvent(int eventCase, List<object> args);
    /*
    // create method to be called by EnigmaReceiveEvent to handle each event
    override public void EnigmaReceiveEvent(int eventCase, List<object> args)
    {
        if (isServer) // update the gameobject on server side
        {
            switch(eventCase){
                case 1:
                    PlaySound(args);
                    break;
            }
        }
    }

    private void PlaySound(List<object> args){
        AudioSource audioSource = GetComponent<AudioSource>();
        float volume = 0.5f;
        if (args.Count > 0 && float.IsNaN(args[0]) == false) volume = args[0];
        audioSource.PlayOneShot(audioSource.clip, volume);
    }
    */

    // tell the server that the object completion to solve the enigma changed
    protected void UpdateEnigma(NetworkIdentity objNI, bool completion)
    {
        m_EnigmaManager.OnUpdateEnigma(objNI, completion);
    }

    // ask the server to throw an event (method inside the daughter script) to all clients, for example play a sound
    protected void ThrowEvent(NetworkIdentity objNI, int eventCase, List<object> args)
    {
        //m_EnigmaManager.CmdThrowEvent(objNI, eventCase, args);
        m_EnigmaManager.OnThrowEvent(objNI, eventCase, args);
    }
}
