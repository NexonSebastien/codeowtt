﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalTest : MonoBehaviour
{
    //Materials pour le renderer des cristaux de solution
    public Material[] materials;
    //Renderer des cristaux de solutions
    public Renderer[] Cristal_rend;
    //cristaux de solution
    [SerializeField]
    GameObject Cristal1;
    [SerializeField]
    GameObject Cristal2;
    [SerializeField]
    GameObject Cristal3;
    [SerializeField]
    GameObject Cristal4;
    [SerializeField]
    GameObject Cristal5;
    [SerializeField]
    GameObject Cristal6;
    [SerializeField]
    GameObject Cristal7;
    [SerializeField]
    GameObject Cristal8;
  
    // List de la solution
    private List<int> ListSoluce1 = new List<int>() { 1, 2, 3, 5 };
    // curseur pour parcourir la liste de solution en fonction de l'anvancement du joueur
    int curseur1 = 0;

    // stade de l'enigme
    bool lv_part1 = false;
    bool lv_part2 = false;
    bool lv_part3 = false;
    bool m_isComplete = false;

    // partie de l'enigme finie
    bool lv_part1end = false;
    bool lv_part2end = false;

    // position des cristauxFinaux
    float position_z = 10.65f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    // l'enigme est complète
    public bool IsCompleted()
    {
        return m_isComplete;
    }

    // Getter de la liste de solution pour l'accès au script PlayMusicCrystal.Cs
 
    public List<int> GetListSoluce()
    {
        return ListSoluce1;
    }

    // On touche un cristal avec le musicRod
    public bool OnHitCrystal(int id)
    {
        // Bonne combinaison
        if (ListSoluce1[curseur1] == id)
        {     
            curseur1 += 1;
            Debug.Log("curseur + 1");
            Debug.Log("curseur = " + curseur1);

            // vérification du stade de l'enigme
            if (curseur1 == ListSoluce1.Count)
            {
                if (curseur1 == 4 && lv_part1end == false)
                {
                    // si partie 1 fini
                    Partie1Enigme();
                }
                if (curseur1 == 6 && lv_part1end == true)
                {
                    // si partie 2 fini
                    Partie2Enigme();
                }
                if (curseur1 == 8 && lv_part2end == true)
                {
                    // si partie 3 fini
                    Partie3Enigme();
                    return true;
                }
            }
            CrystalRenderer(curseur1);       
            return true;
        }
        // mauvaise combinaison 
        else
        {                 
            Debug.Log("mauvaise combinaison");
            curseur1 = 0;
            CrystalRenderer(curseur1);
            return false;
        }
    }

    // fin du stade 1 de l'enigme
    private void Partie1Enigme()
    {

        //partie 1 fini
        lv_part1 = true;

        // ajout de 2 valeur a la liste et retour a 0
        if (lv_part1end == false)
        {
            //Ajout de 2 valeurs supplémentaires a la liste
            ListSoluce1.Add(3);
            ListSoluce1.Add(3);
            //redémarage du curseur 
            Debug.Log("RESET 0");
            curseur1 = 0;
            //Mise a jour des renderers des cristaux de solution
            CrystalRenderer(curseur1);
            //partie 1 fini
            lv_part1end = true;
            // ajout des cristaux phase 2
            CrystalPositionPhase2();
            Debug.Log("ListSoluce1.Count : " + ListSoluce1.Count);
        }
    }
    // fin du stade 2 de l'enigme
    private void Partie2Enigme()
    {
        lv_part2 = true;

        // ajout de 2 valeur a la liste et retour a 0
        if (lv_part2end == false)
        {
            //Ajout de 2 valeurs supplémentaires a la liste
            ListSoluce1.Add(5);
            ListSoluce1.Add(5);
            //redémarage du curseur
            Debug.Log("RESET 0");
            curseur1 = 0;
            //Mise a jour des renderers des cristaux de solution
            CrystalRenderer(curseur1);
            //partie 2 fini
            lv_part2end = true;
            // ajout des cristaux phase 3
            CrystalPositionPhase3();
            Debug.Log("ListSoluce1.Count : " + ListSoluce1.Count);
        }
        CrystalRenderer(curseur1);
    }
    // fin du stade 3 de l'enigme
    private void Partie3Enigme()
    {
        // si partie 3 fini
        if (lv_part3 == true)
        {
            // fin de l'enigme
            m_isComplete = true;
        }
    }

    // couleur des cristaux en fonction de l'avancement
    private void CrystalRenderer(int curseur)
    {
        if (curseur == 0)
        {
            Cristal_rend[0] = Cristal1.GetComponentInChildren<Renderer>();
            Cristal_rend[0].enabled = true;

            Cristal_rend[1] = Cristal2.GetComponentInChildren<Renderer>();
            Cristal_rend[1].enabled = true;

            Cristal_rend[2] = Cristal3.GetComponentInChildren<Renderer>();
            Cristal_rend[2].enabled = true;

            Cristal_rend[3] = Cristal4.GetComponentInChildren<Renderer>();
            Cristal_rend[3].enabled = true;

            Cristal_rend[4] = Cristal5.GetComponentInChildren<Renderer>();
            Cristal_rend[4].enabled = true;

            Cristal_rend[5] = Cristal6.GetComponentInChildren<Renderer>();
            Cristal_rend[5].enabled = true;

            Cristal_rend[6] = Cristal7.GetComponentInChildren<Renderer>();
            Cristal_rend[6].enabled = true;

            Cristal_rend[7] = Cristal8.GetComponentInChildren<Renderer>();
            Cristal_rend[7].enabled = true;

            Cristal_rend[0].sharedMaterial = materials[5];
            Cristal_rend[1].sharedMaterial = materials[6];
            Cristal_rend[2].sharedMaterial = materials[7];
            Cristal_rend[3].sharedMaterial = materials[8];
            Cristal_rend[4].sharedMaterial = materials[9];
            Cristal_rend[5].sharedMaterial = materials[10];
            Cristal_rend[6].sharedMaterial = materials[11];
            Cristal_rend[7].sharedMaterial = materials[12];

        }
        if (curseur == 1)
        {
            Cristal_rend[0] = Cristal1.GetComponentInChildren<Renderer>();
            Cristal_rend[0].enabled = true;

            if (ListSoluce1[curseur1 - 1] == 1)
            {
                Cristal_rend[0].sharedMaterial = materials[0];
            }
            if (ListSoluce1[curseur1 - 1] == 2)
            {
                Cristal_rend[0].sharedMaterial = materials[3];
            }
            if (ListSoluce1[curseur1 - 1] == 3)
            {
                Cristal_rend[0].sharedMaterial = materials[1];
            }
            if (ListSoluce1[curseur1 - 1] == 4)
            {
                Cristal_rend[0].sharedMaterial = materials[2];
            }
            if (ListSoluce1[curseur1 - 1] == 5)
            {
                Cristal_rend[0].sharedMaterial = materials[4];
            }
        }
        if (curseur == 2)
        {
            Cristal_rend[1] = Cristal2.GetComponentInChildren<Renderer>();
            Cristal_rend[1].enabled = true;

            if (ListSoluce1[curseur1 - 1] == 1)
            {
                Cristal_rend[1].sharedMaterial = materials[0]; //rouge
            }
            if (ListSoluce1[curseur1 - 1] == 2)
            {
                Cristal_rend[1].sharedMaterial = materials[3]; // bleu
            }
            if (ListSoluce1[curseur1 - 1] == 3)
            {
                Cristal_rend[1].sharedMaterial = materials[1]; // jaune
            }
            if (ListSoluce1[curseur1 - 1] == 4)
            {
                Cristal_rend[1].sharedMaterial = materials[2]; // vert
            }
            if (ListSoluce1[curseur1 - 1] == 5)
            {
                Cristal_rend[1].sharedMaterial = materials[4]; // noir
            }
        }
        if (curseur == 3)
        {
            Cristal_rend[2] = Cristal3.GetComponentInChildren<Renderer>();
            Cristal_rend[2].enabled = true;

            if (ListSoluce1[curseur1 - 1] == 1)
            {
                Cristal_rend[2].sharedMaterial = materials[0];
            }
            if (ListSoluce1[curseur1 - 1] == 2)
            {
                Cristal_rend[2].sharedMaterial = materials[3];
            }
            if (ListSoluce1[curseur1 - 1] == 3)
            {
                Cristal_rend[2].sharedMaterial = materials[1];
            }
            if (ListSoluce1[curseur1 - 1] == 4)
            {
                Cristal_rend[2].sharedMaterial = materials[2];
            }
            if (ListSoluce1[curseur1 - 1] == 5)
            {
                Cristal_rend[2].sharedMaterial = materials[4];
            }
        }
        if (curseur == 4)
        {
            Cristal_rend[3] = Cristal4.GetComponentInChildren<Renderer>();
            Cristal_rend[3].enabled = true;

            if (ListSoluce1[curseur1 - 1] == 1)
            {
                Cristal_rend[3].sharedMaterial = materials[0];
            }
            if (ListSoluce1[curseur1 - 1] == 2)
            {
                Cristal_rend[3].sharedMaterial = materials[3];
            }
            if (ListSoluce1[curseur1 - 1] == 3)
            {
                Cristal_rend[3].sharedMaterial = materials[1];
            }
            if (ListSoluce1[curseur1 - 1] == 4)
            {
                Cristal_rend[3].sharedMaterial = materials[2];
            }
            if (ListSoluce1[curseur1 - 1] == 5)
            {
                Cristal_rend[3].sharedMaterial = materials[4];
            }
        }
        if (curseur == 5)
        {
            Cristal_rend[4] = Cristal5.GetComponentInChildren<Renderer>();
            Cristal_rend[4].enabled = true;

            if (ListSoluce1[curseur1 - 1] == 1)
            {
                Cristal_rend[4].sharedMaterial = materials[0];
            }
            if (ListSoluce1[curseur1 - 1] == 2)
            {
                Cristal_rend[4].sharedMaterial = materials[3];
            }
            if (ListSoluce1[curseur1 - 1] == 3)
            {
                Cristal_rend[4].sharedMaterial = materials[1];
            }
            if (ListSoluce1[curseur1 - 1] == 4)
            {
                Cristal_rend[4].sharedMaterial = materials[2];
            }
            if (ListSoluce1[curseur1 - 1] == 5)
            {
                Cristal_rend[4].sharedMaterial = materials[4];
            }
        }
        if (curseur == 6)
        {
            Cristal_rend[5] = Cristal6.GetComponentInChildren<Renderer>();
            Cristal_rend[5].enabled = true;

            if (ListSoluce1[curseur1 - 1] == 1)
            {
                Cristal_rend[5].sharedMaterial = materials[0];
            }
            if (ListSoluce1[curseur1 - 1] == 2)
            {
                Cristal_rend[5].sharedMaterial = materials[3];
            }
            if (ListSoluce1[curseur1 - 1] == 3)
            {
                Cristal_rend[5].sharedMaterial = materials[1];
            }
            if (ListSoluce1[curseur1 - 1] == 4)
            {
                Cristal_rend[5].sharedMaterial = materials[2];
            }
            if (ListSoluce1[curseur1 - 1] == 5)
            {
                Cristal_rend[5].sharedMaterial = materials[4];
            }
        }
        if (curseur == 7)
        {
            Cristal_rend[6] = Cristal7.GetComponentInChildren<Renderer>();
            Cristal_rend[6].enabled = true;

            if (ListSoluce1[curseur1 - 1] == 1)
            {
                Cristal_rend[6].sharedMaterial = materials[0];
            }
            if (ListSoluce1[curseur1 - 1] == 2)
            {
                Cristal_rend[6].sharedMaterial = materials[3];
            }
            if (ListSoluce1[curseur1 - 1] == 3)
            {
                Cristal_rend[6].sharedMaterial = materials[1];
            }
            if (ListSoluce1[curseur1 - 1] == 4)
            {
                Cristal_rend[6].sharedMaterial = materials[2];
            }
            if (ListSoluce1[curseur1 - 1] == 5)
            {
                Cristal_rend[6].sharedMaterial = materials[4];
            }
        }
        if (curseur == 8)
        {
            lv_part3 = true;
            Cristal_rend[7] = Cristal8.GetComponentInChildren<Renderer>();
            Cristal_rend[7].enabled = true;

            if (ListSoluce1[curseur1 - 1] == 1)
            {
                Cristal_rend[7].sharedMaterial = materials[0];
            }
            if (ListSoluce1[curseur1 - 1] == 2)
            {
                Cristal_rend[7].sharedMaterial = materials[3];
            }
            if (ListSoluce1[curseur1 - 1] == 3)
            {
                Cristal_rend[7].sharedMaterial = materials[1];
            }
            if (ListSoluce1[curseur1 - 1] == 4)
            {
                Cristal_rend[7].sharedMaterial = materials[2];
            }
            if (ListSoluce1[curseur1 - 1] == 5)
            {
                Cristal_rend[7].sharedMaterial = materials[4];
            }

        }
    }
    // Position des cristaux pour la phase 2
    private void CrystalPositionPhase2()
    {
        Cristal5.transform.position = new Vector3(Cristal5.transform.position.x, Cristal5.transform.position.y, Cristal4.transform.position.z);
        Cristal6.transform.position = new Vector3(Cristal6.transform.position.x, Cristal6.transform.position.y, Cristal4.transform.position.z);
    }
    // Position des cristaux pour la phase 3
    private void CrystalPositionPhase3()
    {
        Cristal7.transform.position = new Vector3(Cristal7.transform.position.x, Cristal7.transform.position.y, Cristal4.transform.position.z);
        Cristal8.transform.position = new Vector3(Cristal8.transform.position.x, Cristal8.transform.position.y, Cristal4.transform.position.z);
    }

    
}



