﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Valve.VR.InteractionSystem;

[System.Obsolete]
public class NetworkObjectEvent_Brasero : NetworkObjectEvent
{
    [SerializeField] private NetworkObjectEvent_Brasero m_braseroPair; // the other brasero with the same symbol
    [SerializeField] private int m_symbolId; // the symbol id on this brasero
    private bool m_isEmbrased; // is this brasero embrased
    private float m_embrasementTime; // in seconds
    private float m_burnTime; // fire source burn time
    private float m_burnInfinite; // fire source burn infinite time
    private float m_lightIntensityHigh;
    private float m_lightIntensityLow;
    private EnigmaManager_Brasero m_braseroManager;
    private FireSource m_fireSource;
    private Light t_light;

    private FMOD.Studio.EventInstance playerState;
    private FMOD.Studio.EventInstance playerState2;

    [FMODUnity.EventRef]
    private string EventBraseroContinue = "event:/Objets/BraseroContinue";

    [FMODUnity.EventRef]
    private string EventBraseroAllume = "event:/Objets/BraseroAllume";

    [FMODUnity.EventRef]
    private string EventBraseroEteint = "event:/Objets/BraseroEteint";

    private int NbrBrazeroEmbrase = 0;


    override protected void Start()
    {
        Debug.Log("[NetworkObjectEvent_Brasero:Start]");
        base.Start();
        m_isEmbrased = false;
        m_embrasementTime = 2.0f;
        m_burnTime = 3.0f;
        m_burnInfinite = 0.0f;
        m_lightIntensityHigh = 10.0f;
        m_lightIntensityLow = 0.0f;
        //m_braseroManager = (EnigmaManager_Brasero) m_EnigmaManager;
        m_fireSource = GetComponentInChildren<FireSource>();
        t_light = GetComponentInChildren<Light>();

        

        playerState = FMODUnity.RuntimeManager.CreateInstance(EventBraseroContinue);
        playerState2 = FMODUnity.RuntimeManager.CreateInstance(EventBraseroContinue);
        playerState.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(this.gameObject.transform.position));
        playerState2.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(m_braseroPair.transform.position));
    }

    public int GetId()
    {
        return m_symbolId;
    }

    public bool IsEmbrased()
    {
        Debug.Log("[NetworkObjectEvent_Brasero:IsEmbrased]");
        return m_isEmbrased;
    }

    public void SetEnigmaManager(EnigmaManager enigmaManager)
    {
        m_EnigmaManager = enigmaManager;
        m_braseroManager = (EnigmaManager_Brasero)m_EnigmaManager;
    }

    public override void EnigmaReceiveEvent(int eventCase, List<object> args)
    {
        if (isServer)
        {
            Debug.Log("[NetworkObjectEvent_Brasero:EnigmaReceiveEvent:case=" + eventCase + "]");
            switch (eventCase)
            {
                case 1:
                    // embrase for seconds
                    RpcEmbraseForSeconds(m_embrasementTime);
                    break;
                case 2:
                    // extinguish
                    RpcExtinguish();
                    break;
                case 3:
                    // embrase
                    RpcEmbrase();
                    break;
            }
        }
    }

    public override void EnigmaUpdateEvent()
    {
        Debug.Log("[NetworkObjectEvent_Brasero:EnigmaUpdateEvent]");
        //RpcEmbraseForSeconds(-1);
        RpcEmbrase();
    }

    [ServerCallback]
    public void OnTriggerEnter(Collider other)
    {
        
        
        if (other.CompareTag("FireSource") && other.TryGetComponent(out FireSource f) && f.isBurning)
        {
            string hie = other.name;
            GameObject parent = other.gameObject;
            while (parent.transform.parent != null)
            {
                parent = parent.transform.parent.gameObject;
                hie += ">" + parent.name;
            }

            Debug.Log("Triggered = " + hie);

            Debug.Log("[NetworkObjectEvent_Brasero:OnTriggerEnter]");
            if (m_isEmbrased && m_braseroPair.IsEmbrased())
            {
                Debug.Log("[NetworkObjectEvent_Brasero:OnTriggerEnter:return=alreadyEmbrased]");
                return;
            }
            if (!m_isEmbrased && m_braseroPair.IsEmbrased() && m_symbolId == m_braseroManager.GetCurrentSymbolId()) // check if this brasero is not embrased to avoid massive datas send to the server
            {
                Debug.Log("[NetworkObjectEvent_Brasero:OnTriggerEnter:UpdateEnigma=bothEmbrased]");
                RpcEmbraseContinue();
                m_braseroManager.ConfirmEmbrasement();
                UpdateEnigma(m_NetId, true);
                UpdateEnigma(m_braseroPair.GetComponent<NetworkIdentity>(), true);
                
            }
            else if (!m_isEmbrased)
            {
                Debug.Log("[NetworkObjectEvent_Brasero:OnTriggerEnter:ThrowEvent=embrase]");
                //ThrowEvent(m_NetId, 1, null); // ask to embrase the brasero
                StartCoroutine(Embrasement());
            }
        }
    }

    IEnumerator Embrasement()
    {
        Debug.Log("[NetworkObjectEvent_Brasero:Embrasement]");
        int symbol = m_braseroManager.GetCurrentSymbolId();
        ThrowEvent(m_NetId, 3, null);  // embrase the brasero
        yield return new WaitForSeconds(m_embrasementTime);
        if (!m_braseroPair.IsEmbrased() || m_symbolId != symbol)
        {
            ThrowEvent(m_NetId, 2, null); ; // extinguish after seconds if brasero pair is not embrased
        }

        yield return null;
    }

    [ClientRpc]
    public void RpcEmbrase()
    {
        m_isEmbrased = true; // embrase
        t_light.intensity = m_lightIntensityHigh;
        m_fireSource.burnTime = m_burnInfinite;
        m_fireSource.isBurning = true;
        FMODUnity.RuntimeManager.PlayOneShotAttached(EventBraseroAllume, this.gameObject);
    }

    [ClientRpc]
    public void RpcExtinguish()
    {
        Debug.Log("[NetworkObjectEvent_Brasero:RpcExtinguish]");
        m_isEmbrased = false; // extinguish after seconds
        t_light.intensity = m_lightIntensityLow;
        m_fireSource.burnTime = m_burnTime;
        m_fireSource.isBurning = true;
        playerState.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        playerState2.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        FMODUnity.RuntimeManager.PlayOneShotAttached(EventBraseroEteint, this.gameObject);
    }


    [ClientRpc]
    public void RpcEmbraseForSeconds(float seconds)
    {

        Debug.Log("[NetworkObjectEvent_Brasero:RpcEmbrase:seconds="+seconds+"]");
        FMODUnity.RuntimeManager.PlayOneShotAttached(EventBraseroAllume, this.gameObject);

        StartCoroutine(Embrase(seconds));
    }

    [ClientRpc]
    private void RpcEmbraseContinue()
    {
        playerState.start();
        playerState2.start();
    }

    // -1 = embrase indefinitely, else embrase for seconds
    IEnumerator Embrase(float seconds)
    {
        NbrBrazeroEmbrase += 1;
        Debug.Log("[NetworkObjectEvent_Brasero:Embrase:seconds=" + seconds + "]");
        int symbol = m_braseroManager.GetCurrentSymbolId();
        m_isEmbrased = true; // embrase
        t_light.intensity = 10;
        if (seconds > 0) // embrase for seconds
        {
            // emit particules for seconds with coroutine
            yield return new WaitForSeconds(seconds);
            if (!m_braseroPair.IsEmbrased() || m_symbolId != symbol)
            {
                m_isEmbrased = false; // extinguish after seconds
                t_light.intensity = 0;
                Debug.Log("[NetworkObjectEvent_Brasero:Embrase:extinguish]");
            }
        }
    }

    public int GetNbrBrazeroEmbrase()
    {
        return NbrBrazeroEmbrase;
    }
}
