﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float m_speed;
    private PlayerMotor m_motor;

    private void Start()
    {
        m_motor = GetComponent<PlayerMotor>();
    }

    // Update is called once per frame
    void Update()
    {
        float xMov = Input.GetAxisRaw("Horizontal");
        float zMov = Input.GetAxisRaw("Vertical");

        Vector3 movHorizontal = transform.right * xMov;
        Vector3 movVertical = transform.forward * zMov;

        Vector3 m_velocity = (movHorizontal + movVertical).normalized * m_speed;
        m_motor.Move(m_velocity);
    }
}
