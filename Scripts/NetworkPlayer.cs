﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Valve.VR;
using Valve.VR.InteractionSystem;

[System.Obsolete]
public class NetworkPlayer : NetworkBehaviour
{
    private GameObject m_VrGameObject; // VR gameobject

    private GameObject m_VrHead; // vr head
    private Hand m_VrRightHand; // vr right hand
    private Hand m_VrLeftHand; // vr left hand
    private GameObject m_BodyCollider;

    [SerializeField] public GameObject m_NetHead; // head on network
    [SerializeField] public GameObject m_NetRightHand; // right hand on network
    [SerializeField] public GameObject m_NetLeftHand; // left hand on network
    [SerializeField] public GameObject m_BodyTrigger;

    /*[HideInInspector]*/
    [SyncVar] public Vector3 m_BodyPositionSynced;
    /*[HideInInspector]*/
    [SyncVar] public Vector3 m_BodyRotationSynced;
    /*[HideInInspector]*/
    [SyncVar] public Vector3 m_NetHeadPositionSynced;
    /*[HideInInspector]*/
    [SyncVar] public Vector3 m_NetHeadRotationSynced;
    /*[HideInInspector]*/
    [SyncVar] public Vector3 m_NetRightHandPositionSynced;
    /*[HideInInspector]*/
    [SyncVar] public Vector3 m_NetRightHandRotationSynced;
    /*[HideInInspector]*/
    [SyncVar] public Vector3 m_NetLeftHandPositionSynced;
    /*[HideInInspector]*/
    [SyncVar] public Vector3 m_NetLeftHandRotationSynced;
    [SyncVar] public Vector3 m_BodyTriggerPositionSynced;

    private Vector3 m_BodyPosition;
    private Vector3 m_BodyRotation;
    private Vector3 m_NetHeadPosition;
    private Vector3 m_NetHeadRotation;
    private Vector3 m_NetRightHandPosition;
    private Vector3 m_NetRightHandRotation;
    private Vector3 m_NetLeftHandPosition;
    private Vector3 m_NetLeftHandRotation;
    private Vector3 m_BodyTriggerPosition;

    [Header("Options")]
    public float _smoothSpeed = 10f;

    private PlayerMenuUI m_playerMenuUI;
    SteamVR_Action_Boolean m_menuAction;

    // this is ONLY called on local player
    public override void OnStartLocalPlayer()
    {
        m_playerMenuUI = transform.Find("Head").Find("PlayerMenuUI").GetComponent<PlayerMenuUI>();
        m_menuAction = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("Menu");

        m_VrGameObject = GameObject.Find("VrGameObject");
        m_VrGameObject.transform.SetParent(this.transform);

        GameObject SteamVRObjects = m_VrGameObject.transform.Find("SteamVRObjects").gameObject; // find the rig in the scene
        m_VrGameObject.transform.Find("SteamVRObjects");
        m_VrHead = SteamVRObjects.transform.Find("VRCamera").gameObject;
        m_VrLeftHand = SteamVRObjects.transform.Find("LeftHand").GetComponent<Hand>();
        m_VrRightHand = SteamVRObjects.transform.Find("RightHand").GetComponent<Hand>();
        m_BodyCollider = SteamVRObjects.transform.Find("BodyCollider").gameObject;

        if(SteamVRObjects.active == false)
        {
            Debug.Log("[m:OnStartLocalPlayer] NoVR");
            m_BodyCollider = m_VrGameObject.transform.Find("NoSteamVRFallbackObjects").transform.Find("FallbackObjects").gameObject;
            GameObject headlessPlayer = m_VrGameObject.transform.Find("NoSteamVRFallbackObjects").gameObject;

            // when running as headless, provide default non-moving objects instead
            m_VrHead = headlessPlayer.transform.Find("FallbackObjects").gameObject;
            m_VrLeftHand = headlessPlayer.transform.Find("FallbackHand").GetComponent<Hand>();
            m_VrRightHand = headlessPlayer.transform.Find("FallbackHand").GetComponent<Hand>();
        }

        m_NetRightHand.transform.Find("Skin").gameObject.SetActive(false);
        m_NetLeftHand.transform.Find("Skin").gameObject.SetActive(false);
        m_NetHead.transform.Find("Skin").gameObject.SetActive(false);

        m_VrGameObject.transform.position = transform.position;
        m_VrGameObject.transform.rotation = transform.rotation;
        m_BodyPosition = transform.position;
        m_BodyRotation = transform.rotation.eulerAngles;

        m_NetHead.transform.position = m_VrHead.transform.position;
        m_NetHead.transform.rotation = m_VrHead.transform.rotation;
        m_NetHeadPosition = m_VrHead.transform.position;
        m_NetHeadRotation = m_VrHead.transform.rotation.eulerAngles;

        m_NetRightHand.transform.position = m_NetRightHand.transform.position;
        m_NetRightHand.transform.rotation = m_NetRightHand.transform.rotation;
        m_NetRightHandPosition = m_VrRightHand.transform.position;
        m_NetRightHandRotation = m_VrRightHand.transform.rotation.eulerAngles;

        m_NetLeftHand.transform.position = m_NetLeftHand.transform.position;
        m_NetLeftHand.transform.rotation = m_NetLeftHand.transform.rotation;
        m_NetLeftHandPosition = m_VrLeftHand.transform.position;
        m_NetLeftHandRotation = m_VrLeftHand.transform.rotation.eulerAngles;

        m_BodyTrigger.transform.position = m_BodyCollider.transform.position;
        m_BodyTriggerPosition = m_BodyCollider.transform.position;

        Debug.Log("HEAD = " + m_VrHead);
        Debug.Log("RIGHT HAND = " + m_VrRightHand);
        Debug.Log("LEFT HAND = " + m_VrLeftHand);
    }

    private void Start()
    {
        if (isLocalPlayer)
        {
            gameObject.name = "Player " + GetComponent<NetworkIdentity>().netId + "(local)";
        }
        else
        {
            gameObject.name = "Player " + GetComponent<NetworkIdentity>().netId + "(remote)";
        }
    }

    void Update()
    {
        //Debug.Log("Right hand (" + m_NetRightHand.transform.position + ") FROM " + m_NetRightHandPosition + " TO " + m_NetRightHandPositionSynced);
        // Send my data
        if (isLocalPlayer)
        {
            // Send my Pos/Rot to server
            CmdUpdateStates(m_BodyPosition, m_BodyRotation, m_NetHeadPosition, m_NetHeadRotation,
                m_NetRightHandPosition, m_NetRightHandRotation, m_NetLeftHandPosition, m_NetLeftHandRotation, m_BodyTriggerPosition);
            /* to remove : show local behaviour */
            m_NetHead.transform.position = Vector3.Lerp(m_NetHead.transform.position, m_NetHeadPositionSynced, _smoothSpeed * Time.deltaTime);
            m_NetHead.transform.rotation = Quaternion.Lerp(m_NetHead.transform.rotation, Quaternion.Euler(m_NetHeadRotationSynced), _smoothSpeed * Time.deltaTime);
            m_NetRightHand.transform.position = Vector3.Lerp(m_NetRightHand.transform.position, m_NetRightHandPositionSynced, _smoothSpeed * Time.deltaTime);
            m_NetRightHand.transform.rotation = Quaternion.Lerp(m_NetRightHand.transform.rotation, Quaternion.Euler(m_NetRightHandRotationSynced), _smoothSpeed * Time.deltaTime);
            m_NetLeftHand.transform.position = Vector3.Lerp(m_NetLeftHand.transform.position, m_NetLeftHandPositionSynced, _smoothSpeed * Time.deltaTime);
            m_NetLeftHand.transform.rotation = Quaternion.Lerp(m_NetLeftHand.transform.rotation, Quaternion.Euler(m_NetLeftHandRotationSynced), _smoothSpeed * Time.deltaTime);
            m_BodyTrigger.transform.position = Vector3.Lerp(m_BodyTrigger.transform.position, m_BodyTriggerPositionSynced, _smoothSpeed * Time.deltaTime);

            GameObject VrRightHandTrackedObject = m_VrRightHand.currentAttachedObject;
            if (VrRightHandTrackedObject != null)
            {
                if(VrRightHandTrackedObject.TryGetComponent(out NetworkIdentity netId))
                {
                    CmdSetAuth(netId, GetComponent<NetworkIdentity>());
                }
            }
            GameObject VrLeftHandTrackedObject = m_VrLeftHand.currentAttachedObject;
            if (VrLeftHandTrackedObject != null)
            {
                if (VrLeftHandTrackedObject.TryGetComponent(out NetworkIdentity netId))
                {
                    CmdSetAuth(netId, GetComponent<NetworkIdentity>());
                }
            }
            GameObject NoSteamVRFallbackObjects = m_VrGameObject.transform.Find("NoSteamVRFallbackObjects").gameObject;
            GameObject fallbackHand = NoSteamVRFallbackObjects.transform.Find("FallbackHand").gameObject;
            Hand fallbackHandHand = fallbackHand.GetComponent<Hand>();
            GameObject VrNoHandTrackedObject = fallbackHandHand.currentAttachedObject;
            if (VrNoHandTrackedObject != null)
            {
                if (VrNoHandTrackedObject.TryGetComponent(out NetworkIdentity netId))
                {
                    CmdSetAuth(netId, GetComponent<NetworkIdentity>());
                }
            }

            if (Input.GetKeyDown(KeyCode.Escape) || m_menuAction.stateDown)
            {
                Debug.Log("[Player:Update:Input=Menu]");
                m_playerMenuUI.Toggle();
            }
        }
        // Read data
        else
        {
            // Apply Pos/Rot to other players
            transform.position = Vector3.Lerp(transform.position, m_BodyPositionSynced, _smoothSpeed * Time.deltaTime);
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(m_BodyRotationSynced), _smoothSpeed * Time.deltaTime);
            m_NetHead.transform.position = Vector3.Lerp(m_NetHead.transform.position, m_NetHeadPositionSynced, _smoothSpeed * Time.deltaTime);
            m_NetHead.transform.rotation = Quaternion.Lerp(m_NetHead.transform.rotation, Quaternion.Euler(m_NetHeadRotationSynced), _smoothSpeed * Time.deltaTime);
            m_NetRightHand.transform.position = Vector3.Lerp(m_NetRightHand.transform.position, m_NetRightHandPositionSynced, _smoothSpeed * Time.deltaTime);
            m_NetRightHand.transform.rotation = Quaternion.Lerp(m_NetRightHand.transform.rotation, Quaternion.Euler(m_NetRightHandRotationSynced), _smoothSpeed * Time.deltaTime);
            m_NetLeftHand.transform.position = Vector3.Lerp(m_NetLeftHand.transform.position, m_NetLeftHandPositionSynced, _smoothSpeed * Time.deltaTime);
            m_NetLeftHand.transform.rotation = Quaternion.Lerp(m_NetLeftHand.transform.rotation, Quaternion.Euler(m_NetLeftHandRotationSynced), _smoothSpeed * Time.deltaTime);
            m_BodyTrigger.transform.position = Vector3.Lerp(m_BodyTrigger.transform.position, m_BodyTriggerPositionSynced, _smoothSpeed * Time.deltaTime);
        }
    }

    private void FixedUpdate()
    {
        if (isLocalPlayer)
        {
            //Debug.Log("RightHandPosition = " + m_NetRightHandPosition+ "| RealPosition = "+ m_VrRightHand.transform.position+ "| GOPosition = "+ m_VrGameObject.transform.Find("SteamVRObjects").transform.Find("RightHand").gameObject.transform.position);

            m_BodyPosition = transform.position;
            m_BodyRotation = transform.rotation.eulerAngles;
            m_NetHeadPosition = m_VrHead.transform.position;
            m_NetHeadRotation = m_VrHead.transform.rotation.eulerAngles;
            m_NetRightHandPosition = m_VrRightHand.transform.position;
            m_NetRightHandRotation = m_VrRightHand.transform.rotation.eulerAngles;
            m_NetLeftHandPosition = m_VrLeftHand.transform.position;
            m_NetLeftHandRotation = m_VrLeftHand.transform.rotation.eulerAngles;
            m_BodyTriggerPosition = m_BodyCollider.transform.position;
        }
    }

    public PlayerMenuUI GetPlayerMenuUI()
    {
        return m_playerMenuUI;
    }

    // Run this function on server
    [Command]
    void CmdUpdateStates(Vector3 BodyPosition, Vector3 BodyRotation, Vector3 HeadPosition, Vector3 HeadRotation,
                Vector3 RightHandPosition, Vector3 RightHandRotation, Vector3 LeftHandPosition, Vector3 LeftHandRotation, Vector3 BodyTriggerPosition)
    {
        m_BodyPositionSynced = BodyPosition;
        m_BodyRotationSynced = BodyRotation;
        m_NetHeadPositionSynced = HeadPosition;
        m_NetHeadRotationSynced = HeadRotation;
        m_NetRightHandPositionSynced = RightHandPosition;
        m_NetRightHandRotationSynced = RightHandRotation;
        m_NetLeftHandPositionSynced = LeftHandPosition;
        m_NetLeftHandRotationSynced = LeftHandRotation;
        m_BodyTriggerPositionSynced = BodyTriggerPosition;
    }

    [Command]
    public void CmdSetAuth(NetworkIdentity objectNetId, NetworkIdentity playerNetId)
    {
        NetworkConnection otherOwner = objectNetId.clientAuthorityOwner;
        if (otherOwner == playerNetId.connectionToClient)
        {
            return;
        }
        else
        {
            if (otherOwner != null)
            {
                Debug.Log("[CmdSetAuth] Remove other owner authority");
                objectNetId.RemoveClientAuthority(otherOwner);
            }
            Debug.Log("[CmdSetAuth] Assign client authority");
            objectNetId.AssignClientAuthority(playerNetId.connectionToClient);
        }
    }
}