﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[System.Obsolete]
public abstract class EnigmaManager : NetworkBehaviour
{
    [SerializeField] protected GameManager m_GameManager;
    protected int m_EnigmaId;
    [SerializeField] protected bool m_IsEnigmaStarted = false;
    [SerializeField] protected bool m_IsEnigmaEnded = false;
    [SerializeField] protected List<NetworkIdentity> m_EnigmaObjectsNI; // list of all network objects that have to be checked and validated from the server side
    protected Dictionary<NetworkIdentity, bool> m_EnigmaObjectsNICompletion; // list of all network objects and their completion (true or false)
    [SerializeField] protected bool m_CanHelp = true; // if false, no help will be given to the players
    [SerializeField] protected float m_TimeUntilHelp = 300.00f; // time until next help in seconds
    protected float m_LastestHelpTime; // latest time a help was given

    abstract protected void OnCompleteEnigma(); // in case of success

    abstract protected void OnFailEnigma(); // in case of fail

    abstract protected void SendHelp(); // help the player

    void Update()
    {
        if (IsRunning()) // is the enigma has started and the enigma has not ended
        {
            if (m_CanHelp && IsTimeToHelp())
            {
                m_LastestHelpTime = Time.time;
                SendHelp();
            }
        }
    }

    public int GetEnigmaId()
    {
        return m_EnigmaId;
    }

    virtual public void StartEnigmaManager(int enigmaId)
    {
        Debug.Log("[EnigmaManager:StartEnigmaManager]");
        m_EnigmaId = enigmaId;
        m_IsEnigmaStarted = true;

        if (float.IsNaN(m_TimeUntilHelp)) m_TimeUntilHelp = 300.00f; // if not set, default time until help is 5 minutes
        if (float.IsNaN(m_LastestHelpTime)) m_LastestHelpTime = Time.time; // set latest help time is current time

        m_EnigmaObjectsNICompletion = new Dictionary<NetworkIdentity, bool>();
        m_EnigmaObjectsNI.ForEach(delegate (NetworkIdentity objNI) // loop into the list of objects
        {
            m_EnigmaObjectsNICompletion.Add(objNI, false); // feed the dictionnary from the list of objects and set default completion = false
        });
        //m_EnigmaObjectsNI.Clear(); // unset elements
        //m_EnigmaObjectsNI = null; // unset list
    }

    protected void EndEnigmaManager()
    {
        Debug.Log("[EnigmaManager:EndEnigmaManager]");
        m_IsEnigmaEnded = true;
    }

    private bool IsTimeToHelp()
    {
        float currentTime = Time.time;
        if (currentTime >= m_LastestHelpTime + m_TimeUntilHelp)
            return true;
        return false;
    }

    // return true if the enigma is started and not ended
    public bool IsRunning()
    {
        return m_IsEnigmaStarted && !m_IsEnigmaEnded;
    }

    protected bool IsEnigmaCompleted()
    {
        return !m_EnigmaObjectsNICompletion.ContainsValue(false); // if false is found, the enigma is not complete, so it returns !(true)
    }

    public bool IsEnigmaObject(NetworkIdentity objNI)
    {
        //return m_EnigmaObjectsNICompletion.ContainsKey(objNI); // return true if the object is found
        return m_EnigmaObjectsNI.Contains(objNI);
    }

    public void OnThrowEvent(NetworkIdentity objNI, int eventCase, List<object> args)
    {
        Debug.Log("[EnigmaManager:OnThrowEvent(IsRunning="+IsRunning().ToString()+", object="+objNI.gameObject.name+", eventCase="+eventCase.ToString()+"]");
        if (IsRunning())
        {
            //NetworkObjectEvent noe = NetworkServer.FindLocalObject(objNI.netId).GetComponent<NetworkObjectEvent>();
            //noe.EnigmaReceiveEvent(eventCase, args); // ask the object to throw an event to all clients
            objNI.GetComponent<NetworkObjectEvent>().EnigmaReceiveEvent(eventCase, args); // ask the object to throw an event to all clients
        }
    }

    public void OnUpdateEnigma(NetworkIdentity objNI, bool complete)
    {
        Debug.Log("[EnigmaManager | OnUpdateEnigma] IsRunning().ToString() : "+ IsRunning().ToString());
        Debug.Log("[EnigmaManager | OnUpdateEnigma] objNI.gameObject.name : " + objNI.gameObject.name);
        Debug.Log("[EnigmaManager | OnUpdateEnigma] complete.ToString() : " + complete.ToString());
        Debug.Log("[EnigmaManager | OnUpdateEnigma] isServer: " + isServer);
        if (IsRunning() && IsEnigmaObject(objNI))
        {
            m_EnigmaObjectsNICompletion[objNI] = complete;
            foreach (KeyValuePair<NetworkIdentity, bool> o in m_EnigmaObjectsNICompletion)
            {
                Debug.Log("[Key="+o.Key.gameObject.name+", Value="+o.Value+"]");
            }
            if (objNI.gameObject.TryGetComponent(out NetworkObjectEvent noe))
            {
                noe.EnigmaUpdateEvent(); // the object from the server side will update its states, then it will synchronize on clients side
            }
            if (IsEnigmaCompleted())
            {
                OnCompleteEnigma();
                EndEnigmaManager();
            }
        }
    }
    
    public void SetCanHelp(bool help)
    {
        m_CanHelp = help;
    }
}
