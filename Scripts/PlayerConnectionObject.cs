﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerConnectionObject : NetworkBehaviour
{

    // Use this for initialization
    void Start()
    {
        // Is this actually my own local PlayerConnectionObject?
        if (isLocalPlayer == false)
        {
            // This object belongs to another player.
            return;
        }

        CmdSpawnMyUnit();
    }

    public GameObject PlayerUnitPrefab;

    // Update is called once per frame
    void Update()
    {
        // Remember: Update runs on EVERYONE's computer, whether or not they own this
        // particular player object.

        if (isLocalPlayer == false)
        {
            return;
        }

    }

    //////////////////////////// COMMANDS
    // Commands are special functions that ONLY get executed on the server.

    [Command]
    void CmdSpawnMyUnit()
    {
        // We are guaranteed to be on the server right now.
        GameObject m_gameObject = Instantiate(PlayerUnitPrefab);

        //go.GetComponent<NetworkIdentity>().AssignClientAuthority( connectionToClient );

        // Now that the object exists on the server, propagate it to all
        // the clients (and also wire up the NetworkIdentity)
        NetworkServer.SpawnWithClientAuthority(m_gameObject, connectionToClient);
    }
}
