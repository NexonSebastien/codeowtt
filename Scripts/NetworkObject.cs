﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Valve.VR.InteractionSystem;

[System.Obsolete]
public class NetworkObject : NetworkBehaviour
{
    private Interactable m_Interactable;
    private bool m_IsGrabbed;

    public Vector3 m_NetObjectPosition;
    public Vector3 m_NetObjectRotation;

    [SyncVar] public Vector3 m_NetObjectPositionSynced;
    [SyncVar] public Vector3 m_NetObjectRotationSynced;

    [SyncVar] GameObject GO;

    NetworkIdentity PlayerAuthority
    {
        set
        {

        }
    }

    // Start is called before the first frame update
    void Start()
    {
        m_Interactable = GetComponent<Interactable>();
        m_IsGrabbed = false;
        m_NetObjectPositionSynced = transform.position;
        m_NetObjectRotationSynced = transform.rotation.eulerAngles;

        GO = this.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_Interactable && m_Interactable.attachedToHand)
        {
            m_IsGrabbed = true;
            CmdUpdateStates(m_NetObjectPosition, m_NetObjectRotation);
            //CmdUpdateGO();
        }
        else
        {
            m_IsGrabbed = false;
        }
    }

    void FixedUpdate()
    {
        if (m_IsGrabbed)
        {
            //Debug.Log(gameObject.name + " is grabbed by owner=" + GetComponent<NetworkIdentity>().clientAuthorityOwner);
            //Debug.Log(gameObject.name + " is grabbed by client=" + connectionToClient);
            /*
            GameObject rootGO = gameObject.transform.root.gameObject;
            Debug.Log("rootGO = " + rootGO.name + "and tag = " + rootGO.tag);
            if (rootGO.CompareTag("Player"))
            {
                NetworkIdentity objNetId = GetComponent<NetworkIdentity>();
                //rootGO.GetComponent<m>().CmdSetAuth(netId, objNetId);
                CmdSetAuth(netId, objNetId);
            }
            */
            m_NetObjectPosition = transform.position;
            m_NetObjectRotation = transform.rotation.eulerAngles;

        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, m_NetObjectPositionSynced, Time.deltaTime);
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(m_NetObjectRotationSynced), Time.deltaTime);
        }
        //Debug.Log("ClientAuthorityOwner = " + GetComponent<NetworkIdentity>().clientAuthorityOwner);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Collision with " + collision.gameObject.transform.root.name);
        if (collision.gameObject.transform.root.CompareTag("Player"))
        {
            Debug.Log("Object hit a player");
            Debug.Log("Server give Auth ? " + isServer.ToString());
            CmdSetAuth(GetComponent<NetworkIdentity>().netId, collision.gameObject.transform.root.gameObject.GetComponent<NetworkIdentity>());
        }
    }

    [Command]
    void CmdUpdateStates(Vector3 NetObjectPosition, Vector3 NetObjectRotation)
    {
        NetworkIdentity objNetId = GetComponent<NetworkIdentity>();
        m_NetObjectPositionSynced = NetObjectPosition;
        m_NetObjectRotationSynced = NetObjectRotation;
        objNetId.AssignClientAuthority(connectionToClient);
        RpcUpdateStates(NetObjectPosition, NetObjectRotation);
        objNetId.RemoveClientAuthority(connectionToClient);
    }

    [ClientRpc]
    void RpcUpdateStates(Vector3 NetObjectPosition, Vector3 NetObjectRotation)
    {
        m_NetObjectPositionSynced = NetObjectPosition;
        m_NetObjectRotationSynced = NetObjectRotation;
        transform.position = NetObjectPosition;
        transform.rotation = Quaternion.Euler(NetObjectPosition);
    }

    [Command]
    void CmdUpdateGO()
    {
        NetworkIdentity objNetId = GetComponent<NetworkIdentity>();
        objNetId.AssignClientAuthority(connectionToClient);
        RpcUpdateGO();
        objNetId.RemoveClientAuthority(connectionToClient);
    }

    [ClientRpc]
    void RpcUpdateGO()
    {
        transform.position = GO.transform.position;
        transform.rotation = GO.transform.rotation;
    }

    [Command]
    public void CmdSetAuth(NetworkInstanceId objectId, NetworkIdentity player)
    {
        /*
        Debug.Log("objectId = " + objectId);
        GameObject iObject = NetworkServer.FindLocalObject(objectId);
        Debug.Log("iObject = " + iObject.name);
        NetworkIdentity networkIdentity = iObject.GetComponent<NetworkIdentity>();
        */
        NetworkIdentity networkIdentity = GetComponent<NetworkIdentity>();
        NetworkConnection otherOwner = networkIdentity.clientAuthorityOwner;

        if (otherOwner == player.connectionToClient)
        {
            return;
        }
        else
        {
            if (otherOwner != null)
            {
                networkIdentity.RemoveClientAuthority(otherOwner);
            }
            networkIdentity.AssignClientAuthority(player.connectionToClient);
        }
        RpcSetAuth(objectId, player);
        RpcLogClientAuthorityOwner(networkIdentity.clientAuthorityOwner.ToString());
        //TargetLogClientAuthorityOwner(networkIdentity.clientAuthorityOwner.ToString());

    }

    [ClientRpc]
    void RpcLogClientAuthorityOwner(string owner)
    {
        Debug.Log("owner = " + owner);
    }
    /*
    [TargetRpc]
    void TargetLogClientAuthorityOwner(string owner)
    {
        Debug.Log("owner = " + owner);
    }
    */
    [ClientRpc]
    public void RpcSetAuth(NetworkInstanceId objectId, NetworkIdentity player)
    {
        /*
        Debug.Log("objectId = " + objectId);
        GameObject iObject = NetworkServer.FindLocalObject(objectId);
        Debug.Log("iObject = " + iObject.name);
        NetworkIdentity networkIdentity = iObject.GetComponent<NetworkIdentity>();
        */
        NetworkIdentity networkIdentity = GetComponent<NetworkIdentity>();
        NetworkConnection otherOwner = networkIdentity.clientAuthorityOwner;

        if (otherOwner == player.connectionToClient)
        {
            return;
        }
        else
        {
            if (otherOwner != null)
            {
                networkIdentity.RemoveClientAuthority(otherOwner);
            }
            networkIdentity.AssignClientAuthority(player.connectionToClient);
        }
    }
}
