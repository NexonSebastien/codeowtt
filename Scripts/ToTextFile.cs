﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;
using System.Linq;
using Valve.VR.InteractionSystem;
using UnityEngine.Networking;

[Obsolete]
public class ToTextFile : NetworkBehaviour
{
    GameManager gameManager;
    Interactable Interactable = new Interactable();
    PlayMusicCrystal PlayMusicCrystal = new PlayMusicCrystal();
    NetworkObjectEvent_MusicCrystal NetworkObjectEvent_MusicCrystal = new NetworkObjectEvent_MusicCrystal();
    Longbow Longbow = new Longbow();
    NetworkObjectEvent_Brasero NetworkObjectEvent_Brasero = new NetworkObjectEvent_Brasero();
    EnigmaManager_SecretEnigma EnigmaManager_SecretEnigma = new EnigmaManager_SecretEnigma();
    private List<float> timerResult;
    private List<Boolean> SecretEnigma; // FIX pas encore réupéré les infos
    private bool SecretEnigmaComplet = false;
    private bool PlayersDeadApollon = false;
    private bool PlayersDeadHephaistos = false;
    private float LobbyTime = 0;
    private float PoseidonTime = 0;
    private float AppollonTime = 0;
    private float HephaistosTime = 0;
    private float CapsuleRoomTime = 0;
    private float GlobalTime = 0;
    private int ObjectCatch = 0;
    private int CristalHit = 0;
    private int SoundListen = 0;
    private int ArrowShot = 0;
    private int BrazeroLighted = 0;
    private int PlayerDead = 0;
    private string PlayerEscaped = "Yes"; // FIX pas encore récupéré les infos pour différencier quel joueur s'échappe 
    private string txtDocumentName;

    private void Start()
    {
        timerResult = new List<float>();
        //create the folder
        Directory.CreateDirectory(Application.streamingAssetsPath + "/Metrics/");
        //create the text file at the already created directory in the start function
        txtDocumentName = Application.streamingAssetsPath + "/Metrics/" + "Metricstest" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
    }


    [ClientRpc]
    public void RpcCreateOrUpdateTextFile()
    {
        //Add métrics to the text files "TIMER" in seconds
        File.AppendAllText(txtDocumentName, "GlobalTime;" + GlobalTime + "\n");
        File.AppendAllText(txtDocumentName, "LobbyTime;" + LobbyTime + "\n");
        File.AppendAllText(txtDocumentName, "PoseidonRoomTime;" + PoseidonTime + "\n");
        if (PlayersDeadApollon == false)
        {
            File.AppendAllText(txtDocumentName, "ApollonRoomTime;" + AppollonTime + "\n");
            if (PlayersDeadHephaistos == false)
            {
                File.AppendAllText(txtDocumentName, "HephaistosTime;" + HephaistosTime + "\n");
                File.AppendAllText(txtDocumentName, "CapsuleRoomTime;" + CapsuleRoomTime + "\n");
            }
        }

        //Add métrics to the text files "INTERACTION"
        //        File.AppendAllText(txtDocumentName, "NbrObjCatch;" + ObjectCatch + "\n");
        //        if (PlayersDeadApollon == true)
        //        {
        //            File.AppendAllText(txtDocumentName, "NbrCristalHit;" + 0 + "\n");
        //            File.AppendAllText(txtDocumentName, "NbrSoundListen;" + 0 + "\n");
        //        }
        //        else
        //        {
        //            File.AppendAllText(txtDocumentName, "NbrCristalHit;" + CristalHit + "\n");
        //            File.AppendAllText(txtDocumentName, "NbrSoundListen;" + SoundListen + "\n");
        //        }
        //        if (PlayersDeadHephaistos == true || PlayersDeadApollon == true)
        //        {
        //            File.AppendAllText(txtDocumentName, "NbrArrowShot;" + 0 + "\n");
        //            File.AppendAllText(txtDocumentName, "NbrBrazeroLighted;" + 0 + "\n");
        //        }
        //        else
        //        {
        //            File.AppendAllText(txtDocumentName, "NbrArrowShot;" + ArrowShot + "\n");
        //            File.AppendAllText(txtDocumentName, "NbrBrazeroLighted;" + BrazeroLighted + "\n");
        //        }


        //Add métrics to the text files "SECRET ENIGMA"                                         // FIX pas encore réupéré les infos
        //        File.AppendAllText(txtDocumentName, "SecretEnigma1;" + SecretEnigma[0] + "\n");
        //        File.AppendAllText(txtDocumentName, "SecretEnigma2;" + SecretEnigma[1] + "\n");
        if (PlayersDeadApollon == false || PlayersDeadHephaistos == false)
        {
            //            File.AppendAllText(txtDocumentName, "SecretEnigma3;" + SecretEnigma[2] + "\n");                        
            //            File.AppendAllText(txtDocumentName, "SecretEnigmaSucces;" + SecretEnigmaComplet + "\n");
        }
        else
        {
            //            File.AppendAllText(txtDocumentName, "SecretEnigma3;" + "false" + "\n");
            File.AppendAllText(txtDocumentName, "SecretEnigmaSucces;" + "False" + "\n");
        }


        //Add métrics to the text files "PLAYER"
        if (PlayersDeadHephaistos == true || PlayersDeadApollon == true)
        {
            File.AppendAllText(txtDocumentName, "PlayersDied;" + "Yes" + "\n");
            File.AppendAllText(txtDocumentName, "PlayersEscape;" + "No" + "\n");
        }
        else
        {
            File.AppendAllText(txtDocumentName, "PlayersDied;" + "No" + "\n");
            File.AppendAllText(txtDocumentName, "PlayersEscape;" + PlayerEscaped + "\n"); //player1? player2? both? //FIX
        }
        if (PlayersDeadApollon == true)
        {
            File.AppendAllText(txtDocumentName, "RoomDied;" + "Apollon" + "\n");
        }
        if (PlayersDeadHephaistos == true)
        {
            File.AppendAllText(txtDocumentName, "RoomDied;" + "Hephaistos" + "\n");
        }

    }

    //-------------------------------------------------------------------------------------------------------------------------------------//

    public void RoomDead(int idRoom)
    {
        PlayerDead = idRoom;
        if (PlayerDead == 2)
        {
            PlayersDeadApollon = true;
        }
        else if (PlayerDead == 3)
        {
            PlayersDeadHephaistos = true;
        }
        Debug.Log("Metrics Player Dead");
    }

    public void TimerCalculation(List<float> timerList)
    {
        timerResult = timerList;
        LobbyTime = timerResult[1];
        PoseidonTime = timerResult[2] - timerResult[1];
        AppollonTime = timerResult[3] - timerResult[2];
        HephaistosTime = timerResult[4] - timerResult[3];
        CapsuleRoomTime = timerResult[5] - timerResult[4];
        GlobalTime = LobbyTime + PoseidonTime + AppollonTime + HephaistosTime + CapsuleRoomTime;
        Debug.Log("Metrics Timer");
        Debug.Log("PoseidonTime" + PoseidonTime);
        Debug.Log("AppollonTime" + AppollonTime);
        Debug.Log("HephaistosTime" + HephaistosTime);
        Debug.Log("CapsuleRoomTime" + CapsuleRoomTime);
        Debug.Log("GlobalTime" + GlobalTime);
    }

    //    public void ObjectInteraction()
    //    {
    //        ObjectCatch = Interactable.GetNbrObjectCatch();
    //        ArrowShot = Longbow.GetNbrArrowShot();
    //        BrazeroLighted = NetworkObjectEvent_Brasero.GetNbrBrazeroEmbrase();
    //        Debug.Log("ObjectCatch" + ObjectCatch);
    //        Debug.Log("CristalHit" + CristalHit);
    //        Debug.Log("SoundListen" + SoundListen);
    //        Debug.Log("ArrowShot" + ArrowShot);
    //        Debug.Log("BrazeroLighted" + BrazeroLighted);
    //    }

    //    public void GetSecretEnigmaComplete()
    //    {
    //        SecretEnigmaComplet = GetComponent<EnigmaManager_SecretEnigma>().GetSecretEnigmaComplete();
    //        Debug.Log("SecretEnigmaComplet : " + SecretEnigmaComplet);
    //    }

    //    public void GetNbrBrazeroEmbraseFile()
    //    {
    //        BrazeroLighted = GetComponent<NetworkObjectEvent_Brasero>().GetNbrBrazeroEmbrase();
    //        Debug.Log("Nbr Brasero Embrase File = " + BrazeroLighted);
    //    }


    [ServerCallback]
    public void Write(float[] timers, int idRoomDead)
    {
        RpcWrite(timers, idRoomDead);
    }

    [ClientRpc]
    private void RpcWrite(float[] timers, int idRoomDead)
    {
        TimerCalculation(timers.ToList());
        RoomDead(idRoomDead);
    }

    private void WriteData(string key, string value)
    {
        File.AppendAllText(txtDocumentName, "SecretEnigmaSucces;" + "False" + "\n");
    }
}
