﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Valve.VR.InteractionSystem;

[System.Obsolete]
public class CloseDoorWhenEnter : NetworkBehaviour
{
    public GameObject m_door;
    private bool m_instanceIsPlaying = false;
    [SyncVar] bool m_wantToStop = false;
    private FMOD.Studio.EventInstance m_instanceDialogueHelp;
    [SerializeField] private string m_eventPlayOneShot = "";
    [SerializeField] private string m_eventInstance = "";
    [SerializeField] private bool m_waitTwoPlayer = true;
    private List<GameObject> m_players;
    [SerializeField] private Network_AudioController m_netAudioController;
    [SerializeField] TeleportArea m_TeleportArea = null;
    private FMOD.Studio.PLAYBACK_STATE m_stateDialogueCommun2;
    private FMOD.Studio.EventInstance m_instanceDialogueCommun2;

    private FMOD.Studio.PLAYBACK_STATE m_stateDialogueEndHephaistos;
    private FMOD.Studio.EventInstance m_instanceDialogueEndHephaistos;


    //public string m_tagNameOfTeleportArea;
    // Start is called before the first frame update
    private void Awake()
    {
        m_players = new List<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        if (this.gameObject.name == "TriggerCapsuleRoomEnter")
        {
            if (isServer)
            {
                if (m_netAudioController.GetIsFinishHephaistosRoom() && m_TeleportArea.locked)
                {
                    RpcUnlockTp();
                }
            }
        }
        
        if(m_instanceIsPlaying==true && m_wantToStop)
        {
            m_instanceDialogueHelp.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            m_instanceIsPlaying = false;
        }
    }
    [ClientRpc]
    private void RpcUnlockTp()
    {
        m_TeleportArea.locked = false;
        m_TeleportArea.markerActive = true;
    }

    [ServerCallback]
    private void OnTriggerEnter(Collider other)
    {
        if(m_waitTwoPlayer)
        {
            if(this.gameObject.name == "TriggerCapsuleRoomEnter")
            {
                if (other.gameObject.name == "BodyTrigger" && !m_door.active && m_netAudioController.GetIsFinishHephaistosRoom())
                {
                    Debug.Log("Nbr de player dans la salle: " + m_players.Count);
                    AddPlayer(other.transform.parent.gameObject);
                    if (m_players.Count == 2)
                    {
                        RpcCloseDoorTriggerCapsuleRoomEnter();
                    }
                }
            }
            else
            {
                if (other.gameObject.name == "BodyTrigger" && !m_door.active)
                {
                    Debug.Log("Nbr de player dans la salle: " + m_players.Count);
                    AddPlayer(other.transform.parent.gameObject);
                    if (m_players.Count == 2)
                    {
                        RpcCloseDoor();
                    }
                }
            }
        }
        else
        {
            RpcCloseDoor();
        }
        
    }
    
    [ClientRpc]
    public void RpcCloseDoor()
    {
        Debug.Log("CLOSEDOOR");
        m_door.gameObject.SetActive(true);

        if(m_eventPlayOneShot != "")
        {
            Debug.Log("Play One Shot : " + m_eventPlayOneShot);
            FMODUnity.RuntimeManager.PlayOneShot(m_eventPlayOneShot);
        }

        if (m_eventInstance != "")
        {
            Debug.Log("Play Instance : " + m_eventInstance);
            m_instanceDialogueHelp = FMODUnity.RuntimeManager.CreateInstance(m_eventInstance);
            m_instanceDialogueHelp.start();
            m_instanceIsPlaying = true;
       
        
        } 
    }

    [ClientRpc]
    public void RpcCloseDoorTriggerCapsuleRoomEnter()
    {
        Debug.Log("RpcCloseDoorTriggerCapsuleRoomEnter");
        m_door.gameObject.SetActive(true);

        m_instanceDialogueEndHephaistos = FMODUnity.RuntimeManager.CreateInstance(m_eventPlayOneShot);
        m_instanceDialogueEndHephaistos.start();
        if(isServer)
        {
            StartCoroutine(WaitDialogueFinish());
        }
    }

    private IEnumerator WaitDialogueFinish()
    {
        while (m_stateDialogueEndHephaistos != FMOD.Studio.PLAYBACK_STATE.STOPPED)
        {
            m_instanceDialogueEndHephaistos.getPlaybackState(out m_stateDialogueEndHephaistos);
            yield return new WaitForSeconds(1);
        }
        m_netAudioController.SetIsFinishEnterCapsuleRoom(true);
    }
    private void AddPlayer(GameObject player)
    {
        if (!m_players.Contains(player))
        {
            Debug.Log("Player Add : " + player.name);
            m_players.Add(player);
        }
    }

    [ServerCallback]
    public void SetWantToStop(bool status)
    {
        m_wantToStop = status;
    }
}
