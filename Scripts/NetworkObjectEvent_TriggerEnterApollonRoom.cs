﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Valve.VR.InteractionSystem;

[System.Obsolete]
public class NetworkObjectEvent_TriggerEnterApollonRoom : NetworkObjectEvent
{
    [SerializeField] public GameObject m_Shield;
    [SerializeField] public GameObject m_OtherShield;
    [SerializeField] public WaterManagement m_Water;
    [SerializeField] [SyncVar] private bool m_activated;
    [SerializeField] private bool m_waterSide;
    [SerializeField] public TeleportArea m_ApollonTp1;
    [SerializeField] public TeleportArea m_ApollonTp2;
    [SerializeField] public TeleportPoint m_ApollonTpp1;

    [SerializeField] private Network_AudioController m_netAudioController;

    override protected void Start()
    {
        base.Start();
        if (isServer)
        {
            m_activated = false;
        }
    }
    [ServerCallback]
    private void Update()
    {
        if (m_netAudioController.GetIsFinishPoseidonRoom() && m_ApollonTp1.locked == true)
        {
            RpcActiveTpApollon();
        }
    }

    [ClientRpc]
    private void RpcActiveTpApollon()
    {
        m_ApollonTp1.locked = false;
        m_ApollonTp2.locked = false;
        m_ApollonTpp1.locked = false;
        m_ApollonTp1.markerActive = true;
        m_ApollonTp2.markerActive = true;
    }

    [ServerCallback]
    public override void EnigmaReceiveEvent(int eventCase, List<object> args)
    {
        throw new System.NotImplementedException();
    }

    [ServerCallback]
    public override void EnigmaUpdateEvent()
    {
        throw new System.NotImplementedException();
    }

    [ServerCallback]
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "BodyTrigger" && m_EnigmaManager.IsRunning() && !m_activated && m_netAudioController.GetIsFinishPoseidonRoom())
        {
            m_activated = true;
            Debug.Log("[NetworkObjectEvent_TriggerEnterApollonRoom:OnTriggerEnter:ThrowEvent(m_Shield)]");
            //ThrowEvent(m_Shield.GetComponent<NetworkIdentity>(), 1, new List<object> { true }); // toggle on shield
            m_Shield.GetComponent<NetworkObjectEvent_Shield>().RpcToggleShieldActive(true);
            if (m_OtherShield.transform.Find("DoorShield").gameObject.active)
            {
                Debug.Log("[NetworkObjectEvent_TriggerEnterApollonRoom:OnTriggerEnter:ThrowEvent(m_Water)]");
                ThrowEvent(m_Water.GetComponent<NetworkIdentity>(), 1, null); // start water managment
                if (m_waterSide)
                {
                    m_Water.gameObject.GetComponent<WaterManagement>().SetPlayer(other.GetComponentInParent<NetworkPlayer>());
                }
            }
        }
    }

}
