﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Valve.VR;

public class CatchObjet : NetworkBehaviour
{
    private bool m_isTake = false;
    [SyncVar] private GameObject m_gameObjName;
    private NetworkIdentity objNetId;
    public int m_force = 1500;
    public int m_forceDrop = 50;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //if (!isLocalPlayer) return;
        if (Input.GetKeyDown(KeyCode.F) && m_isTake & m_gameObjName != null)
        {
            CmdTake();
        }

        if(Input.GetButtonDown("Fire1")&& m_gameObjName != null)
        {
            //CmdLance();
            CmdDrop();
        }

        /*if (Input.GetKeyDown(KeyCode.F) && GoName != null & GoName.gameObject.GetComponent<Rigidbody>().isKinematic == true)
        {
            CmdPose();
        }*/
    }
    [Command]
    void CmdTake()
    {
        objNetId = m_gameObjName.GetComponent<NetworkIdentity>();
        objNetId.AssignClientAuthority(connectionToClient);
        RpcTake();
        objNetId.RemoveClientAuthority(connectionToClient);
    }

    [Command]
    void CmdThrow()
    {
        objNetId = m_gameObjName.GetComponent<NetworkIdentity>();
        objNetId.AssignClientAuthority(connectionToClient);
        RpcThrow();
        objNetId.RemoveClientAuthority(connectionToClient);
    }

    [Command]
    void CmdDrop()
    {
        objNetId = m_gameObjName.GetComponent<NetworkIdentity>();
        objNetId.AssignClientAuthority(connectionToClient);
        RpcDrop();
        objNetId.RemoveClientAuthority(connectionToClient);
    }

    [ClientRpc]
    void RpcTake()
    {
        m_gameObjName.gameObject.GetComponent<Rigidbody>().isKinematic = true;
        m_gameObjName.transform.position = this.gameObject.transform.GetChild(0).gameObject.transform.GetChild(0).transform.position;
        m_gameObjName.gameObject.transform.parent = this.gameObject.transform.GetChild(0).gameObject.transform.GetChild(0).transform;
    }

    [ClientRpc]
    void RpcThrow()
    {
        m_gameObjName.gameObject.transform.parent = null;
        m_gameObjName.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        m_gameObjName.GetComponent<Rigidbody>().AddForce(transform.TransformDirection(Vector3.forward * m_force));
    }

    [ClientRpc]
    void RpcDrop()
    {
        m_gameObjName.gameObject.transform.parent = null;
        m_gameObjName.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        m_gameObjName.GetComponent<Rigidbody>().AddForce(transform.TransformDirection(Vector3.forward * m_forceDrop));
    }

    void OnTriggerEnter(Collider Col)
    {
        if(Col.gameObject.tag == "Catch" || Col.gameObject.tag == "defPosition" || Col.gameObject.tag == "incorectPosition" || Col.gameObject.tag == "amphoras")
        {
            print("In the trigger");
            m_gameObjName = Col.gameObject;
            m_isTake = true;
        }
    }

    void OnTriggerExit(Collider Col)
    {
        if (Col.gameObject.tag == "Catch" || Col.gameObject.tag == "defPosition" || Col.gameObject.tag == "incorectPosition" || Col.gameObject.tag == "amphoras")
        {
            print("Out of the trigger");
            m_isTake = false;
        }
    }
}
