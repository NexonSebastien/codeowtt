﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using Random = UnityEngine.Random;

[System.Obsolete]
public class NetworkObjectEvent_SecretEnigma : NetworkObjectEvent
{
    private List<NetworkObjectEvent_SecretEnigmaItem> m_secretEnigmaItems;
    private bool m_validated;
    [SerializeField] private List<Texture> m_symbolsTextures;
    private int m_symbolIndex;
    [SerializeField] private GameObject m_cristal;
    [SerializeField] private Material m_cristalValidate;
    [SerializeField] private Material m_cristalNonValidate;

    override protected void Start()
    {
        base.Start();
        m_validated = false;
        m_secretEnigmaItems = null;
        if (isServer)
        {
            StartCoroutine(InitSymbols());
        }
    }
    
    private void Update()
    {
        if (m_EnigmaManager.IsRunning() && IsSecretEnigmaItemsReady())
        {
            if (!m_validated && IsSecretEnigmaItemsValidated())
            {
                m_validated = true;
                UpdateSymbolMaterial();
                UpdateEnigma(m_NetId, m_validated);
                RpcUpdateCristalFin(m_validated);
            }
            else if (m_validated && !IsSecretEnigmaItemsValidated())
            {
                m_validated = false;
                UpdateSymbolMaterial();
                UpdateEnigma(m_NetId, m_validated);
                RpcUpdateCristalFin(m_validated);
            }
        }
    }

    [ClientRpc]
    private void RpcUpdateCristalFin(bool m_validated)
    {
        if(m_validated)
        {
            m_cristal.GetComponent<Renderer>().material = m_cristalValidate;
        }
        else
        {
            m_cristal.GetComponent<Renderer>().material = m_cristalNonValidate;
        }
    }

    public int GetSymbolIndex()
    {
        return m_symbolIndex;
    }

    public List<Texture> GetSymbolsTextures()
    {
        return m_symbolsTextures;
    }

    [ServerCallback]
    private IEnumerator InitSymbols()
    {
        yield return new WaitForEndOfFrame();
        Debug.Log("[NetworkObjectEvent_SecretEnigmaItem:InitSymbols]");
        m_secretEnigmaItems = new List<NetworkObjectEvent_SecretEnigmaItem>(GetComponentsInChildren<NetworkObjectEvent_SecretEnigmaItem>());
        System.Random rand = new System.Random();
        m_symbolIndex = rand.Next(0, m_symbolsTextures.Count);
        //Debug.Log("[NetworkObjectEvent_SecretEnigmaItem:InitSymbols:m_symbolIndex=" + m_symbolIndex + ":texture.name=" + m_symbolsTextures[m_symbolIndex].name + "]");
        Dictionary<int, int> symbolsSelectedTimes = new Dictionary<int, int>(); // symbols selected times to prevent multiple solutions
        for(int i=0; i< m_symbolsTextures.Count; i++)
        {
            symbolsSelectedTimes.Add(i, 0);
        }
        for (int items=0; items<m_secretEnigmaItems.Count; items++)
        {
            m_secretEnigmaItems[items].SetSymbolsTextures(m_symbolsTextures);
            List<int> symbolsId = new List<int>();
            for (int i = 0; i < m_symbolsTextures.Count; i++)
            {
                symbolsId.Add(i);
            }
            symbolsId = symbolsId.OrderBy(item => Guid.NewGuid()).ToList();
            List<int> shuffleIndex = new List<int>();
            //List<Texture> symbolsTextures = new List<Texture>();
            int nbSymbols = m_secretEnigmaItems[items].GetNbSymbols();
            for (int i = 0; i < nbSymbols; i++)
            {
                int symbol;
                int j = nbSymbols;
                do
                {
                    symbol = symbolsSelectedTimes[symbolsId[i]] == m_secretEnigmaItems.Count - 1 ? symbolsId[j] : symbolsId[i];
                    //symbol = m_symbolIndex == symbolsId[i] && nbSymbols < m_symbolsTextures.Count && symbolsSelectedTimes[symbolsId[i]] < m_secretEnigmaItems.Count - 1 ? symbolsId[j] : symbolsId[i];
                    
                    //if (!symbolsSelectedTimes.ContainsKey(symbol)) symbolsSelectedTimes[symbol] = 0;
                    //Debug.Log("[NetworkObjectEvent_SecretEnigmaItem:InitSymbols:symbol=" + symbol + "]");
                    //yield return new WaitForSeconds(1);
                    /*
                    string dict = "";
                    for(int d=0; d< symbolsSelectedTimes.Count; d++)
                    {
                        KeyValuePair<int, int> kvp = symbolsSelectedTimes.ElementAt(d);
                        dict += "{" + kvp.Key + "," + kvp.Value + "}";
                    }
                    Debug.Log("[SE] item=" + items + "; symbolSoluce=" + m_symbolIndex + "; symbolTest=" + i + "/" + nbSymbols + " ; index=" + j + "/" + m_symbolsTextures.Count + "; key=" + symbol + "; key[j]=" + symbolsId[j] + "; Dict <key,value> " + dict);
                    */
                    j++;
                }
                while (symbol != m_symbolIndex && j < m_symbolsTextures.Count && symbolsSelectedTimes[symbol] == m_secretEnigmaItems.Count - 1); // avoid multiple solutions
                symbolsSelectedTimes[symbol] += 1;
                //Debug.Log("[SE] item=" + items + "; symbolSoluce=" + m_symbolIndex + "; symbolTest=" + i + "; key=" + symbol + "; Dict<key,value> {" + symbol + "," + symbolsSelectedTimes[symbol] + "}");
                shuffleIndex.Add(symbol);
                //symbolsTextures.Add(m_symbolsTextures[symbol]);
            }
            /*
            if(nbSymbols < m_symbolsTextures.Count)
            {
                int symbolIndex = rand.Next(0, nbSymbols);
                shuffleIndex[symbolIndex] = m_symbolIndex;
                //symbolsTextures[symbolIndex] = m_symbolsTextures[m_symbolIndex];
            }
            */
            if (!shuffleIndex.Contains(m_symbolIndex))
            {
                shuffleIndex[rand.Next(0, nbSymbols)] = m_symbolIndex;
            }
            //m_secretEnigmaItems[items].SetSymbolsTextures(symbolsTextures);
            string debug = "[NetworkObjectEvent_SecretEnigma(" + name + "):SyncSymbols:m_symbolIndex=" + m_symbolIndex + ":m_shuffleIndex[]=[";
            for (int i = 0; i < shuffleIndex.Count; i++)
            {
                debug += shuffleIndex[i] + ",";
            }
            Debug.Log(debug + "]]");
            m_secretEnigmaItems[items].SetShuffleIndex(shuffleIndex, m_symbolIndex);
            /*
            string dict2 = "";
            for (int d = 0; d < symbolsSelectedTimes.Count; d++)
            {
                KeyValuePair<int, int> kvp = symbolsSelectedTimes.ElementAt(d);
                dict2 += "{" + kvp.Key + "," + kvp.Value + "}";
            }
            Debug.Log("[SE] item=" + items + "; symbolSoluce=" + m_symbolIndex + "; Dict2<key,value> " + dict2);
            */
        }
    }

    private bool IsSecretEnigmaItemsReady()
    {
        if (m_secretEnigmaItems == null) return false;
        int secretEnigmaItemsCount = m_secretEnigmaItems.Count;
        if (secretEnigmaItemsCount == 0) return false;
        bool status = true;
        int index = 0;
        while (status && index < secretEnigmaItemsCount)
        {
            status = m_secretEnigmaItems[index].IsReady();
            index++;
        }
        return status;
    }

    private bool IsSecretEnigmaItemsValidated()
    {
        if (m_secretEnigmaItems == null) return false;
        int secretEnigmaItemsCount = m_secretEnigmaItems.Count;
        if (secretEnigmaItemsCount == 0) return false;
        bool status = true;
        int index = 0;
        while(status && index < secretEnigmaItemsCount)
        {
            status = m_secretEnigmaItems[index].IsValidated();
            index++;
        }
        return status;
    }

    private void UpdateSymbolMaterial()
    {
        foreach (NetworkObjectEvent_SecretEnigmaItem item in m_secretEnigmaItems)
        {
            ThrowEvent(item.GetComponent<NetworkIdentity>(), 3, new List<object>() { m_validated });
        }
    }

    private void StopInteractability()
    {
        foreach (NetworkObjectEvent_SecretEnigmaItem item in GetComponentsInChildren<NetworkObjectEvent_SecretEnigmaItem>())
        {
            item.EnigmaReceiveEvent(4, null);
        }
    }

    public override void EnigmaReceiveEvent(int eventCase, List<object> args)
    {
        if (isServer)
        {
            switch (eventCase)
            {
                case 1:
                    RpcStopInteractability();
                    break;
            }
        }
    }

    public override void EnigmaUpdateEvent()
    {
        
    }

    [ClientRpc]
    public void RpcStopInteractability()
    {
        StopInteractability();
    }

    public bool GetValidated()
    {
        return m_validated;
    }
}
