﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GameNetworkManager : NetworkManager
{
    const int maxPlayers = 4;
    int players = 0;

    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {
        if(players < maxPlayers)
        {
            var playerObj = (GameObject)GameObject.Instantiate(playerPrefab, Vector3.zero, Quaternion.identity);
            Debug.Log("Adding player");
            NetworkServer.AddPlayerForConnection(conn, playerObj, playerControllerId);
            players++;
            return;
        }

        //TODO: graceful  disconnect
        conn.Disconnect();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
