﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[System.Obsolete]
public class NetworkObjectDetection : MonoBehaviour
{
    /*
    private void OnTriggerEnter(Collider other)
    {
        if(other.TryGetComponent(out NetworkIdentity ni))
        {
            if(other.TryGetComponent(out NetworkObjectEvent noe) && ni.hasAuthority)
            {
                Debug.Log("[NetworkObjectDetection:OnTriggerEnter:object=" + other.name + "]");
                noe.SetIsOutOfMap();
            }
        }
    }
    */

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.TryGetComponent(out NetworkIdentity ni))
        {
            if (collision.gameObject.TryGetComponent(out NetworkObjectEvent noe) && ni.hasAuthority)
            {
                Debug.Log("[NetworkObjectDetection:OnCollisionEnter:object=" + collision.gameObject.name + "]");
                noe.SetIsOutOfMap();
            }
            else if (collision.gameObject.TryGetComponent(out NetworkObject_OutOfMap oom) && ni.hasAuthority)
            {
                Debug.Log("[NetworkObjectDetection:OnCollisionEnter:object=" + collision.gameObject.name + "]");
                oom.SetIsOutOfMap();
            }
        }
    }
}
