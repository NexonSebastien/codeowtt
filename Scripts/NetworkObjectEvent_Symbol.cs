﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[System.Obsolete]
public class NetworkObjectEvent_Symbol : NetworkObjectEvent
{
    [SerializeField] private int m_id;
    private Material m_material;
    //[SerializeField] Color m_defaultColor; // black rgba=(0,0,0,0)
    //[SerializeField] Color m_illuminationColor; // orange rgba=(255,136,0,255) or hdr=(255,63,0,0)
    [SerializeField] private List<Texture> m_symbolsTextures;
    [SerializeField] private Texture m_symbolsDefaultTexture;

    override protected void Start()
    {
        base.Start();
        m_material = GetComponent<Renderer>().sharedMaterial;
        //m_defaultColor = Color.black;
        //m_illuminationColor = new Color(255, 136, 0, 255);
        //m_material.DisableKeyword("_EMISSION");
    }

    public int GetId()
    {
        return m_id;
    }

    public void SetId(int id)
    {
        m_id = id;
    }

    public int GetTexturesCount()
    {
        return m_symbolsTextures.Count;
    }

    public void SetEnigmaManager(EnigmaManager e)
    {
        m_EnigmaManager = e;
    }
    /*
    public void SetTexture(Texture texture)
    {
        if (isServer)
        {
            RpcSetTexture(texture);
        }
    }
    */
    public override void EnigmaReceiveEvent(int eventCase, List<object> args)
    {
        if (isServer)
        {
            Debug.Log("[NetworkObjectEvent_Symbol:EnigmaReceiveEvent:case=" + eventCase + "]");
            switch (eventCase)
            {
                case 1:
                    // illuminate on
                    RpcIlluminateOn();
                    break;
                case 2:
                    // illuminate off
                    RpcIlluminateOff();
                    break;
                case 3:
                    // set texture
                    RpcSetTextureById((int)args[0]-1);
                    break;
            }
        }
    }

    public override void EnigmaUpdateEvent()
    {
        throw new System.NotImplementedException();
    }

    [ClientRpc]
    public void RpcIlluminateOn()
    {
        // turn on the symbol illumination
        Debug.Log("[NetworkObjectEvent_Symbol:RpcIlluminateOn]");
        m_material.EnableKeyword("_EMISSION");
    }

    [ClientRpc]
    public void RpcIlluminateOff()
    {
        // turn off the symbol illumination
        Debug.Log("[NetworkObjectEvent_Symbol:RpcIlluminateOff]");
        m_material.DisableKeyword("_EMISSION");
    }

    /*
    [ClientRpc]
    public void RpcSetTexture(Texture texture)
    {
        // set a new texture to the material
        Debug.Log("[NetworkObjectEvent_Symbol:RpcSetTexture]");
        m_material.SetTexture("_EmissionMap", texture);
    }
    */

    [ClientRpc]
    public void RpcSetTextureById(int textureId)
    {
        // set a new texture to the material
        Debug.Log("[NetworkObjectEvent_Symbol:RpcSetTextureById:textureId="+textureId+"]");
        Texture texture;
        if(textureId < 0 || textureId > m_symbolsTextures.Count)
        {
            texture = m_symbolsDefaultTexture;
        }
        else
        {
            texture = m_symbolsTextures[textureId];
        }
        m_material.SetTexture("_EmissionMap", texture);
    }
}
