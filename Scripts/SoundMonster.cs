﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using UnityEngine.Networking;
using Random = UnityEngine.Random;

[System.Obsolete]
public class SoundMonster : NetworkBehaviour
{
    private FMOD.Studio.EventInstance m_instance;
    private FMOD.Studio.EventInstance m_instanceMove;

    /*
    private int i = 0;
    private float j = 0;
    private bool verifRandom = false;
    */

    private int m_soundIndexMin = 1;
    private int m_soundIndexMax = 6;
    private int m_intervalSecondsMin = 100;
    private int m_intervalSecondsMax = 500;
    private bool m_isTimeElapsed = true;
    public bool m_canPlaySound = false;
    string Event = "event:/Monstre/Cri3D";
    string EventMove = "event:/Monstre/MonstreDeplace";

    // Start is called before the first frame update
    void Start()
    {
        m_instance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(this.gameObject));
        m_instanceMove = FMODUnity.RuntimeManager.CreateInstance(EventMove);
        m_instanceMove.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(this.gameObject));
        m_instanceMove.start();
    }

    [ServerCallback]
    void Update()
    {
        //RpcCriSon();
        if (m_canPlaySound && m_isTimeElapsed)
        {
            m_isTimeElapsed = false;
            StartCoroutine(PlaySound());
        }
        RpcSoundMoveMonster();
    }

    [ClientRpc]
    private void RpcSoundMoveMonster()
    {
        m_instanceMove.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(this.gameObject));
    }

    [ServerCallback]
    private IEnumerator PlaySound()
    {
        int soundIndex = Random.Range(m_soundIndexMin, m_soundIndexMax + 1);
        int intervalSeconds = Random.Range(m_intervalSecondsMin, m_intervalSecondsMax);
        RpcPlaySound(soundIndex);
        yield return new WaitForSeconds(intervalSeconds);
        m_isTimeElapsed = true;
        yield return null;
    }

    [ClientRpc]
    private void RpcPlaySound(int soundIndex)
    {
        m_instance = FMODUnity.RuntimeManager.CreateInstance(Event + soundIndex.ToString());
        m_instance.start();
        m_instance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(this.gameObject));
    }

    /*
    [ClientRpc]
    private void RpcCriSon()
    {
        //Debug.Log("[TestYann] Verif " + verifRandom);
        if (!verifRandom) 
        {
            string Event = "event:/Monstre/Cri3D";
            i = Random.Range(1, 7);
            j = Random.Range(100, 500);
            string suite = i.ToString();
            m_instance = FMODUnity.RuntimeManager.CreateInstance(Event+suite);
            verifRandom = true;
        }
        //Debug.Log("[TestYann] verif | J | I -- " + verifRandom + " | " + j + " | " + i );
        j -= Time.deltaTime;
        if(j <= 0)
        {
           // Debug.Log("[TestYann] Son Joué");
            m_instance.start();
            verifRandom = false;
        }
        m_instance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(this.gameObject));
    }
    */
}
