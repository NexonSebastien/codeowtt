﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[RequireComponent(typeof(PostProcessVolume))]
public class PostProcessVolumeController : MonoBehaviour
{
    private PostProcessVolume _ppv;

    private void Awake()
    {
        _ppv = GetComponent<PostProcessVolume>();
        SetGlobal(false);
        SetWeight(0);
        SetVignetteIntensity(0);
        ActiveColorGradding(false);
    }

    public void SetGlobal(bool status)
    {
        _ppv.isGlobal = status;
    }

    public void SetWeight(float value)
    {
        _ppv.weight = Mathf.Clamp(value, 0, 1);
    }

    public void SetVignetteIntensity(float value)
    {
        //Debug.Log("[PostProcessVolumeController:SetVignetteIntensity:value=" + value + "]");
        if(_ppv.sharedProfile.TryGetSettings(out Vignette vignette))
        {
            vignette.intensity.value = Mathf.Clamp(value, 0, 1);
        }
    }

    public void ActiveColorGradding(bool active)
    {
        if (_ppv.sharedProfile.TryGetSettings(out ColorGrading colorGrading))
        {
            colorGrading.active = active;
        }
    }
}
