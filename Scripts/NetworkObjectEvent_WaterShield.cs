﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[System.Obsolete]
public class NetworkObjectEvent_WaterShield : NetworkBehaviour
{
    [SerializeField] WaterManagement m_Water;
    [SerializeField] public GameObject m_Door;
    [SerializeField] public GameObject m_OtherDoor;
    private bool isStart = false;

    private void Awake()
    {
    }

    [ServerCallback]
    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.name == "BodyTrigger")
        {
            NetworkIdentity doorNetId = m_Door.GetComponent<NetworkIdentity>();
            if (m_OtherDoor.active && !isStart)
            {
                CmdStartWaterManagment();
                isStart = true;
            }
            RpcTurnOnShield(doorNetId);
            Debug.Log("[NetworkObjectEvent_WaterShield:DisableServerTrigger]");
            gameObject.SetActive(false);
        }
    }

    [Command]
    public void CmdTurnOnShield()
    {
        Debug.Log("[NetworkObjectEvent_WaterShield:CmdTurnOnShield]");
        //RpcTurnOnShield();
    }

    [ClientRpc]
    public void RpcTurnOnShield(NetworkIdentity objNetId)
    {
        Debug.Log("[NetworkObjectEvent_WaterShield:RpcTurnOnShield]");
        objNetId.gameObject.SetActive(true);
    }

    [Command]
    public void CmdStartWaterManagment()
    {
        Debug.Log("[NetworkObjectEvent_WaterShield:CmdStartWaterManagment]");
        RpcStartWaterManagment();
    }

    [ClientRpc]
    public void RpcStartWaterManagment()
    {
        Debug.Log("[NetworkObjectEvent_WaterShield:RpcStartWaterManagment]");
        m_Water.SetStarter();
    }
}
