﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Valve.VR.InteractionSystem;

public class MyInteractable : MonoBehaviour
{
    new Transform m_transform;
    public GameObject m_definitivObject;
    public string m_tagAccepted = "defPosition";
    bool m_haveAObject;
    public AudioClip a_validationObjet;
    private AudioSource _audioSourceValidationObjet;
    public GameObject m_AudioS;
    void Awake()
    {
        m_transform = GetComponent<Transform>();
        m_haveAObject = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }



    // Update is called once per frame
    void Update()
    {
        if (this.transform.childCount == 1 && this.transform.GetChild(0).transform.position != this.transform.position)
        {
            this.transform.GetChild(0).tag = m_tagAccepted;
            this.transform.GetChild(0).GetComponent<Rigidbody>().isKinematic = false;
            this.transform.GetChild(0).parent = null;
            this.GetComponent<BoxCollider>().enabled = true;
            m_haveAObject = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (this.transform.childCount == 0)
        {
            //Debug.Log("Hold");
            if (collision.gameObject == m_definitivObject)
            {
                //Debug.Log("good position");
                
                collision.gameObject.tag = "corectPosition";
                Transform myTransform = collision.gameObject.transform;
                Destroy(collision.gameObject.GetComponent<Throwable>());
                Destroy(collision.gameObject.GetComponent<Interactable>());
                Destroy(collision.gameObject.GetComponent<Rigidbody>());
                Destroy(collision.gameObject.GetComponent<NetworkTransform>());
                //Destroy(collision.gameObject.GetComponent<NetworkIdentity>());
                this.GetComponent<BoxCollider>().enabled = false;


                //Destroy(this.transform.GetChild(0).gameObject);

                collision.gameObject.transform.SetParent(this.transform);
                collision.gameObject.transform.position = this.transform.position;
                collision.gameObject.transform.rotation = this.transform.rotation;
                _audioSourceValidationObjet = m_AudioS.GetComponents<AudioSource>()[1];
                _audioSourceValidationObjet.clip = a_validationObjet;
                _audioSourceValidationObjet.Play();
                Destroy(Instantiate(m_AudioS, collision.gameObject.transform.position, Quaternion.identity), a_validationObjet.length + 1);
            }
            else
            {
                if (collision.gameObject.tag == m_tagAccepted && m_tagAccepted != "defPosition")
                {
                    //Debug.Log("bad position");
                    collision.gameObject.tag = "incorectPosition";

                    collision.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                    this.GetComponent<BoxCollider>().enabled = false;

                    collision.gameObject.transform.SetParent(this.transform);
                    collision.gameObject.transform.position = this.transform.position;
                    collision.gameObject.transform.rotation = this.transform.rotation;
                }
            }
            m_haveAObject = true;
        }
    }
}
