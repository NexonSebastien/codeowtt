﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
//using MongoDB.Bson;
//using MongoDB.Driver;

namespace Data
{
    public enum Headers
    {
        GlobalTime,
        PlayerOne,
        PlayerTwo,
        NbPlayersEscaped,
        NbEnigmaCompleted,
        SecretEnigmaCompleted,
        TimeLobby,
        TimePoseidonRoom,
        NbObjectsGrabbedP1,
        NbObjectsGrabbedP2,
        TimeApollonRoom,
        NbMelodiesListenedP2,
        NbMusicCrystalsHitP1,
        NbGoodHitP1,
        NbWrongHitP1,
        LongestSequenceWithoutFailP1,
        NbPerfectSeriesP1,
        HeightMaxWaterP2,
        TimePlayerHeadUnderWaterP2,
        TimeHephaistosRoom,
        NbBraserosEmbrased,
        NbGoodBraserosEmbrasedP1,
        NbGoodBraserosEmbrasedP2,
        NbWrongBraserosEmbrasedP1,
        NbWrongBraserosEmbrasedP2,
        NbBraserosEmbrasedByArrowP1,
        NbBraserosEmbrasedByArrowP2,
        NbArrowThrownP1,
        NbArrowThrownP2,
        NbArrowEmbrasedP1,
        NbArrowEmbrasedP2,
        NbCrystalsDestructedP1,
        NbCrystalsDestructedP2,
        NbCrystalsDestructedByArrowP1,
        NbCrystalsDestructedByArrowP2,
        NbCrystalsDestroyedBySwordP1,
        NbCrystalsDestroyedBySwordP2,
        NbCrystalsDestroyedByDaggerP1,
        NbCrystalsDestroyedByDaggerP2,
        NbCrystalsDestroyedByMassP1,
        NbCrystalsDestroyedByMassP2,
        NbCrystalsDestroyedByHammerP1,
        NbCrystalsDestroyedByHammerP2,
        NbWallsDestructedP1,
        NbWallsDestructedP2,
        NbPillarsDestructed,
        TimeCapsuleRoom
    }

    [Obsolete]
    public class GameData : NetworkBehaviour
    {
        private Dictionary<Headers, Data> _datas;

        //public static readonly ReadOnlyCollection<Headers> m_headers = Array.AsReadOnly((Headers[])Enum.GetValues(typeof(Headers)));


        public struct Data
        {
            public string _stringFormat;
            public string _value;

            public Data(string stringFormat, string value)
            {
                _stringFormat = stringFormat;
                _value = value;
            }
        }

        private void Awake()
        {
            InitDatas();
        }

        private void Start()
        {
            //PrintDatas();
            //WriteInFileDatas();
            //WriteInDatabase();
        }

        private void InitDatas()
        {
            _datas = new Dictionary<Headers, Data>();
            int headersLength = Enum.GetNames(typeof(Headers)).Length;
            List<Data> datas = new List<Data>()
        {
            new Data("Global time : {0:d} minutes {1:d} seconds", "0" ),
            new Data("Player 1 : {0:s}", "P1" ),
            new Data("Player 2 : {0:s}", "P2" ),
            new Data("Players escaped : {0:d}", "0" ),
            new Data("Enigmas completed : {0:d}", "0" ),
            new Data("Secret enigma competed : {0}", "No" ),
            new Data("Time in lobby : {0:d} minutes {1:d} seconds", "0" ),
            new Data("Time in Poseidon's room : {0:d} minutes {1:d} seconds", "0" ),
            new Data("Objects grabbed : {0:d}", "0" ),
            new Data("Objects grabbed : {0:d}", "0" ),
            new Data("Time in Apollon's room : {0:d} minutes {1:d} seconds", "0" ),
            new Data("Melody listened : {0:d} times", "0" ),
            new Data("Total music crystals hit : {0:d}", "0" ),
            new Data("Good music crystals hit : {0:d}", "0" ),
            new Data("Wrong music crystals hit : {0:d}", "0" ),
            new Data("Longest sequence without fail : {0:d}", "0" ),
            new Data("Perfect series : {0:d}", "0" ),
            new Data("Maximum water height {0:f}", "0.0f" ),
            new Data("Breathing time under water : {0:d} minutes {1:d} seconds", "0" ),
            new Data("Time in Hephaistos's room : {0:d} minutes {1:d} seconds", "0" ),
            new Data("Braseros embrased : {0:d}", "0" ),
            new Data("Good braseros embrased : {0:d}", "0" ),
            new Data("Good braseros embrased : {0:d}", "0" ),
            new Data("Wrong braseros embrased : {0:d}", "0" ),
            new Data("Wrong braseros embrased : {0:d}", "0" ),
            new Data("Braseros emnbrased by an arrow : {0:d}", "0" ),
            new Data("Braseros emnbrased by an arrow : {0:d}", "0" ),
            new Data("Arrow thrown : {0:d}", "0" ),
            new Data("Arrow thrown : {0:d}", "0" ),
            new Data("Arrow embrased : {0:d}", "0" ),
            new Data("Arrow embrased : {0:d}", "0" ),
            new Data("Crystals destructed : {0:d}", "0" ),
            new Data("Crystals destructed : {0:d}", "0" ),
            new Data("Crystals destructed by an arrow : {0:d}", "0" ),
            new Data("Crystals destructed by an arrow : {0:d}", "0" ),
            new Data("Crystals destructed by a sword : {0:d}", "0" ),
            new Data("Crystals destructed by a sword : {0:d}", "0" ),
            new Data("Crystals destructed by a dagger : {0:d}", "0" ),
            new Data("Crystals destructed by a dagger : {0:d}", "0" ),
            new Data("Crystals destructed by a mass : {0:d}", "0" ),
            new Data("Crystals destructed by a mass : {0:d}", "0" ),
            new Data("Crystals destructed by a hammer : {0:d}", "0" ),
            new Data("Crystals destructed by a hammer : {0:d}", "0" ),
            new Data("Walls destructed : {0:d}", "0" ),
            new Data("Walls destructed : {0:d}", "0" ),
            new Data("Pillars destructed : {0:d}", "0" ),
            new Data("Time in Capsule room : {0:d} minutes {1:d} seconds", "0" ),
        };
            for (int i = 0; i < headersLength; i++)
            {
                _datas.Add((Headers)Enum.GetValues(typeof(Headers)).GetValue(i), datas[i]);
            }
        }

        public void PrintDatas()
        {
            for (int i = 0; i < _datas.Count; i++)
            {
                Debug.Log(_datas.Keys.ElementAt(i) + " = " + String.Format(_datas.Values.ElementAt(i)._stringFormat, _datas.Values.ElementAt(i)._value));
            }
        }

        [ClientRpc]
        public void RpcWriteInFileDatas()
        {
            string directoryName = "owtt_datas";
            string date = DateTime.Now.ToString("yyyyMMddHHmmss");
            string extension = "csv";
            string fileName = "owtt_datas_" + date + "." + extension;
            string path = Path.Combine(directoryName, fileName);
            char separator = ';';

            if (!Directory.Exists(directoryName))
            {
                Debug.Log("Directory " + directoryName + " created");
                Directory.CreateDirectory(directoryName);
            }

            Debug.Log("Open file " + path);
            using (StreamWriter file = new StreamWriter(path))
            {
                for (int i = 0; i < _datas.Count; i++)
                {
                    string line = _datas.Keys.ElementAt(i).ToString() + separator + _datas.Values.ElementAt(i)._value.ToString();
                    /*
                    string line = _datas.Keys.ElementAt(i).ToString();
                    for (int j = 0; j < _datas.Values.ElementAt(i)._value.Length; j++)
                    {
                        line += separator + _datas.Values.ElementAt(i)._value[j].ToString();
                    }
                    */
                    Debug.Log("WriteLine = " + line);
                    file.WriteLine(line);
                    file.Flush();
                }
                Debug.Log("Close file " + path);
                file.Close();
            }
        }

        [ServerCallback]
        public void SetValue(int key, string value)
        {
            Debug.Log("SetValue : key =  " + key + " value = " + value);
            if (value != null)
            {
                RpcSetValue(key, value);
            }
        }

        /*public void WriteInDatabase()
        {
            Debug.Log("Connecting to database ...");
            var a = new MongoClient();
            var client = new MongoClient("mongodb+srv://owtt:owtt@onewaytriptogether.s66lu.mongodb.net/myFirstDatabase?retryWrites=true&w=majority");
            Debug.Log("Getting database 'test'");
            var database = client.GetDatabase("test");
        }*/

        [ClientRpc]
        private void RpcSetValue(int key, string value)
        {
            Debug.Log("RpcSetValue : key =  " + key + " value = " + value.ToString());
            Headers k = (Headers)Enum.GetValues(typeof(Headers)).GetValue(key);
            KeyValuePair<Headers, Data> kvp = _datas.ElementAt(key);
            Data d = new Data(kvp.Value._stringFormat, value);
            _datas[k] = d;
        }
    }
}


