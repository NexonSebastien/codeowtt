﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[System.Obsolete]
public class TriggerActiveCanHelp : NetworkBehaviour
{
    private string Event = "event:/Dialogues/Salle1";
    private string EventDoor = "event:/Champs/Champ";
    public FMOD.Studio.EventInstance instance;
    //public FMOD.Studio.EventInstance instanceDoor;
    private bool songHasBeenPlayed = false;
    [SerializeField] public GameObject m_door;
    [SerializeField] EnigmaManager m_EnigmaManager;
    [SerializeField] public GameObject m_OtherShield;
    private FMOD.Studio.PLAYBACK_STATE m_state;

    private bool verifDialogueAide = false;

    [ServerCallback]
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "BodyTrigger" && !songHasBeenPlayed)
        {
            songHasBeenPlayed = true;
            //instanceDoor = FMODUnity.RuntimeManager.CreateInstance(EventDoor);
            //instanceDoor.setParameterByName("Champ", 1f);
            Debug.Log("[ON TRIGGER]");
            RpcActivateHelp();
        }
    }

    private void Start()
    {
        instance = FMODUnity.RuntimeManager.CreateInstance(Event);
    }


    private void PlaySong()
    {
        songHasBeenPlayed = true;
    }

    [ClientRpc]
    private void RpcActivateHelp()
    {
        Debug.Log("[RPC EXECUTE]");
        CloseDoor();
        ActivateHelp();
    }

    [ClientRpc]
    public void RpcDesactiveHelp()
    {
        int position = 0;
        instance.getTimelinePosition(out position);
        instance.getPlaybackState(out m_state);
        Debug.Log("[Dial Pos] position " + position);
        if (position < 300000)
        {
            Debug.Log("[Dialogue Aide Poseidon] N'a pas été joué");
            verifDialogueAide = true;
            instance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            instance.release();
        }
        else if(position > 300000 && position < 380000) 
        {
            Debug.Log("[Dialogue Aide Poseidon] En train de jouer");
            StartCoroutine(WaitEndDialogueHelp());
        }
        else
        {
            Debug.Log("[Dialogue Aide Poseidon] A déjà été joué");
            instance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            instance.release();
        }
    }

    private IEnumerator WaitEndDialogueHelp()
    {
        while (m_state == FMOD.Studio.PLAYBACK_STATE.PLAYING)
        {
            yield return new WaitForSeconds(1);
            instance.getPlaybackState(out m_state);
        }
        instance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        instance.release();
    }

    private void ActivateHelp()
    {
        Debug.Log("[SON POSEIDON AVANT]");
        Debug.Log(m_OtherShield.transform.GetChild(0).name);
        if (m_OtherShield.transform.GetChild(0).gameObject.active)
        {
            songHasBeenPlayed = true;
            Debug.Log("[SON POSEIDON APRES]");
            instance.start();
        }
    }

    public bool GetVerifDialogueAide()
    {
        return verifDialogueAide;
    }


    public void CloseDoor()
    {
        Debug.Log("CLOSEDOOR");
        m_door.SetActive(true);
    }
}
