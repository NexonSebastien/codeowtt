﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[System.Obsolete]
public class PlayerSyncedHandsObjects : NetworkBehaviour
{
    [SerializeField] public GameObject LeftHandObject;
    [SerializeField] public GameObject RightHandObject;
    private GameObject _LeftHandObject;
    private GameObject _RightHandObject;
    NetworkIdentity PlayerNetworkIdentity;

    void Start()
    {
        PlayerNetworkIdentity = GetComponent<NetworkIdentity>();
        //netobjNetId = networkObject.GetComponent<NetworkIdentity>();
        //Debug.Log("[START] networkObject(netId=" + netobjNetId.netId + ",name=" + netobjNetId.name + ")");
    }

    void Update()
    {
        if (!isLocalPlayer) return;

        SetLeftHandObject();
        SetRightHandObject();

        if (Input.GetKeyDown(KeyCode.U))
        {
            Debug.Log("[PLAYER:_LeftHandObject] Player " + PlayerNetworkIdentity.netId.ToString() + " ask for assign authority on network object " + _LeftHandObject.GetComponent<NetworkIdentity>().netId.ToString() + " with authority : " + _LeftHandObject.GetComponent<NetworkIdentity>().hasAuthority);
            CmdAssignClientAuthority(_LeftHandObject.GetComponent<NetworkIdentity>());
            Debug.Log("[PLAYER:_RightHandObject] Player " + PlayerNetworkIdentity.netId.ToString() + " ask for assign authority on network object " + _RightHandObject.GetComponent<NetworkIdentity>().netId.ToString() + " with authority : " + _RightHandObject.GetComponent<NetworkIdentity>().hasAuthority);
            CmdAssignClientAuthority(_RightHandObject.GetComponent<NetworkIdentity>());
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            Debug.Log("[PLAYER:_LeftHandObject] Player " + PlayerNetworkIdentity.netId.ToString() + " ask for remove authority on network object " + _LeftHandObject.GetComponent<NetworkIdentity>().netId.ToString() + " with authority : " + _LeftHandObject.GetComponent<NetworkIdentity>().hasAuthority);
            CmdRemoveClientAuthority(_LeftHandObject.GetComponent<NetworkIdentity>());
            Debug.Log("[PLAYER:_RightHandObject] Player " + PlayerNetworkIdentity.netId.ToString() + " ask for remove authority on network object " + _RightHandObject.GetComponent<NetworkIdentity>().netId.ToString() + " with authority : " + _RightHandObject.GetComponent<NetworkIdentity>().hasAuthority);
            CmdRemoveClientAuthority(_RightHandObject.GetComponent<NetworkIdentity>());
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            // LEFT HAND INTERACTION
            if (_LeftHandObject != null)
            {
                Debug.Log("[PLAYER:_LeftHandObject] Player " + PlayerNetworkIdentity.netId.ToString() + " ask for interact with network object " + _LeftHandObject.GetComponent<NetworkIdentity>().netId.ToString() + " with authority : " + _LeftHandObject.GetComponent<NetworkIdentity>().hasAuthority);
                //CmdInteractWithNetworkObject(netobjNetId);
                Vector3 curObjPos = _LeftHandObject.transform.position;
                Vector3 newObjPos = new Vector3(curObjPos.x + 1, curObjPos.y, curObjPos.z);
                if (_LeftHandObject.GetComponent<NetworkIdentity>().hasAuthority)
                {
                    Debug.Log("[PLAYER:_LeftHandObject] Player " + PlayerNetworkIdentity.netId.ToString() + " has authority to interact with network object " + _LeftHandObject.GetComponent<NetworkIdentity>().netId.ToString());
                    //CmdInteractWithNetworkObjectPosition(netobjNetId, newObjPos);
                    UpdateNetworkObjectStates(_LeftHandObject);
                }
                else
                {
                    Debug.Log("[PLAYER:_LeftHandObject] Player " + PlayerNetworkIdentity.netId.ToString() + " has no authority to interact with network object " + _LeftHandObject.GetComponent<NetworkIdentity>().netId.ToString());
                }
            }
            else
            {
                Debug.Log("[PLAYER:_LeftHandObject] Player " + PlayerNetworkIdentity.netId.ToString() + " ask for interact with NULL network object");
            }
            // RIGHT HAND INTERACTION
            if (_RightHandObject != null)
            {
                Debug.Log("[PLAYER:_RightHandObject] Player " + PlayerNetworkIdentity.netId.ToString() + " ask for interact with network object " + _RightHandObject.GetComponent<NetworkIdentity>().netId.ToString() + " with authority : " + _RightHandObject.GetComponent<NetworkIdentity>().hasAuthority);
                //CmdInteractWithNetworkObject(netobjNetId);
                Vector3 curObjPos = _RightHandObject.transform.position;
                Vector3 newObjPos = new Vector3(curObjPos.x + 1, curObjPos.y, curObjPos.z);
                if (_RightHandObject.GetComponent<NetworkIdentity>().hasAuthority)
                {
                    Debug.Log("[PLAYER:_RightHandObject] Player " + PlayerNetworkIdentity.netId.ToString() + " has authority to interact with network object " + _RightHandObject.GetComponent<NetworkIdentity>().netId.ToString());
                    //CmdInteractWithNetworkObjectPosition(netobjNetId, newObjPos);
                    UpdateNetworkObjectStates(_RightHandObject);
                }
                else
                {
                    Debug.Log("[PLAYER:_RightHandObject] Player " + PlayerNetworkIdentity.netId.ToString() + " has no authority to interact with network object " + _RightHandObject.GetComponent<NetworkIdentity>().netId.ToString());
                }
            }
            else
            {
                Debug.Log("[PLAYER:_RightHandObject] Player " + PlayerNetworkIdentity.netId.ToString() + " ask for interact with NULL network object");
            }
        }
    }

    public void SetLeftHandObject()
    {
        if (LeftHandObject == null && _LeftHandObject == null) return;
        if (LeftHandObject.Equals(_LeftHandObject) == false)
        {
            if (LeftHandObject != null)
            {
                //Debug.LogWarning(NetworkObject.name);
                Debug.Log("[PLAYER:SetLeftHandObject] Player " + PlayerNetworkIdentity.netId.ToString() + " ask for assign authority on network object " + LeftHandObject.GetComponent<NetworkIdentity>().netId.ToString() + " with authority : " + LeftHandObject.GetComponent<NetworkIdentity>().hasAuthority);
                CmdAssignClientAuthority(LeftHandObject.GetComponent<NetworkIdentity>());
            }
            if (_LeftHandObject != null)
            {
                //Debug.LogError(_networkObject.name);
                Debug.Log("[PLAYER:SetLeftHandObject] Player " + PlayerNetworkIdentity.netId.ToString() + " ask for remove authority on network object " + _LeftHandObject.GetComponent<NetworkIdentity>().netId.ToString() + " with authority : " + _LeftHandObject.GetComponent<NetworkIdentity>().hasAuthority);
                CmdRemoveClientAuthority(_LeftHandObject.GetComponent<NetworkIdentity>());
                Debug.Log("[PLAYER:SetLeftHandObject] networkObject(netId=" + _LeftHandObject.GetComponent<NetworkIdentity>().netId + ",name=" + _LeftHandObject.GetComponent<NetworkIdentity>().name + ")");
            }
            _LeftHandObject = LeftHandObject;
        }
    }

    public void SetRightHandObject()
    {
        if (RightHandObject == null && _RightHandObject == null) return;
        if (RightHandObject.Equals(_RightHandObject) == false)
        {
            if (RightHandObject != null)
            {
                //Debug.LogWarning(NetworkObject.name);
                Debug.Log("[PLAYER:SetRightHandObject] Player " + PlayerNetworkIdentity.netId.ToString() + " ask for assign authority on network object " + RightHandObject.GetComponent<NetworkIdentity>().netId.ToString() + " with authority : " + RightHandObject.GetComponent<NetworkIdentity>().hasAuthority);
                CmdAssignClientAuthority(RightHandObject.GetComponent<NetworkIdentity>());
            }
            if (_RightHandObject != null)
            {
                //Debug.LogError(_networkObject.name);
                Debug.Log("[PLAYER:SetRightHandObject] Player " + PlayerNetworkIdentity.netId.ToString() + " ask for remove authority on network object " + _RightHandObject.GetComponent<NetworkIdentity>().netId.ToString() + " with authority : " + _RightHandObject.GetComponent<NetworkIdentity>().hasAuthority);
                CmdRemoveClientAuthority(_RightHandObject.GetComponent<NetworkIdentity>());
                Debug.Log("[PLAYER:SetRightHandObject] networkObject(netId=" + _RightHandObject.GetComponent<NetworkIdentity>().netId + ",name=" + _RightHandObject.GetComponent<NetworkIdentity>().name + ")");
            }
            _RightHandObject = RightHandObject;
        }
    }

    private void UpdateNetworkObjectStates(GameObject networkObject)
    {
        if(networkObject != null)
        {
            //CmdInteractWithNetworkObjectPosition(networkObject.GetComponent<NetworkIdentity>(), networkObject.transform.position);
        }
    }

    [Command]
    public void CmdAssignClientAuthority(NetworkIdentity objNetId)
    {
        Debug.Log("[COMMAND] Current authority is assigned to player " + objNetId.clientAuthorityOwner);
        objNetId.AssignClientAuthority(connectionToClient);
        Debug.Log("[COMMAND] New authority is assigned to player " + objNetId.clientAuthorityOwner);
    }

    [Command]
    public void CmdRemoveClientAuthority(NetworkIdentity objNetId)
    {
        Debug.Log("[COMMAND] Current authority is assigned to player " + objNetId.clientAuthorityOwner);
        objNetId.RemoveClientAuthority(connectionToClient);
        Debug.Log("[COMMAND] New authority is assigned to player " + objNetId.clientAuthorityOwner);
    }

    [Command]
    public void CmdInteractWithNetworkObject(NetworkIdentity objNetId)
    {
        Debug.Log("[COMMAND] Current object position = " + objNetId.gameObject.transform.position.ToString());
        Vector3 curObjPos = objNetId.gameObject.transform.position;
        Vector3 newObjPos = new Vector3(curObjPos.x + 1, curObjPos.y, curObjPos.z);
        objNetId.gameObject.transform.position = newObjPos;
        Debug.Log("[COMMAND] New object position = " + objNetId.gameObject.transform.position.ToString());
        //TargetInteractWithNetworkObject(connectionToClient, objNetId, objNetId.gameObject.transform.position);
        RpcInteractWithNetworkObject(objNetId, objNetId.gameObject.transform.position);
    }

    [TargetRpc]
    public void TargetInteractWithNetworkObject(NetworkConnection playerConn, NetworkIdentity objNetId, Vector3 newObjPos)
    {
        Debug.Log("[TARGET] Current object position = " + objNetId.gameObject.transform.position.ToString());
        objNetId.gameObject.transform.position = newObjPos;
        Debug.Log("[TARGET] New object position = " + objNetId.gameObject.transform.position.ToString());
    }

    [ClientRpc]
    public void RpcInteractWithNetworkObject(NetworkIdentity objNetId, Vector3 newObjPos)
    {
        Debug.Log("[RPC] Current object position = " + objNetId.gameObject.transform.position.ToString());
        objNetId.gameObject.transform.position = newObjPos;
        Debug.Log("[RPC] New object position = " + objNetId.gameObject.transform.position.ToString());
    }

    [Command]
    public void CmdInteractWithNetworkObjectPosition(NetworkIdentity objNetId, Vector3 newObjPos)
    {
        Debug.Log("[COMMAND] Current object position = " + objNetId.gameObject.transform.position.ToString());
        objNetId.gameObject.transform.position = newObjPos;
        Debug.Log("[COMMAND] New object position = " + objNetId.gameObject.transform.position.ToString());
        //TargetInteractWithNetworkObject(connectionToClient, objNetId, objNetId.gameObject.transform.position);
        RpcInteractWithNetworkObjectPosition(objNetId, objNetId.gameObject.transform.position);
    }

    [ClientRpc]
    public void RpcInteractWithNetworkObjectPosition(NetworkIdentity objNetId, Vector3 newObjPos)
    {
        Debug.Log("[RPC] Current object position = " + objNetId.gameObject.transform.position.ToString());
        objNetId.gameObject.transform.position = newObjPos;
        Debug.Log("[RPC] New object position = " + objNetId.gameObject.transform.position.ToString());
    }
}
