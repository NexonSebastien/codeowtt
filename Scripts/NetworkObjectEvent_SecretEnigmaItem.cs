﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using Valve.VR.InteractionSystem;

[System.Obsolete]
public class NetworkObjectEvent_SecretEnigmaItem : NetworkObjectEvent
{
    private const float ANGLE_MAX = 360.0f;
    private const float ANGLE_DETECTION_HALF = 10.0f;

    [SerializeField] [SyncVar] private bool m_validated;
    private int m_nbSymbols;
    private float m_angleStep;
    private Dictionary<float, bool> m_symbolsAngle;
    [SerializeField] private List<Material> m_symbolsMaterialDefault;
    private CircularDrive m_circularDrive;
    private List<int> m_shuffleIndex;
    [SerializeField] private bool m_syncList;
    [SerializeField] private List<Texture> m_symbolsTextures;
    [SerializeField] [SyncVar] public int m_symbolIndex;
    [SerializeField] [SyncVar] public bool m_ready;
    private Color m_symbolColorDefault;
    private Color m_symbolColorValidated;

    override protected void Start()
    {
       base.Start();
       m_validated = false;
       m_nbSymbols = m_symbolsMaterialDefault.Count;
       m_angleStep = ANGLE_MAX / m_nbSymbols;
       m_symbolColorDefault = Color.white;
       m_symbolColorValidated = Color.magenta;
       InitSymbolsAngle();
       m_circularDrive = GetComponent<CircularDrive>();
       UpdateSymbolsMaterialToDefault();
        if (!isServer)
        {
            if (!m_syncList)
            {
                StartCoroutine(SyncSymbols());
            }
        }
    }

    [ServerCallback]
    private void Update()
    {
        if (m_ready)
        {
            CheckAngle(m_circularDrive.outAngle);
        }
    }

    public bool IsReady()
    {
       return m_ready;
    }

    public bool IsValidated()
    {
       return m_validated;
    }

    public int GetNbSymbols()
    {
       return m_nbSymbols;
    }

    public void SetShuffleIndex(List<int> shuffleIndex, int soluceSymbolIndex)
    {
        m_shuffleIndex = shuffleIndex;
        m_symbolIndex = soluceSymbolIndex;
        //m_symbolsTextures = GetComponentInParent<NetworkObjectEvent_SecretEnigma>().GetSymbolsTextures();
        SetMaterialTextures();
        m_syncList = true;
        m_ready = true;
        //Debug.Log("[NetworkObjectEvent_SecretEnigmaItem:SetShuffleIndex:m_ready=" + m_ready + "]");
    }

    private IEnumerator SyncSymbols()
    {
        yield return new WaitForEndOfFrame();
        NetworkPlayer player = GameObject.Find("VrGameObject").GetComponentInParent<NetworkPlayer>();
        //Debug.Log("[NetworkObjectEvent_SecretEnigmaItem(" + transform.root.name + ">" + name + "):SyncSymbols:setAuthority:" + hasAuthority + "]");
        player.CmdSetAuth(m_NetId, player.GetComponent<NetworkIdentity>());
        yield return new WaitUntil(() => m_ready && hasAuthority);
        //Debug.Log("[NetworkObjectEvent_SecretEnigmaItem:SetShuffleIndex(" + transform.root.name + ">" + name + "):SyncSymbols:m_ready=" + m_ready + ":hasAuthority="+hasAuthority+"]");
        m_syncList = false;
        CmdSyncList();
        yield return new WaitUntil(() => m_syncList);
        string debug = "[NetworkObjectEvent_SecretEnigmaItem(" + transform.root.name +">"+ name + "):SyncSymbols:m_symbolIndex=" + m_symbolIndex + ":m_shuffleIndex[]=[";
        string debugIndex = debug;
        for (int i = 0; i < m_shuffleIndex.Count; i++)
        {
            debugIndex += m_shuffleIndex[i] + ",";
        }
        Debug.Log(debugIndex + "]]");
        SetMaterialTextures();
        yield return null;
    }

    private void SetMaterialTextures()
    {
        for(int i=0; i< m_nbSymbols; i++)
        {
            SetMaterialTexture(i, m_shuffleIndex[i]);
        }
    }

    public void SetSymbolsTextures(List<Texture> symbolsTextures)
    {
        m_symbolsTextures = symbolsTextures;
    }

    private void InitSymbolsAngle()
    {
       m_symbolsAngle = new Dictionary<float, bool>();
       for(int i=0; i<m_nbSymbols; i++)
       {
           m_symbolsAngle.Add(m_angleStep * i, false);
       }
    }
    
    [ServerCallback]
    private void CheckAngle(float angle)
    {
        //float angleClamped = Mathf.Clamp(angle % ANGLE_MAX, 0.0f, ANGLE_MAX);
        float angleClamped;
        if(angle >= 0)
        {
            angleClamped = angle % ANGLE_MAX;
        }
        else
        {
            angleClamped = ANGLE_MAX - Mathf.Abs(angle) % ANGLE_MAX;
        }
        int closestIndex = Mathf.RoundToInt(angleClamped / m_angleStep);
        if (closestIndex == m_symbolsAngle.Count) // exception for angle 360 (= angle 0)
        {
            closestIndex = 0;
            angleClamped -= ANGLE_MAX;
        }
        float closestAngle = m_symbolsAngle.ElementAt(closestIndex).Key;
        if(angleClamped >= closestAngle - ANGLE_DETECTION_HALF  && angleClamped <= closestAngle + ANGLE_DETECTION_HALF
            && closestIndex == m_shuffleIndex.IndexOf(m_symbolIndex))
        {
            m_validated = true;
        }
        else
        {
            if (m_validated)
            {
                m_validated = false;
            }
        }
    }

    private void UpdateSymbolMaterial(int indexMaterial, bool validated)
    {
        //Debug.Log("[NetworkObjectEvent_SecretEnigmaItem:UpdateSymbolMaterial:index=" + indexMaterial + ":validated=" + validated + "]");
        Material material = FindMaterial(indexMaterial);
        material.SetColor("_EmissionColor", validated ? m_symbolColorValidated : m_symbolColorDefault);
    }
    
    private void UpdateSymbolsMaterialToDefault()
    {
        for (int i = 0; i < m_symbolsMaterialDefault.Count; i++)
        {
            UpdateSymbolMaterial(i, false);
        }
    }

    private void SetMaterialTexture(int indexMaterial, int indexTexture)
    {
        //Debug.Log("[NetworkObjectEvent_SecretEnigmaItem:SetMaterialTexture:indexMaterial=" + indexMaterial + ":indexTexture=" + indexTexture + "texture.name=" + m_symbolsTextures[indexTexture].name + "]");
        Material material = FindMaterial(indexMaterial);
        material.SetTexture("_EmissionMap", m_symbolsTextures[indexTexture]);
    }

    private Material FindMaterial(int indexMaterial)
    {
        Material[] materials = GetComponent<Renderer>().sharedMaterials;
        Material material = null;
        bool indexFounded = false;
        int i = 0;
        //Debug.Log("[NetworkObjectEvent_SecretEnigmaItem:FindMaterial:renderers.Length=" + materials.Length + "]");
        while (!indexFounded && i < materials.Length)
        {
            if (materials[i].name.Equals(m_symbolsMaterialDefault[indexMaterial].name))
            {
                //Debug.Log("[NetworkObjectEvent_SecretEnigmaItem:FindMaterial:material.name=" + materials[i].name + "]");
                indexFounded = true;
                material = materials[i];
            }
            i++;
        }
        return material;
    }
    private void StopInteractability()
    {
        //Debug.Log("[NetworkObjectEvent_SecretEnigmaItem:StopInteractability]");
        //Destroy(GetComponent<CircularDriveController>());
        Destroy(GetComponent<CircularDrive>());
        Destroy(GetComponent<Interactable>());
    }

    public override void EnigmaReceiveEvent(int eventCase, List<object> args)
    {
        if (isServer)
        {
            switch (eventCase)
            {
                case 1:
                    RpcUpdateSymbolMaterial((int)args[0], (bool)args[1]);
                    break;
                case 2:
                    RpcSetMaterialTexture((int)args[0], (int)args[1]);
                    break;
                case 3:
                    RpcUpdateCurrentSymbolMaterial((bool)args[0]);
                    break;
                case 4:
                    RpcStopInteractability();
                    break;
                case 5:
                    SendSyncList();
                    break;
            }
        }
    }

    public override void EnigmaUpdateEvent()
    {
        throw new System.NotImplementedException();
    }
    
    [ClientRpc]
    public void RpcUpdateSymbolMaterial(int indexMaterial, bool validated)
    {
        UpdateSymbolMaterial(indexMaterial, validated);
    }

    [ClientRpc]
    public void RpcSetMaterialTexture(int indexMaterial, int indexTexture)
    {
        SetMaterialTexture(indexMaterial, indexTexture);
    }

    [ClientRpc]
    public void RpcUpdateCurrentSymbolMaterial(bool validated)
    {
        //Debug.Log("[NetworkObjectEvent_SecretEnigmaItem:RpcUpdateCorrentSymbolMaterial:validated=" + validated + "]");
        UpdateSymbolMaterial(m_shuffleIndex.IndexOf(m_symbolIndex), validated);
    }

    [ClientRpc]
    public void RpcStopInteractability()
    {
        StopInteractability();
    }

    [Command]
    public void CmdSyncList()
    {
        ThrowEvent(m_NetId, 5, null);
    }

    private void SendSyncList()
    {
        Debug.Log("[NetworkObjectEvent_SecretEnigmaItem:SendSyncList]");
        string debugIndex = "[NetworkObjectEvent_SecretEnigmaItem(" + transform.root.name + ">" + name + "):SendSyncList:m_symbolIndex=" + m_symbolIndex + ":m_shuffleIndex[]=[";
        for (int i=0; i<m_shuffleIndex.Count; i++)
        {
            RpcSyncList(i, m_shuffleIndex[i]);
            debugIndex += m_shuffleIndex[i] + ",";
        }
        Debug.Log(debugIndex + "]]");
    }

    [ClientRpc]
    public void RpcSyncList(int index, int value)
    {
        //Debug.Log("[NetworkObjectEvent_SecretEnigmaItem(" + transform.root.name + ">"+name+ "):RpcSyncList:index=" + index + ":value=" + value + "]");
        StartCoroutine(ReceiveSyncList(index, value));
    }

    private IEnumerator ReceiveSyncList(int index, int value)
    {
        if (m_shuffleIndex == null){
            m_shuffleIndex = new List<int>();
        }
        if (index < m_symbolsMaterialDefault.Count)
        {
            yield return new WaitUntil(() => index == m_shuffleIndex.Count);
            //Debug.Log("[NetworkObjectEvent_SecretEnigmaItem(" + transform.root.name + ">" + name + "):ReceiveSyncList:index=" + index + ":value=" + value + "]");
            m_shuffleIndex.Add(value);
            if (m_shuffleIndex.Count == m_symbolsMaterialDefault.Count)
            {
                m_syncList = true;
            }
        }
    }
}