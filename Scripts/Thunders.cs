﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thunders : MonoBehaviour
{
    [SerializeField]
    public GameObject Eclair;
    [SerializeField]
    public GameObject eclairStart;
    [SerializeField]
    public GameObject eclairEnd;

    public float time = 40;
    public double time2 = 0.5;
    public bool verifTime = false;
    public float randomX1 = 0;
    public float randomX2 = 0;
    public float randomEclair = 0;
    public float randomSound = 0;

    public AudioClip eThunder1;
    public AudioClip eThunder2;
    public AudioClip eThunder3;
    public AudioClip eThunder4;
    public AudioClip eThunder5;
    public AudioClip eThunder6;
    public AudioClip eThunder7;
    public AudioClip eThunder8;
    public AudioClip eThunder9;
    public AudioClip eThunder10;
    public AudioClip eThunder11;
    public AudioClip eThunder12;
    public AudioClip eThunder13;
    public AudioClip eThunder14;
    public AudioClip eThunder15;
    public AudioClip eThunder16;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        randomX1 = Random.Range(120, 380);
        randomX2 = Random.Range(120, 380);
        randomEclair = Random.Range(10, 30);
        randomSound = Random.Range(1, 16);
        eclairStart.gameObject.transform.position = new Vector3(randomX1, eclairStart.gameObject.transform.position.y, eclairStart.gameObject.transform.position.z);
        eclairEnd.gameObject.transform.position = new Vector3(randomX2, eclairEnd.gameObject.transform.position.y, eclairEnd.gameObject.transform.position.z);

        if (time >=0)
        {
            time -= Time.deltaTime;
            Eclair.SetActive(false);
        }
        else if (time2 >= 0) 
        {
            time2 -= Time.deltaTime;
        }
        else if (verifTime == false)
        {
            Eclair.SetActive(true);
            time2 = 1;
            verifTime = true;
        }
        else
        {
            Eclair.SetActive(false);
                if (randomSound == 1)
                {
                    MakeSound(eThunder1);
                }
                if (randomSound == 2)
                {
                    MakeSound(eThunder2);
                }
                if (randomSound == 3)
                {
                    MakeSound(eThunder3);
                }
                if (randomSound == 4)
                {
                    MakeSound(eThunder4);
                }
                if (randomSound == 5)
                {
                    MakeSound(eThunder5);
                }
                if (randomSound == 6)
                {
                    MakeSound(eThunder6);
                }
                if (randomSound == 7)
                {
                    MakeSound(eThunder7);
                }
                if (randomSound == 8)
                {
                    MakeSound(eThunder8);
                }
                if (randomSound == 9)
                {
                    MakeSound(eThunder9);
                }
                if (randomSound == 10)
                {
                    MakeSound(eThunder10);
                }
                if (randomSound == 11)
                {
                    MakeSound(eThunder11);
                }
                if (randomSound == 12)
                {
                    MakeSound(eThunder12);
                }
                if (randomSound == 13)
                {
                    MakeSound(eThunder13);
                }
                if (randomSound == 14)
                {
                    MakeSound(eThunder14);
                }
                if (randomSound == 15)
                {
                    MakeSound(eThunder15);
                }
                if (randomSound == 16)
                {
                    MakeSound(eThunder16);
                }
            time2 = randomEclair;
            verifTime = false;
        }
    }

    private void MakeSound(AudioClip originalClip)
    {
        AudioSource.PlayClipAtPoint(originalClip, transform.position);
    }
}
