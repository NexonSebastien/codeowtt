﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class EnigmManager : MonoBehaviour
{
    private int m_solveObject;
    private bool m_solveEnigma;
    // Start is called before the first frame update
    private void Awake()
    {
        
    }
    void Start()
    {
        m_solveObject = GameObject.FindGameObjectsWithTag("corectPosition").Length;
        m_solveEnigma = false;
    }

    // Update is called once per frame
    void Update()
    {
        GameObject[] m_gameOjects;
        m_gameOjects = GameObject.FindGameObjectsWithTag("corectPosition");
        
        if (m_gameOjects.Length == 12 && m_solveEnigma == false)
        {
            Debug.Log("you win");
            m_solveEnigma = true;
            GameObject[] m_myDoors;
            m_myDoors = GameObject.FindGameObjectsWithTag("outDoorSimilarRoom");
            foreach(GameObject portes in m_myDoors)
            {
                portes.SetActive(false);
                
            }

            GameObject[] p1TpZone = GameObject.FindGameObjectsWithTag("TeleportAreaCorridorP1S1toS2");
            foreach (GameObject tpzone in p1TpZone)
            {
                tpzone.GetComponent<TeleportArea>().SetLocked(false);
                tpzone.GetComponent<TeleportArea>().markerActive = true;
            }
            GameObject[] p2TpZone = GameObject.FindGameObjectsWithTag("TeleportAreaCorridorP2S1toS2");
            foreach(GameObject tpzone in p2TpZone)
            {
                tpzone.GetComponent<TeleportArea>().SetLocked(false);
                tpzone.GetComponent<TeleportArea>().markerActive = true;
            }
        }
    }
}
