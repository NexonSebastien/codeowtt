﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[System.Obsolete]
public class NetworkObjectEvent_DestructWall : NetworkObjectEvent
{
    public override void EnigmaReceiveEvent(int eventCase, List<object> args)
    {
        if (isServer)
        {
            Debug.Log("[NetworkObjectEvent_DestructWall:EnigmaReceiveEvent:case=" + eventCase + "]");
            switch (eventCase)
            {
                case 1:
                    // destroy wall
                    RpcDestroyWall();
                    break;
            }
        }
    }

    public override void EnigmaUpdateEvent()
    {
        throw new System.NotImplementedException();
    }

    override protected void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    [ServerCallback]
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("[NetworkObjectEvent_DestructWall:OnTriggerEnter:other=" + other.name + "]");
        if (other.CompareTag("Hammer"))
        {
            ThrowEvent(m_NetId, 1, null); // destroy wall
        }
    }

    [ClientRpc]
    public void RpcDestroyWall()
    {
        Debug.Log("[NetworkObjectEvent_DestructWall:RpcDestroyWall]");
        this.GetComponent<BoxCollider>().enabled = false;
        this.transform.FindChild("WallHephaistosSecretRoom").GetComponent<Animator>().SetBool("IsDestroy", true);
        FMODUnity.RuntimeManager.PlayOneShotAttached("event:/Objets/Mur",this.transform.FindChild("WallHephaistosSecretRoom").gameObject);
    }
}
