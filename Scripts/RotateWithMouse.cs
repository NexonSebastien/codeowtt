﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class RotateWithMouse : MonoBehaviour
{
    [SerializeField] [Range(0, 2)] int m_IndexOfMouseButtonToHold = 0;

    [SerializeField] float m_RotAngularSpeed = 100f;
    [SerializeField] float m_KSlerpRot = 2f;
    [SerializeField] float m_MaxRotAngleDuringOneFrame = 20f;
    Quaternion m_TargetOrientationQuat;

    Transform m_Transform;
    Vector3 m_PreviousMousePos;

    private void Awake()
    {
        m_Transform = transform;
    }

    void Start()
    {
        m_TargetOrientationQuat = transform.rotation;
        m_PreviousMousePos = Input.mousePosition;
    }



    private void OnMouseDrag()
    {
        Vector3 mouseMove = - new Vector3((Input.mousePosition - m_PreviousMousePos).x,0);


        if (Input.GetMouseButton(m_IndexOfMouseButtonToHold))
        {
            Vector3 rotAxis = Vector3.Cross(mouseMove, Camera.main.transform.forward).normalized;

            float rotAngle = Mathf.Min(m_RotAngularSpeed * mouseMove.magnitude, m_MaxRotAngleDuringOneFrame);

            Quaternion rotQ = Quaternion.AngleAxis(rotAngle, rotAxis);

            m_TargetOrientationQuat = rotQ * m_Transform.rotation;
        }

        m_Transform.rotation = Quaternion.Slerp(m_Transform.rotation, m_TargetOrientationQuat, Time.deltaTime * m_KSlerpRot);

        m_PreviousMousePos = Input.mousePosition;
    }
}
