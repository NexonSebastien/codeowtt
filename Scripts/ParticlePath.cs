﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlePath : MonoBehaviour
{
	public GameObject path1StartPos;
	public GameObject path2StartPos;
	public GameObject path3StartPos;
	public GameObject path4StartPos;
	public GameObject vfx_particlesTraveling;

	public void GetPath()
    {
		Debug.Log("Particle PATH : GETPath");
		if (path1StartPos != null)
		{
			GameObject pathEffect1 = Instantiate(vfx_particlesTraveling, path1StartPos.transform.position, Quaternion.identity);
			pathEffect1.GetComponent<ParticlePathScript>().pathName = "Path01";
			Destroy(pathEffect1, pathEffect1.GetComponent<ParticleSystem>().main.duration + pathEffect1.GetComponent<ParticleSystem>().main.startLifetime.constantMax);
		}
		if (path2StartPos != null)
		{
			GameObject pathEffect2 = Instantiate(vfx_particlesTraveling, path2StartPos.transform.position, Quaternion.identity);
			pathEffect2.GetComponent<ParticlePathScript>().pathName = "Path02";
			Destroy(pathEffect2, pathEffect2.GetComponent<ParticleSystem>().main.duration + pathEffect2.GetComponent<ParticleSystem>().main.startLifetime.constantMax);
		}
		if (path3StartPos != null)
		{
			GameObject pathEffect3 = Instantiate(vfx_particlesTraveling, path3StartPos.transform.position, Quaternion.identity);
			pathEffect3.GetComponent<ParticlePathScript>().pathName = "Path03";
			Destroy(pathEffect3, pathEffect3.GetComponent<ParticleSystem>().main.duration + pathEffect3.GetComponent<ParticleSystem>().main.startLifetime.constantMax);
		}
		if (path4StartPos != null)
		{
			GameObject pathEffect4 = Instantiate(vfx_particlesTraveling, path4StartPos.transform.position, Quaternion.identity);
			pathEffect4.GetComponent<ParticlePathScript>().pathName = "Path04";
			Destroy(pathEffect4, pathEffect4.GetComponent<ParticleSystem>().main.duration + pathEffect4.GetComponent<ParticleSystem>().main.startLifetime.constantMax);
		}
		if (path1StartPos != null)
		{
			GameObject pathEffect1 = Instantiate(vfx_particlesTraveling, path1StartPos.transform.position, Quaternion.identity);
			pathEffect1.GetComponent<ParticlePathScript>().pathName = "Path05";
			Destroy(pathEffect1, pathEffect1.GetComponent<ParticleSystem>().main.duration + pathEffect1.GetComponent<ParticleSystem>().main.startLifetime.constantMax);
		}
		if (path2StartPos != null)
		{
			GameObject pathEffect2 = Instantiate(vfx_particlesTraveling, path2StartPos.transform.position, Quaternion.identity);
			pathEffect2.GetComponent<ParticlePathScript>().pathName = "Path06";
			Destroy(pathEffect2, pathEffect2.GetComponent<ParticleSystem>().main.duration + pathEffect2.GetComponent<ParticleSystem>().main.startLifetime.constantMax);
		}
		if (path3StartPos != null)
		{
			GameObject pathEffect3 = Instantiate(vfx_particlesTraveling, path3StartPos.transform.position, Quaternion.identity);
			pathEffect3.GetComponent<ParticlePathScript>().pathName = "Path07";
			Destroy(pathEffect3, pathEffect3.GetComponent<ParticleSystem>().main.duration + pathEffect3.GetComponent<ParticleSystem>().main.startLifetime.constantMax);
		}
		if (path4StartPos != null)
		{
			GameObject pathEffect4 = Instantiate(vfx_particlesTraveling, path4StartPos.transform.position, Quaternion.identity);
			pathEffect4.GetComponent<ParticlePathScript>().pathName = "Path08";
			Destroy(pathEffect4, pathEffect4.GetComponent<ParticleSystem>().main.duration + pathEffect4.GetComponent<ParticleSystem>().main.startLifetime.constantMax);
		}
	}
}
