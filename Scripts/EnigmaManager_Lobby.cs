﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.Video;
using Valve.VR.InteractionSystem;

[System.Obsolete]
public class EnigmaManager_Lobby : EnigmaManager
{
    [SerializeField] Material m_SkyboxMat;
    [SerializeField] Light[] m_light;
    [SerializeField] GameObject m_boat;
    [SerializeField] float m_speedBoat = 5;
    [SerializeField] float m_capsuleSpeed = 10;
    [SerializeField] float m_rotateSkyboxSpeed = 0.2f;

    [SerializeField] GameObject m_capsuleP1;
    [SerializeField] GameObject m_capsuleP2;
    [SerializeField] GameObject m_monster;
    [SerializeField] Transform m_monsterDescentePos;
    [SerializeField] float m_MonsterSpeed = 10;
    [SerializeField] GameObject m_monsterScreem;
    [SerializeField] Transform m_monsterScreemPos;
    [SerializeField] float m_MonsterScreemSpeed = 10;

    [SerializeField] Transform m_capsuleEndPositionP1;
    [SerializeField] Transform m_capsuleEndPositionP2;
    [SerializeField] Light[] m_lightCapsuleP1;
    [SerializeField] Light[] m_lightCapsuleP2;
    [SerializeField] PostProcessVolume m_PostProcessVolume;
    [SerializeField] PostProcessVolume m_PostProcessVolumeWater;
    [SerializeField] GameObject m_Lobby;
    [SerializeField] Network_AudioController m_Network_AudioController;
    [SerializeField] EnigmaManager m_nextEnigmaPart;

    [SerializeField] private List<TeleportArea> m_teleportAreaBoat;

    

    private Vector3 m_lastPositionMin;
    private Vector3 m_lastPositionMax;
    private bool m_SkyboxIsActive = false;
    private bool verifTrigger = false;
    private bool m_coupe = false;
    private bool verifCommun1 = false;
    private GameObject player2;



    private bool m_IsInWater = false;
    private bool m_IsSetupWater = false;
    private bool isWaitingDown = false;
    private bool sound2Start = false;
    private bool sound3Start = false;

    [SerializeField] [SyncVar] bool m_IsInCapsuleP1 = false;
    [SerializeField] [SyncVar] bool m_IsInCapsuleP2 = false;
    [SyncVar] bool sound1Finish = false;
    [SyncVar] bool sound2Finish = false;
    [SyncVar] bool m_canMove = true;
    [SyncVar] bool m_isStartedDescent = false;
    [SyncVar] bool m_CloseCapsule = false;



    private int m_direction = -1;
    private int nombrePlayers = 0;

    private FMOD.Studio.EventInstance m_instanceDialogueLobbyEva;
    private FMOD.Studio.EventInstance m_instanceDialogueLobbyJacob;
    private FMOD.Studio.EventInstance m_instanceDialogue1;
    private FMOD.Studio.EventInstance m_instanceDialogue2;
    private FMOD.Studio.EventInstance m_instanceDialogue3;
    private FMOD.Studio.EventInstance m_instanceDialogueCommun1;
    private FMOD.Studio.EventInstance m_instanceWave;
    private FMOD.Studio.EventInstance m_instanceAbysse;
    private FMOD.Studio.EventInstance m_instanceSas1;
    private FMOD.Studio.PLAYBACK_STATE m_state1;
    private FMOD.Studio.PLAYBACK_STATE m_state2;
    private FMOD.Studio.PLAYBACK_STATE m_state3;
    private FMOD.Studio.PLAYBACK_STATE m_stateCommun1;
    private FMOD.Studio.PLAYBACK_STATE m_stateEva;
    private FMOD.Studio.PLAYBACK_STATE m_stateJacob;
    private FMOD.Studio.PLAYBACK_STATE m_stateSas1;
    private string EventDialogueLobbyEva = "event:/Dialogues/LobbyEva";
    private string EventDialogueLobbyJacob = "event:/Dialogues/LobbyJacob";
    private string Event1 = "event:/Dialogues/Descente";
    private string Event2 = "event:/Dialogues/Descente2";
    private string Event3 = "event:/Dialogues/Descente3";
    private string EventDialogueCommun1 = "event:/Dialogues/LobbyCommun1";
    private string EventWave = "event:/Ambiance/Vague";
    private string EventAbysse = "event:/Ambiance/Abysses";
    private string EventChain = "event:/Objets/Chaine";
    private string EventSas1= "event:/Dialogues/DialogueSas1";
    private Vignette m_vignette;
    private ColorGrading m_colorGrading;
    private VideoPlayer m_videoPlayerP1;
    private VideoPlayer m_videoPlayerP2;

    private void Start()
    {
        nombrePlayers = NetworkManager.singleton.numPlayers;
        Debug.Log("Yann " + nombrePlayers);
        if (nombrePlayers == 1)
        {
            m_instanceDialogueLobbyEva = FMODUnity.RuntimeManager.CreateInstance(EventDialogueLobbyEva);
            m_instanceDialogueLobbyEva.start();
        }
        else if (nombrePlayers == 0)
        {
            m_instanceDialogueLobbyJacob = FMODUnity.RuntimeManager.CreateInstance(EventDialogueLobbyJacob);
            m_instanceDialogueLobbyJacob.start();
        }
        m_instanceDialogue1 = FMODUnity.RuntimeManager.CreateInstance(Event1);
        m_instanceDialogue2 = FMODUnity.RuntimeManager.CreateInstance(Event2);
        m_instanceDialogue3 = FMODUnity.RuntimeManager.CreateInstance(Event3);
        m_instanceWave = FMODUnity.RuntimeManager.CreateInstance(EventWave);
        m_instanceAbysse = FMODUnity.RuntimeManager.CreateInstance(EventAbysse);
        
        m_lastPositionMin = new Vector3(m_boat.transform.position.x, m_boat.transform.position.y, -1750);
        m_lastPositionMax = new Vector3(m_boat.transform.position.x, m_boat.transform.position.y, -650);
        m_PostProcessVolumeWater.profile.TryGetSettings<Vignette>(out m_vignette);
        m_PostProcessVolumeWater.profile.TryGetSettings<ColorGrading>(out m_colorGrading);
        m_videoPlayerP1 = m_capsuleP1.transform.FindChild("Tablet Home 2").transform.FindChild("GameObject").transform.FindChild("Video").gameObject.GetComponent<VideoPlayer>();
        m_videoPlayerP2 = m_capsuleP2.transform.FindChild("Tablet Home 2").transform.FindChild("GameObject").transform.FindChild("Video").gameObject.GetComponent<VideoPlayer>();
        ActiveSkybox();
        StartWaveSound();
    }

    [ServerCallback]
    private void Update()
    {
        if (m_IsEnigmaStarted && !m_IsEnigmaEnded)
        {
            if (m_IsInCapsuleP1 && m_IsInCapsuleP2)
            {
                if(!m_CloseCapsule)
                {
                    CheckIfCloseCapsule();
                    m_CloseCapsule = true;
                }
                
                if (m_isStartedDescent)
                {
                   
                    if (!m_IsSetupWater)
                    {
                        InWater();
                    }
                    
                    
                    if (m_canMove)
                    {
                        if(sound2Finish)
                        {
                            m_capsuleSpeed *= 3;
                            MoveCapsule();
                        }
                        else
                        {
                            MoveCapsule();
                        }
                    }
                    else
                    {
                        if (sound1Finish && !sound2Start)
                        {
                            RpcDialogueLight();
                            sound2Start = true;
                        }
                        if (sound2Finish && !sound3Start)
                        {
                            RpcDialogueMonster();
                            sound3Start = true;
                        }
                        else
                        {
                            m_instanceDialogue3.getPlaybackState(out m_state3);
                            if (m_state3 == FMOD.Studio.PLAYBACK_STATE.STOPPED && sound2Finish)
                            {
                                m_canMove = true;
                            }
                        }
                    }
                }
                else
                {
                    if(!isWaitingDown)
                    {
                        StartCoroutine(WaitMoveCapsule());
                        isWaitingDown = true;
                    }   
                }
            }
            else
            {
                RpcMoveBoat();
                RpcRotateSkyBox();
            }
            RpcDialogueReturn();

            if (m_Network_AudioController.GetIsFinishPlayingEva() && m_Network_AudioController.GetIsFinishPlayingJacob() && verifCommun1 == false)
            {
                Debug.Log("Yann Commun 1 Joué");
                RpcDialogueCommun1();
                verifCommun1 = true;
            }
        }
    }

    private IEnumerator WaitDialogueFinish()
    {
        while(m_stateCommun1 != FMOD.Studio.PLAYBACK_STATE.STOPPED)
        {
            m_instanceDialogueCommun1.getPlaybackState(out m_stateCommun1);
            yield return new WaitForSeconds(1);
        }
        m_Network_AudioController.SetIsFinishPlayingPlacingBook(true); 
    }

    private IEnumerator WaitDialogueFinishSas()
    {
        while (m_stateSas1 != FMOD.Studio.PLAYBACK_STATE.STOPPED)
        {
            m_instanceSas1.getPlaybackState(out m_stateSas1);
            yield return new WaitForSeconds(1);
        }
        m_Network_AudioController.SetIsFinishPlayingCorals(true);
    }


    private void LateUpdate()
    {
        if(m_isStartedDescent && !m_canMove)
        {
            if (!sound1Finish)
            {
                m_instanceDialogue1.getPlaybackState(out m_state1);
                if (m_state1 == FMOD.Studio.PLAYBACK_STATE.STOPPED)
                {
                    sound1Finish = true;
                }
            }
            else if (!sound2Finish)
            {
                m_instanceDialogue2.getPlaybackState(out m_state2);
                if (m_state2 == FMOD.Studio.PLAYBACK_STATE.STOPPED)
                {
                    sound2Finish = true;
                }
            }
        }
    }

    [ServerCallback]
    protected override void OnCompleteEnigma()
    {
        Debug.Log("[EnigmaManager_Lobby:OnCompleteEnigma]");
        RpcOpenCapsuleDoor(m_capsuleP1);
        RpcOpenCapsuleDoor(m_capsuleP2);
        StartCoroutine(WaitRemovePlayerFromCapsule());
        m_nextEnigmaPart.StartEnigmaManager(m_EnigmaId);
        EndEnigmaManager();
        //DestroyLobby();
    }

    IEnumerator WaitRemovePlayerFromCapsule()
    {
        yield return new WaitForSeconds(2);
        RpcRemovePlayerFromCapsule();
    }


    protected override void OnFailEnigma()
    {
        Debug.Log("[EnigmaManager_Lobby:OnFailEnigma]");
    }

    protected override void SendHelp()
    {
        Debug.Log("[EnigmaManager_Lobby:SendHelp]");
    }

    [ClientRpc]
    private void RpcActiveSkybox()
    {
        Debug.Log("[EnigmaManager_Lobby:RpcActiveSkybox]");
        ActiveSkybox();
    }


    [ClientRpc]
    private void RpcMoveBoat()
    {
        //Debug.Log("[EnigmaManager_Lobby:RpcMoveBoat]");
        MoveBoat();
    }
    
    [ClientRpc]
    private void RpcRotateSkyBox()
    {
        //Debug.Log("[EnigmaManager_Lobby:RpcRotateSkyBox]");
        if(RenderSettings.skybox)
        {
            RotateSkyBox();
        }
    }


    [ClientRpc]
    private void RpcSetupIsInWater()
    {
        RemoveSkybox();
        ActivePostProcessing();
        DestroyThunderScript();
        StopWaveSound();
        FMODUnity.RuntimeManager.PlayOneShot("event:/Ambiance/Plongeon");
        m_instanceAbysse.start();
        
    }

    [ClientRpc]
    private void RpcCloseCapsuleDoor(GameObject capsule)
    {
        CloseCapsuleDoor(capsule);
        FMODUnity.RuntimeManager.PlayOneShot("event:/Ambiance/PorteCapsule");
    }

    [ClientRpc]
    private void RpcOpenCapsuleDoor(GameObject capsule)
    {
        OpenCapsuleDoor(capsule);
    }

    [ClientRpc]
    private void RpcRemovePlayerFromCapsule()
    {
        RemovePlayerFromCapsule();
    }

    [ClientRpc]
    private void RpcInstanceAudioStart(int nbrOfDialogue)
    {
        switch (nbrOfDialogue)
        {
            case 1:
                Debug.Log("Dialogue 1");
                m_videoPlayerP1.Play();
                m_videoPlayerP2.Play();

                m_instanceDialogue1.start();
                break;
            case 2:
                Debug.Log("Dialogue 2");
                m_instanceDialogue2.start();
                break;
            case 3:
                Debug.Log("Dialogue 3");
                m_instanceDialogue3.start();
                break;
        }
    }

    

    [ClientRpc]
    private void RpcDialogueLight()
    {
        Debug.Log("RpcDialogueLight");
        m_instanceDialogue2.start();
        StopLightCapsule();
        StartCoroutine(MoveMonster());
    }

    [ClientRpc]
    private void RpcDialogueMonster()
    {
        Debug.Log("RpcDialogueMonster");
        m_instanceDialogue3.start();
        StartCoroutine(MoveMonsterScreem());
        StartCoroutine(FadeOut());
        if (m_capsuleP1.GetComponentInChildren<NetworkPlayer>() != null)
        {
            ChangePlayerVision(m_capsuleP1.GetComponentInChildren<NetworkPlayer>().GetComponent<NetworkIdentity>());
        }
        if (m_capsuleP2.GetComponentInChildren<NetworkPlayer>() != null)
        {
            ChangePlayerVision(m_capsuleP2.GetComponentInChildren<NetworkPlayer>().GetComponent<NetworkIdentity>());
        }
        //@todo désactiver le son de disonance
    }

    IEnumerator WaitMoveCapsule()
    {
        RpcInstanceAudioStart(1);
        yield return new WaitForSeconds(15);
        StartDecent();
        yield return new WaitForSeconds(20);
        m_canMove = false;
    }


    IEnumerator FadeOut()
    {
        float intensity = m_vignette.intensity.value;
        float postExposure = m_colorGrading.postExposure.value;
        Debug.Log("Fade | m_colorGrading.postExposure.value / intensity" + m_colorGrading.postExposure.value + "" + intensity);
        if (m_colorGrading.postExposure.value == 0)
        {
            Debug.Log("FadeOut");
            while(postExposure> -100)
            {
                intensity += 0.01f;
                postExposure--;
                FadeOut(intensity, postExposure);
                yield return new WaitForSeconds(0.1f);
            }
        }
        else
        {
            Debug.Log("FadeIn");
            while (postExposure < 0)
            {
                intensity -= 0.01f;
                postExposure++;
                FadeOut(intensity, postExposure);
                yield return new WaitForSeconds(0.1f);
            }
            m_PostProcessVolumeWater.enabled = false;
            m_instanceSas1 = FMODUnity.RuntimeManager.CreateInstance(EventSas1);
            m_instanceSas1.start();
            if (isServer)
            {
                StartCoroutine(WaitDialogueFinishSas());
            }
        }
        yield return true;
    }

    private void RemoveSkybox()
    {
        RenderSettings.skybox = null;
        foreach (Light light in m_light)
        {
            light.enabled = false;
        }
    }

    private void ActiveSkybox()
    {
        RenderSettings.skybox = m_SkyboxMat;
        foreach (Light light in m_light)
        {
            light.enabled = true;
        }
    }

    private void InWater()
    {
        if(m_IsInWater)
        {
            RpcSetupIsInWater();
            m_IsSetupWater = true;
        }
    }

    private void MoveBoat()
    {
        // Marche avant 
        if (m_boat.transform.position.z < m_lastPositionMin.z && m_direction == -1)
        {
            TurnBoat(90);
            m_direction = 1;
        }

        //Marche arriere
        if (m_boat.transform.position.z > m_lastPositionMax.z && m_direction == 1)
        {
            TurnBoat(-90);
            m_direction = -1;

        }

        m_boat.transform.position = m_boat.transform.position + new Vector3(0, 0, m_speedBoat * m_direction * Time.deltaTime);
    }

    private void TurnBoat(int rotation)
    {
        var rotationVector = m_boat.transform.rotation.eulerAngles;
        rotationVector.y = rotation;
        m_boat.transform.rotation = Quaternion.Euler(rotationVector);
    }

    private void RotateSkyBox()
    {

        RenderSettings.skybox.SetFloat("_Rotation", Time.time* m_rotateSkyboxSpeed);
    }

    private void StartDecent()
    {
        m_isStartedDescent = true;
        RpcSoundChain();
    }

    [ClientRpc]
    private void RpcSoundChain()
    {
        FMODUnity.RuntimeManager.PlayOneShot(EventChain);
    }

    [ServerCallback]
    private void MoveCapsule()
    {

        if (m_capsuleP1.transform.position != m_capsuleEndPositionP1.position && m_capsuleP2.transform.position != m_capsuleEndPositionP2.position)
        {
            m_capsuleP1.transform.position = Vector3.MoveTowards(m_capsuleP1.transform.position, m_capsuleEndPositionP1.position, Time.deltaTime * m_capsuleSpeed);
            m_capsuleP2.transform.position = Vector3.MoveTowards(m_capsuleP2.transform.position, m_capsuleEndPositionP2.position, Time.deltaTime * m_capsuleSpeed);
            if (Mathf.Approximately(m_capsuleP1.transform.position.x, m_capsuleEndPositionP1.transform.position.x)
                && Mathf.Approximately(m_capsuleP1.transform.position.y, m_capsuleEndPositionP1.transform.position.y)
                && Mathf.Approximately(m_capsuleP1.transform.position.z, m_capsuleEndPositionP1.transform.position.z)) // if current position is the average target position
            {
                m_capsuleP1.transform.position = m_capsuleEndPositionP1.transform.position; // force position
            }
            if (Mathf.Approximately(m_capsuleP2.transform.position.x, m_capsuleEndPositionP2.transform.position.x)
                && Mathf.Approximately(m_capsuleP2.transform.position.y, m_capsuleEndPositionP2.transform.position.y)
                && Mathf.Approximately(m_capsuleP2.transform.position.z, m_capsuleEndPositionP2.transform.position.z)) // if current position is the average target position
            {
                m_capsuleP2.transform.position = m_capsuleEndPositionP2.transform.position; // force position
            }
        }
        else
        {
            Debug.Log("Touch Down");
            RpcTouchDown();
            OnUpdateEnigma(m_capsuleP1.GetComponent<NetworkIdentity>(), true);
            OnUpdateEnigma(m_capsuleP2.GetComponent<NetworkIdentity>(), true);
        }
    }

    [ClientRpc]
    private void RpcTouchDown()
    {
        Debug.Log("RpcTouchDown");
        m_PostProcessVolume.enabled = true;
        StartCoroutine(FadeOut());
        m_instanceAbysse.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        m_instanceAbysse.release();
    }

    IEnumerator MoveMonster()
    {
        while(m_monster.transform.position != m_monsterDescentePos.position)
        {
            m_monster.transform.position = Vector3.MoveTowards(m_monster.transform.position, m_monsterDescentePos.position, Time.deltaTime * m_MonsterSpeed);
            yield return new WaitForEndOfFrame();
        }  
    }

    IEnumerator MoveMonsterScreem()
    {
        while (m_monsterScreem.transform.position != m_monsterScreemPos.position)
        {
            m_monsterScreem.transform.position = Vector3.MoveTowards(m_monsterScreem.transform.position, m_monsterScreemPos.position, Time.deltaTime * m_MonsterScreemSpeed);
            yield return new WaitForEndOfFrame();
        }
    }

    private void ActivePostProcessing()
    {
        m_PostProcessVolumeWater.enabled = true;
    }

    private void ChangePlayerVision(NetworkIdentity player)
    {
        if(player != null && player.isLocalPlayer)
        {
            Debug.Log("[EnigmaManager_Lobby:ChangePlayerVision(" + player.name + "]");
            PostProcessVolumeController ppvc = player.GetComponent<PostProcessVolumeController>();
            ppvc.SetGlobal(true);
            ppvc.SetWeight(0.95f);
            ppvc.SetVignetteIntensity(0.2f);
            ppvc.ActiveColorGradding(true);
        }
    }

    private void DestroyLobby()
    {
        Destroy(m_Lobby);
    }

    private void DestroyThunderScript()
    {
        Destroy(this.GetComponent<Thunders>());
    }

    private void CloseCapsuleDoor(GameObject capsule)
    {
        capsule.transform.FindChild("Capsule").transform.FindChild("Vitre").GetComponent<Animator>().SetBool("IsClose", true);
    }

    private void OpenCapsuleDoor(GameObject capsule)
    {
        capsule.transform.FindChild("Capsule").transform.FindChild("Vitre").GetComponent<Animator>().SetBool("IsClose", false);
    }

    private void CheckIfCloseCapsule()
    {
        RpcLockAreaBoat();
        RpcCloseCapsuleDoor(m_capsuleP1);
        RpcCloseCapsuleDoor(m_capsuleP2);
    }

    private void RemovePlayerFromCapsule()
    {
        //m player = GameObject.Find("VrGameObject").transform.parent.GetComponent<m>();
        //player.transform.parent = null;

        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach(GameObject p in players)
        {
            p.transform.parent = null;
        }
    }


    private void Dialogue(int numDialogue)
    {
        RpcInstanceAudioStart(numDialogue);
    }

    private void StopLightCapsule()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/Ambiance/LumieresCapsule");
        Debug.Log("EnigmaManager_Lobby:StopLightCapsule");
        foreach (Light light in m_lightCapsuleP1)
        {
            light.intensity = 0;
        }
        foreach (Light light in m_lightCapsuleP2)
        {
            light.intensity = 0;
        }
    }

    private void StartWaveSound()
    {
        m_instanceWave.start();
    }

    private void StopWaveSound()
    {
        m_instanceWave.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        m_instanceWave.release();
    }

    private void FadeOut(float myIntensity, float myPostExposure)
    {
        //intensity 0 | 1
        m_vignette.intensity.value = myIntensity;

        //postExposure 0 | -90
        m_colorGrading.postExposure.value = myPostExposure;
    }

    //SETTER

    public void SetIsInCapsuleP1(bool IsInCapsule)
    {
        Debug.Log("P1InCapsule");
        m_IsInCapsuleP1 = IsInCapsule;
        RpcLockTp(IsInCapsule, false);
    }

    public void SetIsInCapsuleP2(bool IsInCapsule)
    {
        Debug.Log("P2InCapsule");
        m_IsInCapsuleP2 = IsInCapsule;
        RpcLockTp(IsInCapsule, true);
    }

    public void SetIsInWater(bool IsInWater)
    {
        m_IsInWater = true;
    }

    
    public void Dialogues()
    {
        //m_instanceDialogueLobbyEva.setParameterByName("Coupe", 1f);
        //m_coupe = true;
        m_instanceDialogueLobbyEva.setParameterByName("Transition", 1f);
        int position = 0;
        m_instanceDialogueLobbyEva.getTimelinePosition(out position);
        Debug.Log("Yann " + position);
        if (position >= 0 && position < 7500)
        {
            m_instanceDialogueLobbyEva.setParameterByName("Coupe1", 1f);
            Debug.Log("Yann Revient 1");
            //m_instanceDialogueLobbyEva.setParameterByName("Transition", 0f);
            m_instanceDialogueLobbyEva.setParameterByName("Revient", 1f);
        }
        if (position >= 7500 && position < 10000)
        {
            Debug.Log("Yann Revient 2");
            m_instanceDialogueLobbyEva.setParameterByName("Coupe2", 1f);
            //m_instanceDialogueLobbyEva.setParameterByName("Transition", 0f);
            m_instanceDialogueLobbyEva.setParameterByName("Revient", 2f);
        }
        if (position >= 10000 && position < 18200)
        {
            Debug.Log("Yann Revient 3");
            m_instanceDialogueLobbyEva.setParameterByName("Coupe3", 1f);
            //m_instanceDialogueLobbyEva.setParameterByName("Transition", 0f);
            m_instanceDialogueLobbyEva.setParameterByName("Revient", 4f);
        }
        if (position >= 18200 && position < 21400)
        {
            Debug.Log("Yann Revient 4");
            m_instanceDialogueLobbyEva.setParameterByName("Coupe4", 1f);
            //m_instanceDialogueLobbyEva.setParameterByName("Transition", 0f);
            m_instanceDialogueLobbyEva.setParameterByName("Revient", 5f);
        }
        if (position >= 21400 && position < 36100)
        {
            Debug.Log("Yann Revient 5");
            m_instanceDialogueLobbyEva.setParameterByName("Coupe5", 1f);
            //m_instanceDialogueLobbyEva.setParameterByName("Transition", 0f);
            m_instanceDialogueLobbyEva.setParameterByName("Revient", 8f);
        }
        if (position >= 36100 && position < 54600)
        {
            Debug.Log("Yann Revient 6");
            m_instanceDialogueLobbyEva.setParameterByName("Coupe6", 1f);
            //m_instanceDialogueLobbyEva.setParameterByName("Transition", 0f);
            m_instanceDialogueLobbyEva.setParameterByName("Revient", 9f);
        }
        if (position >= 54600 && position < 69000)
        {
            Debug.Log("Yann Revient 7");
            m_instanceDialogueLobbyEva.setParameterByName("Coupe7", 1f);
            //m_instanceDialogueLobbyEva.setParameterByName("Transition", 0f);
            m_instanceDialogueLobbyEva.setParameterByName("Revient", 10f);
        }
        if (position >= 69000 && position <= 80500)
        {
            Debug.Log("Yann Revient 8");
            m_instanceDialogueLobbyEva.setParameterByName("Coupe8", 1f); 
            m_instanceDialogueLobbyEva.setParameterByName("CoupeFin", 1f);
            //m_instanceDialogueLobbyEva.setParameterByName("Transition", 0f);
        }

    }

    public bool GetIsCloseCapsule()
    {
        return m_CloseCapsule;
    }

    [ServerCallback]
    void OnTriggerEnter(Collider coll)
    {
        Debug.Log("Yann Trigger Lobby");
        Debug.Log("Yann collision : " + coll.name.Equals("BodyTrigger"));
        Debug.Log("Yann verifTrigger : " + verifTrigger);
        if (coll.name.Equals("BodyTrigger") && verifTrigger == false)
        {
            Dialogues();
            verifTrigger = true;
        }
    }


    [ClientRpc]
    private void RpcDialogueCommun1()
    {
        m_instanceDialogueCommun1 = FMODUnity.RuntimeManager.CreateInstance(EventDialogueCommun1);
        m_instanceDialogueCommun1.start();
        if (isServer)
        {
            StartCoroutine(WaitDialogueFinish());
        }
    }

    [ClientRpc]
    private void RpcDialogueReturn()
    {
        if (nombrePlayers == 1)
        {
            int position = 0;
            m_instanceDialogueLobbyEva.getTimelinePosition(out position);
            m_instanceDialogueLobbyEva.getPlaybackState(out m_stateEva);
            //Debug.Log("Yann Où en est EVA Avant " + position);
            if ((m_stateEva == FMOD.Studio.PLAYBACK_STATE.STOPPED  || position > 118000) && !m_Network_AudioController.GetIsFinishPlayingEva())
            {
                Debug.Log("Yann Update Eva");
                m_Network_AudioController.SetIsFinishPlayingEva(true);
                Debug.Log("Yann Où en est EVA Après " + m_stateEva);
            }
        }
        if (nombrePlayers == 0)
        {
            m_instanceDialogueLobbyJacob.getPlaybackState(out m_stateJacob);
            if (m_stateJacob == FMOD.Studio.PLAYBACK_STATE.STOPPED && !m_Network_AudioController.GetIsFinishPlayingJacob())
            {
                Debug.Log("Yann Update Jacob");

                if (player2 == null)
                {
                    player2 = GameObject.Find("VrGameObject").transform.parent.gameObject;
                    Debug.Log("Yann player" + player2.name);
                }


                player2.GetComponent<NetworkPlayer>().CmdSetAuth(m_Network_AudioController.GetComponent<NetworkIdentity>(), player2.GetComponent<NetworkIdentity>());
                if (m_Network_AudioController.hasAuthority)
                {
                    m_Network_AudioController.SetIsFinishPlayingJacob(true);
                }
            }
        }
    }

    [Command]
    private void CmdLockTp(bool status, bool isP2)
    {
        Debug.Log("[EnigmaManager_Lobby | CmdLockTp] status : " + status + " | isP2 : " + isP2);
        RpcLockTp(status,isP2);
    }

    [ClientRpc]
    private void RpcLockTp(bool status, bool isP2)
    {
        Debug.Log("[EnigmaManager_Lobby | RpcLockTp] status : " + status + " | isP2 : " + isP2);
        if(isP2)
        {
            m_capsuleP2.GetComponentInChildren<TeleportPoint>().locked = status;
            m_capsuleP2.GetComponentInChildren<TeleportPoint>().markerActive = !status;
        }
        else
        {
            m_capsuleP1.GetComponentInChildren<TeleportPoint>().locked = status;
            m_capsuleP2.GetComponentInChildren<TeleportPoint>().markerActive = !status;
        }
    }

    [ClientRpc]
    private void RpcLockAreaBoat()
    {
        foreach(TeleportArea tp in m_teleportAreaBoat)
        {
            tp.locked = true;
            tp.markerActive = false;
        }
    }
}
