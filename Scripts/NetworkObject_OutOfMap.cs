﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[System.Obsolete]
public class NetworkObject_OutOfMap : NetworkBehaviour
{
    protected NetworkIdentity m_NetId;
    private Vector3 m_defaultPosition;
    private Quaternion m_defaultRotation;
    private Rigidbody m_defaultRigidbody;
    private RigidbodyConstraints m_defaultRigidbodyConstraints;
    private bool m_defaultUseGravity;
    private bool m_defaultIsKinematic;
    private float m_distanceMax;
    private bool m_isOutOfMap;

    // Start is called before the first frame update
    void Start()
    {
        m_NetId = GetComponent<NetworkIdentity>();
        m_distanceMax = 100.0f;
        m_defaultPosition = transform.position;
        m_defaultRotation = transform.rotation;
        if (TryGetComponent(out Rigidbody r))
        {
            m_defaultRigidbody = r;
            m_defaultRigidbodyConstraints = r.constraints;
            m_defaultUseGravity = r.useGravity;
            m_defaultIsKinematic = r.isKinematic;
        }
        else
        {
            m_defaultRigidbody = null;
        }
        StartCoroutine(CheckOutOfMap());
    }

    private IEnumerator CheckOutOfMap()
    {
        yield return new WaitUntil(() => IsTooFar() || m_isOutOfMap);
        if (hasAuthority && transform.root.tag != "Player")
        {
            m_isOutOfMap = false;
            Debug.Log("[NetworkObjectEvent:CheckOutOfMap:object=" + name + ":distance>" + m_distanceMax + "]");
            if (!isServer)
            {
                CmdResetOutOfMap(GameObject.Find("VrGameObject").GetComponentInParent<NetworkIdentity>());
            }
            else
            {
                ResetOutOfMap();
            }
        }
        StartCoroutine(CheckOutOfMap());
    }

    [Command]
    private void CmdResetOutOfMap(NetworkIdentity player)
    {
        GetComponent<NetworkIdentity>().RemoveClientAuthority(player.connectionToClient);
        ResetOutOfMap();
    }

    [ServerCallback]
    private void ResetOutOfMap()
    {
        Debug.Log("[NetworkObjectEvent:ResetOutOfMap]");
        if (m_defaultRigidbody != null)
        {
            m_defaultRigidbody.constraints = RigidbodyConstraints.FreezeAll;
            m_defaultRigidbody.velocity = new Vector3(0, 0, 0);
            m_defaultRigidbody.angularVelocity = new Vector3(0, 0, 0);
        }
        transform.rotation = m_defaultRotation;
        transform.position = m_defaultPosition;
        if (m_defaultRigidbody != null)
        {
            m_defaultRigidbody.isKinematic = m_defaultIsKinematic;
            m_defaultRigidbody.useGravity = m_defaultUseGravity;
            m_defaultRigidbody.constraints = m_defaultRigidbodyConstraints;
        }
    }

    private bool IsTooFar()
    {
        return Vector3.Distance(transform.position, m_defaultPosition) > m_distanceMax;
    }

    public void SetIsOutOfMap()
    {
        m_isOutOfMap = true;
    }
}
