﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class LoadLevelRoom1NoVR : MonoBehaviour
{
    [SerializeField] NetworkManager networkManager;
    [SerializeField] GameObject playerPrefab;
    public void LoadLevelRoom()
    {
        Debug.Log("NON VR");
        UnityEngine.XR.XRSettings.enabled = false;
        NetworkManager.singleton.playerPrefab = playerPrefab;
        NetworkManager.singleton.onlineScene = "similarRoomNonVr";
    }
}
