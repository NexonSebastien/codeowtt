﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundShield : MonoBehaviour
{
    private FMOD.Studio.EventInstance instance;
    private string Event = "event:/Champs/Champ";
    private bool verifEnable = false;

    // Start is called before the first frame update
    void Start()
    {
        instance = FMODUnity.RuntimeManager.CreateInstance(Event);
        //FMODUnity.RuntimeManager.PlayOneShotAttached("event:/Champs/ChampContinue", this.gameObject);
        instance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(this.gameObject));
        if (verifEnable == false)
        {
            instance.start();
            verifEnable = true;
        }
    }

    private void OnEnable()
    {
        //instance = FMODUnity.RuntimeManager.CreateInstance(Event);
        //instance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(this.gameObject));
        if (verifEnable == true)
        {
            instance.setParameterByName("Champ", 0f);
            instance.start();
            verifEnable = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StopSonShield()
    {
        instance.setParameterByName("Champ", 1f);
        WaitSoundFinish();
    }

    IEnumerator WaitSoundFinish()
    {
        yield return new WaitForSeconds(15);
        instance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        instance.release();
    }
}
