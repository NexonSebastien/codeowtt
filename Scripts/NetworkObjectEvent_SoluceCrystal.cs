﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[System.Obsolete]
public class NetworkObjectEvent_SoluceCrystal : NetworkObjectEvent
{
    [SerializeField] Material m_defaultMaterial;
    [SerializeField] List<Material> m_materials;

    override protected void Start()
    {
        base.Start();
    }

    public override void EnigmaReceiveEvent(int eventCase, List<object> args)
    {
        Debug.Log("[NetworkObjectEvent_SoluceCrystal:EnigmaReceiveEvent:eventCase="+eventCase+"]");
        if (isServer) // update the gameobject on server side
        {
            switch (eventCase)
            {
                case 1:
                    RpcSetPositionZ((float)args[0]); // update position
                    break;
                case 2:
                    RpcSetMaterial((int)args[0] - 1); // match crystal id with material index
                    break;
            }
        }
    }

    public override void EnigmaUpdateEvent()
    {
        throw new System.NotImplementedException();
    }

    [ClientRpc]
    public void RpcSetPositionZ(float posZ)
    {
        Debug.Log("[NetworkObjectEvent_SoluceCrystal:RpcSetPositionZ:posZ=" + posZ + "]");
        transform.position = new Vector3(transform.position.x, transform.position.y, posZ);
        // opti : faire translater l'object au lieu de le téléporter
    }

    [ClientRpc]
    public void RpcSetMaterial(int materialIndex)
    {
        if (materialIndex < 0 || materialIndex > m_materials.Count)
        {
            Debug.Log("[NetworkObjectEvent_SoluceCrystal:RpcSetMaterial:materialIndex=" + materialIndex + ":oldMaterial.name="+ GetComponentInChildren<Renderer>().sharedMaterial.name +":newMaterial.name="+ m_defaultMaterial .name+ "]");
            GetComponentInChildren<Renderer>().sharedMaterial = m_defaultMaterial;
        }
        else
        {
            Debug.Log("[NetworkObjectEvent_SoluceCrystal:RpcSetMaterial:materialIndex=" + materialIndex + ":oldMaterial.name=" + GetComponentInChildren<Renderer>().sharedMaterial.name + ":newMaterial.name=" + m_materials[materialIndex].name + "]");
            GetComponentInChildren<Renderer>().sharedMaterial = m_materials[materialIndex];
        }
        GetComponentInChildren<Renderer>().enabled = true;
    }
}
