﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Valve.VR.InteractionSystem;

[System.Obsolete]
public class TriggerSas : NetworkBehaviour
{
    [SerializeField] GameObject m_Door;
    [SerializeField] EnigmaManager_Sas m_ManagerSas = null;
    [SerializeField] bool isP2;
    [SerializeField] TeleportArea m_DisableTp = null;
    [SerializeField] private Network_AudioController m_netAudioController;
    private bool isPlayed;


    private void OnTriggerStay(Collider other)
    {
        if (isServer)
        {
            if (other.gameObject.name == "BodyTrigger" && m_netAudioController.GetIsFinishPlayingCorals() && !isPlayed)
            {
                if (m_Door.GetComponent<Animator>().GetBool("IsOpen"))
                {
                    RpcClose();
                    if (m_ManagerSas)
                    {
                        if (isP2)
                        {
                            m_ManagerSas.SetP2InSas(true);
                            m_ManagerSas.SetP2Sas(other.GetComponentInParent<NetworkPlayer>());
                        }
                        else
                        {
                            m_ManagerSas.SetP1InSas(true);
                            m_ManagerSas.SetP1Sas(other.GetComponentInParent<NetworkPlayer>());
                        }
                    }
                }
                else
                {
                    RpcOpen();
                }
                isPlayed = true;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(isServer)
        {
            if (other.gameObject.name == "BodyTrigger" && m_netAudioController.GetIsFinishPlayingCorals() && !isPlayed)
            {
                if (m_Door.GetComponent<Animator>().GetBool("IsOpen"))
                {
                    RpcClose();
                    if (m_ManagerSas)
                    {
                        if (isP2)
                        {
                            m_ManagerSas.SetP2InSas(true);
                            m_ManagerSas.SetP2Sas(other.GetComponentInParent<NetworkPlayer>());
                        }
                        else
                        {
                            m_ManagerSas.SetP1InSas(true);
                            m_ManagerSas.SetP1Sas(other.GetComponentInParent<NetworkPlayer>());
                        }
                    }
                }
                else
                {
                    RpcOpen();
                }
                isPlayed = true;
            }
        }
    }

    [ClientRpc]
    private void RpcClose()
    {
        Debug.Log("Yann ferme Sas");
        //Son de fermeture de la porte
        FMODUnity.RuntimeManager.PlayOneShotAttached("event:/Sas/FermeSas", m_Door);
        m_Door.GetComponent<Animator>().SetBool("IsOpen", false);
        if (m_DisableTp)
        {
            m_DisableTp.locked = true;
        }
    }

    [ClientRpc]
    private void RpcOpen()
    {
        Debug.Log("Yann ouvre Sas");
        //Son de l'ouverture de la porte
        FMODUnity.RuntimeManager.PlayOneShotAttached("event:/Sas/OuvreSas", m_Door);
        m_Door.GetComponent<Animator>().SetBool("IsOpen", true);
    }
}
