﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[System.Obsolete]
public class FunctionNetwork : NetworkBehaviour

{

    public void ActiveGravity()
    {
        if(this.GetComponent<Rigidbody>().useGravity == false)
        {
            CmdActiveGravity();
        }
    }

    [Command]
    public void CmdActiveGravity()
    {
        RpcActiveGravity();
    }

    [ClientRpc]
    private void RpcActiveGravity()
    {
        this.GetComponent<Rigidbody>().useGravity = true;
    }


    public void ActiveKinematic(bool status)
    {
        StartCoroutine(WaitForAuthority(status));
    }

    private IEnumerator WaitForAuthority(bool status)
    {
        yield return new WaitUntil(() => hasAuthority);
        if (!isServer)
        {
            CmdActiveKinematic(status);
        }
        else
        {
            RpcActiveKinematic(status);
        }
    }

    [Command]
    public void CmdActiveKinematic(bool status)
    {
        RpcActiveKinematic(status);
    }

    [ClientRpc]
    private void RpcActiveKinematic(bool status)
    {
        this.GetComponent<Rigidbody>().isKinematic = status;
    }
}
