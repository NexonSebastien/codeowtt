﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

[System.Obsolete]
public class GameStatsUI : NetworkBehaviour
{
    private PlayerMenuUI m_playerMenuUI;
    private Text m_endGameMessageText;
    private float m_fadeToAlpha;
    private float m_fadeDuration;
    private Color m_victoryTextColor;
    private Color m_defeatTextColor;
    private string m_victoryText;
    private string m_defeatText;

    void Awake()
    {
        //m_playerMenuUI = transform.GetComponent<PlayerMenuUI>();
        m_endGameMessageText = transform.GetChild(0).GetChild(0).GetComponent<Text>();
        HideEndGameMessage();
        m_fadeToAlpha = 0.0f;
        m_fadeDuration = 1.0f;
        m_victoryTextColor = Color.green;
        m_defeatTextColor = Color.red;
        m_victoryText = "You escaped !";
        m_defeatText = "You died !";
    }

    private void ShowEndGameMessage(bool victory, string message)
    {
        Debug.Log("[GameStatsUI:ShowEndGameMessage:victory=" + victory + "]");
        if (victory)
        {
            SetEndGameMessage(m_victoryText, message);
            SetTextColor(m_victoryTextColor);
        }
        else
        {
            SetEndGameMessage(m_defeatText, message);
            SetTextColor(m_defeatTextColor);
        }
        transform.GetChild(0).gameObject.SetActive(true);
        /*
        m_uiText.CrossFadeAlpha(m_fadeToAlpha, m_fadeDuration, true);
        m_uiText.color = new Color(m_uiText.color.r, m_uiText.color.g, m_uiText.color.b, m_fadeToAlpha);
        */
        //GameObject.Find("VrGameObject").transform.parent.GetComponent<m>().GetPlayerMenuUI().Show();
    }

    private void HideEndGameMessage()
    {
        Debug.Log("[GameStatsUI:HideUI]");
        m_endGameMessageText.gameObject.SetActive(false);
    }

    private void SetEndGameMessage(string text, string message)
    {
        m_endGameMessageText.text = text + "\n" + message;
    }

    private void SetTextColor(Color color)
    {
        m_endGameMessageText.color = color;
    }

    [Command]
    public void CmdShowEndGameMessage(bool victory, string message)
    {
        RpcShowEndGameMessage(victory, message);
    }

    [Command]
    public void CmdHideEndGameMessage()
    {
        RpcHideEndGameMessage();
    }

    [ClientRpc]
    public void RpcShowEndGameMessage(bool victory, string message)
    {
        Debug.Log("[GameStatsUI:RpcShowEndGameMessage:victory=" + victory + ":message=" + message + "]");
        ShowEndGameMessage(victory, message);
    }

    [ClientRpc]
    public void RpcHideEndGameMessage()
    {
        Debug.Log("[GameStatsUI:RpcHideEndGameMessage]");
        HideEndGameMessage();
    }
}
