﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Valve.VR.InteractionSystem;

[System.Obsolete]
public class EnigmaManager_ApollonRoom : EnigmaManager
{
    [SerializeField] WaterManagement m_Water;
    [SerializeField] GameObject m_ShieldPlayer1;
    [SerializeField] GameObject m_ShieldPlayer1_Corridor;
    [SerializeField] GameObject m_ShieldPlayer2;
    [SerializeField] GameObject m_ShieldPlayer2_Corridor;

    [SerializeField] GameObject m_allLightP1;
    [SerializeField] GameObject m_allLightP2;
    [SerializeField] GameObject m_allLightBoth;

    [SerializeField] GameObject m_teleportAreas;

    private string Event = "event:/Musiques/Enigme2Ok";
    public GameObject m_AudioS;
    private bool m_IsWaterDown;
    private int nbrCrisHit;
    private int nbrPlay;

    private Network_AudioController m_netAudioController;
    private string EventDialogueSalle2Fin = "event:/Dialogues/Salle2Fin";
    private FMOD.Studio.PLAYBACK_STATE m_stateDialogueSalle2Fin;
    private FMOD.Studio.EventInstance m_instanceDialogueSalle2Fin;

    [SerializeField] private SoundShield ShieldOff1;
    [SerializeField] private SoundShield ShieldOff2;
    [SerializeField] private TeleportArea m_bothTp;
    [SerializeField] private TeleportArea m_P1Tp;
    [SerializeField] private TeleportArea m_P2Tp;
    private bool isActive = false;


    private void Start()
    {
        nbrPlay = 0;
        nbrCrisHit = 0;
        m_IsWaterDown = false;
        m_netAudioController = GameObject.Find("Network_AudioController").GetComponent<Network_AudioController>();
    }

    private void Update()
    {   
        if(isServer)
        {
            if (m_netAudioController.GetIsFinishApollonRoom()&&!isActive)
            {
                isActive = true;
                RpcTurnOnTeleportAreasAfterDialogue();
            }
        } 
        if (isServer && !m_IsWaterDown)
        {
            m_IsWaterDown = m_Water.IsFinished();
            if (m_IsWaterDown)
            {
                RpcTurnOnLight();
                RpcTurnOnTeleportAreas();
                RpcTurnOffShields();
                RpcDialogueFinEnigmeApollon();
                m_GameManager.OnCompleteEnigma(m_EnigmaId);
            }
            else if (m_Water.IsPlayerDrown())
            {
                OnFailEnigma();
            }
        }
    }

    [ServerCallback]
    protected override void OnCompleteEnigma()
    {
        Debug.Log("[EnigmaManager_ApollonRoom:OnCompleteEnigma]");
        RpcSetWaterDown();
        //m_GameManager.OnCompleteEnigma(this);
    }

    protected override void OnFailEnigma()
    {
        Debug.Log("[EnigmaManager_ApollonRoom:OnFailEnigma]");
        m_GameManager.OnFailEnigma(m_EnigmaId, "One of you drowned ...");
        EndEnigmaManager();
        m_Water.RpcSetWaterFinished();
    }

    protected override void SendHelp()
    {
        Debug.Log("[EnigmaManager_ApollonRoom:SendHelp]");
    }

    [ClientRpc]
    public void RpcTurnOffShields()
    {
        FMODUnity.RuntimeManager.PlayOneShot(Event);


        ShieldOff1.StopSonShield();
        ShieldOff2.StopSonShield();

        m_ShieldPlayer1.SetActive(false);
        m_ShieldPlayer2.SetActive(false);

        m_ShieldPlayer1_Corridor.SetActive(false);
        m_ShieldPlayer1_Corridor.GetComponent<SoundShield>().StopSonShield();
        m_ShieldPlayer2_Corridor.SetActive(false);
        m_ShieldPlayer2_Corridor.GetComponent<SoundShield>().StopSonShield();
    }

    [ClientRpc]
    public void RpcSetWaterDown()
    {
        m_Water.setWaterDown();
    }

    [ClientRpc]
    public void RpcTurnOnTeleportAreas()
    {
        Debug.Log("[EnigmaManager_ApollonRoom:RpcTurnOnTeleportAreas");

        // turn active children
        for (int i = 0; i < m_teleportAreas.transform.childCount; i++)
        {
            m_teleportAreas.transform.GetChild(i).gameObject.SetActive(true);
        }


        m_P1Tp.SetLocked(false);
        m_P1Tp.markerActive = true;

        m_P2Tp.SetLocked(false);
        m_P2Tp.markerActive = true;

    }

    [ClientRpc]
    public void RpcTurnOnLight()
    {
        StartCoroutine(WaitStopLight(m_allLightP1));
        StartCoroutine(WaitStopLight(m_allLightP2));
        StartCoroutine(WaitStopLight(m_allLightBoth));
    }

    [ClientRpc]
    private void RpcDialogueFinEnigmeApollon()
    {
        m_instanceDialogueSalle2Fin = FMODUnity.RuntimeManager.CreateInstance(EventDialogueSalle2Fin);
        m_instanceDialogueSalle2Fin.start();
        if (isServer)
        {
            StartCoroutine(WaitDialogueFinish());
        }
    }

    private IEnumerator WaitDialogueFinish()
    {
        while (m_stateDialogueSalle2Fin != FMOD.Studio.PLAYBACK_STATE.STOPPED)
        {
            m_instanceDialogueSalle2Fin.getPlaybackState(out m_stateDialogueSalle2Fin);
            yield return new WaitForSeconds(1);
        }
        m_netAudioController.SetIsFinishApollonRoom(true);
    }

    [ClientRpc]
    private void RpcTurnOnTeleportAreasAfterDialogue()
    {
        m_bothTp.SetLocked(false);
        m_bothTp.markerActive = true;
    }

    IEnumerator WaitStopLight(GameObject light)
    {
        for (int i = 0; i < light.gameObject.transform.childCount; i++)
        {
            light.gameObject.transform.GetChild(i).gameObject.GetComponent<Light>().enabled = true;
            yield return new WaitForSeconds(0.5f);

        }
        yield return 0;
    }

    public void IncrementNbrCristHit()
    {
        nbrCrisHit++;
    }

    public int GetNbrCristHit()
    {
        return nbrCrisHit;
    }

    public void IncrementNbrPlay()
    {
        nbrPlay++;
    }

    public int GetNbrCristPlay()
    {
        return nbrPlay;
    }

}
