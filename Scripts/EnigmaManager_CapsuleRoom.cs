﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using Valve.VR.InteractionSystem;

[System.Obsolete]
public class EnigmaManager_CapsuleRoom : EnigmaManager
{
    [SerializeField] private EnigmaManager_SecretEnigma m_secretEnigma;
    private NetworkObjectEvent_PlacingObject m_crystal;
    private Dictionary<NetworkObjectEvent_Capsule, bool> m_capsulesWithPlayer;
    private bool m_ready;
    private FMOD.Studio.PLAYBACK_STATE m_stateDialogueStone;
    private FMOD.Studio.EventInstance m_instanceDialogueStone;
    [SerializeField] private GameObject m_audioSource;
    [SerializeField] private AudioClip m_scientistDialog;
    [SerializeField] private float m_scientistDialogTime;
    [SerializeField] private GameObject m_teleportArea;
    [SerializeField] private NetworkObjectEvent_Monster m_monster;
    [SerializeField] private Network_AudioController m_NetworkAudioController;
    private bool m_isPlayDialogueStone = false;

    private void Start()
    {
        m_capsulesWithPlayer = new Dictionary<NetworkObjectEvent_Capsule, bool>();
        m_ready = false;
        if (float.IsNaN(m_scientistDialogTime) || m_scientistDialogTime==0) m_scientistDialogTime = m_scientistDialog.length;
        StartCoroutine(InitVars());
    }

    private IEnumerator InitVars()
    {
        yield return new WaitForEndOfFrame();
        foreach (NetworkIdentity ni in m_EnigmaObjectsNI)
        {
            if (ni.TryGetComponent(out NetworkObjectEvent_PlacingObject po))
            {
                m_crystal = po;
            }
            else if(ni.TryGetComponent(out NetworkObjectEvent_Capsule c))
            {
                m_capsulesWithPlayer.Add(c, false);
            }
        }
        m_ready = true;
        yield return null;
    }

    private void Update()
    {
        if (isServer && m_ready && IsRunning())
        {
            CheckEnigmaPartiallyCompleted();
        }
    }

    private IEnumerator PreCompleteEnigma()
    {

        float startDialogTime = Time.time;
        RpcLockTeleportArea();
        yield return new WaitUntil(() => m_NetworkAudioController.GetIsFinishPlacingStone());
        RpcScientistDialog();
        for (int i = 0; i < m_capsulesWithPlayer.Count; i++)
        {
            if (m_capsulesWithPlayer.ElementAt(i).Value == true)
            {
                m_capsulesWithPlayer.ElementAt(i).Key.RpcCloseCapsule();
                m_capsulesWithPlayer.ElementAt(i).Key.RpcMoveCapsule();
            }
        }
        m_monster.StartRunning(true);
        //yield return new WaitUntil(() => (Time.time - startDialogTime) >= m_scientistDialogTime);
        yield return new WaitForSeconds(m_scientistDialogTime);
        m_monster.StopRunning();
        m_GameManager.OnCompleteEnigma(m_EnigmaId);
    }

    protected override void OnCompleteEnigma()
    {
        Debug.Log("[EnigmaManager_CapsuleRoom:OnCompleteEnigma]");
        StartCoroutine(PreCompleteEnigma());
    }

    protected override void OnFailEnigma()
    {
        throw new System.NotImplementedException();
    }

    protected override void SendHelp()
    {
        throw new System.NotImplementedException();
    }

    public void OnPlayerEnterCapsule(NetworkObjectEvent_Capsule capsule)
    {
        if (m_capsulesWithPlayer.Keys.Contains(capsule))
        {
            m_capsulesWithPlayer[capsule] = true;
            if(!m_isPlayDialogueStone)
            {
                RpcDialogueStone();
                m_isPlayDialogueStone = true;
            }   
        }
    }


    [ClientRpc]
    private void RpcDialogueStone()
    {
        if(m_secretEnigma.IsRunning())
        {
            m_instanceDialogueStone = FMODUnity.RuntimeManager.CreateInstance("event:/Dialogues/FinNegative2");
            m_instanceDialogueStone.start();
        }
        else
        {
            m_instanceDialogueStone = FMODUnity.RuntimeManager.CreateInstance("event:/Dialogues/FinPositive2");
            m_instanceDialogueStone.start();
        }
        if(isServer)
        {
            StartCoroutine(WaitDialogueStoneFinish());
        }
    }

    private IEnumerator WaitDialogueStoneFinish()
    {
        while (m_stateDialogueStone != FMOD.Studio.PLAYBACK_STATE.STOPPED)
        {
            m_instanceDialogueStone.getPlaybackState(out m_stateDialogueStone);
            yield return new WaitForSeconds(1);
        }
        m_NetworkAudioController.SetIsFinishPlacingStone(true);
    }

    public void OnPlayerExitCapsule(NetworkObjectEvent_Capsule capsule)
    {
        if (m_capsulesWithPlayer.Keys.Contains(capsule))
        {
            m_capsulesWithPlayer[capsule] = false;
        }
    }

    private void CheckEnigmaPartiallyCompleted()
    {
        //Debug.Log("[EnigmaManager_CapsuleRoom:CheckEnigmaPartiallyCompleted:m_secretEnigma.IsRunning()=" + m_secretEnigma.IsRunning() + ":m_crystal.m_isWellPlaced:" + m_crystal.m_isWellPlaced + ":m_capsulesWithPlayer.Values.Contains(true)=" + m_capsulesWithPlayer.Values.Contains(true) + "]");
        if (m_secretEnigma.IsRunning() && m_capsulesWithPlayer.Values.Contains(true) && m_crystal.m_isWellPlaced
             && m_crystal.transform.parent.parent.parent.parent.parent.parent.parent.parent.parent.parent.parent.GetComponent<NetworkObjectEvent_Capsule>().GetPlayer() != null)
        {
            EndEnigmaManager();
            OnCompleteEnigma();
        }
    }

    [ClientRpc]
    public void RpcScientistDialog()
    {
        m_crystal.gameObject.GetComponent<SoundRuby>().StopSon();
        StartCoroutine(ScientistDialog());
    }

    private IEnumerator ScientistDialog()
    {
        Debug.Log("[EnigmaManager_CapsuleRoom:ScientistDialog]");
        if(m_secretEnigma.IsRunning())
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/Dialogues/FinSynchNeg");
        }
        else
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/Dialogues/FinSynchPos");
        }
        yield return null;
    }

    [ClientRpc]
    private void RpcLockTeleportArea()
    {
        m_teleportArea.GetComponent<TeleportArea>().locked = true;
    }
}
