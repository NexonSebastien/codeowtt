﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MyGameManager : NetworkBehaviour
{
    [SerializeField] MyNetworkObject m_Object;
    [SerializeField] bool m_Status;
    [SerializeField] int m_Int;

    // Start is called before the first frame update
    void Start()
    {
        m_Int = 0;
        m_Status = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateObjectStatus(bool newStatus)
    {
        m_Status = newStatus;
    }

    public void UpdateObjectStatus(NetworkIdentity id, bool status)
    {
        if (m_Object.GetComponent<NetworkIdentity>().Equals(id))
        {
            m_Status = status;
            if (m_Status)
            {
                m_Object.SetColor(Random.ColorHSV());
            }
        }
    }
    public void UpdateObjectStatus(NetworkIdentity id, int status)
    {
        if (m_Object.GetComponent<NetworkIdentity>().Equals(id))
        {
            m_Int = status;
        }
    }
}
