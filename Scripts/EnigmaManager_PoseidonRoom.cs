﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Valve.VR.InteractionSystem;

[System.Obsolete]
public class EnigmaManager_PoseidonRoom : EnigmaManager
{
    [SerializeField] GameObject m_ShieldPlayer1;
    [SerializeField] GameObject m_ShieldPlayer2;
    public FMOD.Studio.EventInstance instanceDoor;
    private string EventDoor = "event:/Champs/Champ";
    private string EventMusic = "event:/Musiques/Enigme1Ok";
    [SerializeField] private TeleportArea m_teleportAreaP1;
    [SerializeField] private TeleportArea m_teleportAreaP2;
    //Son
    private string TriggerDialogue = "TriggerDialogue";
    private bool triggerOn = false;

    public FMOD.Studio.EventInstance instance;

    [SerializeField] private SoundShield ShieldOff1;
    [SerializeField] private SoundShield ShieldOff2;
    [SerializeField] private TriggerActiveCanHelp verifHelpSound;
    [SerializeField] private Network_AudioController m_network_AudioController;
    private string EventDialogueEndPoseidon = "event:/Dialogues/LobbyCommun2";
    private FMOD.Studio.PLAYBACK_STATE m_stateDialogueEndPoseidon;
    private FMOD.Studio.EventInstance m_instanceDialogueEndPoseidon;



    private void Start()
    {
        instanceDoor.setParameterByName("Champ", 1f);
    }

    [ServerCallback]
    protected override void OnCompleteEnigma()
    {
        Debug.Log("[EnigmaManager_PoseidonRoom:OnCompleteEnigma]");
        RpcTurnOffShields();
        RpcTurnOnTeleportAreas();
        verifHelpSound.RpcDesactiveHelp();
        RpcDialogueFinPoseidon();
        m_GameManager.OnCompleteEnigma(m_EnigmaId);
    }

    

    protected override void OnFailEnigma()
    {
        Debug.Log("[EnigmaManager_PoseidonRoom:OnFailEnigma]");
        m_GameManager.OnFailEnigma(m_EnigmaId, null);
    }

    protected override void SendHelp()
    {
    }

    [ClientRpc]
    public void RpcTurnOffShields()
    {
        ShieldOff1.StopSonShield();
        ShieldOff2.StopSonShield();
        FMODUnity.RuntimeManager.PlayOneShot(EventMusic);
        m_ShieldPlayer1.SetActive(false);
        m_ShieldPlayer2.SetActive(false);
    }

    [ClientRpc]
    public void RpcTurnOnTeleportAreas()
    {
        Debug.Log("[EnigmaManager_PoseidonRoom:RpcTurnOnTeleportAreas]");
        m_teleportAreaP1.SetLocked(false);
        m_teleportAreaP1.markerActive = true;

        m_teleportAreaP2.SetLocked(false);
        m_teleportAreaP2.markerActive = true;
    }

    [ClientRpc]
    private void RpcDialogueFinPoseidon()
    {
        if (verifHelpSound.GetVerifDialogueAide())
        {
            StartCoroutine(WaitHelpDialogue());
        }
        else
        {
            StartCoroutine(WaitDialogueFin());
        }
    }

    private IEnumerator WaitDialogueFin()
    {
        yield return new WaitForSeconds(12);
        m_instanceDialogueEndPoseidon = FMODUnity.RuntimeManager.CreateInstance("event:/Dialogues/Salle1Fin");
        m_instanceDialogueEndPoseidon.start();
        if (isServer)
        {
            StartCoroutine(WaitDialogueFinish());
        }
    }

    private IEnumerator WaitHelpDialogue()
    {
        yield return new WaitForSeconds(12);
        FMODUnity.RuntimeManager.PlayOneShot("event:/Dialogues/Salle1Coupee");
        yield return new WaitForSeconds(40);
        m_instanceDialogueEndPoseidon = FMODUnity.RuntimeManager.CreateInstance("event:/Dialogues/Salle1Fin");
        m_instanceDialogueEndPoseidon.start();
        if (isServer)
        {
            StartCoroutine(WaitDialogueFinish());
        }
    }

    private IEnumerator WaitDialogueFinish()
    {
        while (m_stateDialogueEndPoseidon != FMOD.Studio.PLAYBACK_STATE.STOPPED)
        {
            m_instanceDialogueEndPoseidon.getPlaybackState(out m_stateDialogueEndPoseidon);
            yield return new WaitForSeconds(1);
        }
        m_network_AudioController.SetIsFinishPoseidobRoom(true);
    }
}
