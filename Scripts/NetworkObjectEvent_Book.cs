﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Valve.VR.InteractionSystem;

[System.Obsolete]
public class NetworkObjectEvent_Book : NetworkObjectEvent_PlacingObject
{
    [SerializeField] private NetworkObjectEvent_Book m_otherBook;
    private bool m_validated;
    [SerializeField] private GameObject m_teleportAreas;
    private Network_AudioController m_netAudioController;
    private string EventDialogueCommun2 = "event:/Dialogues/LobbyCommun2";
    private FMOD.Studio.PLAYBACK_STATE m_stateDialogueCommun2;
    private FMOD.Studio.EventInstance m_instanceDialogueCommun2;

    private void Awake()
    {
        m_netAudioController = GameObject.Find("Network_AudioController").GetComponent<Network_AudioController>();
    }

    public override void EnigmaUpdateEvent()
    {
        base.EnigmaUpdateEvent();
        RpcTurnOnTeleportAreas();
        RpcScientistDialog();
    }

    private IEnumerator WaitDialogueFinish()
    {
        while (m_stateDialogueCommun2 != FMOD.Studio.PLAYBACK_STATE.STOPPED)
        {
            m_instanceDialogueCommun2.getPlaybackState(out m_stateDialogueCommun2);
            yield return new WaitForSeconds(1);
        }
        m_netAudioController.SetIsFinishPlayingPlacingHammer(true);
    }

    public bool IsValidated()
    {
        return m_validated;
    }

    protected override void ObjectPlaced(bool status, int placesIndex)
    {
        Debug.Log("[NetworkObjectEvent_PlacingObject:ObjectPlaced:status=" + status + "]");
        Debug.Log("[NetworkObjectEvent_PlacingObject:ObjectPlaced:m_netAudioController.GetIsFinishPlayingPlacingBook()=" + m_netAudioController.GetIsFinishPlayingPlacingBook() + "]");
        if(status && !m_validated)
        {
            OnObjectWellPlaced(placesIndex);
            if (m_otherBook.IsValidated())
            {
                UpdateEnigma(m_NetId, true);
                m_otherBook.UpdateEnigma(m_otherBook.m_NetId, true);
            }
            m_validated = status;
        }
        
    }

    [ClientRpc]
    public void RpcScientistDialog()
    {
        m_instanceDialogueCommun2 = FMODUnity.RuntimeManager.CreateInstance(EventDialogueCommun2);
        Debug.Log("[NetworkObjectEvent_Book:RpcScientifDialog]");
        m_instanceDialogueCommun2.start();
        if (isServer)
        {
            StartCoroutine(WaitDialogueFinish());
        }
    }

    [ClientRpc]
    public void RpcTurnOnTeleportAreas()
    {
        Debug.Log("[NetworkObjectEvent_Book:RpcTurnOnTeleportAreas]");

        // turn active children
        for (int i = 0; i < m_teleportAreas.transform.childCount; i++)
        {
            m_teleportAreas.transform.GetChild(i).GetComponent<TeleportArea>().SetLocked(false);
            m_teleportAreas.transform.GetChild(i).GetComponent<TeleportArea>().markerActive = true;
        }
    }

    protected override void OnTriggerEnter(Collider other)
    {
        if(m_netAudioController.GetIsFinishPlayingPlacingBook())
        {
            base.OnTriggerEnter(other);
        }
    }

    protected override void OnTriggerStay(Collider other)
    {
        if (m_netAudioController.GetIsFinishPlayingPlacingBook())
        {
            base.OnTriggerStay(other);
        }
    }
}
