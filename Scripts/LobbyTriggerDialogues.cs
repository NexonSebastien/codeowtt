﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Valve.VR.InteractionSystem;

[System.Obsolete]
public class LobbyTriggerDialogues : NetworkBehaviour
{
    private bool verifTrigger = false;
    private bool isPlayed = false;
    [SerializeField] private Network_AudioController m_netAudioController;
    [SerializeField] private TeleportPoint m_capsule1Tp;
    [SerializeField] private TeleportPoint m_capsule2Tp;
    private string EventDialogueLobbyCommun4 = "event:/Dialogues/LobbyCommun4";
    private FMOD.Studio.PLAYBACK_STATE m_stateDialogueLobbyCommun4;
    private FMOD.Studio.EventInstance m_instanceDialogueLobbyCommun4;

    // Update is called once per frame
    void Update()
    {
        if(isServer)
        {
            if (m_netAudioController.GetIsFinishPlayingGoOnCapsule() && !isPlayed)
            {
                RpcActiveCapsuleTp();
                isPlayed = true;
            }
        }
    }

    [ServerCallback]
    void OnTriggerEnter(Collider coll)
    {
        Debug.Log("Yann Trigger Dialogues");
        Debug.Log("Yann collision : " + coll.name.Equals("BodyTrigger"));
        Debug.Log("Yann verifTrigger : " + verifTrigger);
        if (coll.name.Equals("BodyTrigger") && verifTrigger == false && m_netAudioController.GetIsFinishPlayingGoToBoatBack())
        {
            RpcDialogueCapsule();
            verifTrigger = true;
        }
    }

    [ServerCallback]
    void OnTriggerStay(Collider coll)
    {
        if (coll.name.Equals("BodyTrigger") && verifTrigger == false && m_netAudioController.GetIsFinishPlayingGoToBoatBack())
        {
            RpcDialogueCapsule();
            verifTrigger = true;
        }
    }

    [ClientRpc]
    private void RpcDialogueCapsule()
    {
        m_instanceDialogueLobbyCommun4 = FMODUnity.RuntimeManager.CreateInstance(EventDialogueLobbyCommun4);
        Debug.Log("Yann Trigger Dialogues Ok");
        m_instanceDialogueLobbyCommun4.start();
        if(isServer)
        {
            StartCoroutine(WaitDialogueFinish());
        }
    }


    private IEnumerator WaitDialogueFinish()
    {
        while (m_stateDialogueLobbyCommun4 != FMOD.Studio.PLAYBACK_STATE.STOPPED)
        {
            m_instanceDialogueLobbyCommun4.getPlaybackState(out m_stateDialogueLobbyCommun4);
            yield return new WaitForSeconds(1);
        }
        m_netAudioController.SetIsFinishPlayingGoOnCapsule(true);
    }

    [ClientRpc]
    private void RpcActiveCapsuleTp()
    {
        m_capsule1Tp.locked = false;
        m_capsule2Tp.locked = false;
    }
}
