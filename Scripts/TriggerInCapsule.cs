﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Valve.VR.InteractionSystem;

public class TriggerInCapsule : NetworkBehaviour
{
    [SerializeField] EnigmaManager_Lobby m_EMLobby= null;
    [SerializeField] bool isP2;
    [SerializeField] TeleportPoint m_teleportPoint;
    GameObject m_player;
    bool isFinish = false;


    private void Update()
    {
        if(m_EMLobby.GetIsCloseCapsule() && !isFinish && m_player != null)
        {
            isFinish = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("[TriggerInCapsule | OnTriggerEnter]");
        if (other.gameObject.name == "BodyTrigger" && m_player == null && !isFinish)
        {
            GameObject player = other.GetComponentInParent<NetworkPlayer>().gameObject;
            if (!SamePlayer(player))
            {
                player.transform.parent = this.gameObject.transform.parent;

                if(isServer)
                {
                    if (isP2)
                    {
                        Debug.Log("[TriggerInCapsule | OnTriggerEnter] ISP2 : " + player.name);
                        m_EMLobby.SetIsInCapsuleP2(true);
                    }
                    else
                    {
                        Debug.Log("[TriggerInCapsule | OnTriggerEnter] ISP1 : " + player.name);
                        m_EMLobby.SetIsInCapsuleP1(true);
                    }
                }
            }
        }
    }

    

    private void OnTriggerExit(Collider other)
    {
        Debug.Log("[TriggerInCapsule | OnTriggerExit]");
        if (other.gameObject.name == "BodyTrigger" && m_player == other.GetComponentInParent<NetworkPlayer>().gameObject && !isFinish)
        {

            m_player.transform.parent = null;
            m_player = null;
            if (isServer)
            {
                if (isP2)
                {
                    m_EMLobby.SetIsInCapsuleP2(false);
                }
                else
                {
                    m_EMLobby.SetIsInCapsuleP1(false);
                }
            }
        }
    }

    private bool SamePlayer(GameObject player)
    {
        if (m_player != player)
        {
            m_player = player;
            return false;
        }
        return true;
    }
}
