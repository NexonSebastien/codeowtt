﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Valve.VR.InteractionSystem;

[System.Obsolete]
public class NetworkObjectEvent_Bell : NetworkObjectEvent
{
    [SerializeField] private NetworkObjectEvent_Bell m_otherBell;
    private bool m_validated;
    private bool m_available;
    [SerializeField] private GameObject m_teleportAreas;

    private string Event = "event:/Objets/Cloche";

    private bool verifDialogue = false;
    [SerializeField] private Network_AudioController m_netAudioController;
    private string EventDialogueLobbyCommun3 = "event:/Dialogues/LobbyCommun3";
    private FMOD.Studio.PLAYBACK_STATE m_stateDialogueLobbyCommun3;
    private FMOD.Studio.EventInstance m_instanceDialogueLobbyCommun3;

    override protected void Start()
    {
        base.Start();
        m_available = true;
        m_validated = false;
    }

    [ServerCallback]
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Hammer") && m_netAudioController.GetIsFinishPlayingPlacingHammer())
        {
            Debug.Log("[NetworkObjectEvent_Bell:OnTriggerEnter:other=Hammer]");
            ThrowEvent(m_NetId, 1, null);
        }
    }
    public override void EnigmaReceiveEvent(int eventCase, List<object> args)
    {
        if (isServer)
        {
            switch (eventCase)
            {
                case 1:
                    if (m_available) // prevent too much spam
                    {
                        m_available = false;
                        m_validated = true;
                        RpcRingBell();
                        m_available = true;
                        if (m_otherBell.IsValidated())
                        {
                            UpdateEnigma(m_NetId, true);
                            UpdateEnigma(m_otherBell.m_NetId, true);
                        }
                    }
                    break;
            }
        }
    }

    public override void EnigmaUpdateEvent()
    {
        RpcTurnOnTeleportAreas();
        RpcScientistDialog();
    }

    public bool IsValidated()
    {
        return m_validated;
    }

    [ClientRpc]
    public void RpcRingBell()
    {
        Debug.Log("[NetworkObjectEvent_Bell:RpcRingBell]");
        FMODUnity.RuntimeManager.PlayOneShotAttached(Event,this.gameObject);
    }

    [ClientRpc]
    public void RpcScientistDialog()
    {
        Debug.Log("[NetworkObjectEvent_Bell:RpcScientifDialog]");
        if (!verifDialogue)
        {
            m_instanceDialogueLobbyCommun3 = FMODUnity.RuntimeManager.CreateInstance(EventDialogueLobbyCommun3);
            m_instanceDialogueLobbyCommun3.start();
            verifDialogue = true;
            if(isServer)
            {
                StartCoroutine(WaitDialogueFinish());
            }
        }
    }
    private IEnumerator WaitDialogueFinish()
    {
        while (m_stateDialogueLobbyCommun3 != FMOD.Studio.PLAYBACK_STATE.STOPPED)
        {
            m_instanceDialogueLobbyCommun3.getPlaybackState(out m_stateDialogueLobbyCommun3);
            yield return new WaitForSeconds(1);
        }
        m_netAudioController.SetIsFinishPlayingGoToBoatBack(true);
    }

    [ClientRpc]
    public void RpcTurnOnTeleportAreas()
    {
        Debug.Log("[NetworkObjectEvent_Bell:RpcTurnOnTeleportAreas]");

        // turn active children
        for (int i = 0; i < m_teleportAreas.transform.childCount; i++)
        {
            m_teleportAreas.transform.GetChild(i).GetComponent<TeleportArea>().SetLocked(false);
            m_teleportAreas.transform.GetChild(i).GetComponent<TeleportArea>().markerActive = true;
        }
    }
}
