﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Linq;
using System;
using Data;
using UnityEngine.Rendering.PostProcessing;

[System.Obsolete]
[RequireComponent(typeof(NetworkIdentity))]
public class GameManager : NetworkBehaviour
{
    [SerializeField] private List<EnigmaManager> m_enigmaManagers;
    [SerializeField] PostProcessVolume m_PostProcessVolumeWater;
    [SerializeField] private EnigmaManager m_enigmaSecret;
    
    private int m_enigmaIndex;
    private int m_enigmaDead;
    private Dictionary<EnigmaManager, bool> m_enigmaManagersDict;
    private int m_enigmaSecreteId;
    private bool m_isGameEnded;
    private float m_timeStart = 0;
    private List<float> Timer;
    private Vignette m_vignette;
    private ColorGrading m_colorGrading;
    private bool isFadeOut = false;
    [SerializeField] private Credit m_creditP1;
    [SerializeField] private Credit m_creditP2;
    [SerializeField] private Transform m_posPlayer1Credit;
    [SerializeField] private Transform m_posPlayer2Credit;




    [ServerCallback]
    void Start()
    {
        Timer = new List<float>();
        ShowGame(PlayerPrefs.GetInt("gameVisibility") == 1 ? true : false);
        InitEnigmaManagersDict();
        StartEnigmaManager();
    }

    [ServerCallback]
    private void Update()
    {
        m_timeStart += Time.deltaTime;
    }

    [ServerCallback]
    private void ShowGame(bool isVisible)
    {
        if (isVisible)
        {
            NetworkDiscovery nd = GetComponent<NetworkDiscovery>();
            nd.Initialize();
            nd.StartAsServer();
        }
    }

    [ServerCallback]
    public void HideVisibleGame()
    {
        NetworkDiscovery nd = GetComponent<NetworkDiscovery>();
        nd.StopBroadcast();
    }

    [ServerCallback]
    private void InitEnigmaManagersDict()
    {
        m_enigmaManagersDict = new Dictionary<EnigmaManager, bool>();
        m_enigmaIndex = 0;
        if (m_enigmaSecret)
        {
            m_enigmaManagersDict.Add(m_enigmaSecret, false);
            m_enigmaSecreteId = 0;
            m_enigmaIndex++;
            m_enigmaSecret.StartEnigmaManager(m_enigmaSecreteId);
        }
        for (int i = 0; i < m_enigmaManagers.Count; i++)
        {
            m_enigmaManagersDict.Add(m_enigmaManagers[i], false);
        }
    }

    [ServerCallback]
    public void AddEnigmaManager(EnigmaManager enigmaManager)
    {
        m_enigmaManagersDict.Add(enigmaManager, false);
    }

    [ServerCallback]
    private void StartNextEnigmaManager()
    {
        Debug.Log("[GameManager:StartNextEnigmaManager]");
        if (++m_enigmaIndex < m_enigmaManagersDict.Count)
        {
            StartEnigmaManager();
        }
        else
        {
            EndGame(true);
        }
    }

    [ServerCallback]
    private void StartEnigmaManager()
    {
        Debug.Log("[GameManager:StartEnigmaManager:m_enigmaIndex=" + m_enigmaIndex + ", name=" + m_enigmaManagersDict.ElementAt(m_enigmaIndex).Key.name + "]");
        if (m_enigmaIndex < m_enigmaManagersDict.Count)
        {
            m_enigmaManagersDict.ElementAt(m_enigmaIndex).Key.StartEnigmaManager(m_enigmaIndex);

            GetTimer();

        }
    }

    [ServerCallback]
    public void GetTimer()
    {
        Timer.Add(m_timeStart);
    }

    [ServerCallback]
    public bool IsEnigmaSecretCompleted()
    {
        return m_enigmaManagersDict[m_enigmaSecret];
    }

    [ServerCallback]
    public void OnCompleteEnigma(int enigmaId)
    {
        if (!isServer) return;
        Debug.Log("[GameManager:OnCompleteEnigma:enigmaId=" + enigmaId + "]");
        if (enigmaId == m_enigmaIndex)
        {
            m_enigmaManagersDict[m_enigmaManagersDict.ElementAt(enigmaId).Key] = true;
            StartNextEnigmaManager();
        }
        else if (enigmaId == m_enigmaSecreteId)
        {
            m_enigmaManagersDict[m_enigmaManagersDict.ElementAt(enigmaId).Key] = true;
        }
    }

    [ServerCallback]
    public void OnFailEnigma(int enigmaId, string message)
    {
        Debug.Log("[GameManager:OnFailEnigma:enigmaId=" + enigmaId + "]");
        if (enigmaId == m_enigmaIndex)
        {
            EndGame(false, message);
            m_enigmaDead = m_enigmaIndex;
        }
    }

    [ServerCallback]
    private void EndGame(bool victory, string message = null)
    {
        if (!m_isGameEnded)
        {
            Debug.Log("[GameManager:EndGame:victory=" + victory + "]");
            m_isGameEnded = true;
            Timer.Add(m_timeStart);

            //@todo changer pour nouvelle metrics
            //GetComponent<ToTextFile>().Write(Timer.ToArray(), m_enigmaDead);
            //GetComponent<ToTextFile>().RpcCreateOrUpdateTextFile();
            //SetDatas();

            RpcEndGame(victory, message);
        }
    }

    [ClientRpc]
    private void RpcEndGame(bool victory, string message)
    {
        Debug.Log("[GameManager:RpcEndGame:victory=" + victory + "]");
        StartCoroutine(ShowCredits(victory, message,isServer));
    }

    private IEnumerator ShowCredits(bool victory, string message, bool isP1)
    {
        Debug.Log("[GameManager:ShowCredits]");
        // fade out
        m_PostProcessVolumeWater.enabled = true;
        StartCoroutine(FadeOut());
        yield return new WaitUntil(() => isFadeOut);
        FMODUnity.RuntimeManager.CreateInstance("event:/Musiques/MusiqueCredit").start();

        TeleportPlayer(isP1);
        // fade in
        StartCoroutine(FadeOut());
        yield return new WaitUntil(() => !isFadeOut);
        StartCoroutine(PlayCredits(victory, message, isP1));
        yield return null;
    }

    private IEnumerator PlayCredits(bool victory, string message, bool isP1)
    {
        if(isP1)
        {
            m_creditP1.Play();
            yield return new WaitWhile(m_creditP1.GetIsPlaying);
            ShowPlayerMenuUI(victory, message);
        }
        else
        {
            m_creditP2.Play();
            yield return new WaitWhile(m_creditP2.GetIsPlaying);
            ShowPlayerMenuUI(victory, message);
        }
        
    }

    private void TeleportPlayer(bool isP1)
    {
        GameObject vrGameObject = GameObject.Find("VrGameObject");
        if(isP1)
        {
            vrGameObject.transform.position = m_posPlayer1Credit.position;
        }
        else
        {
            vrGameObject.transform.position = m_posPlayer2Credit.position;
        }
        foreach(Camera c in vrGameObject.GetComponentsInChildren<Camera>())
        {
            c.farClipPlane = 300;
        }
        //int distanceValue = 100;
        //int distanceTimes = (int)vrGameObject.transform.parent.GetComponent<NetworkIdentity>().netId.Value;
        //float position = distanceValue * distanceTimes;
        //Vector3 newPosition = new Vector3(position, position, position);
        //vrGameObject.transform.position = newPosition;
        //Debug.Log("[GameManager:TeleportPlayer:" + newPosition.ToString() + "]");
    }

    private void ShowPlayerMenuUI(bool victory, string message)
    {
        Debug.Log("[GameManager:ShowPlayerMenuUI:victory=" + victory + "]");
        GameObject.Find("VrGameObject").transform.parent.GetComponent<NetworkPlayer>().GetPlayerMenuUI().ShowEndGameMessage(victory ? PlayerMenuUI.GAME_STATUS_VICTORY : PlayerMenuUI.GAME_STATUS_DEFEAT, message);
    }

    [ServerCallback]
    private void SetDatas()
    {
        for (int i = 0; i < Timer.Count; i++)
        {
            Debug.Log("TImer[" + i + "] : " + Timer[i]);
        }
        //Get values 
        float time = Timer[0] + Timer[1];
        EnigmaManager_ApollonRoom ApollonRoom = (EnigmaManager_ApollonRoom)m_enigmaManagers[2];
        GameData gd = GetComponent<GameData>();

        //Set Values
        gd.SetValue((int)Headers.NbMusicCrystalsHitP1, ApollonRoom.GetNbrCristHit().ToString());
        gd.SetValue((int)Headers.GlobalTime, time.ToString());
        //gd.SetValue(Headers.TimePoseidonRoom, new object[] { (object)(Timer[2] - Timer[1]) });
        gd.SetValue((int)Headers.TimeCapsuleRoom, (Timer[1] - Timer[0]).ToString());
        //@todo mettre les autre metrics

        //WriteValue
        gd.RpcWriteInFileDatas();
    }

    IEnumerator FadeOut()
    {
        m_PostProcessVolumeWater.profile.TryGetSettings<Vignette>(out m_vignette);
        m_PostProcessVolumeWater.profile.TryGetSettings<ColorGrading>(out m_colorGrading);
        float intensity = m_vignette.intensity.value;
        float postExposure = m_colorGrading.postExposure.value;
        Debug.Log("Fade | m_colorGrading.postExposure.value / intensity" + m_colorGrading.postExposure.value + "" + intensity);
        if (m_colorGrading.postExposure.value == 1)
        {
            Debug.Log("FadeOut");
            while (postExposure > -100)
            {
                intensity += 0.01f;
                postExposure--;
                FadeOut(intensity, postExposure);
                yield return new WaitForSeconds(0.1f);
            }
            isFadeOut = true;
        }
        else
        {
            Debug.Log("FadeIn");
            while (postExposure < 1)
            {
                intensity -= 0.01f;
                postExposure++;
                FadeOut(intensity, postExposure);
                yield return new WaitForSeconds(0.1f);
            }
            m_PostProcessVolumeWater.enabled = false;
            isFadeOut = false;
        }
        yield return null;
    }

    private void FadeOut(float myIntensity, float myPostExposure)
    {
        //intensity 0 | 1
        m_vignette.intensity.value = myIntensity;

        //postExposure 0 | -90
        m_colorGrading.postExposure.value = myPostExposure;
    }
}
