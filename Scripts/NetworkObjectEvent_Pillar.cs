﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[System.Obsolete]
public class NetworkObjectEvent_Pillar : NetworkObjectEvent
{
    public GameObject path1StartPos;
    public GameObject path2StartPos;
    public GameObject path3StartPos;
    public GameObject path4StartPos;
    public GameObject vfx_particlesTraveling;
    private bool particle = false;
    //Son
    private string EventPillarDestruct = "event:/Objets/PilierDestruction";
    private string EventPillarReconstruct = "event:/Objets/PilierReconstruction";
    private string EventMonster = "event:/Monstre/CriDestruction";

    override protected void Start()
    {
        base.Start();
    }



    public void SetEnigmaManager(EnigmaManager enigmaManager)
    {
        m_EnigmaManager = enigmaManager;
    }

    public override void EnigmaReceiveEvent(int eventCase, List<object> args)
    {
        if (isServer)
        {
            switch (eventCase)
            {
                case 1:
                    RpcDestructPillar((bool)args[0]);
                    break;
            }
        }
    }

    public override void EnigmaUpdateEvent()
    {
        throw new System.NotImplementedException();
    }

    [ClientRpc]
    private void RpcDestructPillar(bool destruct)
    {
        Debug.Log("[NetworkObjectEvent_Pillar:DestructPillar:destruct=" + destruct + "]");
        if (destruct)
        {
            StartCoroutine(Destruction());
        }
        else
        {
            //@todo sons reconstruction
            FMODUnity.RuntimeManager.PlayOneShot(EventPillarReconstruct, GetComponent<Transform>().position);
            this.transform.GetComponent<Animator>().SetBool("IsDestroy", false);
            particle = true;
            Debug.Log("Particle PATH = true");
            //GetComponent<ParticlePath>().GetPath();
        }

        if (particle == true)
        {
            Debug.Log("Particle PATH : GetParticle");
            GetParticle();
        }
    }

    private void GetParticle()
    {
        particle = false;
        Debug.Log("Particle PATH : GETPath");
        if (path1StartPos != null)
        {
            GameObject pathEffect1 = Instantiate(vfx_particlesTraveling, path1StartPos.transform.position, Quaternion.identity);
            pathEffect1.GetComponent<ParticlePathScript>().pathName = "Path01";
            Destroy(pathEffect1, pathEffect1.GetComponent<ParticleSystem>().main.duration + pathEffect1.GetComponent<ParticleSystem>().main.startLifetime.constantMax);
            Debug.Log("Particle PATH : Path1");
        }
        if (path2StartPos != null)
        {
            GameObject pathEffect2 = Instantiate(vfx_particlesTraveling, path2StartPos.transform.position, Quaternion.identity);
            pathEffect2.GetComponent<ParticlePathScript>().pathName = "Path02";
            Destroy(pathEffect2, pathEffect2.GetComponent<ParticleSystem>().main.duration + pathEffect2.GetComponent<ParticleSystem>().main.startLifetime.constantMax);
            Debug.Log("Particle PATH : Path2");
        }
        if (path3StartPos != null)
        {
            GameObject pathEffect3 = Instantiate(vfx_particlesTraveling, path3StartPos.transform.position, Quaternion.identity);
            pathEffect3.GetComponent<ParticlePathScript>().pathName = "Path03";
            Destroy(pathEffect3, pathEffect3.GetComponent<ParticleSystem>().main.duration + pathEffect3.GetComponent<ParticleSystem>().main.startLifetime.constantMax);
            Debug.Log("Particle PATH : Path3");
        }
        if (path4StartPos != null)
        {
            GameObject pathEffect4 = Instantiate(vfx_particlesTraveling, path4StartPos.transform.position, Quaternion.identity);
            pathEffect4.GetComponent<ParticlePathScript>().pathName = "Path04";
            Destroy(pathEffect4, pathEffect4.GetComponent<ParticleSystem>().main.duration + pathEffect4.GetComponent<ParticleSystem>().main.startLifetime.constantMax);
            Debug.Log("Particle PATH : Path4");
        }
        if (path1StartPos != null)
        {
            GameObject pathEffect1 = Instantiate(vfx_particlesTraveling, path1StartPos.transform.position, Quaternion.identity);
            pathEffect1.GetComponent<ParticlePathScript>().pathName = "Path05";
            Destroy(pathEffect1, pathEffect1.GetComponent<ParticleSystem>().main.duration + pathEffect1.GetComponent<ParticleSystem>().main.startLifetime.constantMax);
            Debug.Log("Particle PATH : Path5");
        }
        if (path2StartPos != null)
        {
            GameObject pathEffect2 = Instantiate(vfx_particlesTraveling, path2StartPos.transform.position, Quaternion.identity);
            pathEffect2.GetComponent<ParticlePathScript>().pathName = "Path06";
            Destroy(pathEffect2, pathEffect2.GetComponent<ParticleSystem>().main.duration + pathEffect2.GetComponent<ParticleSystem>().main.startLifetime.constantMax);
            Debug.Log("Particle PATH : Path6");
        }
        if (path3StartPos != null)
        {
            GameObject pathEffect3 = Instantiate(vfx_particlesTraveling, path3StartPos.transform.position, Quaternion.identity);
            pathEffect3.GetComponent<ParticlePathScript>().pathName = "Path07";
            Destroy(pathEffect3, pathEffect3.GetComponent<ParticleSystem>().main.duration + pathEffect3.GetComponent<ParticleSystem>().main.startLifetime.constantMax);
            Debug.Log("Particle PATH : Path7");
        }
        if (path4StartPos != null)
        {
            GameObject pathEffect4 = Instantiate(vfx_particlesTraveling, path4StartPos.transform.position, Quaternion.identity);
            pathEffect4.GetComponent<ParticlePathScript>().pathName = "Path08";
            Destroy(pathEffect4, pathEffect4.GetComponent<ParticleSystem>().main.duration + pathEffect4.GetComponent<ParticleSystem>().main.startLifetime.constantMax);
            Debug.Log("Particle PATH : Path8");
        }
    }

    private IEnumerator Destruction()
    {
        FMODUnity.RuntimeManager.PlayOneShot(EventMonster);
        yield return new WaitForSeconds(3);
        FMODUnity.RuntimeManager.PlayOneShot(EventPillarDestruct, GetComponent<Transform>().position);
        this.transform.GetComponent<Animator>().SetBool("IsDestroy", true);
    }
}
