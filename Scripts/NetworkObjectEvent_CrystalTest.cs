﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

[System.Obsolete]
public class NetworkObjectEvent_CrystalTest : NetworkObjectEvent
{
    [SerializeField] private List<NetworkObjectEvent_SoluceCrystal> m_soluceCrystals;
    private List<MusicCrystal> m_musicCrystals;
    private bool m_isComplete;
    private List<int> m_listSoluceSize;
    private List<List<int>> m_listSoluce;
    private int m_indexLists;
    private int m_indexListElement;

    override protected void Start()
    {
        base.Start();
        m_isComplete = false;
        m_indexLists = 0;
        m_indexListElement = 0;
        m_listSoluceSize = new List<int>() { 4, 6, 8 };
        StartCoroutine(InitMusicCrystals());
        StartCoroutine(InitRandomListSoluce());
    }

    private IEnumerator InitMusicCrystals()
    {
        Debug.Log("[NetworkObjectEvent_CrystalTest:InitMusicCrystals]");
        yield return new WaitForEndOfFrame();
        m_musicCrystals = GetComponent<NetworkObjectEvent_MusicCrystal>().GetMusicCrystals();
        yield return null;
    }

    private IEnumerator InitRandomListSoluce()
    {
        Debug.Log("[NetworkObjectEvent_CrystalTest:InitRandomListSoluce]");
        yield return new WaitForEndOfFrame();
        m_listSoluce = new List<List<int>>();
        for(int i=0; i<m_listSoluceSize.Count; i++)
        {
            m_listSoluce.Add(RandomListInt(m_listSoluceSize[i]));
            string sList = "List[" + i + "] = ";
            for(int j=0; j<m_listSoluce[i].Count; j++)
            {
                sList += m_listSoluce[i][j];
                if(j < m_listSoluce[i].Count - 1){ sList += ", "; }
            }
            Debug.Log("[NetworkObjectEvent_CrystalTest:InitRandomListSoluce:list=[" + sList + "]]");
        }
        yield return null;
    }

    private List<int> RandomListInt(int maxElements)
    {
        Debug.Log("[NetworkObjectEvent_CrystalTest:RandomListInt:maxElements="+ maxElements + "]");
        System.Random _rand = new System.Random();
        return Enumerable.Range(0, maxElements).Select(r => _rand.Next(1, m_musicCrystals.Count + 1)).ToList();
    }

    public bool IsCompleted()
    {
        //Debug.Log("[NetworkObjectEvent_CrystalTest:IsCompleted="+ m_isComplete + "]");
        return m_isComplete;
    }

    public List<int> GetCurrentListSoluce()
    {
        Debug.Log("[NetworkObjectEvent_CrystalTest:GetCurrentListSoluce]");
        return m_listSoluce[m_indexLists];
    }

    public bool OnHitCrystal(int id)
    {
        if (m_listSoluce[m_indexLists][m_indexListElement] == id)
        {
            Debug.Log("[NetworkObjectEvent_CrystalTest:OnHitCrystal:id=" + id + ":return=true]");
            StartCoroutine(NextElement(id));
            return true;
        }
        else
        {
            Debug.Log("[NetworkObjectEvent_CrystalTest:OnHitCrystal:id=" + id + ":return=false]");
            StartCoroutine(RestartList());
            return false;
        }
    }

    private IEnumerator NextElement(int id)
    {
        if (++m_indexListElement >= m_listSoluce[m_indexLists].Count)
        {
            StartCoroutine(RestartList());
            if (++m_indexLists >= m_listSoluce.Count)
            {
                Debug.Log("[NetworkObjectEvent_CrystalTest:NextElement:id=" + id + ":isComplete[" + m_indexLists + "][" + m_indexListElement + "]]");
                m_isComplete = true; // enigma completed
            }
            else
            {
                Debug.Log("[NetworkObjectEvent_CrystalTest:NextElement:id=" + id + ":nextList["+ m_indexLists + "]["+ m_indexListElement + "]]");
                // update two new soluce crystals positions to match list length
                AddSoluceCrystal(m_soluceCrystals[m_listSoluceSize[m_indexLists]-2]);
                AddSoluceCrystal(m_soluceCrystals[m_listSoluceSize[m_indexLists]-1]);
            }
        }
        else
        {
            Debug.Log("[NetworkObjectEvent_CrystalTest:NextElement:id=" + id + ":nextElement[" + m_indexLists + "][" + (m_indexListElement-1) + "]]");
            SetCrystalMaterial(m_soluceCrystals[m_indexListElement-1], id); // set crystal material
        }
        yield return null; 
    }

    private IEnumerator RestartList()
    {
        Debug.Log("[NetworkObjectEvent_CrystalTest:RestartList]");
        m_indexListElement = 0; // reset element index
        ResetCrystalsMaterial(); // reset all crystals material to default
        yield return null;
    }

    private void AddSoluceCrystal(NetworkObjectEvent_SoluceCrystal soluceCrystal)
    {
        Debug.Log("[NetworkObjectEvent_CrystalTest:AddSoluceCrystal:soluceCrystal.name="+ soluceCrystal .name+ "]");
        ThrowEvent(soluceCrystal.GetComponent<NetworkIdentity>(), 1, new List<object>() { m_soluceCrystals[0].transform.position.z });
    }

    private void SetCrystalMaterial(NetworkObjectEvent_SoluceCrystal soluceCrystal, int materialIndex)
    {
        Debug.Log("[NetworkObjectEvent_CrystalTest:SetCrystalMaterial:soluceCrystal.name=" + soluceCrystal.name + ":materialIndex="+ materialIndex + "]");
        ThrowEvent(soluceCrystal.GetComponent<NetworkIdentity>(), 2, new List<object>() { materialIndex });
    }

    private void ResetCrystalsMaterial()
    {
        Debug.Log("[NetworkObjectEvent_CrystalTest:ResetCrystalsMaterial]");
        for (int i=0; i<m_listSoluceSize[m_indexLists]; i++)
        {
            SetCrystalMaterial(m_soluceCrystals[i], -1);
        }
    }

    public override void EnigmaUpdateEvent()
    {
        throw new System.NotImplementedException();
    }

    public override void EnigmaReceiveEvent(int eventCase, List<object> args)
    {
        throw new System.NotImplementedException();
    }
}