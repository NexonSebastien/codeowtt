﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CristalSoluce : MonoBehaviour
{

    //1234 - 1235 - 5342 - 1235 - 1234

    List<int> ListSoluce = new List<int>() { 1, 2, 3, 4, 1, 2, 3, 5, 5, 3, 4, 2, 1, 2, 3, 5, 1, 2, 3, 4 };
    //List<int> ListSoluce = new List<int>() { 1, 2};
    int sizeOfList;
    int curseur = 0;

    // Start is called before the first frame update
    void Start()
    {
        sizeOfList = ListSoluce.Count;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public bool IsCompleted()
    {
        return curseur == sizeOfList;
    }

    public bool OnHitCrystal(int id)
    {
        // si l'enigme est finie
        if (curseur == sizeOfList)
        {
            return true;
        }
        // avancement du curseur 
        if (ListSoluce[curseur] == id)
        {
            // ok
            curseur += 1;
            Debug.Log("curseur + 1");
            return true;
        }
        else
        {
            // ko
            curseur = 0;
            Debug.Log("mauvaise combinaison");
            return false;

        }
    }
    /*
    public void OnCollisionEnter(Collision collision)
    {
        // if the music crystal is hit by the music rod and is available for interaction,
        // set the music crystal unvailable then play sound and change crystal translucency temporarily 
        if (collision.collider.CompareTag("CrystalBLACK"))
        {
            Debug.Log("TUCH BLACK CRYSTAL");

    }
    */
    /*
    int black = 0;
    int yellow = 0;
    int red = 0;
    int blue = 0;
    int green = 0;
    
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "musicCrystalGREEN")
        {
            Debug.Log("TUCH GREEN CRYSTAL");
            green = 1;
            Debug.Log(black + "tuch");
            Debug.Log(yellow + "tuch");
            Debug.Log(red + "tuch");
            Debug.Log(blue + "tuch");
            Debug.Log(green + "tuch");

            OnHitCrystal(1);
        }

        if (collision.gameObject.name == "musicCrystalYELLOW")
        {
            Debug.Log("TUCH YELLOW CRYSTAL");
            yellow = 1;
            Debug.Log(black + "tuch");
            Debug.Log(yellow + "tuch");
            Debug.Log(red + "tuch");
            Debug.Log(blue + "tuch");
            Debug.Log(green + "tuch");

            OnHitCrystal(2);
        }

        if (collision.gameObject.name == "musicCrystalBLACK")
            {
                Debug.Log("TUCH BLACK CRYSTAL");
                black = 1;
                Debug.Log(black + "tuch");
                Debug.Log(yellow + "tuch");
                Debug.Log(red + "tuch");
                Debug.Log(blue + "tuch");
                Debug.Log(green + "tuch");

            OnHitCrystal(3);
        }

        if (collision.gameObject.name == "musicCrystalRED")
        {
            Debug.Log("TUCH RED CRYSTAL");
            red = 1;
            Debug.Log(black + "tuch");
            Debug.Log(yellow + "tuch");
            Debug.Log(red + "tuch");
            Debug.Log(blue + "tuch");
            Debug.Log(green + "tuch");

            OnHitCrystal(4);
        }

        if (collision.gameObject.name == "musicCrystalBLUE")
        {
            Debug.Log("TUCH BLUE CRYSTAL");
            blue = 1;
            Debug.Log(black + "tuch");
            Debug.Log(yellow + "tuch");
            Debug.Log(red + "tuch");
            Debug.Log(blue + "tuch");
            Debug.Log(green + "tuch");

            OnHitCrystal(5);
        }
 
    }
    */
    /*
    if (collision.gameObject.name == "musicCrystalGREEN")
    {
        Debug.Log("TUCH GREEN CRYSTAL");
    }
    if (collision.gameObject.name == "musicCrystalRED")
    {
        Debug.Log("TUCH RED CRYSTAL");
    }
    if (collision.gameObject.name == "musicCrystalBLUE")
    {
        Debug.Log("TUCH BLUE CRYSTAL");
    }
    if (collision.gameObject.name == "musicCrystalYELLOW")
    {
        Debug.Log("TUCH YELLOW CRYSTAL");
    }
    */

}
