﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[System.Obsolete]
public class Network_AudioController : NetworkBehaviour
{
    public static Network_AudioController singleton;

    [SerializeField] [SyncVar] bool isFinishPlayingEva = false;
    [SerializeField] [SyncVar] bool isFinishPlayingJacob = false;
    [SerializeField] [SyncVar] bool isFinishPlayingPlacingBook = false;
    [SerializeField] [SyncVar] bool isFinishPlayingPlacingHammer = false;
    [SerializeField] [SyncVar] bool isFinishPlayingGoToBoatBack = false;
    [SerializeField] [SyncVar] bool isFinishPlayingGoOnCapsule = false;
    [SerializeField] [SyncVar] bool isFinishPlayingCorals = false;
    [SerializeField] [SyncVar] bool isFinishPoseidonRoom = false;
    [SerializeField] [SyncVar] bool isFinishApollonRoom = false;
    [SerializeField] [SyncVar] bool isFinishHephaistosRoom = false;
    [SerializeField] [SyncVar] bool isFinishEnterCapsuleRoom = false;
    [SerializeField] [SyncVar] bool isFinishButton = false;
    [SerializeField] [SyncVar] bool isFinishPlacingStone = false;

    public bool GetIsFinishPlayingEva()
    {
        return isFinishPlayingEva;
    }
    public void SetIsFinishPlayingEva(bool value)
    {
        Debug.Log("[Network_AudioController | SetIsFinishPlayingEva] value : " + value);
        isFinishPlayingEva = value;
    }

    public bool GetIsFinishPlayingJacob()
    {
        return isFinishPlayingJacob;
    }
    public void SetIsFinishPlayingJacob(bool value)
    {
        if (isServer)
        {
            isFinishPlayingJacob = value;
        }
        else
        {
            Debug.Log("[Network_AudioController | SetIsFinishPlayingJacob] value : " + value);
            CmdSetIsFinishPlayingJacob(value);
        }
    }

    
    public bool GetIsFinishPlayingPlacingBook()
    {
        return isFinishPlayingPlacingBook;
    }
    public void SetIsFinishPlayingPlacingBook(bool value)
    {
        Debug.Log("[Network_AudioController | SetIsFinishPlayingPlacingBook] value : " + value);
        isFinishPlayingPlacingBook = value;
    }


    public bool GetIsFinishPlayingPlacingHammer()
    {
        return isFinishPlayingPlacingHammer;
    }
    public void SetIsFinishPlayingPlacingHammer(bool value)
    {
        Debug.Log("[Network_AudioController | SetIsFinishPlayingPlacingHammer] value : " + value);
        isFinishPlayingPlacingHammer = value;
    }

    public bool GetIsFinishPlayingGoOnCapsule()
    {
        return isFinishPlayingGoOnCapsule;
    }
    public void SetIsFinishPlayingGoOnCapsule(bool value)
    {
        Debug.Log("[Network_AudioController | SetIsFinishPlayingGoOnCapsule] value : " + value);
        isFinishPlayingGoOnCapsule = value;
    }

    public bool GetIsFinishPlayingGoToBoatBack()
    {
        return isFinishPlayingGoToBoatBack;
    }
    public void SetIsFinishPlayingGoToBoatBack(bool value)
    {
        Debug.Log("[Network_AudioController | SetIsFinishPlayingGoToBoatBack] value : " + value);
        isFinishPlayingGoToBoatBack = value;
    }

    public bool GetIsFinishPlayingCorals()
    {
        return isFinishPlayingCorals;
    }
    public void SetIsFinishPlayingCorals(bool value)
    {
        Debug.Log("[Network_AudioController | SetIsFinishPlayingCorals] value : " + value);
        isFinishPlayingCorals = value;
    }

    public bool GetIsFinishPoseidonRoom()
    {
        return isFinishPoseidonRoom;
    }
    public void SetIsFinishPoseidobRoom(bool value)
    {
        Debug.Log("[Network_AudioController | SetIsFinishPoseidonRoom] value : " + value);
        isFinishPoseidonRoom = value;
    }

    public bool GetIsFinishApollonRoom()
    {
        return isFinishApollonRoom;
    }
    public void SetIsFinishApollonRoom(bool value)
    {
        Debug.Log("[Network_AudioController | SetIsFinishApollonRoom] value : " + value);
        isFinishApollonRoom = value;
    }
    
    public bool GetIsFinishHephaistosRoom()
    {
        return isFinishHephaistosRoom;
    }
    public void SetIsFinishHephaistosRoom(bool value)
    {
        Debug.Log("[Network_AudioController | SetIsFinishHephaistosRoom] value : " + value);
        isFinishHephaistosRoom = value;
    }
    
    public bool GetIsFinishEnterCapsuleRoom()
    {
        return isFinishEnterCapsuleRoom;
    }
    public void SetIsFinishEnterCapsuleRoom(bool value)
    {
        Debug.Log("[Network_AudioController | SetIsFinishEnterCapsuleRoom] value : " + value);
        isFinishEnterCapsuleRoom = value;
    }
    
    public bool GetIsFinishButton()
    {
        return isFinishButton;
    }
    public void SetIsFinishButton(bool value)
    {
        Debug.Log("[Network_AudioController | SetIsFinishButton] value : " + value);
        isFinishButton = value;
    }

    public bool GetIsFinishPlacingStone()
    {
        return isFinishPlacingStone;
    }
    public void SetIsFinishPlacingStone(bool value)
    {
        Debug.Log("[Network_AudioController | SetIsFinishPlacingStone] value : " + value);
        isFinishPlacingStone = value;
    }

    [Command]
    private void CmdSetIsFinishPlayingJacob(bool value)
    {
        Debug.Log("[Network_AudioController | CmdSetIsFinishPlayingJacob] value : " + value);
        isFinishPlayingJacob = value;
    }
    
}
