﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Destroys this object when it is detached from the hand
//
//=============================================================================

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;

namespace Valve.VR.InteractionSystem
{
	//-------------------------------------------------------------------------
	[RequireComponent( typeof( Interactable ) )]
	public class DestroyOnDetachedFromHand : NetworkBehaviour
	{
		//-------------------------------------------------
		private void OnDetachedFromHand( Hand hand )
		{
			Debug.Log("[DestroyOnDetachedFromHand | OnDetachedFromHand] "+ gameObject.name);
			if(isServer)
            {
				UnSpawn(gameObject.GetComponent<NetworkIdentity>());
			}
			else
            {
				CmdDestroy(gameObject.GetComponent<NetworkIdentity>());
			}
			
		}


		[Command]
		private void CmdDestroy(NetworkIdentity id)
        {
			UnSpawn(id);
        }

		private void UnSpawn(NetworkIdentity id)
        {
			Debug.Log("[DestroyOnDetachedFromHand | UnSpawn] id : "+id.name);
			NetworkServer.UnSpawn(id.gameObject);
		}
    }
}
