﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Handles the spawning and returning of the ItemPackage
//
//=============================================================================

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine.Events;
using UnityEngine.Networking;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Valve.VR.InteractionSystem
{
	//-------------------------------------------------------------------------
	[RequireComponent( typeof( Interactable ) )]
    [System.Obsolete]
    public class ItemPackageSpawner : NetworkBehaviour
	{
		public ItemPackage itemPackage
		{
			get
			{
				return _itemPackage;
			}
			set
			{
				CreatePreviewObject();
			}
		}

		public ItemPackage _itemPackage;

		public bool useItemPackagePreview = true;
        private bool useFadedPreview = false;
		private GameObject previewObject;

		public bool requireGrabActionToTake = false;
		public bool requireReleaseActionToReturn = false;
		public bool showTriggerHint = false;

		[EnumFlags]
		public Hand.AttachmentFlags attachmentFlags = Hand.defaultAttachmentFlags;

		public bool takeBackItem = false; // if a hand enters this trigger and has the item this spawner dispenses at the top of the stack, remove it from the stack

		public bool acceptDifferentItems = false;

		private GameObject spawnedItem;
		GameObject otherHandObjectToAttach;
		private bool itemIsSpawned = false;

		public UnityEvent pickupEvent;
		public UnityEvent dropEvent;

		public bool justPickedUpItem = false;

		private bool otherHandObjectToAttachIsReady = true;
		private bool spawnedItemIsReady = true;
		private bool isClearPreview;


		//-------------------------------------------------
		private IEnumerator CreatePreviewObject()
		{
			if ( useItemPackagePreview )
			{
				isClearPreview = false;
				if (!isServer)
				{
					CmdClearPreview();
				}
				else
				{
					RpcClearPreview();
				}
				while(!isClearPreview)
                {
					yield return new WaitForEndOfFrame();
                }

				if (useItemPackagePreview)
				{
					if (itemPackage != null)
					{
						if (useFadedPreview == false) // if we don't have a spawned item out there, use the regular preview
						{
							if (itemPackage.previewPrefab != null)
							{
								Debug.Log("[ItemPackageSpawner | previewPrefab | Client]");
								previewObject = null;
								NetworkIdentity player = GameObject.Find("VrGameObject").GetComponentInParent<NetworkPlayer>().GetComponent<NetworkIdentity>();
								Debug.Log("[ItemPackageSpawner | previewPrefab | Client] player.name : " + player.name);
								Debug.Log("[ItemPackageSpawner | previewPrefab | Client] temPackage.itemPrefab.name " + itemPackage.previewPrefab.name);
								StartCoroutine(WaitForAuthorityPos(itemPackage.previewPrefab, player, transform, Quaternion.identity));
							}
						}
						else // there's a spawned item out there. Use the faded preview
						{
							if (itemPackage.fadedPreviewPrefab != null)
							{
								Debug.Log("[ItemPackageSpawner | fadedPreviewPrefab | Client]");
								previewObject = null;
								NetworkIdentity player = GameObject.Find("VrGameObject").GetComponentInParent<NetworkPlayer>().GetComponent<NetworkIdentity>();
								Debug.Log("[ItemPackageSpawner | fadedPreviewPrefab | Client] player.name : " + player.name);
								Debug.Log("[ItemPackageSpawner | fadedPreviewPrefab | Client] temPackage.itemPrefab.name " + itemPackage.fadedPreviewPrefab.name);
								StartCoroutine(WaitForAuthorityPos(itemPackage.fadedPreviewPrefab, player, transform, Quaternion.identity));
							}
						}
					}
				}
			}
		}

        


        //-------------------------------------------------
        void Start()
		{
			VerifyItemPackage();
		}


		//-------------------------------------------------
		private void VerifyItemPackage()
		{
			if ( itemPackage == null )
			{
				ItemPackageNotValid();
			}

			if ( itemPackage.itemPrefab == null )
			{
				ItemPackageNotValid();
			}
		}


		//-------------------------------------------------
		private void ItemPackageNotValid()
		{
			Debug.LogError("<b>[SteamVR Interaction]</b> ItemPackage assigned to " + gameObject.name + " is not valid. Destroying this game object.", this);
			if(isServer)
            {
				NetworkServer.UnSpawn(gameObject);
			}
			else
            {
				CmdDestroy(gameObject.GetComponent<NetworkIdentity>());
			}
			
		}


		[Command]
		//-------------------------------------------------
		private void CmdClearPreview()
		{
			RpcClearPreview();
		}

		[ClientRpc]
		private void RpcClearPreview()
		{
			foreach (Transform child in transform)
			{
				Debug.Log("Destroy RpcClearPreview "+ child.name);
				Destroy(child.gameObject);
			}
			isClearPreview = true;
		}


		//-------------------------------------------------
		void Update()
		{
			if ( ( itemIsSpawned == true ) && ( spawnedItem == null ) && spawnedItemIsReady == true)
			{
				itemIsSpawned = false;
				useFadedPreview = false;
				dropEvent.Invoke();
				CreatePreviewObject();
			}
		}


		//-------------------------------------------------
		private void OnHandHoverBegin( Hand hand )
		{
			ItemPackage currentAttachedItemPackage = GetAttachedItemPackage( hand );

			if ( currentAttachedItemPackage == itemPackage ) // the item at the top of the hand's stack has an associated ItemPackage
			{
				if ( takeBackItem && !requireReleaseActionToReturn ) // if we want to take back matching items and aren't waiting for a trigger press
				{
					TakeBackItem( hand );
				}
			}

			if (!requireGrabActionToTake) // we don't require trigger press for pickup. Spawn and attach object.
			{
				SpawnAndAttachObject( hand, GrabTypes.Scripted );
			}

			if (requireGrabActionToTake && showTriggerHint )
			{
                hand.ShowGrabHint("PickUp");
			}
		}


		//-------------------------------------------------
		private void TakeBackItem( Hand hand )
		{
			RemoveMatchingItemsFromHandStack( itemPackage, hand );

			if ( itemPackage.packageType == ItemPackage.ItemPackageType.TwoHanded )
			{
				RemoveMatchingItemsFromHandStack( itemPackage, hand.otherHand );
			}
		}


		//-------------------------------------------------
		private ItemPackage GetAttachedItemPackage( Hand hand )
		{
			GameObject currentAttachedObject = hand.currentAttachedObject;

			if ( currentAttachedObject == null ) // verify the hand is holding something
			{
				return null;
			}

			ItemPackageReference packageReference = hand.currentAttachedObject.GetComponent<ItemPackageReference>();
			if ( packageReference == null ) // verify the item in the hand is matchable
			{
				return null;
			}

			ItemPackage attachedItemPackage = packageReference.itemPackage; // return the ItemPackage reference we find.

			return attachedItemPackage;
		}


		//-------------------------------------------------
		private void HandHoverUpdate( Hand hand )
		{
			if ( takeBackItem && requireReleaseActionToReturn )
			{
                if (hand.isActive)
				{
					ItemPackage currentAttachedItemPackage = GetAttachedItemPackage( hand );
                    if (currentAttachedItemPackage == itemPackage && hand.IsGrabEnding(currentAttachedItemPackage.gameObject))
					{
						TakeBackItem( hand );
						return; // So that we don't pick up an ItemPackage the same frame that we return it
					}
				}
			}

			if ( requireGrabActionToTake )
			{
                GrabTypes startingGrab = hand.GetGrabStarting();

				if (startingGrab != GrabTypes.None)
				{
					SpawnAndAttachObject( hand, GrabTypes.Scripted);
				}
			}
		}


		//-------------------------------------------------
		private void OnHandHoverEnd( Hand hand )
		{
			if ( !justPickedUpItem && requireGrabActionToTake && showTriggerHint )
			{
                hand.HideGrabHint();
			}

			justPickedUpItem = false;
		}


		//-------------------------------------------------
		private void RemoveMatchingItemsFromHandStack( ItemPackage package, Hand hand )
		{
            if (hand == null)
                return;

			for ( int i = 0; i < hand.AttachedObjects.Count; i++ )
			{
				ItemPackageReference packageReference = hand.AttachedObjects[i].attachedObject.GetComponent<ItemPackageReference>();
				if ( packageReference != null )
				{
					ItemPackage attachedObjectItemPackage = packageReference.itemPackage;
					if ( ( attachedObjectItemPackage != null ) && ( attachedObjectItemPackage == package ) )
					{
						GameObject detachedItem = hand.AttachedObjects[i].attachedObject;
						hand.DetachObject( detachedItem );
					}
				}
			}
		}


		//-------------------------------------------------
		private void RemoveMatchingItemTypesFromHand( ItemPackage.ItemPackageType packageType, Hand hand )
		{
			for ( int i = 0; i < hand.AttachedObjects.Count; i++ )
			{
				ItemPackageReference packageReference = hand.AttachedObjects[i].attachedObject.GetComponent<ItemPackageReference>();
				if ( packageReference != null )
				{
					if ( packageReference.itemPackage.packageType == packageType )
					{
						GameObject detachedItem = hand.AttachedObjects[i].attachedObject;
						hand.DetachObject( detachedItem );
					}
				}
			}
		}


		//-------------------------------------------------
		private void SpawnAndAttachObject( Hand hand, GrabTypes grabType )
		{
			if ( hand.otherHand != null )
			{
				//If the other hand has this item package, take it back from the other hand
				ItemPackage otherHandItemPackage = GetAttachedItemPackage( hand.otherHand );
				if ( otherHandItemPackage == itemPackage )
				{
					TakeBackItem( hand.otherHand );
				}
			}

			if ( showTriggerHint )
			{
                hand.HideGrabHint();
			}

			if ( itemPackage.otherHandItemPrefab != null )
			{
				if ( hand.otherHand.hoverLocked )
				{
                    Debug.Log( "<b>[SteamVR Interaction]</b> Not attaching objects because other hand is hoverlocked and we can't deliver both items." );
                    return;
				}
			}

			// if we're trying to spawn a one-handed item, remove one and two-handed items from this hand and two-handed items from both hands
			if ( itemPackage.packageType == ItemPackage.ItemPackageType.OneHanded )
			{
				RemoveMatchingItemTypesFromHand( ItemPackage.ItemPackageType.OneHanded, hand );
				RemoveMatchingItemTypesFromHand( ItemPackage.ItemPackageType.TwoHanded, hand );
				RemoveMatchingItemTypesFromHand( ItemPackage.ItemPackageType.TwoHanded, hand.otherHand );
			}

			// if we're trying to spawn a two-handed item, remove one and two-handed items from both hands
			if ( itemPackage.packageType == ItemPackage.ItemPackageType.TwoHanded )
			{
				RemoveMatchingItemTypesFromHand( ItemPackage.ItemPackageType.OneHanded, hand );
				RemoveMatchingItemTypesFromHand( ItemPackage.ItemPackageType.OneHanded, hand.otherHand );
				RemoveMatchingItemTypesFromHand( ItemPackage.ItemPackageType.TwoHanded, hand );
				RemoveMatchingItemTypesFromHand( ItemPackage.ItemPackageType.TwoHanded, hand.otherHand );
			}

			StartCoroutine(WaitForSpawnAndAttachObjectHand(hand,grabType));
		}
		private IEnumerator WaitForSpawnAndAttachObjectHand(Hand hand, GrabTypes grabType)
		{
			Debug.Log("[ItemPackageSpawner | WaitForSpawnAndAttachObjectHand | spawnedItem | Client]");
			spawnedItem = null;
			spawnedItemIsReady = false;
			NetworkIdentity player = GameObject.Find("VrGameObject").GetComponentInParent<NetworkPlayer>().GetComponent<NetworkIdentity>();
			Debug.Log("[ItemPackageSpawner | WaitForSpawnAndAttachObjectHand | spawnedItem | Client] player.name : " + player.name);
			Debug.Log("[ItemPackageSpawner | WaitForSpawnAndAttachObjectHand | spawnedItem | Client] itemPackage.itemPrefab.name " + itemPackage.itemPrefab.name);
			StartCoroutine(WaitForAuthorityHandGrab(itemPackage.itemPrefab, player, hand, grabType, 1));
			while (!spawnedItemIsReady)
			{
				Debug.Log("[ItemPackageSpawner | WaitForSpawnAndAttachObjectHand | spawnedItem | Client] Wait spawnedItem");
				yield return new WaitForEndOfFrame();
			}

			if ((itemPackage.otherHandItemPrefab != null) && (hand.otherHand.isActive))
			{
				Debug.Log("[ItemPackageSpawner | WaitForSpawnAndAttachObjectHand | otherHandObjectToAttach | Client]");
				otherHandObjectToAttach = null;
				otherHandObjectToAttachIsReady = false;
				Debug.Log("[ItemPackageSpawner | WaitForSpawnAndAttachObjectHand | otherHandObjectToAttach | Client] player.name : " + player.name);
				Debug.Log("[ItemPackageSpawner | WaitForSpawnAndAttachObjectHand | otherHandObjectToAttach | Client] itemPackage.itemPrefab.name " + itemPackage.otherHandItemPrefab.name);
				StartCoroutine(WaitForAuthorityHandGrab(itemPackage.otherHandItemPrefab, player, hand, grabType, 2));
				while (!otherHandObjectToAttachIsReady)
				{
					Debug.Log("[ItemPackageSpawner | WaitForSpawnAndAttachObjectHand | otherHandObjectToAttach | Client] Wait otherHandObjectToAttach");
					yield return new WaitForEndOfFrame();
				}
			}
			itemIsSpawned = true;

			justPickedUpItem = true;

			if (takeBackItem)
			{
				useFadedPreview = true;
				pickupEvent.Invoke();
				CreatePreviewObject();
			}
		}

		private IEnumerator WaitForAuthorityPos(GameObject prefab, NetworkIdentity player, Transform transform, Quaternion rot)
		{
			Debug.Log("[ItemPackageSpawner | WaitForAuthorityPos] hasAutority" + this.hasAuthority);
			if (!this.hasAuthority)
			{
				player.GetComponent<NetworkPlayer>().CmdSetAuth(this.GetComponent<NetworkIdentity>(), player);

				while (!this.hasAuthority)
				{
					Debug.Log("[ItemPackageSpawner | WaitForAuthorityPos] Wait authority");
					yield return new WaitForEndOfFrame();
				}
			}
			Debug.Log("[ItemPackageSpawner | WaitForAuthorityPos] hasAuthority : " + hasAuthority);
			Debug.Log("[ItemPackageSpawner | WaitForAuthorityPos] prefab : " + prefab.name + " | player : " + player.name);

			if(isServer)
            {
				Debug.Log("[ItemPackageSpawner | WaitForAuthorityPos | Server]");
				previewObject = Instantiate(prefab, transform.position, rot) as GameObject;
				Debug.Log("[ItemPackageSpawner | WaitForAuthorityPos | Server] Instanciate previewObject : " + previewObject.name + " | player : " + player.name);
				bool isSpawn = NetworkServer.SpawnWithClientAuthority(previewObject, player.connectionToClient);
				Debug.Log("[ItemPackageSpawner | WaitForAuthorityPos | Server] SpawnWithClientAuthority : " + isSpawn);
			}
			else
            {
				CmdSpawnWithClientAuthorityPos(prefab.name, player, transform.position, rot);
			}
			StartCoroutine(WaitForSpawnPos(transform, rot));
		}

		private IEnumerator WaitForAuthorityHandGrab(GameObject prefab, NetworkIdentity player, Hand hand, GrabTypes grabType, int var)
        {
			Debug.Log("[ItemPackageSpawner | WaitForAuthorityHandGrab] hasAutority" + this.hasAuthority);
			if (!this.hasAuthority)
			{
				player.GetComponent<NetworkPlayer>().CmdSetAuth(this.GetComponent<NetworkIdentity>(), player);

				while (!this.hasAuthority)
				{
					Debug.Log("[ItemPackageSpawner | WaitForAuthorityHandGrab] Wait authority");
					yield return new WaitForEndOfFrame();
				}
			}
			Debug.Log("[ItemPackageSpawner | WaitForAuthorityHandGrab] hasAuthority : " + hasAuthority);
			Debug.Log("[ItemPackageSpawner | WaitForAuthorityHandGrab] prefab : " + prefab.name + " | player : " + player.name);

			if(isServer)
            {
				
				if(var == 1)
                {
					Debug.Log("[ItemPackageSpawner | WaitForAuthorityHandGrab | spawnedItem | Server]");
					spawnedItem = GameObject.Instantiate(prefab);
					Debug.Log("[ItemPackageSpawner | WaitForAuthorityHandGrab | spawnedItem | Server] Instanciate spawnedItem : " + spawnedItem.name + " | player : " + player.name);
					bool isSpawn = NetworkServer.SpawnWithClientAuthority(spawnedItem, player.connectionToClient);
					Debug.Log("[ItemPackageSpawner | WaitForAuthorityHandGrab | spawnedItem | Server] SpawnWithClientAuthority : " + isSpawn);
				}
				else
                {
					Debug.Log("[ItemPackageSpawner | WaitForAuthorityHandGrab | otherHandObjectToAttach | Server]");
					otherHandObjectToAttach = GameObject.Instantiate(prefab);
					Debug.Log("[ItemPackageSpawner | WaitForAuthorityHandGrab | otherHandObjectToAttach | Server] Instanciate otherHandObjectToAttach : " + otherHandObjectToAttach.name + " | player : " + player.name);
					bool isSpawn = NetworkServer.SpawnWithClientAuthority(otherHandObjectToAttach, player.connectionToClient);
					Debug.Log("[ItemPackageSpawner | WaitForAuthorityHandGrab | otherHandObjectToAttach | Server] SpawnWithClientAuthority : " + isSpawn);
				}
			}
			else
            {
				CmdSpawnWithClientAuthority(prefab.name, player, var);
			}
			StartCoroutine(WaitForSpawnHandGrab(hand, grabType, var));
		}

        private IEnumerator WaitForSpawnHandGrab(Hand hand, GrabTypes grabType, int var)
        {
			if(var == 1)
            {
				while (!spawnedItem)
				{
					Debug.Log("[ItemPackageSpawner | WaitForSpawnHandGrab] Wait spawnedItem");
					yield return new WaitForEndOfFrame();
				}
				Debug.Log("[ItemPackageSpawner | WaitForSpawnHandGrab] spawnedItem.name :" + spawnedItem.name);
				spawnedItem.SetActive(true);
				hand.AttachObject(spawnedItem, grabType, attachmentFlags);
				spawnedItemIsReady = true;
			}
			else
            {
				while (!otherHandObjectToAttach)
				{
					Debug.Log("[ItemPackageSpawner | WaitForSpawnHandGrab] Wait otherHandObjectToAttach");
					yield return new WaitForEndOfFrame();
				}
				Debug.Log("[ItemPackageSpawner | WaitForSpawnHandGrab] otherHandObjectToAttach.name :" + otherHandObjectToAttach.name);
				otherHandObjectToAttach.SetActive(true);
				hand.otherHand.AttachObject(otherHandObjectToAttach, grabType, attachmentFlags);
				otherHandObjectToAttachIsReady = true;
			}	
		}

		private IEnumerator WaitForSpawnPos(Transform transform, Quaternion rot)
		{
			while (!previewObject)
			{
				Debug.Log("[ItemPackageSpawner | WaitForSpawnPos] Wait previewObject");
				yield return new WaitForEndOfFrame();
			}
			Debug.Log("[ItemPackageSpawner | WaitForSpawnPos] previewObject.name :" + previewObject.name);
			previewObject.transform.parent = transform;
			previewObject.transform.localRotation = rot;
		}

		[Command]
        private void CmdSpawnWithClientAuthority(String prefabName, NetworkIdentity networkIdentity, int var)
        {
			GameObject prefab = null;
            List<GameObject> spawnPrefabs = NetworkManagerCustom.singleton.spawnPrefabs;
			foreach(GameObject spawnPrefab in spawnPrefabs)
            {
				Debug.Log("[ItemPackageSpawner | CmdSpawnWithClientAuthority] spawnPrefab.name : " + spawnPrefab.name);
				if (prefabName == spawnPrefab.name)
                {
					prefab = spawnPrefab;
					Debug.Log("[ItemPackageSpawner | CmdSpawnWithClientAuthority] prefab.name : " + prefab.name);
				}
            }

			Debug.Log("[ItemPackageSpawner | CmdSpawnWithClientAuthority] prefab.name : " + prefab.name);
			GameObject mySpawnedItem = GameObject.Instantiate(prefab);
			Debug.Log("[ItemPackageSpawner | CmdSpawnWithClientAuthority] Instanciate spawnedItem : " + mySpawnedItem.name + " | player : " + networkIdentity.name);
			bool isSpawn = NetworkServer.SpawnWithClientAuthority(mySpawnedItem, networkIdentity.connectionToClient);
			Debug.Log("[ItemPackageSpawner | CmdSpawnWithClientAuthority] SpawnWithClientAuthority : " + isSpawn);
			RpcUpdateMySpawnedItem(mySpawnedItem,var);
		}

		[Command]
		private void CmdSpawnWithClientAuthorityPos(String prefabName, NetworkIdentity networkIdentity, Vector3 pos, Quaternion rot)
		{
			GameObject prefab = null;
			List<GameObject> spawnPrefabs = NetworkManagerCustom.singleton.spawnPrefabs;
			foreach (GameObject spawnPrefab in spawnPrefabs)
			{
				Debug.Log("[ItemPackageSpawner | CmdSpawnWithClientAuthority] spawnPrefab.name : " + spawnPrefab.name);
				if (prefabName == spawnPrefab.name)
				{
					prefab = spawnPrefab;
					Debug.Log("[ItemPackageSpawner | CmdSpawnWithClientAuthority] prefab.name : " + prefab.name);
				}
			}

			Debug.Log("[ItemPackageSpawner | CmdSpawnWithClientAuthority] prefab.name : " + prefab.name);
			GameObject mySpawnedItem = GameObject.Instantiate(prefab, pos, rot);
			Debug.Log("[ItemPackageSpawner | CmdSpawnWithClientAuthority] Instanciate spawnedItem : " + mySpawnedItem.name + " | player : " + networkIdentity.name);
			bool isSpawn = NetworkServer.SpawnWithClientAuthority(mySpawnedItem, networkIdentity.connectionToClient);
			Debug.Log("[ItemPackageSpawner | CmdSpawnWithClientAuthority] SpawnWithClientAuthority : " + isSpawn);
			RpcUpdateMySpawnedItem(mySpawnedItem, 0);
		}

		[ClientRpc]
        private void RpcUpdateMySpawnedItem(GameObject mySpawnedItem, int var)
        {
			Debug.Log("[ItemPackageSpawner | RpcUpdateMySpawnedItem] mySpawnedItem.name : " + mySpawnedItem.name);

			switch(var)
            {
				case 0:
					previewObject = mySpawnedItem;
					break;
				case 1:
					spawnedItem = mySpawnedItem;
					break;
				case 2:
					otherHandObjectToAttach = mySpawnedItem;
					break;
			}
		}

		//Destroy

		[Command]
		private void CmdDestroy(NetworkIdentity id)
		{
			Debug.Log("CmdDestroy : " + id.name);
			NetworkServer.UnSpawn(id.gameObject);
		}

		[Command]
		private void CmdGameObjectDestroy(NetworkIdentity id)
		{
			NetworkServer.UnSpawn(id.gameObject);
		}

		[Command]
		private void CmdGameObjectDestroyImediate(NetworkIdentity id)
		{
			NetworkServer.UnSpawn(id.gameObject);
		}
	}
}
