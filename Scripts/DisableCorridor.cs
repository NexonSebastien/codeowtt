﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class DisableCorridor : MonoBehaviour
{
    public GameObject m_allLight;
    public GameObject m_teleportArea;
    [SerializeField] private Network_AudioController m_netAudioController;
    private bool isPlayed = false;
    //public string m_tagNameOfTeleportArea;

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        TeleportArea TpA = m_teleportArea.GetComponent<TeleportArea>();
        if (other.gameObject.name == "BodyTrigger" && TpA.locked == false && m_netAudioController.GetIsFinishPoseidonRoom() && !isPlayed)
        {
            isPlayed = true;
            TpA.SetLocked(true);
            TpA.markerActive = false;

            StartCoroutine(WaitStopLight());
        }
    }

    private void OnTriggerStay(Collider other)
    {
        TeleportArea TpA = m_teleportArea.GetComponent<TeleportArea>();
        if (other.gameObject.name == "BodyTrigger" && TpA.locked == false && m_netAudioController.GetIsFinishPoseidonRoom() && !isPlayed)
        {
            isPlayed = true;
            TpA.SetLocked(true);
            TpA.markerActive = false;

            StartCoroutine(WaitStopLight());
        }
    }

    IEnumerator WaitStopLight()
    {
        for (int i = 0; i < m_allLight.gameObject.transform.childCount; i++)
        {
            m_allLight.gameObject.transform.GetChild(i).gameObject.GetComponent<Light>().enabled = false;
            yield return new WaitForSeconds(0.5f);
            
        }
        Destroy(this.gameObject.GetComponent<DisableCorridor>());
        yield return 0;
    }
}
