﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drawing : MonoBehaviour
{
    private ParticleSystem m_particleSystem;


    // Start is called before the first frame update
    void Start()
    {
        m_particleSystem = this.transform.GetChild(0).GetComponent<ParticleSystem>();
        m_particleSystem.Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnCollisionEnter(Collision collision)
    {
        //Collider myCollider = collision.GetContact(0).thisCollider;
        
        if (collision.gameObject.tag == "drawable")
        {
            //Debug.Log("DRAW : Start");
            //ps.Play();
            
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "drawable")
        {
            //Debug.Log("DRAW : Stop");
            //ps.Pause(true);
        }

    }
}
