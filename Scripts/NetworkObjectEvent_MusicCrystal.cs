﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

[System.Obsolete]
[RequireComponent(typeof(NetworkObjectEvent_MusicCrystal))]
public class NetworkObjectEvent_MusicCrystal : NetworkObjectEvent
{
    [SerializeField] private List<MusicCrystal> m_MusicCrystals;
    [SerializeField] NetworkIdentity m_WaterNetId;
    [SerializeField] private List<NetworkObjectEvent_SoluceCrystal> m_soluceCrystals;
    private List<MusicCrystal> m_musicCrystals;
    private bool m_isComplete;
    private List<int> m_listSoluceSize;
    private List<List<int>> m_listSoluce;
    private List<int[]> m_listSoluceSync;
    [SyncVar] private int m_indexLists;
    [SyncVar] private int m_indexListElement;
    private System.Random _rand;
    private int HitCrystal = 0;
    EnigmaManager_ApollonRoom Apollon;

    override protected void Start()
    {
        Apollon = (EnigmaManager_ApollonRoom)m_EnigmaManager;
        base.Start();
        _rand = new System.Random();
        m_isComplete = false;
        m_listSoluceSize = new List<int>() { 4, 6, 8 };
        StartCoroutine(InitMusicCrystals());
        m_listSoluce = new List<List<int>>();
        m_listSoluceSync = new List<int[]>();
        for (int i = 0; i < m_listSoluceSize.Count; i++)
        {
            m_listSoluceSync.Add(new int[m_listSoluceSize[i]]);
        }
        if (isServer)
        {
            StartCoroutine(InitRandomListSoluce());
        }
        else
        {
            if (m_listSoluce.Count == m_listSoluceSize.Count)
            {
                Debug.Log("[NetworkObjectEvent_MusicCrystal:Start:skipAskingForSyncListSoluceForHost]");
                return;
            }
            /*
            Debug.Log("[NetworkObjectEvent_MusicCrystal:Start:checkingForPlayer]");
            m[] players = (m[])FindObjectsOfType(typeof(m));
            m player = null;
            int index = 0;
            while (player == null && index < players.Length)
            {
                if (players[index].isLocalPlayer)
                {
                    Debug.Log("[NetworkObjectEvent_MusicCrystal:Start:playerFound]");
                    player = players[index];
                }
                index++;
            }
            Debug.Log("[NetworkObjectEvent_MusicCrystal:Start:setAuthority:" + hasAuthority + "]");
            player.CmdSetAuth(m_NetId, player.GetComponent<NetworkIdentity>());
            Debug.Log("[NetworkObjectEvent_MusicCrystal:Start:setAuthority:" + hasAuthority + "]");
            Debug.Log("[NetworkObjectEvent_MusicCrystal:Start:AskSyncListSoluce()]");
            */
            StartCoroutine(AskSyncListSoluce());
        }
    }

    private void Update()
    {
        if (!isServer) return;
        NetworkObjectEvent_MusicCrystal ct = NetworkServer.FindLocalObject(m_NetId.netId).GetComponent<NetworkObjectEvent_MusicCrystal>();
        if (ct.IsCompleted() && m_EnigmaManager.IsRunning())
        {
            Debug.Log("[NetworkObjectEvent_MusicCrystal:OnTriggerEnter(IsCompleted(After)=" + ct.IsCompleted().ToString() + "]");
            UpdateEnigma(m_NetId, true);
        }

    }

    private void PrintListSoluce()
    {
        string message = "[PrintListSoluce] = ";
        for (int i = 0; i < m_listSoluce.Count; i++)
        {
            message += "list(" + i + ")={";
            for (int j = 0; j < m_listSoluce[i].Count; j++)
            {
                message += m_listSoluce[i][j];
                if (j < m_listSoluce[i].Count - 1) { message += ", "; }
            }
            message += "}";
            if (i < m_listSoluce.Count - 1) { message += ", "; }
        }
        Debug.Log(message);
    }

    private void PrintListSoluceSync()
    {
        string message = "[PrintListSoluceSync] = ";
        for (int i = 0; i < m_listSoluceSync.Count; i++)
        {
            message += "list(" + i + ")={";
            for (int j = 0; j < m_listSoluceSync[i].Length; j++)
            {
                message += m_listSoluceSync[i][j];
                if (j < m_listSoluceSync[i].Length - 1) { message += ", "; }
            }
            message += "}";
            if (i < m_listSoluceSync.Count - 1) { message += ", "; }
        }
        Debug.Log(message);
    }

    public override void EnigmaReceiveEvent(int eventCase, List<object> args)
    {
        Debug.Log("[NetworkObjectEvent_MusicCrystal:EnigmaReceiveEvent(IsRunning=" + m_EnigmaManager.IsRunning().ToString() + ", eventCase=" + eventCase.ToString() + "]");
        if (isServer)
        {
            switch (eventCase)
            {
                case 1:
                    // rod hit crystal : crystal interaction
                    RpcInteract((int)args[0], (bool)args[1]);
                    break;
                case 2:
                    // hit wrong crystal : accelerate water
                    WaterAccelerate();
                    break;
                case 3:
                    ReplicateListSoluce();
                    break;
            }
        }
    }

    public override void EnigmaUpdateEvent()
    {
        //throw new System.NotImplementedException();
    }

    [ServerCallback]
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("musicCrystal"))
        {
            int crystalId = other.GetComponent<MusicCrystal>().GetId();
            NetworkObjectEvent_MusicCrystal ct = NetworkServer.FindLocalObject(m_NetId.netId).GetComponent<NetworkObjectEvent_MusicCrystal>();
            List<object> args = new List<object> { crystalId }; // get music crystal id
            Debug.Log("[NetworkObjectEvent_MusicCrystal:OnTriggerEnter(IsCompleted(Before)=" + ct.IsCompleted().ToString() + "]");
            if (!ct.IsCompleted())
            {
                bool correctHit = ct.OnHitCrystal(crystalId);
                args.Add(correctHit);
                ThrowEvent(m_NetId, 1, args);
                if (!correctHit)
                {
                    ThrowEvent(m_NetId, 2, null);

                }

            }
        }
    }

    [ClientRpc]
    private void RpcInteract(int crystalId, bool correctHit)
    {
        for (int i = 0; i < m_MusicCrystals.Count; i++)
        {
            if (m_MusicCrystals[i].GetId() == crystalId)
            {
                m_MusicCrystals[i].Interact(correctHit);
            }
        }
    }

    private void WaterAccelerate()
    {
        Debug.Log("[NetworkObjectEvent_MusicCrystal:WaterAccelerate]");
        m_WaterNetId.GetComponent<WaterManagement>().Accelerate();
    }

    public List<MusicCrystal> GetMusicCrystals()
    {
        return m_MusicCrystals;
    }

    private IEnumerator InitMusicCrystals()
    {
        Debug.Log("[NetworkObjectEvent_MusicCrystal:InitMusicCrystals]");
        yield return new WaitForEndOfFrame();
        m_musicCrystals = GetComponent<NetworkObjectEvent_MusicCrystal>().GetMusicCrystals();
        yield return null;
    }

    private IEnumerator InitRandomListSoluce()
    {
        Debug.Log("[NetworkObjectEvent_MusicCrystal:InitRandomListSoluce]");
        m_indexLists = 0;
        m_indexListElement = 0;
        yield return new WaitForEndOfFrame();
        for (int i = 0; i < m_listSoluceSize.Count; i++)
        {
            m_listSoluce.Add(RandomListInt(m_listSoluceSize[i]));
            string sList = "List[" + i + "] = ";
            for (int j = 0; j < m_listSoluce[i].Count; j++)
            {
                sList += m_listSoluce[i][j];
                if (j < m_listSoluce[i].Count - 1) { sList += ", "; }
            }
            Debug.Log("[NetworkObjectEvent_MusicCrystal:InitRandomListSoluce:list=[" + sList + "]]");
        }
        PrintListSoluce();
        yield return null;
    }

    private List<int> RandomListInt(int maxElements)
    {
        Debug.Log("[NetworkObjectEvent_MusicCrystal:RandomListInt:maxElements=" + maxElements + "]");
        return Enumerable.Range(0, maxElements).Select(r => _rand.Next(1, m_musicCrystals.Count + 1)).ToList();
    }

    private IEnumerator AskSyncListSoluce()
    {
        yield return new WaitForEndOfFrame();
        NetworkPlayer player = GameObject.Find("VrGameObject").GetComponentInParent<NetworkPlayer>();
        player.CmdSetAuth(m_NetId, player.GetComponent<NetworkIdentity>());
        Debug.Log("[NetworkObjectEvent_MusicCrystal:AskSyncListSoluce:WaitUntilHasAuthority(start):" + hasAuthority + "]");
        yield return new WaitUntil(() => hasAuthority);
        Debug.Log("[NetworkObjectEvent_MusicCrystal:AskSyncListSoluce:WaitUntilHasAuthority(end):" + hasAuthority + "]");
        CmdAskSyncListSoluce();
        //Debug.Log("[NetworkObjectEvent_MusicCrystal:AskSyncListSoluce:WaitWhile:" + m_listSoluce.Count + " == " + (m_listSoluceSize.Count - 1) + "]");
        //yield return new WaitUntil(() => m_listSoluce.Count == m_listSoluceSize.Count);
        //Debug.Log("[NetworkObjectEvent_MusicCrystal:AskSyncListSoluce:WaitWhile:" + m_listSoluce[m_listSoluceSize.Count - 1].Count + " == " + m_listSoluceSize[m_listSoluceSize.Count - 1] + "]");
        //yield return new WaitUntil(() => m_listSoluce[m_listSoluceSize.Count - 1].Count == m_listSoluceSize[m_listSoluceSize.Count - 1]);
        /*
        for (int i = 0; i < m_listSoluceSize.Count; i++)
        {
            string sList = "List[" + i + "] = ";
            for (int j = 0; j < m_listSoluce[i].Count; j++)
            {
                sList += m_listSoluce[i][j];
                if (j < m_listSoluce[i].Count - 1) { sList += ", "; }
            }
            Debug.Log("[NetworkObjectEvent_MusicCrystal:AskSyncListSoluce:list=[" + sList + "]]");
        }
        */
        Debug.Log("[NetworkObjectEvent_MusicCrystal:AskSyncListSoluce:listReceived]");
        yield return null;
    }

    [Command]
    public void CmdAskSyncListSoluce()
    {
        Debug.Log("[NetworkObjectEvent_MusicCrystal:CmdAskSyncListSoluce]");
        for (int i = 0; i < m_listSoluceSize.Count; i++)
        {
            for (int j = 0; j < m_listSoluce[i].Count; j++)
            {
                RpcFeedSyncListSoluceElement(i, j, m_listSoluce[i][j]);
            }
        }
        /*
        m_NetId.RemoveClientAuthority(m_NetId.connectionToClient);
        m_NetId.AssignClientAuthority(m_NetId.connectionToServer);
        */
    }

    private void ReplicateListSoluce()
    {
        if (!isServer) return;
        Debug.Log("[NetworkObjectEvent_MusicCrystal:ReplicateListSoluce]");
        for (int i = 0; i < m_listSoluceSize.Count; i++)
        {
            for (int j = 0; j < m_listSoluce[i].Count; j++)
            {
                RpcFeedSyncListSoluceElement(i, j, m_listSoluce[i][j]);
            }
        }
    }

    [ClientRpc]
    public void RpcFeedSyncListSoluceElement(int indexList, int indexElement, int value)
    {
        Debug.Log("[NetworkObjectEvent_MusicCrystal:RpcFeedSyncListSoluceElement:[" + indexList + "][" + indexElement + "]=" + value + "]");
        StartCoroutine(FeedSyncListSoluceElement(indexList, indexElement, value));
    }

    private IEnumerator FeedSyncListSoluceElement(int indexList, int indexElement, int value)
    {
        Debug.Log("[NetworkObjectEvent_MusicCrystal:FeedSyncListSoluceElement:[" + indexList + "][" + indexElement + "]=" + value + "]");
        /*yield return new WaitWhile(() => m_listSoluce.Count < indexList);
        yield return new WaitWhile(() => m_listSoluce[indexList].Count < indexElement);*/
        /*
        Debug.Log("[NetworkObjectEvent_MusicCrystal:FeedSyncListSoluceElement:[" + indexList + "][" + indexElement + "]=" + value + "] check before add");
        if (indexElement == m_listSoluce[indexList].Count) // skip if index > m_listSoluce.Count
        {
            Debug.Log("[NetworkObjectEvent_MusicCrystal:FeedSyncListSoluceElement:[" + indexList + "][" + indexElement + "]=" + value + "] added");
            m_listSoluce[indexList].Add(value);
        }
        Debug.Log("[NetworkObjectEvent_MusicCrystal:FeedSyncListSoluceElement:[" + indexList + "][" + indexElement + "]=" + value + "] added");
        */
        /*
        if(indexList == m_listSoluceSync.Count)
        {
            Debug.Log("[NetworkObjectEvent_MusicCrystal:FeedSyncListSoluceElement:list added at index " + indexList + "]");
            m_listSoluceSync.Add(new List<int>());
        }
        m_listSoluceSync[indexList].Add(value);
        if(m_listSoluceSync.Count == m_listSoluceSize.Count && m_listSoluceSync[m_listSoluceSync.Count - 1].Count == m_listSoluceSize[m_listSoluceSize.Count - 1])
        {
            m_listSoluce = m_listSoluceSync;
            PrintListSoluce();
        }
        */
        if (indexList >= 0 && indexList < m_listSoluceSync.Count && indexElement >= 0 && indexElement < m_listSoluceSync[indexList].Length)
        {
            m_listSoluceSync[indexList][indexElement] = value;
            PrintListSoluceSync();
            if (indexList == m_listSoluceSync.Count - 1 && indexElement == m_listSoluceSync[indexList].Length - 1)
            {
                m_listSoluce.Clear();
                for (int i = 0; i < m_listSoluceSync.Count; i++)
                {
                    m_listSoluce.Add(m_listSoluceSync[i].ToList());
                    PrintListSoluce();
                }
                m_listSoluceSync.Clear();
            }
        }
        yield return null;
    }

    public bool IsCompleted()
    {
        //Debug.Log("[NetworkObjectEvent_MusicCrystal:IsCompleted="+ m_isComplete + "]");
        return m_isComplete;
    }

    public List<int> GetCurrentListSoluce()
    {
        Debug.Log("[NetworkObjectEvent_MusicCrystal:GetCurrentListSoluce]");
        return m_listSoluce[m_indexLists];
        //return NetworkServer.FindLocalObject(m_NetId.netId).GetComponent<NetworkObjectEvent_MusicCrystal>().GetCurrentListSoluce();
    }

    public bool OnHitCrystal(int id)
    {
        //metric
        Apollon.IncrementNbrCristHit();
        
        if (m_listSoluce[m_indexLists][m_indexListElement] == id)
        {
            Debug.Log("[NetworkObjectEvent_MusicCrystal:OnHitCrystal:id=" + id + ":return=true]");
            StartCoroutine(NextElement(id));
            return true;
        }
        else
        {
            Debug.Log("[NetworkObjectEvent_MusicCrystal:OnHitCrystal:id=" + id + ":return=false]");
            StartCoroutine(RestartList());
            return false;
        }

    }


    private IEnumerator NextElement(int id)
    {
        if (++m_indexListElement >= m_listSoluce[m_indexLists].Count)
        {
            StartCoroutine(RestartList());
            if (++m_indexLists >= m_listSoluce.Count)
            {
                Debug.Log("[NetworkObjectEvent_MusicCrystal:NextElement:id=" + id + ":isComplete[" + m_indexLists + "][" + m_indexListElement + "]]");
                m_isComplete = true; // enigma completed
            }
            else
            {
                Debug.Log("[NetworkObjectEvent_MusicCrystal:NextElement:id=" + id + ":nextList[" + m_indexLists + "][" + m_indexListElement + "]]");
                // update two new soluce crystals positions to match list length
                AddSoluceCrystal(m_soluceCrystals[m_listSoluceSize[m_indexLists] - 2]);
                AddSoluceCrystal(m_soluceCrystals[m_listSoluceSize[m_indexLists] - 1]);
            }
        }
        else
        {
            Debug.Log("[NetworkObjectEvent_MusicCrystal:NextElement:id=" + id + ":nextElement[" + m_indexLists + "][" + (m_indexListElement - 1) + "]]");
            SetCrystalMaterial(m_soluceCrystals[m_indexListElement - 1], id); // set crystal material
        }
        yield return null;
    }

    private IEnumerator RestartList()
    {
        Debug.Log("[NetworkObjectEvent_MusicCrystal:RestartList]");
        m_indexListElement = 0; // reset element index
        ResetCrystalsMaterial(); // reset all crystals material to default
        yield return null;
    }

    private void AddSoluceCrystal(NetworkObjectEvent_SoluceCrystal soluceCrystal)
    {
        Debug.Log("[NetworkObjectEvent_MusicCrystal:AddSoluceCrystal:soluceCrystal.name=" + soluceCrystal.name + "]");
        ThrowEvent(soluceCrystal.GetComponent<NetworkIdentity>(), 1, new List<object>() { m_soluceCrystals[0].transform.position.z });
    }

    private void SetCrystalMaterial(NetworkObjectEvent_SoluceCrystal soluceCrystal, int materialIndex)
    {
        Debug.Log("[NetworkObjectEvent_MusicCrystal:SetCrystalMaterial:soluceCrystal.name=" + soluceCrystal.name + ":materialIndex=" + materialIndex + "]");
        ThrowEvent(soluceCrystal.GetComponent<NetworkIdentity>(), 2, new List<object>() { materialIndex });
    }

    private void ResetCrystalsMaterial()
    {
        Debug.Log("[NetworkObjectEvent_MusicCrystal:ResetCrystalsMaterial]");
        for (int i = 0; i < m_listSoluceSize[m_indexLists]; i++)
        {
            SetCrystalMaterial(m_soluceCrystals[i], -1);
        }
    }

    public void NbrPlayCristal()
    {
        //metric
        Apollon.IncrementNbrPlay();
    }
}
