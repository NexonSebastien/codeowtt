﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MyNetworkObject : NetworkBehaviour
{
    [SyncVar]
    public Vector3 myColorVec3;
    public Color Color;
    public MyGameManager mgm;

    public Material m_Material;
    public bool m_Status;
    public NetworkIdentity m_Id;

    // Start is called before the first frame update
    void Start()
    {
        m_Material = GetComponent<Renderer>().material;
        //myColorVec3 = new Vector3(Color.red.r,Color.red.g,Color.red.b);

        m_Material.color = new Color(myColorVec3.x, myColorVec3.y, myColorVec3.z);
        m_Status = false;
        m_Id = GetComponent<NetworkIdentity>();
    }

    // Update is called once per frame
    private void Update()
    {
        
    }

    public void SetColor(Color newColor)
    {
        Debug.Log("set new color");
        myColorVec3 = new Vector3(newColor.r, newColor.g, newColor.b);
        RpcUpdateColor(myColorVec3);
    }

    [ServerCallback]
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("trigger enter with player");
            //myColorVec3 = new Vector3(Random.ColorHSV().r, Random.ColorHSV().g, Random.ColorHSV().b);
            mgm.UpdateObjectStatus(GetComponent<NetworkIdentity>(),true);
            mgm.UpdateObjectStatus(GetComponent<NetworkIdentity>(),15);

        }
    }

    [ServerCallback]
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("trigger exit with player");
            //myColorVec3 = new Vector3(Random.ColorHSV().r, Random.ColorHSV().g, Random.ColorHSV().b);
            mgm.UpdateObjectStatus(GetComponent<NetworkIdentity>(), false);
            mgm.UpdateObjectStatus(GetComponent<NetworkIdentity>(), 0);

        }
    }

    [ClientRpc]
    public void RpcUpdateColor(Vector3 v)
    {
        m_Material.color = new Color(v.x, v.y, v.z);
    }
}
