﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundRuby : MonoBehaviour
{
    private FMOD.Studio.EventInstance instance;
    private string Event = "event:/Objets/Ruby";

    // Start is called before the first frame update
    void Start()
    {
        instance = FMODUnity.RuntimeManager.CreateInstance(Event);
        //FMODUnity.RuntimeManager.PlayOneShotAttached("event:/Champs/ChampContinue", this.gameObject);
        instance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(this.gameObject));
        instance.start();
    }

    // Update is called once per frame
    void Update()
    {
        instance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(this.gameObject));
    }

    public void StopSon()
    {
        instance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        instance.release();
    }
}
