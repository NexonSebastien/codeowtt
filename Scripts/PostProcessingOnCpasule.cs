﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostProcessingOnCpasule : MonoBehaviour
{
    [SerializeField] EnigmaManager_Lobby m_EMLobby = null;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Ocean")
        {
            m_EMLobby.SetIsInWater(true);
        }
    }
}
