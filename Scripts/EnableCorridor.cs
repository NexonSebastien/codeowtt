﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableCorridor : MonoBehaviour
{
    public GameObject m_allLight;
    //public string m_tagNameOfTeleportArea;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "BodyTrigger")
        {
            /*GameObject[] allTeleportArea = GameObject.FindGameObjectsWithTag(m_tagNameOfTeleportArea);

            foreach(GameObject teleportArea in allTeleportArea)
            {
                teleportArea.SetActive(true);
            }*/

            StartCoroutine(WaitStartLight());
        }
    }

    IEnumerator WaitStartLight()
    {
        for (int i = m_allLight.gameObject.transform.childCount - 1; i >= 0; i--)
        {
            m_allLight.gameObject.transform.GetChild(i).gameObject.GetComponent<Light>().enabled = true;
            yield return new WaitForSeconds(0.2f);
            
        }
        yield return 0;
    }
}
