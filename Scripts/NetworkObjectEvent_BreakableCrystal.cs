﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[System.Obsolete]
public class NetworkObjectEvent_BreakableCrystal : NetworkObjectEvent
{
    [SerializeField] private Material m_materialUnvailable;
    [SerializeField] private Material m_materialAvailable;
    private float m_intensityHigh;
    private float m_intensityLow;

    override protected void Start()
    {
        base.Start();
        m_intensityHigh = 10.0f;
        m_intensityLow = 0.0f;
        SetIsAvailable(false);
    }

    public override void EnigmaReceiveEvent(int eventCase, List<object> args)
    {
        if (isServer)
        {
            switch (eventCase)
            {
                case 1:
                    RpcSetIsAvailable((bool)args[0]);
                    break;
            }
        }
    }

    public override void EnigmaUpdateEvent()
    {
        Debug.Log("[NetworkObjectEvent_BreakableCrystal:EnigmaUpdateEvent]");
        RpcBreakCrystal();
    }

    [ServerCallback]
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider && collision.gameObject.CompareTag("CrystalBreaker") || collision.gameObject.CompareTag("Hammer") || collision.gameObject.CompareTag("projectile"))
        {
            Debug.Log("[NetworkObjectEvent_BreakableCrystal:OnCollisionEnter]");
            UpdateEnigma(m_NetId, true);
        }
    }

    [ClientRpc]
    public void RpcBreakCrystal()
    {
        Debug.Log("[NetworkObjectEvent_BreakableCrystal:RpcBreakCrystal]");
        Destroy(GetComponent<BoxCollider>());
        Destroy(GetComponent<Rigidbody>());
        int iMax = transform.childCount - 1;
        FMODUnity.RuntimeManager.PlayOneShotAttached("event:/Objets/DestructionCristal", this.gameObject);
        for (int i = iMax; i >= 0; i--)
        {
            GameObject child = transform.GetChild(i).gameObject;
            child.AddComponent(typeof(Rigidbody));
            child.AddComponent(typeof(MeshCollider));
            child.GetComponent<MeshCollider>().convex = true;
        }
    }

    [ClientRpc]
    public void RpcSetIsAvailable(bool available)
    {
        Debug.Log("[NetworkObjectEvent_BreakableCrystal:RpcSetIsAvailable:available" + available + "]");
        SetIsAvailable(available);
    }

    private void SetIsAvailable(bool available)
    {
        Debug.Log("[NetworkObjectEvent_BreakableCrystal:SetIsAvailable:available" + available + "]");
        Material material;
        float intensity;
        if (available)
        {
            material = m_materialAvailable;
            intensity = m_intensityHigh;
        }
        else
        {
            material = m_materialUnvailable;
            intensity = m_intensityLow;
        }
        GetComponentInChildren<Light>().intensity = intensity;

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).TryGetComponent(out Renderer renderer))
            {
                renderer.sharedMaterial = material;
            }
        }
    }
}
