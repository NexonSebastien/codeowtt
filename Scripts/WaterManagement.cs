﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Valve.VR.InteractionSystem;

[System.Obsolete]
public class WaterManagement : NetworkObjectEvent
{
    private float m_defaultSpeed; // speed by default (m/s)
    private float m_accelerate; //accelerate when you fail (m/s)
    private float m_slow; //slow the speed when you turn the valve (m/s)
    private float m_downSpeed; // speed of the water when she downs (m/s) 
    private float m_speed; // speed of the water (m/s)
    private float m_duration; // acceleration time (second)
    private float m_maxSpeed; // maximal speed water can reach (m/s)
    private bool m_interactWithvalve; // if the valve is interacted by player
    private bool m_waterDown; // if water goes downside
    private float m_startPosY; // water position at the start
    [SerializeField] private bool m_isStarted; // event is activate by the player
    [SerializeField] private bool m_isFinished; // water is back to initial position
    [SerializeField] private GameObject m_valve;
    private FMOD.Studio.EventInstance m_instance;
    private FMOD.Studio.EventInstance m_instanceVidange;
    private bool firstVidange = true;
    private float m_vitesseSon;
    private bool m_sonOk = false;
    private float m_valveAngle;
    private GameObject m_player;
    [SerializeField] private float m_timePlayerHeadUnderWater;
    private bool m_headUnderWater;
    private float m_drownTime;
    //private PostProcessVolumeController m_ppvc;
    private FMOD.Studio.EventInstance m_instanceDialogueHelp;


    override protected void Start()
    {
        m_startPosY = transform.position.y;
        m_defaultSpeed = 0.00333f;
        m_speed = m_defaultSpeed;
        m_accelerate = 0.00011f;
        m_slow = 0.00011f;
        m_downSpeed = 0.2f;
        m_duration = 1f;
        m_maxSpeed = 0.00666f;
        m_interactWithvalve = false;
        m_waterDown = false;
        m_isFinished = false;
        m_isStarted = false;
        m_valveAngle = m_valve.GetComponent<CircularDrive>().outAngle;
        m_vitesseSon = 333;
        m_timePlayerHeadUnderWater = 0.0f;
        m_headUnderWater = false;
        m_drownTime = 90.0f;
        //m_ppvc = GetComponent<PostProcessVolumeController>();
    }
    private void Update()
    {
        if(m_isStarted == true && m_isFinished == false)
        {
            

            if (m_valve.GetComponent<CircularDrive>().outAngle > m_valveAngle)
            {
                SetValveInteraction(true);
            }
            else if(m_valve.GetComponent<CircularDrive>().outAngle < m_valveAngle)
            {
                SetValveInteraction(false);
            }
            m_valveAngle = m_valve.GetComponent<CircularDrive>().outAngle;

            if (m_waterDown)
            {
                m_instance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                m_instance.release();
                if (firstVidange)
                {
                    m_instanceVidange = FMODUnity.RuntimeManager.CreateInstance("event:/Ambiance/Vidange");
                    m_instanceVidange.start();
                    firstVidange = false;
                }
                if(isServer)
                {
                    WaterDown(); 
                }
            }
            else
            {
                WaterUp();
            }
            if(m_player != null)
            {
                if (transform.position.y > m_player.transform.GetChild(0).position.y + 1.45f)
                {
                    m_timePlayerHeadUnderWater += Time.deltaTime; // increase time when player's head is under water
                    m_headUnderWater = true;
                }
                else
                {
                    //m_timePlayerHeadUnderWater = 0.0f;
                    m_timePlayerHeadUnderWater -= Time.deltaTime * 7.5f; // decrease time when player's head is not under water
                    m_headUnderWater = false;
                }
                m_timePlayerHeadUnderWater = Mathf.Clamp(m_timePlayerHeadUnderWater, 0, m_drownTime);
                RpcChangePlayerVision(m_player.GetComponent<NetworkIdentity>(), m_timePlayerHeadUnderWater, m_headUnderWater);
                //ChangePlayerVision();
            }
        }
    }

    public IEnumerator Accelerate()
    {
        m_speed = Mathf.Clamp(m_speed + m_accelerate, m_defaultSpeed, m_maxSpeed);
        yield return new WaitForSeconds(m_duration);
        m_speed = Mathf.Clamp(m_speed - m_accelerate, m_defaultSpeed, m_maxSpeed);
        yield return null;
    }
    private void WaterUp() 
    {
        float speed = m_speed;
        m_vitesseSon = m_speed * 100000;
        if (m_interactWithvalve)
        {
            speed -= m_slow;
        }
        if (m_sonOk == false)
        {

            if(isServer)
            {
                RpcSoundWaterChange();
            }
            m_sonOk = true;
        }
        m_instance.setParameterByName("Debit", m_vitesseSon);
        transform.Translate(Vector3.up * speed * Time.deltaTime) ;
    }

    [ClientRpc]
    private void RpcSoundWaterChange()
    {
        m_instance = FMODUnity.RuntimeManager.CreateInstance("event:/Ambiance/Eau");
        m_instance.setParameterByName("Debit", m_vitesseSon);
        m_instance.start();
    }

    private void WaterDown()
    {   
        if(transform.position.y > m_startPosY)
        {
            transform.Translate(Vector3.down * m_downSpeed * Time.deltaTime);
        }
        else
        {
            RpcSoundFinish();
            m_isFinished = true;
        }
    }

    [ClientRpc]
    private void RpcSoundFinish()
    {
        m_instanceDialogueHelp.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        m_instanceDialogueHelp.release();
        m_instanceVidange.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        m_instanceVidange.release();
    }

    public void setWaterDown()
    {
        m_waterDown = true;
    }
    public void SetValveInteraction(bool interaction)
    {
        m_interactWithvalve = interaction;
    }
    public void SetStarter()
    {
        m_isStarted = true;
        if (isServer)
        {
            RpcDialogue();
        }
    }

    public bool IsFinished()
    {
        return m_isFinished;
    }

    public override void EnigmaUpdateEvent()
    {
        throw new System.NotImplementedException();
    }

    public override void EnigmaReceiveEvent(int eventCase, List<object> args)
    {
        if (isServer)
        {
            switch (eventCase)
            {
                case 1:
                    RpcSetStarter();
                    break;
            }
        }
    }

    [ClientRpc]
    public void RpcSetStarter()
    {
        Debug.Log("[WaterManagement:RpcSetStarter]");
        SetStarter();
    }

    [ClientRpc]
    public void RpcSetWaterFinished()
    {
        m_isFinished = true;
    }

    [ClientRpc]
    public void RpcDialogue()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/Dialogues/Salle2");
        m_instanceDialogueHelp = FMODUnity.RuntimeManager.CreateInstance("event:/Dialogues/Salle2Aide");
        m_instanceDialogueHelp.start();
    }

    public void SetPlayer(NetworkPlayer player)
    {
        if (!isServer) return;
        m_player = player.gameObject;
        //m_ppvc = player.GetComponent<PostProcessVolumeController>();
    }

    public bool IsPlayerDrown()
    {
        return m_timePlayerHeadUnderWater > m_drownTime;
    }

    [ClientRpc]
    private void RpcChangePlayerVision(NetworkIdentity playerNetId, float timePlayerHeadUnderWater, bool headUnderWater)
    {
        ChangePlayerVision(playerNetId, timePlayerHeadUnderWater, headUnderWater);
    }

    private void ChangePlayerVision(NetworkIdentity playerNetId, float timePlayerHeadUnderWater, bool headUnderWater)
    {
        if (playerNetId.isLocalPlayer)
        {
            PostProcessVolumeController ppvc = playerNetId.GetComponent<PostProcessVolumeController>();
            float value = Mathf.Log10(timePlayerHeadUnderWater / m_drownTime * 100) / 2;
            ppvc.SetGlobal(value > 0);
            ppvc.SetWeight(headUnderWater ? 0.95f : value);
            ppvc.SetVignetteIntensity(value);
            ppvc.ActiveColorGradding(headUnderWater);
        }
    }
}
