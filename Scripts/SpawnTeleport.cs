﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTeleport : MonoBehaviour
{
    [SerializeField] GameObject m_teleport;
    [SerializeField] GameObject m_playerVR;

    // Start is called before the first frame update
    void Start()
    {
        Instantiate(m_playerVR, m_playerVR.transform);
        Instantiate(m_teleport, m_teleport.transform);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
