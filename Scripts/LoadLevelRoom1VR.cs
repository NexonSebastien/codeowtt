﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class LoadLevelRoom1VR : MonoBehaviour
{
    [SerializeField] NetworkManager networkManager;
    [SerializeField] GameObject playerVrPrefab;
    public void LoadLevelRoom()
    {
        Debug.Log("VR");
        UnityEngine.XR.XRSettings.enabled = true;
        /**
         * Desactivate cause of bug made to null
         */
        //NetworkManager.singleton.playerPrefab = playerVrPrefab;

        NetworkManager.singleton.playerPrefab = playerVrPrefab;
        NetworkManager.singleton.autoCreatePlayer = false;
        NetworkManager.singleton.onlineScene = "similarRoomVr";
    }
}
