﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Valve.VR.InteractionSystem;

[System.Obsolete]
public class CircularDriveController : NetworkBehaviour
{
    private CircularDrive m_circularDrive;
    private NetworkIdentity m_circularDriveNetId;
    private GameObject m_vrGameObject;
    public float outAngle;

    private void Awake()
    {
        m_circularDrive = GetComponent<CircularDrive>();
        m_circularDriveNetId = GetComponent<NetworkIdentity>();
        m_vrGameObject = GameObject.Find("VrGameObject");
    }

    private void Update()
    {
        if(TryGetComponent(out CircularDrive c))
        {
            Hand playerHand = m_circularDrive.GetPlayerHand();
            if (!m_circularDriveNetId.hasAuthority)
            {
                m_circularDrive.outAngle = outAngle;
                if (playerHand != null)
                {
                    NetworkPlayer player = m_vrGameObject.GetComponentInParent<NetworkPlayer>();
                    player.CmdSetAuth(m_circularDriveNetId, player.GetComponent<NetworkIdentity>());
                }
            }
            if (m_circularDriveNetId.hasAuthority)
            {
                outAngle = m_circularDrive.outAngle;
                if (!isServer)
                {
                    CmdSetOutAngle(outAngle);
                }
                else
                {
                    RpcSetOutAngle(outAngle);
                }
            }
        }
    }

    [Command]
    public void CmdSetOutAngle(float angle)
    {
        RpcSetOutAngle(angle);
    }

    [ClientRpc]
    public void RpcSetOutAngle(float angle)
    {
        if (!m_circularDriveNetId.hasAuthority)
        {
            outAngle = angle;
            m_circularDrive.outAngle = outAngle;
        }
    }
}
