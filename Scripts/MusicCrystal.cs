﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[ExecuteInEditMode]
public class MusicCrystal : MonoBehaviour
{
    [SerializeField] private int m_Id; // crystal id

    [Header("Default values")]
    [SerializeField] private Color m_colorTint1; // current color tint 1 of the music crystal
    [SerializeField] private Color m_colorTint2; // current color tint 2 of the music crystal
    [SerializeField] private Color m_emissionColor; // current emission color of the music crystal
    [SerializeField] [Range(0, 10)] private float m_emissionPower; // current emission power of the music crystal
    [SerializeField] [Range(0, 50)] private int m_translucency; // current translucency of the music crystal

    [Header("Interaction values")]
    [SerializeField] private Color m_colorTint1Interact; // interaction color tint 1 of the music crystal
    [SerializeField] private Color m_colorTint2Interact; // interaction color tint 2 of the music crystal
    [SerializeField] private Color m_emissionColorInteract; // interaction emission color of the music crystal
    [SerializeField] [Range(0, 10)] private float m_emissionPowerInteract; // interaction emission power of the music crystal
    [SerializeField] [Range(0, 50)] private int m_translucencyInteract; // interaction translucency of the music crystal

    private AudioSource m_audioSource; // music crystal sound
    [SerializeField] AudioClip m_goodSound; // good song to play when hit by music rod
    [SerializeField] AudioClip m_badSound; // bad song to play when hit by music rod
    private MeshRenderer[] m_meshRenderers; // children renderers
    public bool available; // define if the music crystal is interatable
    private float delay; // delay before next interaction
    public float startTime; // current time when the interaction starts

    public bool isAvailable() { return available; }

    private void Awake()
    {
        m_meshRenderers = GetComponentsInChildren<MeshRenderer>();
        SetDefaultValues();
    }
    void Start()
    {
        m_audioSource = GetComponent<AudioSource>();
        available = true;
        delay = 1.0f;
        startTime = 0.0f;
    }

    void Update()
    {
        // wait delay then set the music crystal available for interaction
        if(!available && (startTime + delay) < Time.time)
        {
            available = true;
        }
    }

    public int GetId()
    {
        return m_Id;
    }
    public AudioClip GetAudioClip()
    {
        return m_goodSound;
    }

    // set default values to custom the music crystal
    private void SetDefaultValues()
    {
        // set default values to children
        for (int i = 0; i < m_meshRenderers.Length; i++)
        {
            m_meshRenderers[i].sharedMaterial.SetColor("_ColorTint1", m_colorTint1);
            m_meshRenderers[i].sharedMaterial.SetColor("_ColorTint2", m_colorTint2);
            m_meshRenderers[i].sharedMaterial.SetColor("_EmissionColor", m_emissionColor);
            m_meshRenderers[i].sharedMaterial.SetFloat("_EmissionPower", m_emissionPower);
            m_meshRenderers[i].sharedMaterial.SetInt("_Translucency", m_translucency);
        }
    }

    // set interaction values to custom the music crystal
    private void SetInteractionValues()
    {
        // set interaction values to children
        for (int i = 0; i < m_meshRenderers.Length; i++)
        {
            m_meshRenderers[i].sharedMaterial.SetColor("_ColorTint1", m_colorTint1Interact);
            m_meshRenderers[i].sharedMaterial.SetColor("_ColorTint2", m_colorTint2Interact);
            m_meshRenderers[i].sharedMaterial.SetColor("_EmissionColor", m_emissionColorInteract);
            m_meshRenderers[i].sharedMaterial.SetFloat("_EmissionPower", m_emissionPowerInteract);
            m_meshRenderers[i].sharedMaterial.SetInt("_Translucency", m_translucencyInteract);
        }
    }

    /*
    private void OnTriggerEnter(Collider other)
    {
        // if the music crystal is hit by the music rod and is available for interaction,
        // set the music crystal unvailable then play sound and change crystal translucency temporarily 
        if (other.gameObject.CompareTag("musicRod") && available)
        {
            startTime = Time.time;
            available = false;
            StartCoroutine(PlayMusicCrystal());
            StartCoroutine(Interact(m_audioSource.clip.length)); // use audio clip length in seconds as default time to update the crystal behaviour
        }
    }
    */

    public void Interact(bool correctHit)
    {
        startTime = Time.time;
        available = false;
        AudioClip clip;
        if (correctHit)
        {
            clip = m_goodSound;
        }
        else
        {
            clip = m_badSound;
        }
        StartCoroutine(PlayMusicCrystal(clip));
        StartCoroutine(Interact(clip.length)); // use audio clip length in seconds as default time to update the crystal behaviour
    }

    // play the sound of the audio source
    private IEnumerator PlayMusicCrystal(AudioClip clip)
    {
        m_audioSource.PlayOneShot(clip);
        yield return null;
    }

    // update the crystal behaviour with interaction values,
    // after some seconds passed in paramater,
    // update the crystal behaviour with default values
    private IEnumerator Interact(float flashDelay)
    {
        SetInteractionValues();
        yield return new WaitForSecondsRealtime(flashDelay); // wait seconds before update the crystal behaviour
        SetDefaultValues();
        yield return null;
    }
}
