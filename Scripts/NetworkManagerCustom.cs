﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.SceneManagement;

[System.Obsolete]
public class NetworkManagerCustom : NetworkManager
{
    [SerializeField] private int nbPlayers = 0;
    private int maxPlayers = 2;
    
    public override void OnServerConnect(NetworkConnection conn)
    {
        nbPlayers++;
        if (nbPlayers == maxPlayers)
        {
            Debug.Log("[NetworkManagerCustom:OnServerConnect:maxPlayers]");
            StartCoroutine(HideGame());
        }
        base.OnServerConnect(conn);
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
        nbPlayers = 0;
        Debug.Log("[NetworkManagerCustom:OnServerDisconnect]");
        if (NetworkServer.active)
        {
            Debug.Log("[NetworkManagerCustom:OnServerDisconnect:NetworkServer.active=true]");
            if (singleton.IsClientConnected())
            {
                Debug.Log("[NetworkManagerCustom:OnServerDisconnect:StopHost]");
                singleton.StopHost();
            }
            else
            {
                Debug.Log("[NetworkManagerCustom:OnServerDisconnect:StopServer]");
                singleton.StopServer();
            }
        }
        else
        {
            Debug.Log("[NetworkManagerCustom:OnServerDisconnect:NetworkServer.active=false]");
            if (singleton.IsClientConnected())
            {
                Debug.Log("[NetworkManagerCustom:OnServerDisconnect:StopClient]");
                singleton.StopClient();
            }
        }

        /*
        List<NetworkInstanceId> networkIds = new List<NetworkInstanceId>(conn.clientOwnedObjects);

        foreach (NetworkInstanceId netId in networkIds)
        {
            GameObject gameObject = NetworkServer.FindLocalObject(netId);
            NetworkIdentity netIdentity = gameObject.GetComponent<NetworkIdentity>();

            if (!netIdentity.GetComponent<NetworkPlayer>())
            {
                if (gameObject.GetComponentInParent<NetworkPlayer>())
                {
                    Debug.Log("[NetworkManagerCustom:OnServerDisconnect:gameObject.name=" + gameObject.name + " is child of a player and removed from player.name=" + gameObject.GetComponentInParent<NetworkPlayer>().name + "]");
                    gameObject.transform.SetParent(null);
                }
                else
                {
                    Debug.Log("[NetworkManagerCustom:OnServerDisconnect:gameObject.name=" + gameObject.name + "]");
                }
                netIdentity.RemoveClientAuthority(conn);
            }
        }
        */
        base.OnServerDisconnect(conn);
    }

    public override void OnClientDisconnect(NetworkConnection conn)
    {
        nbPlayers = 0;
        Debug.Log("[NetworkManagerCustom:OnClientDisconnect]");
        if (NetworkServer.active)
        {
            Debug.Log("[NetworkManagerCustom:OnClientDisconnect:NetworkServer.active=true]");
            if (singleton.IsClientConnected())
            {
                Debug.Log("[NetworkManagerCustom:OnClientDisconnect:StopHost]");
                singleton.StopHost();
            }
            else
            {
                Debug.Log("[NetworkManagerCustom:OnClientDisconnect:StopServer]");
                singleton.StopServer();
            }
        }
        else
        {
            Debug.Log("[NetworkManagerCustom:OnClientDisconnect:NetworkServer.active=false]");
            if (singleton.IsClientConnected())
            {
                Debug.Log("[NetworkManagerCustom:OnClientDisconnect:StopClient]");
                singleton.StopClient();
            }
        }
        base.OnClientDisconnect(conn);
    }

    private IEnumerator HideGame()
    {
        yield return new WaitForEndOfFrame();
        List<GameObject> dontDestroyOnLoadObjects = GetDontDestroyOnLoadObjects();
        foreach (GameObject go in dontDestroyOnLoadObjects)
        {
            if (go.TryGetComponent(out GameManager gm))
            {
                gm.HideVisibleGame();
            }
        }
    }

    public static List<GameObject> GetDontDestroyOnLoadObjects()
    {
        List<GameObject> result = new List<GameObject>();

        List<GameObject> rootGameObjectsExceptDontDestroyOnLoad = new List<GameObject>();
        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            rootGameObjectsExceptDontDestroyOnLoad.AddRange(SceneManager.GetSceneAt(i).GetRootGameObjects());
        }

        List<GameObject> rootGameObjects = new List<GameObject>();
        Transform[] allTransforms = Resources.FindObjectsOfTypeAll<Transform>();
        for (int i = 0; i < allTransforms.Length; i++)
        {
            Transform root = allTransforms[i].root;
            if (root.hideFlags == HideFlags.None && !rootGameObjects.Contains(root.gameObject))
            {
                rootGameObjects.Add(root.gameObject);
            }
        }

        for (int i = 0; i < rootGameObjects.Count; i++)
        {
            if (!rootGameObjectsExceptDontDestroyOnLoad.Contains(rootGameObjects[i]))
                result.Add(rootGameObjects[i]);
        }

        return result;
    }
}
