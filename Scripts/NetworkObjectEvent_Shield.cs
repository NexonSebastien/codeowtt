﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[System.Obsolete]
public class NetworkObjectEvent_Shield : NetworkObjectEvent
{
    GameObject m_Door;
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] public AudioClip a_door;

    override protected void Start()
    {
        base.Start();
        m_Door = transform.Find("DoorShield").gameObject;
        _audioSource = m_Door.GetComponents<AudioSource>()[2];
    }

    public override void EnigmaReceiveEvent(int eventCase, List<object> args)
    {
        Debug.Log("[NetworkObjectEvent_Shield:EnigmaReceiveEvent(eventCase=" + eventCase.ToString() + ")]");
        if (isServer || true)
        {
            Debug.Log("[NetworkObjectEvent_Shield(isSever):EnigmaReceiveEvent(eventCase="+eventCase.ToString()+")]");
            switch (eventCase)
            {
                case 1:
                    // toggle shield active


                    //RpcToggleShieldActive((bool)args[0]);
                    /*
                    bool active = (bool)args[0];
                    gameObject.SetActive(active);
                    if (active)
                    {
                        OnCloseDoor();
                    }
                    else
                    {
                        OnOpenDoor();
                    }
                    */
                    CmdToggleShieldActive((bool)args[0]);
                    break;
            }
        }
    }

    public override void EnigmaUpdateEvent()
    {
        throw new System.NotImplementedException();
    }

    [Command]
    public void CmdToggleShieldActive(bool active)
    {
        Debug.Log("[NetworkObjectEvent_Shield:CmdToggleShieldActive(active=" + active.ToString() + ")]");
        RpcToggleShieldActive(active);
    }

    [ClientRpc]
    public void RpcToggleShieldActive(bool active)
    {
        Debug.Log("[NetworkObjectEvent_Shield:RpcToggleShieldActive(active="+active.ToString()+")]");
        if(m_Door == null) m_Door = GameObject.FindWithTag("DoorApollon").gameObject;
        m_Door.SetActive(active);
        if (active)
        {
            OnCloseDoor();
        }
        else
        {
            OnOpenDoor();
        }
    }

    public void OnOpenDoor()
    {
        Debug.Log("[NetworkObjectEvent_Shield:OnOpenDoor]");
    }

    public void OnCloseDoor()
    {
        Debug.Log("[NetworkObjectEvent_Shield:OnCloseDoor]");
        _audioSource = m_Door.GetComponents<AudioSource>()[2];
        _audioSource.clip = a_door;
        _audioSource.Play();
    }
}
