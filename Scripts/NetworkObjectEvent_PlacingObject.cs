﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Valve.VR.InteractionSystem;

[System.Obsolete]
public class NetworkObjectEvent_PlacingObject : NetworkObjectEvent
{
    //[SerializeField] GameObject m_place;
    [SerializeField] private List<GameObject> m_places;
    [SyncVar] public int m_placesIndex;
    [SyncVar] public bool m_isWellPlaced;
    private bool m_checkTrigger;

    //Son
    private string EventValidation = "event:/Objets/Validation";

    override protected void Start()
    {
        base.Start();
        if (isServer)
        {
            m_isWellPlaced = false;
        }
        if (!m_isWellPlaced)
        {
            m_placesIndex = -1;
            m_checkTrigger = false;
        }
        else
        {
            m_checkTrigger = true;
            SetStates(m_placesIndex);
            DisableComponents(m_placesIndex);
        }
    }

    public override void EnigmaReceiveEvent(int eventCase, List<object> args)
    {
        throw new System.NotImplementedException();
    }

    public override void EnigmaUpdateEvent()
    {
        /*
        Debug.Log("[NetworkObjectEvent_PlacingObject:EnigmaUpdateEvent()]");
        RpcSetStates(); // set object transform
        RpcPlayValidationSound();
        RpcDisableComponents(); // remove components to prevent the player to be able to interact with the well placed object
        */
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        CheckTrigger(other);
    }

    protected virtual void OnTriggerStay(Collider other)
    {
        CheckTrigger(other);
    }

    protected void CheckTrigger(Collider other)
    {
        // only the player with authority on the object is able to check if the object is in his hand
        if (!m_isWellPlaced && hasAuthority && m_places.Contains(other.gameObject) && transform.root.tag != "Player" && !m_checkTrigger)
        {
            Debug.Log("[NetworkObjectEvent_PlacingObject:CheckTrigger(m_place)]");
            m_checkTrigger = true;
            if (isServer)
            {
                m_placesIndex = m_places.IndexOf(other.gameObject);
                ObjectPlaced(true, m_places.IndexOf(other.gameObject));
            }
            else
            {
                CmdObjectPlaced(true, m_places.IndexOf(other.gameObject));
            }
        }
    }

    [Command]
    private void CmdObjectPlaced(bool status, int placesIndex)
    {
        m_placesIndex = placesIndex;
        ObjectPlaced(status, placesIndex);
        RpcSetCheckTrigger(status);
    }

    virtual protected void ObjectPlaced(bool status, int placesIndex)
    {
        Debug.Log("[NetworkObjectEvent_PlacingObject:ObjectPlaced:status=" + status + "]");
        UpdateEnigma(m_NetId, status);
        if (status)
        {
            OnObjectWellPlaced(placesIndex);
        }
    }

    protected void OnObjectWellPlaced(int placesIndex)
    {
        Debug.Log("[NetworkObjectEvent_PlacingObject:OnObjectWellPlaced]");
        RpcSetStates(placesIndex); // set object transform
        RpcPlayValidationSound();
        RpcDisableComponents(placesIndex); // remove components to prevent the player to be able to interact with the well placed object
        StartCoroutine(SetObjectWellPlaced(true));
    }

    private IEnumerator SetObjectWellPlaced(bool status)
    {
        yield return new WaitForEndOfFrame();
        m_isWellPlaced = true;
    }

    [ClientRpc]
    public void RpcSetStates(int placesIndex)
    {
        SetStates(placesIndex);
    }

    private void SetStates(int placesIndex)
    {
        Debug.Log("[NetworkObjectEvent_PlacingObject:SetStates()]");
        transform.position = m_places[placesIndex].transform.position;
        transform.rotation = m_places[placesIndex].transform.rotation;
        transform.SetParent(m_places[placesIndex].transform);
    }

    [ClientRpc]
    public void RpcPlayValidationSound()
    {
        Debug.Log("[NetworkObjectEvent_PlacingObject:RpcPlayValidationSound()]");
        FMODUnity.RuntimeManager.PlayOneShot(EventValidation);
    }

    [ClientRpc]
    public void RpcDisableComponents(int placesIndex)
    {
        DisableComponents(placesIndex);
    }

    private void DisableComponents(int placesIndex)
    {
        Debug.Log("[NetworkObjectEvent_PlacingObject:DisableComponents()]");
        Destroy(GetComponent<Throwable>());
        Destroy(GetComponent<Interactable>());
        Destroy(GetComponent<Rigidbody>());
        Destroy(m_places[placesIndex].GetComponent<BoxCollider>());
    }

    [ClientRpc]
    public void RpcSetCheckTrigger(bool status)
    {
        m_checkTrigger = status;
    }
}
