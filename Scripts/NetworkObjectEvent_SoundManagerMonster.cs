﻿using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

[System.Obsolete]
public class NetworkObjectEvent_SoundManagerMonster : NetworkObjectEvent
{
    private FMOD.Studio.EventInstance m_instanceMonstre;
    private string EventMonstre = "event:/Monstre/Cri";
    public float time = 60;

    // Start is called before the first frame update
    override protected void Start()
    {
        base.Start();
        m_instanceMonstre = FMODUnity.RuntimeManager.CreateInstance(EventMonstre);
    }


    [ServerCallback]
    void Update()
    {
        //RpcPlayMonster();
    }

    private bool IsPlaying(FMOD.Studio.EventInstance instance)
    {
        FMOD.Studio.PLAYBACK_STATE state;
        instance.getPlaybackState(out state);
        return state != FMOD.Studio.PLAYBACK_STATE.STOPPED;
    }

    public void PlayMonster()
    {
        if (time >= 0)
        {
            time -= Time.deltaTime;
        }
        else
        {
            if (IsPlaying(m_instanceMonstre) == true)
            {
                m_instanceMonstre.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                m_instanceMonstre.release();
            }
            m_instanceMonstre.start();
            time = Random.Range(300, 600);
            Debug.Log("Son joué " + time);
        }
    }

    public override void EnigmaUpdateEvent()
    {
        throw new System.NotImplementedException();
    }

    public override void EnigmaReceiveEvent(int eventCase, List<object> args)
    {
        throw new System.NotImplementedException();
    }
}
