﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Utility;

public class MenuSky : MonoBehaviour
{
    public float m_rotateSpeed = 0.2f;
    // Update is called once per frame
    void Update()
    {
        RenderSettings.skybox.SetFloat("_Rotation", Time.time * m_rotateSpeed);
    }
}
