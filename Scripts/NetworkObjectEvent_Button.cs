﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Valve.VR.InteractionSystem;

[System.Obsolete]
public class NetworkObjectEvent_Button : NetworkObjectEvent
{
    [SerializeField] private GameObject m_door;
    private bool m_wasButtonPressed;
    private bool m_isDialoguePlay = false;
    [SerializeField] private Network_AudioController m_netAudioController;

    [SerializeField] private SoundShield ShieldOff1;
    [SerializeField] private EnigmaManager_SecretEnigma m_SecretEnigma;
    [SerializeField] private TeleportArea m_TeleportArea;
    [SerializeField] private NetworkObjectEvent_SecretEnigma m_secretPoseidon;
    [SerializeField] private NetworkObjectEvent_SecretEnigma m_secretApollon;
    [SerializeField] private NetworkObjectEvent_SecretEnigma m_secretCapsule;
    private FMOD.Studio.PLAYBACK_STATE m_stateDialogueButton;
    private FMOD.Studio.EventInstance m_instanceDialogueButton;
    private bool m_EndNotDefinitly;

    override protected void Start()
    {
        base.Start();
        if (isServer)
        {
            m_wasButtonPressed = false;
        }
    }

    [ServerCallback]
    private void Update()
    {
        if(m_netAudioController.GetIsFinishEnterCapsuleRoom() && m_TeleportArea.locked)
        {
            RpcActiveTpZone();
        }
    }

    [ClientRpc]
    private void RpcActiveTpZone()
    {
        m_TeleportArea.locked = false;
        m_TeleportArea.markerActive = true;
    }

    public void OnButtonReleased()
    {

        StartCoroutine(WaitOnButtonReleased());
    }

    public void OnButtonPressed()
    {
        StartCoroutine(WaitOnButtonPressed());
    }

    private IEnumerator WaitOnButtonPressed()
    {
        Debug.Log("[NetworkObjectEvent_Button:OnButtonPressed:!isServer=" + !isServer + "]");
        if (m_netAudioController.GetIsFinishEnterCapsuleRoom())
        {
            if (!hasAuthority)
            {
                NetworkPlayer player = GameObject.Find("VrGameObject").GetComponentInParent<NetworkPlayer>();
                Debug.Log("[NetworkObjectEvent_Button:Start:m_player.name=" + player.name + "]");
                player.CmdSetAuth(m_NetId, player.GetComponent<NetworkIdentity>());
                yield return new WaitUntil(() => hasAuthority);
            }
            if (!isServer)
            {
                CmdButtonPressed(true);
            }
            else
            {
                ButtonPressed(true);
            }
        }
        yield return null;
    }

    private IEnumerator WaitOnButtonReleased()
    {
        Debug.Log("[NetworkObjectEvent_Button:OnButtonPressed:!isServer=" + !isServer + "]");
        if (!hasAuthority)
        {
            NetworkPlayer player = GameObject.Find("VrGameObject").GetComponentInParent<NetworkPlayer>();
            player.CmdSetAuth(m_NetId, player.GetComponent<NetworkIdentity>());
            yield return new WaitUntil(() => hasAuthority);
        }
        if (!isServer)
        {

            CmdButtonPressed(false);
        }
        else
        {
            ButtonPressed(false);
        }
        yield return null;
    }

        [Command]
    private void CmdButtonPressed(bool isPressed)
    {
        Debug.Log("[NetworkObjectEvent_Button:CmdButtonPressed:isPressed=" + isPressed + "]");
        ButtonPressed(isPressed);
    }

    private void ButtonPressed(bool isPressed)
    {
        if (m_wasButtonPressed && !isPressed)
        {
            Debug.Log("[NetworkObjectEvent_Button:ButtonPressed:buttonReleased]");
            m_wasButtonPressed = false;
            ThrowEvent(m_NetId, 1, new List<object> { true });
            UpdateEnigma(m_NetId, false);
        }
        else if(!m_wasButtonPressed && isPressed)
        {
            Debug.Log("[NetworkObjectEvent_Button:ButtonPressed:buttonPressed]");
            m_wasButtonPressed = true;
            ThrowEvent(m_NetId, 1, new List<object> { false });
            UpdateEnigma(m_NetId, true);
            if (!m_isDialoguePlay)
            {
                Dialogue();
                m_isDialoguePlay = true;
            }
        }
    }

    private void Dialogue()
    {
        if (m_SecretEnigma.IsRunning())
        {
            if (!m_secretPoseidon.GetValidated() || !m_secretApollon.GetValidated())
            {
                Debug.Log("[NetworkObjectEvent_Buton | Dialogue] FinNegative définitive");
                if (isServer) { m_EndNotDefinitly = false; }

                RpcPlayDialogueInstance("event:/Dialogues/FinNegative1");
            }
            else
            {
                Debug.Log("[NetworkObjectEvent_Buton | Dialogue] FinNegative temporaire");
                if (!m_EndNotDefinitly)
                {
                    m_EndNotDefinitly = true;
                    RpcPlayDialogueInstance("event:/Dialogues/FinNegative1");
                }
                else
                {
                    m_isDialoguePlay = false;
                }
            }
        }
        else
        {
            Debug.Log("[NetworkObjectEvent_Buton | Dialogue] FinPositive");
            m_netAudioController.SetIsFinishButton(false);
            m_EndNotDefinitly = false;
            RpcPlayDialogueInstance("event:/Dialogues/FinPositive1");
        }
    }

    [ClientRpc]
    private void RpcPlayDialogueInstance(string myEvent)
    {
        m_instanceDialogueButton = FMODUnity.RuntimeManager.CreateInstance(myEvent);
        m_instanceDialogueButton.start();

        if (isServer)
        {
            StartCoroutine(WaitDialogueFinish());
        }
    }

    private IEnumerator WaitDialogueFinish()
    {
        while (m_stateDialogueButton != FMOD.Studio.PLAYBACK_STATE.STOPPED)
        {
            m_instanceDialogueButton.getPlaybackState(out m_stateDialogueButton);
            yield return new WaitForSeconds(1);
        }
        m_netAudioController.SetIsFinishButton(true);
        if(m_EndNotDefinitly)
        {
            m_isDialoguePlay = false;
        }
    }

    [ClientRpc]
    public void RpcToggleShieldActive(bool active)
    {
        Debug.Log("[NetworkObjectEvent_Button:RpcToggleShieldActive(active=" + active.ToString() + ")]");
        GameObject shield = m_door.transform.FindChild("DoorShield").gameObject;
        if (active)
        {
            shield.SetActive(active);
            OnCloseDoor(shield);
        }
        else
        {
            OnOpenDoor(shield);
            shield.SetActive(active);
        }
    }

    public void OnOpenDoor(GameObject shield)
    {
        Debug.Log("[NetworkObjectEvent_Button:OnOpenDoor]");
    }

    public void OnCloseDoor(GameObject shield)
    {
        Debug.Log("[NetworkObjectEvent_Button:OnCloseDoor]");
        ShieldOff1.StopSonShield();
    }

    public override void EnigmaReceiveEvent(int eventCase, List<object> args)
    {
        if (isServer)
        {
            switch (eventCase)
            {
                case 1:
                    RpcToggleShieldActive((bool)args[0]);
                    break;
            }
        }
    }

    public override void EnigmaUpdateEvent()
    {
        
    }
}
