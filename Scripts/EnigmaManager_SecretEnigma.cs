﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

[System.Obsolete]
public class EnigmaManager_SecretEnigma : EnigmaManager
{

    [SerializeField] private List<NetworkObjectEvent_Capsule> m_capsules;

    private bool SecretEnigmaComplete;

    protected override void OnCompleteEnigma()
    {
        foreach (NetworkIdentity ni in m_EnigmaObjectsNICompletion.Keys)
        {
            if (ni.TryGetComponent(out NetworkObjectEvent_SecretEnigma se))
            {
                se.EnigmaReceiveEvent(1, null);
            }
        }
        foreach (NetworkObjectEvent_Capsule capsule in m_capsules)
        {
            capsule.RpcOpenCapsule();
        }
        m_GameManager.OnCompleteEnigma(m_EnigmaId);
        SecretEnigmaComplete = true;
    }

    protected override void OnFailEnigma()
    {
        SecretEnigmaComplete = false;
        throw new System.NotImplementedException();
    }

    protected override void SendHelp()
    {
        throw new System.NotImplementedException();
    }

    public bool GetSecretEnigmaComplete()
    {
        return SecretEnigmaComplete;
    }
}
