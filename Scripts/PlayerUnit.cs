﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

// A PlayerUnit is a unit controlled by a player

public class PlayerUnit : NetworkBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    Vector3 m_velocity;

    // The position we think is most correct for this player.
    Vector3 bestGuessPosition;

    // This is a constantly updated value about our latency to the server
    // i.e. how many second it takes for us to receive a one-way message
    // TODO: This should probably be something we get from the PlayerConnectionObject
    float m_ourLatency;

    // This higher this value, the faster our local position will match the best guess position
    float m_latencySmoothingFactor = 10;

    // Update is called once per frame
    void Update()
    {
        // This function runs on ALL PlayerUnits
        
        if (hasAuthority == false)
        {
            // We aren't the authority for this object, but we still need to update
            // our local position for this object based on our best guess of where
            // it probably is on the owning player's screen.

            bestGuessPosition = bestGuessPosition + (m_velocity * Time.deltaTime);
            transform.position = Vector3.Lerp(transform.position, bestGuessPosition, Time.deltaTime * m_latencySmoothingFactor);

            return;
        }
        


        // If we get to here, we are the authoritative owner of this object
        //transform.Translate(velocity * Time.deltaTime);
        
        if ( /* some input */ true)
        {
            // The player is asking us to change our direction/speed (i.e. velocity)
            
            m_velocity = new Vector3(1, 0, 0);

            CmdUpdateVelocity(m_velocity, transform.position);
            
        }
        /*
        if (hasAuthority)
        {
            Debug.Log("hasAuthority");
            Vector3 newPosition = transform.position;
            if (Input.GetKey(KeyCode.Q))
            {
                newPosition.z -= 1;
            }
            if (Input.GetKey(KeyCode.D))
            {
                newPosition.z += 1;
            }
            if (Input.GetKey(KeyCode.Z))
            {
                newPosition.x += 1;
            }
            if (Input.GetKey(KeyCode.S))
            {
                newPosition.x -= 1;
            }
            CmdUpdatePlayerUnitPosition(newPosition);
        }
        */

    }

    [Command]
    void CmdUpdatePlayerUnitPosition(Vector3 newPosition)
    {
        transform.position = newPosition;
        RpcUpdatePlayerUnitPosition(transform.position);
    }

    [ClientRpc]
    private void RpcUpdatePlayerUnitPosition(Vector3 newPosition)
    {
        if(hasAuthority == false)
        {
            transform.position = newPosition;
        }
    }

    [Command]
    void CmdUpdateVelocity(Vector3 v, Vector3 p)
    {
        Debug.Log("CmdUpdateVelocity");
        // I am on a server
        transform.position = p;
        m_velocity = v;

        // If we know what our current latency is, we could do something like this:
        //  transform.position = p + (v * (thisPlayersLatencyToServer))

        // Now let the clients know the correct position of this object.
        RpcUpdateVelocity(m_velocity, transform.position);
    }

    [ClientRpc]
    void RpcUpdateVelocity(Vector3 v, Vector3 p)
    {
        Debug.Log("RpcUpdateVelocity");
        // I am on a client

        if (hasAuthority)
        {
            // Hey, this is my own object. I "should" already have the most accurate
            // position/velocity (possibly more "Accurate") than the server
            // Depending on the game, I MIGHT want to change to patch this info
            // from the server, even though that might look a little wonky to the user.

            // Let's assume for now that we're just going to ignore the message from the server.
            return;
        }

        // I am a non-authoratative client, so I definitely need to listen to the server.

        // If we know what our current latency is, we could do something like this:
        //  transform.position = p + (v * (ourLatency))

        //transform.position = p;

        m_velocity = v;
        bestGuessPosition = p + (m_velocity * (m_ourLatency));


        // Now position of player one is as close as possible on all player's screens

        // IN FACT, we don't want to directly update transform.position, because then 
        // players will keep teleporting/blinking as the updates come in. It looks dumb.


    }


}
