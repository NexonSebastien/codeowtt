﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class ValveTurn : MonoBehaviour
{
    private bool isTurn;
    // Start is called before the first frame update
    void Start()
    {
        isTurn = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(!isTurn)
        {
            StartCoroutine(TurnValve());
        }

    }

    IEnumerator TurnValve()
    {
        isTurn = true;
        if (GetComponent<CircularDrive>().outAngle > GetComponent<CircularDrive>().minAngle)
        {
            transform.Rotate(Vector3.up, -2);
            GetComponent<CircularDrive>().outAngle -= 2;
        }
        
        yield return new WaitForSeconds(0.1f);

        isTurn = false;
        yield return 0;
    }
}
