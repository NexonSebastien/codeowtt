﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[System.Obsolete]
public class EnigmaManager_1 : EnigmaManager
{
    private void Start()
    {
        Color c = Random.ColorHSV();
        c = Color.blue;
        /*
        foreach (KeyValuePair<NetworkIdentity, bool> o in m_EnigmaObjectsNICompletion)
        {
            o.Key.GetComponent<NetworkObjectEvent_1>().SetColor(new Vector3(c.r, c.g, c.b));
        }
        */
        for (int i = 0; i < m_EnigmaObjectsNI.Count; i++)
        {
            m_EnigmaObjectsNI[i].GetComponent<NetworkObjectEvent_1>().SetColor(new Vector3(c.r, c.g, c.b));
        }
    }

    protected override void OnCompleteEnigma()
    {
        Debug.Log("[EnigmaManager_1:OnCompleteEnigma]");
        UpdateObjectsColor(Color.green);
        m_GameManager.OnCompleteEnigma(m_EnigmaId);

        // desactive le shield
        // zone de tp
        // activer shield dans apollon room
        // start water managment
    }

    protected override void OnFailEnigma()
    {
        Debug.Log("[EnigmaManager_1:OnFailEnigma]");
        UpdateObjectsColor(Color.red);
        m_GameManager.OnFailEnigma(m_EnigmaId, null);
    }

    protected override void SendHelp()
    {
        Debug.Log("[EnigmaManager_1:SendHelp]");
    }

    private void UpdateObjectsColor(Color color)
    {
        List<object> args = new List<object> {color.r, color.g, color.b };
        foreach (KeyValuePair<NetworkIdentity, bool> o in m_EnigmaObjectsNICompletion)
        {
            OnThrowEvent(o.Key, 1, args);
        }
    }
}
