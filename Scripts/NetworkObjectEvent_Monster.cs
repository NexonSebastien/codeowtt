﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Obsolete]
public class NetworkObjectEvent_Monster : NetworkObjectEvent
{
    [SerializeField] private Transform m_positionA;
    [SerializeField] private Transform m_positionB;
    private bool m_isStartPositionA;
    private bool m_isRunning;
    private float m_lerpTime;
    private float m_lerpStartTime;
    [SerializeField] private float m_lerpTravelTime;
    private Vector3 m_positionSource;
    private Vector3 m_positionTarget;
    [SerializeField] private float m_timeBeforeStart;
    [SerializeField] private float m_minTimeBetweenTwoPassage;
    [SerializeField] private float m_maxTimeBetweenTwoPassage;
    [SerializeField] private bool m_canRun = true;
    private SoundMonster m_soundMonster;

    override protected void Start()
    {
        base.Start();
        transform.position = m_positionA.position;
        m_isStartPositionA = true;
        m_isRunning = false;
        m_lerpStartTime = Time.time;
        if (m_canRun)
        {
            StartCoroutine(StartMonster(false));
        }
        if(TryGetComponent(out SoundMonster sm))
        {
            m_soundMonster = sm;
        }
        else
        {
            m_soundMonster = null;
        }
    }

    private void Update()
    {
        if (m_isRunning)
        {
            transform.position = Lerp(m_positionSource, m_positionTarget, m_lerpStartTime, m_lerpTime);
            if (Mathf.Approximately(transform.position.x, m_positionTarget.x)
                && Mathf.Approximately(transform.position.y, m_positionTarget.y)
                && Mathf.Approximately(transform.position.z, m_positionTarget.z)) // if current position is the average target position
            {
                transform.position = m_positionTarget; // force position
                m_isRunning = false;
                m_isStartPositionA = !m_isStartPositionA;
                transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + 180.0f, transform.rotation.eulerAngles.z);
                Debug.Log("[NetworkObjectEvent_Monster:Update:position=" + transform.position.ToString() + "]");
            }
        }
    }

    private void MonsterPassage()
    {
        if (m_isRunning || !m_canRun) return;
        Debug.Log("[NetworkObjectEvent_Monster:MonsterPassage:travel=" + (m_isStartPositionA ? "A->B" : "B->A") + "]");
        if (!m_isStartPositionA)
        {
            m_positionSource = transform.position;
            m_positionTarget = m_positionA.position;
            m_isStartPositionA = false;
        }
        else
        {
            m_positionSource = transform.position;
            m_positionTarget = m_positionB.position;
            m_isStartPositionA = true;
        }
        m_lerpTime = m_lerpTravelTime;
        m_lerpStartTime = Time.time;
        m_isRunning = true;
    }

    private Vector3 Lerp(Vector3 start, Vector3 end, float lerpStartTime, float lerpTime)
    {
        float timeSincedStarted = Time.time - lerpStartTime;
        float percentage = timeSincedStarted / lerpTime;
        return Vector3.Lerp(start, end, percentage);
    }

    private IEnumerator MonsterPassageRoutine()
    {
        Debug.Log("[NetworkObjectEvent_Monster:MonsterPassageRoutine:MonsterPassage]");
        MonsterPassage();
        Debug.Log("[NetworkObjectEvent_Monster:MonsterPassageRoutine:WaitWhile(() => m_isRunning)]");
        yield return new WaitWhile(() => m_isRunning);
        float timeStart = Time.time;
        float timeBetweenPassage = Random.Range(m_minTimeBetweenTwoPassage, m_maxTimeBetweenTwoPassage);
        Debug.Log("[NetworkObjectEvent_Monster:MonsterPassageRoutine:WaitForSecondsRealtime(" + timeBetweenPassage + "))]");
        //yield return new WaitUntil(() => Time.time >= timeStart + timeBetweenPassage);
        yield return new WaitForSecondsRealtime(timeBetweenPassage);
        if (m_canRun)
        {
            StartCoroutine(MonsterPassageRoutine());
        }
    }

    private IEnumerator StartMonster(bool forceRunning)
    {
        Debug.Log("[NetworkObjectEvent_Monster:StartMonster:WaitUntil(m_EnigmaManager.IsRunning)]");
        yield return new WaitForEndOfFrame();
        yield return new WaitUntil(() => m_EnigmaManager.IsRunning() || forceRunning);
        if (m_soundMonster != null)
        {
            m_soundMonster.m_canPlaySound = true;
        }
        float timeStart = Time.time;
        Debug.Log("[NetworkObjectEvent_Monster:StartMonster:WaitForSecondsRealtime(" + m_timeBeforeStart + "))]");
        //yield return new WaitUntil(() => Time.time >= timeStart + m_timeBeforeStart);
        yield return new WaitForSecondsRealtime(m_timeBeforeStart);
        StartCoroutine(EndMonster());
        StartCoroutine(MonsterPassageRoutine());
        yield return null;
    }

    private IEnumerator EndMonster()
    {
        Debug.Log("[NetworkObjectEvent_Monster:EndMonster:WaitWhile(m_EnigmaManager.IsRunning)]");
        yield return new WaitForEndOfFrame();
        yield return new WaitWhile(m_EnigmaManager.IsRunning);
        if(m_soundMonster != null)
        {
            m_soundMonster.m_canPlaySound = false;
        }
        Debug.Log("[NetworkObjectEvent_Monster:EndMonster:StopAllCoroutines()]");
        StopAllCoroutines();
        yield return null;
    }

    public void StartRunning(bool forceRunning)
    {
        m_canRun = true;
        if (!m_isRunning)
        {
            StartCoroutine(StartMonster(forceRunning));
        }
    }

    public void StopRunning()
    {
        m_canRun = false;
    }

    public override void EnigmaUpdateEvent()
    {
        throw new System.NotImplementedException();
    }

    public override void EnigmaReceiveEvent(int eventCase, List<object> args)
    {
        throw new System.NotImplementedException();
    }
}
