﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatMoving : MonoBehaviour
{
    public float m_speed = 5;
    public Vector2 m_offset = new Vector2(700, -730);
    private int m_direction = 1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x > m_offset.x)
        {
            m_direction = -1;
            var rotationVector = transform.rotation.eulerAngles;
            rotationVector.y = 270;
            transform.rotation = Quaternion.Euler(rotationVector);
            transform.position = transform.position + new Vector3(0, 0, 150);  // Always multiplicate by the time
        }
        else if (transform.position.x < m_offset.y)
        {
            m_direction = 1;
            var rotationVector = transform.rotation.eulerAngles;
            rotationVector.y = 90;
            transform.rotation = Quaternion.Euler(rotationVector);
            transform.position = transform.position + new Vector3(0, 0, -150);  // Always multiplicate by the time
        }

        transform.position = transform.position + new Vector3(m_speed * m_direction * Time.deltaTime,0 , 0);  // Always multiplicate by the time
    }
}
