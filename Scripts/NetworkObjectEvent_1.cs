﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[System.Obsolete]
public class NetworkObjectEvent_1 : NetworkObjectEvent
{
    [SyncVar] [SerializeField] public Vector3 m_Color;
    private Material m_Material;
    override protected void Start()
    {
        base.Start();
        m_Material = GetComponent<Renderer>().material;
    }

    private void Update()
    {
        m_Material.color = new Color(m_Color.x, m_Color.y, m_Color.z);
    }

    public override void EnigmaReceiveEvent(int eventCase, List<object> args)
    {
        if (isServer)
        {
            switch (eventCase)
            {
                case 1:
                    SetColor(new Vector3((float)args[0], (float)args[1], (float)args[2]));
                    break;
            }
        }
    }

    public override void EnigmaUpdateEvent()
    {
        //throw new System.NotImplementedException();
    }

    public void SetColor(Vector3 color)
    {
        //Debug.Log("[NetworkObjectEvent_1:SetColor]");
        m_Color = color;
        RpcSetColor(m_Color);
    }

    [ClientRpc]
    public void RpcSetColor(Vector3 color)
    {
        //Debug.Log("[NetworkObjectEvent_1:RpcSetColor]");
        m_Material.color = new Color(color.x, color.y, color.z);
    }

    [ServerCallback]
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            //Debug.Log("trigger enter with player");
            //myColorVec3 = new Vector3(Random.ColorHSV().r, Random.ColorHSV().g, Random.ColorHSV().b);
            Color color = Random.ColorHSV();
            List<object> args = new List<object> {color.r, color.g, color.b };
            //CmdThrowEvent(m_NetId, 1, args);
            //CmdUpdateEnigma(m_NetId, true);
            
            //m_EnigmaManager.OnThrowEvent(m_NetId, 1, args);
            //m_EnigmaManager.OnUpdateEnigma(m_NetId, true);

            ThrowEvent(m_NetId, 1, args);
            UpdateEnigma(m_NetId, true);
        }
    }

    [ServerCallback]
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            //Debug.Log("trigger exit with player");
            //myColorVec3 = new Vector3(Random.ColorHSV().r, Random.ColorHSV().g, Random.ColorHSV().b);
        }
    }
}
