﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Credit : MonoBehaviour
{
    [SerializeField] float m_speed = 50;
    [SerializeField] float m_lengh = 2350f;
    // Start is called before the first frame update
    [SerializeField] private bool isPlaying = false;
    private Vector3 lastPosition;
    void Start()
    {
        lastPosition = new Vector3(this.transform.position.x, m_lengh, this.transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        if(isPlaying)
        {
            if (this.transform.position.y >= lastPosition.y)
            {
                isPlaying = false;
                this.transform.GetChild(0).gameObject.SetActive(false);
            }
            else
            {
                this.transform.position = Vector3.MoveTowards(this.transform.position, lastPosition, Time.deltaTime * m_speed);
            }   
        }
    }

    public void Play()
    {
        this.transform.GetChild(0).gameObject.SetActive(true);
        isPlaying = true;
    }

    public bool GetIsPlaying()
    {
        return isPlaying;
    }

}
