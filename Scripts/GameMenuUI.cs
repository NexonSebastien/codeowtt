﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

[System.Obsolete]
public class GameMenuUI : NetworkBehaviour
{
    private NetworkManager m_networkManager;
    private GameObject m_menuUI;
    private GameObject m_gameUI;
    private string m_defaultIpAddress;
    private string m_ipAddress;

    private void Awake()
    {
        m_networkManager = transform.parent.GetComponent<NetworkManager>();
        m_menuUI = transform.FindChild("MenuUI").gameObject;
        m_gameUI = transform.FindChild("GameUI").gameObject;
        m_defaultIpAddress = "127.0.0.1";
        m_ipAddress = m_defaultIpAddress;
    }

    private void Start()
    {
        ShowMenuUI();
        HideGameUI();
    }

    public void ShowMenuUI()
    {
        Debug.Log("[GameMenuUI:ShowMenuUI]");
        m_menuUI.SetActive(true);
    }

    public void HideMenuUI()
    {
        Debug.Log("[GameMenuUI:HideMenuUI]");
        m_menuUI.SetActive(false);
    }

    public void ShowGameUI()
    {
        Debug.Log("[GameMenuUI:ShowGameUI]");
        m_gameUI.SetActive(true);
    }

    public void HideGameUI()
    {
        Debug.Log("[GameMenuUI:HideGameUI]");
        m_gameUI.SetActive(false);
    }

    public bool IsMenuUIActive()
    {
        return m_menuUI.activeSelf;
    }

    public bool IsGameUIActive()
    {
        return m_gameUI.activeSelf;
    }

    public void SetIpAddress(string ipAddress)
    {
        m_ipAddress = ipAddress;
    }

    public void HostGame()
    {
        if (UnityEngine.Application.platform != RuntimePlatform.WebGLPlayer)
        {
            Debug.Log("[GameMenuUI:HostGame]");
            m_networkManager.StartHost();
            HideMenuUI();
        }
        else
        {
            Debug.LogError("[GameMenuUI:HostGame] Can't start host game !");
        }
    }

    public void JoinGame()
    {
        string ipAddress = m_ipAddress;
        if (string.IsNullOrEmpty(m_ipAddress)) ipAddress = m_defaultIpAddress; // set to local ip address if ip address text is empty
        Debug.Log("[GameMenuUI:JoinGame:ipAddress=" + ipAddress + "]");
        m_networkManager.networkAddress = ipAddress;
        m_networkManager.StartClient();
    }

    public void BackToMenu()
    {
        Debug.Log("[GameMenuUI:BackToMenu]");
        if (NetworkServer.active)
        {
            Debug.Log("[GameMenuUI:BackToMenu:NetworkServer.active=true]");
            if (m_networkManager.IsClientConnected())
            {
                Debug.Log("[GameMenuUI:BackToMenu:StopHost]");
                m_networkManager.StopHost();
            }
            else
            {
                Debug.Log("[GameMenuUI:BackToMenu:StopServer]");
                m_networkManager.StopServer();
            }
        }
        else
        {
            Debug.Log("[GameMenuUI:BackToMenu:NetworkServer.active=false]");
            if (m_networkManager.IsClientConnected())
            {
                Debug.Log("[GameMenuUI:BackToMenu:StopClient]");
                m_networkManager.StopClient();
            }
        }
        HideGameUI();
        ShowMenuUI();
    }

    public void ExitGame()
    {
        Debug.Log("[GameMenuUI:ExitGame]");
        Application.Quit(0);
    }
}
