﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SpawnPlayer : MonoBehaviour
{
    [SerializeField]
    private NetworkManager networkManager;
    [SerializeField]
    private GameObject m_teleportPrefab;
    
    private void Awake()
    {
        
    }

    // Start is called before the first frame update
    void Start()
    {
        Instantiate(networkManager.playerPrefab, transform.position, transform.rotation);
        if (UnityEngine.XR.XRSettings.enabled == true)
        {
            Instantiate(m_teleportPrefab, transform.position, transform.rotation);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
