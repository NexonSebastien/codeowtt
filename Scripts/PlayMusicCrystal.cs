﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Obsolete]
public class PlayMusicCrystal : MonoBehaviour
{
    [SerializeField] private NetworkObjectEvent_MusicCrystal m_musicCrystal;
    private List<MusicCrystal> m_musicCrystals;

    private float m_delay;
    private bool m_isPlaying;

    private List<AudioClip> a_audioclip;
    private AudioSource _audioSourceAmbiance;


    // Start is called before the first frame update
    void Start()
    {
        m_delay = 2;
        m_isPlaying = false;
        _audioSourceAmbiance = this.GetComponent<AudioSource>();
        a_audioclip = new List<AudioClip>();
        m_musicCrystals = m_musicCrystal.GetMusicCrystals();
        for (int i = 0; i < m_musicCrystals.Count; i++)
        {
            a_audioclip.Add(m_musicCrystals[i].GetAudioClip());
        }
    }

    public void PlayCrystalMusic()
    {
        if (m_isPlaying == false)
        {
            Debug.Log("[PlayMusicCrystal:PlayCrystalMusic]");
            m_isPlaying = true;
            StartCoroutine(OnPlayMusicCrystal());
            NetworkObjectEvent_MusicCrystal PlayCristal = (NetworkObjectEvent_MusicCrystal)m_musicCrystal;
            PlayCristal.NbrPlayCristal();
        }
    }

    private IEnumerator OnPlayMusicCrystal()
    {
        Debug.Log("[OnPlayMusicCrystal:PlayCrystalMusic]");
        List<int> m_listCrystals = m_musicCrystal.GetCurrentListSoluce();
        for (int i = 0; i < m_listCrystals.Count; i++)
        {
            PlayAudioClip(a_audioclip[m_listCrystals[i] - 1]);
            yield return new WaitForSecondsRealtime(m_delay);
        }
        m_isPlaying = false;
    }

    private void PlayAudioClip(AudioClip audioClip)
    {
        Debug.Log("[PlayMusicCrystal:PlayAudioClip:audioclip.name = " + audioClip.name + "]");
        _audioSourceAmbiance.clip = audioClip;
        _audioSourceAmbiance.PlayOneShot(audioClip);

    }
}
