﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.Networking;
using Random = UnityEngine.Random;

[System.Obsolete]
public class EnigmaManager_Brasero : EnigmaManager
{
    private class Vector2Brasero
    {
        public NetworkObjectEvent_Brasero _brasero1;
        public NetworkObjectEvent_Brasero _brasero2;

        public Vector2Brasero(NetworkObjectEvent_Brasero brasero1, NetworkObjectEvent_Brasero brasero2)
        {
            _brasero1 = brasero1;
            _brasero2 = brasero2;
        }
    }

    [SerializeField] private EnigmaManager m_nextEnigmaPart;
    [SerializeField] private GameObject m_roomLightsRoot;
    [SerializeField] private Material m_roomMaterialNeons;
    [SerializeField] private List<NetworkObjectEvent_Symbol> m_symbols;
    private int m_indexSymbols;
    private List<int> m_shuffleIndex;
    private List<List<NetworkObjectEvent_Brasero>> m_braserosPools;
    [SerializeField] private List<int> m_nbBraserosPerPool;
    [SerializeField] private List<NetworkObjectEvent_Brasero> m_braserosPoolOne;
    [SerializeField] private List<NetworkObjectEvent_Brasero> m_braserosPoolTwo;
    [SerializeField] private List<NetworkObjectEvent_Brasero> m_braserosPoolThree;
    [SerializeField] private CloseDoorWhenEnter  m_closeDoorWhenEnter;

    //Son
    private string EventValidation = "event:/Objets/Validation";

    private void Start()
    {
        m_indexSymbols = 0;
        if (isServer && m_roomLightsRoot && !m_roomLightsRoot.active && m_roomMaterialNeons)
        {
            RpcActiveRoomLights(false);
            RpcToggleRoomMaterialNeonsEmission(false);
        }
    }

    override public void StartEnigmaManager(int enigmaId)
    {
        Debug.Log("[EnigmaManager_Brasero:StartEnigmaManager]");
        StartCoroutine(SelectBraserosFromPools(enigmaId));
    }

    public int GetCurrentSymbolId()
    {
        Debug.Log("[EnigmaManager_Brasero:GetCurrentSymbolId]");
        return m_symbols[m_indexSymbols].GetId();
    }

    protected override void OnCompleteEnigma()
    {
        if (m_nextEnigmaPart != null)
        {
            if(isServer && m_roomLightsRoot && !m_roomLightsRoot.active && m_roomMaterialNeons)
            {
                //m_closeDoorWhenEnter.SetWantToStop(true);
                RpcActiveRoomLights(true);
                RpcToggleRoomMaterialNeonsEmission(true);
            }
            m_nextEnigmaPart.StartEnigmaManager(m_EnigmaId);
        }
        else
        {
            m_GameManager.OnCompleteEnigma(m_EnigmaId);
        }
    }

    protected override void OnFailEnigma()
    {
        throw new System.NotImplementedException();
    }

    protected override void SendHelp()
    {
        throw new System.NotImplementedException();
    }

    public void ConfirmEmbrasement()
    {
        Debug.Log("[EnigmaManager_Brasero:ConfirmEmbrasement]");
        RpcPlaySoundValidation();
        NextSymbol();
    }

    [ClientRpc]
    public void RpcPlaySoundValidation()
    {
        FMODUnity.RuntimeManager.PlayOneShot(EventValidation);
    }

    private void NextSymbol()
    {
        Debug.Log("[EnigmaManager_Brasero:NextSymbol]");
        if (m_indexSymbols < m_symbols.Count-1)
        {
            m_indexSymbols++;
            m_symbols[m_indexSymbols].EnigmaReceiveEvent(3, new List<object>() { m_shuffleIndex[m_indexSymbols] });
        }
    }

    private IEnumerator SelectBraserosFromPools(int enigmaId)
    {
        yield return new WaitForEndOfFrame();

        m_braserosPools = new List<List<NetworkObjectEvent_Brasero>>() { m_braserosPoolOne, m_braserosPoolTwo, m_braserosPoolThree };
        Dictionary<int, Vector2Brasero> braserosDict = new Dictionary<int, Vector2Brasero>();

        // assoc braseros by id
        m_braserosPools.ForEach(delegate (List<NetworkObjectEvent_Brasero> lb)
        {
            lb.ForEach(delegate (NetworkObjectEvent_Brasero b) {
                b.SetEnigmaManager(this);
                int id = b.GetId();
                if (braserosDict.ContainsKey(id))
                {
                    braserosDict[id]._brasero2 = b; // add second brasero
                }
                else
                {
                    braserosDict.Add(id, new Vector2Brasero(b, null)); // add first brasero
                }
            });
        });

        List<List<int>> braserosIdShuffle = new List<List<int>>();
        // select nb braseros per pool
        for (int i=0; i<m_braserosPools.Count; i++)
        {
            // prevent overflow
            if(i >= m_nbBraserosPerPool.Count)
            {
                m_nbBraserosPerPool.Add(0);
            }

            List<int> braserosId = new List<int>();
            for (int j=0; j < m_braserosPools[i].Count; j++)
            {
                int id = m_braserosPools[i][j].GetId();
                if (!braserosId.Contains(id))
                {
                    braserosId.Add(id);
                    //Debug.Log("[EnigmaManager_Brasero:SelectBraserosFromPools:brasero.name = " + m_braserosPools[i][j].transform.parent.parent.parent.parent.parent.parent.name + ":id="+id+"]");
                }
            }
            braserosId = braserosId.OrderBy(item => Guid.NewGuid()).ToList(); // shuffle the list
            braserosIdShuffle.Add(braserosId);
        }

        int addElements = 0;
        // add braseros to enigma elements
        for (int i=0; i< m_nbBraserosPerPool.Count; i++)
        {
            for(int j=0; j<m_nbBraserosPerPool[i]; j++)
            {
                if(addElements < m_symbols.Count) // prevent adding to much elements
                {
                    addElements++;
                    m_EnigmaObjectsNI.Add(braserosDict[braserosIdShuffle[i][j]]._brasero1.GetComponent<NetworkIdentity>());
                    m_EnigmaObjectsNI.Add(braserosDict[braserosIdShuffle[i][j]]._brasero2.GetComponent<NetworkIdentity>());
                }
            }
        }

        braserosDict.Clear();
        braserosIdShuffle.Clear();
        base.StartEnigmaManager(enigmaId);
        StartCoroutine(InitSymbols());
        StartCoroutine(ExtinguishBraseros());
        
        yield return null;
    }

    IEnumerator InitSymbols()
    {
        yield return new WaitForEndOfFrame();
        Debug.Log("[EnigmaManager_Brasero:InitSymbols]");
        int texturesCount = m_symbols[0].GetTexturesCount();
        List<int> symbolsId = new List<int>();
        for(int i=0; i<m_EnigmaObjectsNI.Count; i++)
        {
            int id = m_EnigmaObjectsNI[i].GetComponent<NetworkObjectEvent_Brasero>().GetId();
            if (!symbolsId.Contains(id))
            {
                symbolsId.Add(id);
                Debug.Log("[EnigmaManager_Brasero:InitSymbols:id = " + id + "]");
            }
        }
        m_shuffleIndex = symbolsId.OrderBy(item => Guid.NewGuid()).ToList();
        for (int i = 0; i < m_symbols.Count; i++)
        {
            m_symbols[i].SetEnigmaManager(this); // set manager
            m_symbols[i].SetId(m_shuffleIndex[i]); // set the symbol id
            if (i == 0)
            {
                m_symbols[i].EnigmaReceiveEvent(3, new List<object>() { m_shuffleIndex[0] });
            }
            else
            {
                m_symbols[i].EnigmaReceiveEvent(3, new List<object>() { -1 });
            }

            //Debug.Log("[EnigmaManager_Brasero:InitSymbols:_EmissionMap=" + m_symbols[i].GetComponent<Renderer>().sharedMaterial.GetTexture("_EmissionMap").name + "]");
        }
    }

    private IEnumerator ExtinguishBraseros()
    {
        // extinguish all braseros
        for(int i=0; i<m_braserosPools.Count; i++)
        {
            for(int j=0; j<m_braserosPools[i].Count; j++)
            {
                OnThrowEvent(m_braserosPools[i][j].GetComponent<NetworkIdentity>(), 2, null); // ask each brasero to extinguish itself
            }
        }
        yield return null;
    }

    [ClientRpc]
    public void RpcActiveRoomLights(bool active)
    {
        m_roomLightsRoot.SetActive(active);
    }

    [ClientRpc]
    public void RpcToggleRoomMaterialNeonsEmission(bool emission)
    {
        if (emission)
        {
            m_roomMaterialNeons.EnableKeyword("_EMISSION");
        }
        else
        {
            m_roomMaterialNeons.DisableKeyword("_EMISSION");
        }
    }
}
